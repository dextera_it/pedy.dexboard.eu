-- MySQL dump 10.16  Distrib 10.1.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pedy
-- ------------------------------------------------------
-- Server version	10.1.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AuditLog`
--

DROP TABLE IF EXISTS `AuditLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuditLog` (
  `AuditLogId` int(11) NOT NULL,
  PRIMARY KEY (`AuditLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Clinic`
--

DROP TABLE IF EXISTS `Clinic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Clinic` (
  `ClinicId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL DEFAULT '0',
  `DescEL` varchar(100) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ClinicId`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClinicDepartment`
--

DROP TABLE IF EXISTS `ClinicDepartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicDepartment` (
  `ClinicDepartmentId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ClinicDepartmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClinicIncident`
--

DROP TABLE IF EXISTS `ClinicIncident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicIncident` (
  `ClinicIncidentId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(100) NOT NULL,
  `DescShortEL` varchar(100) DEFAULT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `tooltip` mediumtext,
  `form_index` tinyint(4) DEFAULT NULL,
  `locale` char(3) DEFAULT NULL,
  PRIMARY KEY (`ClinicIncidentId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClinicSpecialityRel`
--

DROP TABLE IF EXISTS `ClinicSpecialityRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicSpecialityRel` (
  `ClinicTypeId` int(11) NOT NULL,
  `PersonelSpecialityId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClinicTransaction`
--

DROP TABLE IF EXISTS `ClinicTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicTransaction` (
  `ClinicTransactionId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `HealthUnitId` int(11) NOT NULL,
  `ClinicDepartmentId` int(11) NOT NULL DEFAULT '1',
  `ClinicTypeId` int(11) NOT NULL COMMENT '						',
  `ClinicIncidentId` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `RefYear` int(11) NOT NULL,
  `RefMonth` int(11) NOT NULL,
  `RefDate` date DEFAULT NULL,
  `PersonelId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ClinicTransactionId`),
  KEY `fk_ClinicTransaction_HealthUnitId` (`HealthUnitId`),
  CONSTRAINT `fk_ClinicTransaction_HealthUnitId` FOREIGN KEY (`HealthUnitId`) REFERENCES `HealthUnit` (`HealthUnitId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1117680 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClinicType`
--

DROP TABLE IF EXISTS `ClinicType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClinicType` (
  `ClinicTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ClinicTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dim_Hospital`
--

DROP TABLE IF EXISTS `Dim_Hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dim_Hospital` (
  `PK_Hospital` int(11) NOT NULL,
  `Hospital` varchar(45) DEFAULT NULL,
  `PK_HospitalSizeCat` int(11) NOT NULL,
  `Ype_id` int(11) NOT NULL,
  PRIMARY KEY (`PK_Hospital`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dim_Klinikes`
--

DROP TABLE IF EXISTS `Dim_Klinikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dim_Klinikes` (
  `PK_Kliniki` int(11) NOT NULL,
  `Kliniki` varchar(254) DEFAULT NULL,
  `PotitionTypeName` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`PK_Kliniki`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dim_Tomeis`
--

DROP TABLE IF EXISTS `Dim_Tomeis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dim_Tomeis` (
  `PK_Tomeas` int(11) NOT NULL,
  `Tomeas` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PK_Tomeas`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dim_YPE`
--

DROP TABLE IF EXISTS `Dim_YPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dim_YPE` (
  `Ype_id` int(11) NOT NULL,
  `Ype_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Ype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HUIncidentRel`
--

DROP TABLE IF EXISTS `HUIncidentRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HUIncidentRel` (
  `HealthUnitTypeId` int(11) NOT NULL,
  `ClinicIncidentId` int(11) NOT NULL,
  `ClinicDepartmentId` int(11) NOT NULL DEFAULT '1',
  UNIQUE KEY `unqRec` (`HealthUnitTypeId`,`ClinicIncidentId`,`ClinicDepartmentId`) USING BTREE,
  KEY `fk_HUIncidentRel_ClinicIncidentId` (`ClinicIncidentId`),
  KEY `fk_HUIncidentRel_ClinicDepartmentId` (`ClinicDepartmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Health Unit and report Incident Relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthCareLevel`
--

DROP TABLE IF EXISTS `HealthCareLevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthCareLevel` (
  `HealthCareLevelId` int(11) NOT NULL,
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`HealthCareLevelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthCommittee`
--

DROP TABLE IF EXISTS `HealthCommittee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthCommittee` (
  `HealthCommitteeId` int(11) NOT NULL AUTO_INCREMENT,
  `DescEL` varchar(254) DEFAULT NULL,
  `MemoColor` char(16) DEFAULT NULL,
  PRIMARY KEY (`HealthCommitteeId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Health Committees';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthDistrict`
--

DROP TABLE IF EXISTS `HealthDistrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthDistrict` (
  `HealthDistrictId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `DescEL` varchar(254) NOT NULL,
  `DescShortEL` varchar(45) NOT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`HealthDistrictId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Hellenic Health Districts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthOrganization`
--

DROP TABLE IF EXISTS `HealthOrganization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthOrganization` (
  `HealthOrganizationId` int(11) NOT NULL AUTO_INCREMENT,
  `DescEL` varchar(45) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`HealthOrganizationId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnit`
--

DROP TABLE IF EXISTS `HealthUnit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnit` (
  `HealthUnitId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Record Status',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL DEFAULT '0',
  `HealthUnitTypeId` int(11) NOT NULL,
  `DescEL` varchar(254) NOT NULL,
  `DescShortEL` varchar(100) DEFAULT NULL,
  `HealthDistrictId` int(11) DEFAULT NULL,
  PRIMARY KEY (`HealthUnitId`),
  KEY `fk_HealthUnit_HealthUnitTypeId_idx` (`HealthUnitTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=10008 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitClinicRel`
--

DROP TABLE IF EXISTS `HealthUnitClinicRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitClinicRel` (
  `HealthUnitId` int(11) NOT NULL,
  `HealthUnitTypeId` int(11) NOT NULL,
  `ClinicTypeId` int(11) NOT NULL,
  `ClinicDepartmentId` int(11) NOT NULL DEFAULT '1',
  UNIQUE KEY `unqRec` (`HealthUnitId`,`HealthUnitTypeId`,`ClinicTypeId`,`ClinicDepartmentId`) USING BTREE,
  KEY `fk_HealthUnitClinicRel_HealthUnitTypeId_idx` (`HealthUnitTypeId`),
  KEY `fk_HealthUnitClinicRel_ClinicTypeId_idx` (`ClinicTypeId`),
  KEY `fk_HealthUnitClinicRel_ClinicDepartmentId_idx` (`ClinicDepartmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitDetail`
--

DROP TABLE IF EXISTS `HealthUnitDetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitDetail` (
  `HealthUnitId` int(11) NOT NULL,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `Address` varchar(45) DEFAULT NULL,
  `PostalCode` int(11) DEFAULT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Personel` int(11) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  KEY `fk_HealthUnitDetail_1_idx` (`HealthUnitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitGIS`
--

DROP TABLE IF EXISTS `HealthUnitGIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitGIS` (
  `HealthUnitId` int(11) NOT NULL,
  KEY `fk_HealthUnitGIS_1_idx` (`HealthUnitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitMunicipalityRel`
--

DROP TABLE IF EXISTS `HealthUnitMunicipalityRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitMunicipalityRel` (
  `MunicipalityId` int(11) DEFAULT NULL,
  `HealthUnitId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitRel`
--

DROP TABLE IF EXISTS `HealthUnitRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitRel` (
  `HealthDistrictID` int(11) NOT NULL DEFAULT '1',
  `HealthUnitId` int(11) DEFAULT NULL,
  `UnitL2` int(11) DEFAULT NULL,
  `UnitL3` int(11) DEFAULT NULL,
  UNIQUE KEY `uq_healthunitrel` (`HealthUnitId`,`UnitL2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Health Unit Relation in district and other unit.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitSize`
--

DROP TABLE IF EXISTS `HealthUnitSize`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitSize` (
  `HealthUnitSizeId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`HealthUnitSizeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HealthUnitType`
--

DROP TABLE IF EXISTS `HealthUnitType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HealthUnitType` (
  `HealthUnitTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`HealthUnitTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Infirmary`
--

DROP TABLE IF EXISTS `Infirmary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Infirmary` (
  `InfirmaryId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL DEFAULT '0',
  `DescEL` varchar(100) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`InfirmaryId`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='Health Unit Infirmaries';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Info_level`
--

DROP TABLE IF EXISTS `Info_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Info_level` (
  `InfolevelId` int(10) unsigned NOT NULL,
  `Info_level` varchar(55) NOT NULL,
  PRIMARY KEY (`InfolevelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalAct`
--

DROP TABLE IF EXISTS `MedicalAct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalAct` (
  `MedicalActId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '100',
  `DescEL` varchar(45) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MedicalActId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalActTransaction`
--

DROP TABLE IF EXISTS `MedicalActTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalActTransaction` (
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_id` smallint(6) DEFAULT NULL,
  `locale` varchar(2) DEFAULT NULL,
  `MedicalActTransactionId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`MedicalActTransactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalActTypeRel`
--

DROP TABLE IF EXISTS `MedicalActTypeRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalActTypeRel` (
  `HealthUnitId` int(11) NOT NULL,
  `MedicalActId` int(11) NOT NULL,
  `MedicalTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Medical Act & Type Relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalCategory`
--

DROP TABLE IF EXISTS `MedicalCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalCategory` (
  `MedicalCategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`MedicalCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalCategoryActRel`
--

DROP TABLE IF EXISTS `MedicalCategoryActRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalCategoryActRel` (
  `MedicalCategoryId` int(11) NOT NULL,
  `MedicalActId` int(11) NOT NULL,
  UNIQUE KEY `unqRec` (`MedicalCategoryId`,`MedicalActId`),
  KEY `fk_MedicalCategoryActRel_MedicalActId_idx` (`MedicalActId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalCategoryActTypeRel`
--

DROP TABLE IF EXISTS `MedicalCategoryActTypeRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalCategoryActTypeRel` (
  `MedicalCategoryId` int(11) NOT NULL,
  `MedicalActId` int(11) NOT NULL,
  `MedicalTypeId` int(11) NOT NULL,
  UNIQUE KEY `unqRec` (`MedicalCategoryId`,`MedicalActId`,`MedicalTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalCategoryTypeRel`
--

DROP TABLE IF EXISTS `MedicalCategoryTypeRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalCategoryTypeRel` (
  `MedicalCategoryId` int(11) NOT NULL,
  `MedicalTypeId` int(11) NOT NULL,
  `MedicalActId` int(11) DEFAULT NULL,
  UNIQUE KEY `index1` (`MedicalCategoryId`,`MedicalTypeId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalTransaction`
--

DROP TABLE IF EXISTS `MedicalTransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalTransaction` (
  `MedicalTransactionId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `HealthUnitId` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `MedicalActId` int(11) NOT NULL,
  `MedicalTypeId` int(11) NOT NULL,
  `RefYear` int(11) NOT NULL,
  `RefMonth` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `RefDate` date DEFAULT NULL,
  `PatientAttributeInsurance` tinyint(4) DEFAULT NULL,
  `PatientAttributeOrigination` tinyint(4) DEFAULT NULL,
  `PersonelId` int(11) DEFAULT NULL,
  `MunicipalityId` int(11) DEFAULT NULL,
  `PatientAmka` char(11) DEFAULT NULL,
  PRIMARY KEY (`MedicalTransactionId`),
  KEY `fk_MedicalTransaction_HealthUnitId` (`HealthUnitId`)
) ENGINE=InnoDB AUTO_INCREMENT=265471 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalType`
--

DROP TABLE IF EXISTS `MedicalType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalType` (
  `MedicalTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MedicalTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MedicalTypeHUTypeRel`
--

DROP TABLE IF EXISTS `MedicalTypeHUTypeRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalTypeHUTypeRel` (
  `HealthUnitTypeId` int(11) NOT NULL,
  `MedicalTypeId` int(11) NOT NULL,
  KEY `fk_MedicalTypeHUTypeRel_HealthUnitTypeId_idx` (`HealthUnitTypeId`),
  KEY `fk_MedicalTypeHUTypeRel_MedicalTypeId_idx` (`MedicalTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Municipality`
--

DROP TABLE IF EXISTS `Municipality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Municipality` (
  `MunicipalityId` int(10) unsigned NOT NULL,
  `DescEL` varchar(55) NOT NULL,
  PRIMARY KEY (`MunicipalityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PatientAttribute`
--

DROP TABLE IF EXISTS `PatientAttribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PatientAttribute` (
  `id` tinyint(4) NOT NULL,
  `attribute` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PatientAttributeValue`
--

DROP TABLE IF EXISTS `PatientAttributeValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PatientAttributeValue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PatientAttributeId` tinyint(4) NOT NULL,
  `Value` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PatientOrigination`
--

DROP TABLE IF EXISTS `PatientOrigination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PatientOrigination` (
  `PatientOriginationId` int(10) unsigned NOT NULL,
  `DescEL` varchar(55) NOT NULL,
  PRIMARY KEY (`PatientOriginationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PerHomeCategory`
--

DROP TABLE IF EXISTS `PerHomeCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PerHomeCategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '100',
  `DescLongEL` varchar(45) DEFAULT NULL,
  `DescrShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Personel`
--

DROP TABLE IF EXISTS `Personel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Personel` (
  `PersonelId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `trn` char(9) DEFAULT NULL,
  `amka` char(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `PersonelCategoryId` int(11) NOT NULL,
  `PersonelEducationId` int(11) NOT NULL,
  `PersonelSpecialityId` int(11) NOT NULL,
  `PersonelPositionId` int(11) DEFAULT NULL,
  `PersonelStatusId` int(11) NOT NULL DEFAULT '1',
  `PersonelDepartmentId` int(11) DEFAULT NULL,
  `LastName` varchar(45) NOT NULL,
  `FirstName` varchar(45) NOT NULL,
  `FatherName` varchar(45) NOT NULL,
  `BirthDate` date DEFAULT NULL,
  `Age` int(11) NOT NULL,
  `Gender` char(1) DEFAULT NULL,
  `personel_id` int(10) unsigned zerofill NOT NULL,
  `RefHealthUnitId` int(10) unsigned DEFAULT NULL,
  `RefUnitStartDate` date DEFAULT NULL,
  `RefUnitEndDate` date DEFAULT NULL,
  PRIMARY KEY (`PersonelId`),
  KEY `fk_Personel_PersonelCategory_idx` (`PersonelCategoryId`),
  KEY `fk_Personel_HealthUnitId` (`HealthUnitId`),
  KEY `fk_Personel_PersonelPositionId_idx` (`PersonelPositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=7380 DEFAULT CHARSET=utf8 COMMENT='Personel Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelAttendanceBook`
--

DROP TABLE IF EXISTS `PersonelAttendanceBook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelAttendanceBook` (
  `PersonelAttendanceBookId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '1',
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `PersonelId` int(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `RefDate` date NOT NULL,
  `PersonelStatusId` int(11) NOT NULL,
  `RefHealthUnitId` int(11) NOT NULL,
  PRIMARY KEY (`PersonelAttendanceBookId`),
  KEY `fk_PersonelAttendanceBook_HealthUnitId_idx` (`HealthUnitId`),
  KEY `fk_PersonelAttendanceBook_PersonelId_idx` (`PersonelId`),
  KEY `fk_PersonelAttendanceBook_PersonelStatusId_idx` (`PersonelStatusId`),
  KEY `PersonelAttendanceBook_RefHealthUnitId_idx` (`RefHealthUnitId`),
  KEY `idx_RefDate` (`RefDate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1375351 DEFAULT CHARSET=utf8 COMMENT='Personel attendance book.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelAttendanceBookRafina`
--

DROP TABLE IF EXISTS `PersonelAttendanceBookRafina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelAttendanceBookRafina` (
  `PersonelAttendanceBookRafinaId` int(11) NOT NULL AUTO_INCREMENT,
  `PersonelId` int(11) NOT NULL,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PersonelStatusId` int(11) NOT NULL,
  `PersonelStatusGroupId` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Details` varchar(255) NOT NULL,
  `Duration` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  PRIMARY KEY (`PersonelAttendanceBookRafinaId`),
  KEY `fk_PersonelAttendanceBookRafina_PersonelId_idx` (`PersonelId`),
  KEY `fk_PersonelAttendanceBookRafina_PersonelStatusId_idx` (`PersonelStatusId`),
  KEY `fk_PersonelAttendanceBookRafina_PersonelStatusGroupId_idx` (`PersonelStatusGroupId`)
) ENGINE=MyISAM AUTO_INCREMENT=3658 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelCategory`
--

DROP TABLE IF EXISTS `PersonelCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelCategory` (
  `PersonelCategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '100',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`PersonelCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Personel Categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelDepartment`
--

DROP TABLE IF EXISTS `PersonelDepartment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelDepartment` (
  `PersonelDepartmentId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(100) NOT NULL,
  `DescShortEL` varchar(100) DEFAULT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PersonelDepartmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelEducation`
--

DROP TABLE IF EXISTS `PersonelEducation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelEducation` (
  `PersonelEducationId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '100',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`PersonelEducationId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Personel Education Levels';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelHealthUnitHistory`
--

DROP TABLE IF EXISTS `PersonelHealthUnitHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelHealthUnitHistory` (
  `PersonelId` int(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime DEFAULT NULL,
  `RefHealthUnitId` int(11) DEFAULT NULL,
  `PersonelStatusId` int(11) DEFAULT NULL,
  `statusid` tinyint(4) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6417 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelMoves`
--

DROP TABLE IF EXISTS `PersonelMoves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelMoves` (
  `PersonelId` int(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelPosition`
--

DROP TABLE IF EXISTS `PersonelPosition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelPosition` (
  `PersonelPositionId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '100',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`PersonelPositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelSchedule`
--

DROP TABLE IF EXISTS `PersonelSchedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelSchedule` (
  `PersonelScheduleId` int(11) NOT NULL AUTO_INCREMENT,
  `PersonelId` int(11) NOT NULL,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `HealthCommitteeId` int(11) NOT NULL,
  `Start` date NOT NULL,
  `End` date NOT NULL,
  PRIMARY KEY (`PersonelScheduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=26554 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelSpeciality`
--

DROP TABLE IF EXISTS `PersonelSpeciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelSpeciality` (
  `PersonelSpecialityId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '1',
  `DescEL` varchar(254) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`PersonelSpecialityId`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelSpecialityHistory`
--

DROP TABLE IF EXISTS `PersonelSpecialityHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelSpecialityHistory` (
  `PersonelId` int(11) NOT NULL,
  `SpecialityId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime DEFAULT NULL,
  `amka` varchar(45) DEFAULT NULL,
  `statusid` tinyint(4) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6421 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelStatus`
--

DROP TABLE IF EXISTS `PersonelStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelStatus` (
  `PersonelStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `DescEL` varchar(100) NOT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `PersonelStatusGroupId` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`PersonelStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Status of Personel in HU';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelStatusCombi`
--

DROP TABLE IF EXISTS `PersonelStatusCombi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelStatusCombi` (
  `PersonelStatusId` int(11) NOT NULL,
  `PersonelStatusGroupId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonelStatusGroup`
--

DROP TABLE IF EXISTS `PersonelStatusGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonelStatusGroup` (
  `PersonelStatusGroupId` int(11) NOT NULL,
  `DescEL` varchar(255) NOT NULL,
  PRIMARY KEY (`PersonelStatusGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ReportCategory`
--

DROP TABLE IF EXISTS `ReportCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ReportCategory` (
  `MedicalTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` int(11) NOT NULL DEFAULT '100',
  `DescEL` varchar(45) DEFAULT NULL,
  `DescShortEL` varchar(45) DEFAULT NULL,
  `UserId` int(11) DEFAULT '0',
  PRIMARY KEY (`MedicalTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Status`
--

DROP TABLE IF EXISTS `Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Status` (
  `StatusId` tinyint(4) NOT NULL AUTO_INCREMENT,
  `DescEN` varchar(45) NOT NULL,
  PRIMARY KEY (`StatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Status Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `HealthUnitId` int(11) NOT NULL,
  `RequireReset` tinyint(4) DEFAULT NULL,
  `email` varchar(100) DEFAULT 'null',
  `name` varchar(255) DEFAULT NULL,
  `HealthDistrictId` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `idx_username` (`username`) USING BTREE,
  KEY `fk_Users_HealthUnit1_idx` (`HealthUnitId`)
) ENGINE=InnoDB AUTO_INCREMENT=1028 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `area_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area_type_id` tinyint(3) unsigned NOT NULL,
  `area` varchar(100) NOT NULL,
  PRIMARY KEY (`area_id`),
  KEY `fk_area_at_idx` (`area_type_id`),
  CONSTRAINT `fk_area_at` FOREIGN KEY (`area_type_id`) REFERENCES `area_type` (`area_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9329 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area_type`
--

DROP TABLE IF EXISTS `area_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area_type` (
  `area_type_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `area_type` varchar(45) NOT NULL,
  PRIMARY KEY (`area_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loc` char(2) DEFAULT NULL,
  `code` char(2) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'Y',
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`),
  KEY `ordering` (`ordering`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country_translation`
--

DROP TABLE IF EXISTS `country_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_translation` (
  `idCountry` int(10) unsigned NOT NULL,
  `language` char(5) NOT NULL DEFAULT 'el-GR',
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`idCountry`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_condition`
--

DROP TABLE IF EXISTS `dental_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_condition` (
  `dental_condition_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `locale` varchar(2) NOT NULL DEFAULT 'el',
  `dental_condition_code` char(1) NOT NULL,
  `ordering` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dental_condition_id`),
  KEY `ix_dental_condition_code` (`dental_condition_code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_dmfcode`
--

DROP TABLE IF EXISTS `dental_dmfcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_dmfcode` (
  `dental_dmfcode_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `dmfCode` varchar(10) NOT NULL,
  PRIMARY KEY (`dental_dmfcode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_mouthcondition`
--

DROP TABLE IF EXISTS `dental_mouthcondition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_mouthcondition` (
  `dental_mouthcondition_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `locale` varchar(5) DEFAULT 'el',
  PRIMARY KEY (`dental_mouthcondition_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_tooth`
--

DROP TABLE IF EXISTS `dental_tooth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_tooth` (
  `dental_tooth_id` tinyint(3) unsigned NOT NULL,
  `dental_dmfcode_id` tinyint(3) unsigned NOT NULL,
  `dental_rel` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`dental_tooth_id`),
  KEY `fk_dmfc_idx` (`dental_dmfcode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_transaction`
--

DROP TABLE IF EXISTS `dental_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_transaction` (
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status_id` smallint(6) DEFAULT NULL,
  `locale` varchar(2) DEFAULT NULL,
  `dental_transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `health_unit_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `school_class_id` int(11) DEFAULT NULL,
  `InfolevelId` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `birthday` date NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `nationality_id` int(10) unsigned NOT NULL,
  `father_profession` varchar(100) DEFAULT NULL,
  `mother_profession` varchar(100) DEFAULT NULL,
  `isMale` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`dental_transaction_id`),
  KEY `ix_dt_natinality` (`nationality_id`),
  KEY `fk_school_school_id` (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18062 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`dexadmin`@`localhost`*/ /*!50003 TRIGGER `dental_transaction_BEFORE_INSERT` BEFORE INSERT ON `dental_transaction` FOR EACH ROW BEGIN
	set NEW.age = DATE_FORMAT(NEW.exam_date,"%Y")-DATE_FORMAT(NEW.birthday,'%Y')-(IF(DATE_FORMAT(NEW.exam_date,"%m-%d") < DATE_FORMAT(NEW.birthday,'%m-%d'),1,0));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dental_transaction_mouth`
--

DROP TABLE IF EXISTS `dental_transaction_mouth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_transaction_mouth` (
  `dental_transaction_mouth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dental_transaction_id` int(10) unsigned NOT NULL,
  `dental_mouthcondition_id` tinyint(3) unsigned NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL,
  `status_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`dental_transaction_mouth_id`),
  KEY `ix_dtm_dt` (`dental_transaction_id`),
  KEY `ix_dtm_dmt` (`dental_mouthcondition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5192 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dental_transaction_tooth`
--

DROP TABLE IF EXISTS `dental_transaction_tooth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dental_transaction_tooth` (
  `dental_transaction_tooth_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dental_transaction_id` int(10) unsigned NOT NULL,
  `dental_tooth_id` tinyint(3) unsigned NOT NULL,
  `dental_condition_id` tinyint(3) unsigned NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status_id` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`dental_transaction_tooth_id`),
  KEY `ix_dtt_dt` (`dental_transaction_id`),
  KEY `ix_dtt_dto` (`dental_tooth_id`),
  KEY `ix_dtt_dtc` (`dental_condition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=391705 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `examination`
--

DROP TABLE IF EXISTS `examination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination` (
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `status_id` smallint(6) NOT NULL DEFAULT '1',
  `locale` varchar(2) NOT NULL DEFAULT 'el',
  `medical_device_id` int(11) NOT NULL DEFAULT '1',
  `examination_id` int(11) NOT NULL,
  `health_unit_id` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `exam_date` timestamp NULL DEFAULT NULL,
  `ear` char(1) NOT NULL,
  `exam_level_id` int(11) DEFAULT NULL,
  `exam_method_id` int(11) DEFAULT NULL,
  `exam_result_id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`examination_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `raw_xml_exam`
--

DROP TABLE IF EXISTS `raw_xml_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raw_xml_exam` (
  `IdXRawXMLExam` int(11) NOT NULL AUTO_INCREMENT,
  `PatientRecordNumber` int(11) DEFAULT NULL,
  `HospitaID` int(11) DEFAULT NULL,
  `Discharged` datetime DEFAULT NULL,
  `Deceased` datetime DEFAULT NULL,
  `NicuStatus` varchar(45) DEFAULT NULL,
  `ConsentState` varchar(45) DEFAULT NULL,
  `TrackingConsent` varchar(45) DEFAULT NULL,
  `ScreeningConsent` varchar(45) DEFAULT NULL,
  `Medication` varchar(45) DEFAULT NULL,
  `FreeText1` varchar(500) DEFAULT NULL,
  `FreeText2` varchar(500) DEFAULT NULL,
  `FreeText3` varchar(500) DEFAULT NULL,
  `Id` text,
  PRIMARY KEY (`IdXRawXMLExam`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `school_id` int(11) NOT NULL,
  `school_level_id` tinyint(3) unsigned NOT NULL,
  `description` varchar(100) NOT NULL,
  `locale` char(2) NOT NULL DEFAULT 'el',
  `area_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`school_id`),
  KEY `fk_sc_sl_idx` (`school_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_class`
--

DROP TABLE IF EXISTS `school_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_class` (
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `status_id` smallint(6) DEFAULT NULL,
  `locale` varchar(2) NOT NULL DEFAULT 'el',
  `school_class_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`school_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_class_map`
--

DROP TABLE IF EXISTS `school_class_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_class_map` (
  `school_id` int(11) NOT NULL,
  `school_class_id` int(11) NOT NULL,
  PRIMARY KEY (`school_id`,`school_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_educationlevel`
--

DROP TABLE IF EXISTS `school_educationlevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_educationlevel` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `educationlevel` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_level`
--

DROP TABLE IF EXISTS `school_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_level` (
  `school_level_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `school_educationlevel_id` tinyint(3) unsigned NOT NULL,
  `school_level` varchar(20) NOT NULL,
  `locale` varchar(5) NOT NULL DEFAULT 'el',
  PRIMARY KEY (`school_level_id`),
  KEY `fk_scledl_idx` (`school_educationlevel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_level_class`
--

DROP TABLE IF EXISTS `school_level_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_level_class` (
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `status_id` smallint(6) DEFAULT NULL,
  `locale` varchar(2) NOT NULL DEFAULT 'el',
  `school_level_class_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `school_level_id` tinyint(3) unsigned NOT NULL,
  `school_level_class` varchar(45) NOT NULL,
  PRIMARY KEY (`school_level_class_id`),
  KEY `ix_slc_slid` (`school_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_Personel`
--

DROP TABLE IF EXISTS `stg_Personel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_Personel` (
  `PersonelId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `trn` char(9) DEFAULT NULL,
  `amka` char(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `PersonelCategoryId` int(11) NOT NULL,
  `PersonelEducationId` int(11) NOT NULL,
  `PersonelSpecialityId` int(11) NOT NULL,
  `PersonelPositionId` int(11) DEFAULT NULL,
  `PersonelStatusId` int(11) NOT NULL DEFAULT '1',
  `PersonelDepartmentId` int(11) DEFAULT NULL,
  `LastName` varchar(45) NOT NULL,
  `FirstName` varchar(45) NOT NULL,
  `FatherName` varchar(45) NOT NULL,
  `BirthDate` date DEFAULT NULL,
  `Age` int(11) NOT NULL,
  `Gender` char(1) DEFAULT NULL,
  `personel_id` int(10) unsigned zerofill NOT NULL,
  `RefHealthUnitId` int(10) unsigned DEFAULT NULL,
  `RefUnitStartDate` date DEFAULT NULL,
  `RefUnitEndDate` date DEFAULT NULL,
  PRIMARY KEY (`PersonelId`),
  KEY `fk_Personel_PersonelCategory_idx` (`PersonelCategoryId`),
  KEY `fk_Personel_HealthUnitId` (`HealthUnitId`),
  KEY `fk_Personel_PersonelPositionId_idx` (`PersonelPositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=7359 DEFAULT CHARSET=utf8 COMMENT='Personel Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_PersonelAttendanceBook`
--

DROP TABLE IF EXISTS `stg_PersonelAttendanceBook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_PersonelAttendanceBook` (
  `PersonelAttendanceBookId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '1',
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `PersonelId` int(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `RefDate` date NOT NULL,
  `PersonelStatusId` int(11) NOT NULL,
  `RefHealthUnitId` int(11) NOT NULL,
  PRIMARY KEY (`PersonelAttendanceBookId`),
  KEY `fk_PersonelAttendanceBook_HealthUnitId_idx` (`HealthUnitId`),
  KEY `fk_PersonelAttendanceBook_PersonelId_idx` (`PersonelId`),
  KEY `fk_PersonelAttendanceBook_PersonelStatusId_idx` (`PersonelStatusId`),
  KEY `PersonelAttendanceBook_RefHealthUnitId_idx` (`RefHealthUnitId`),
  KEY `idx_RefDate` (`RefDate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1374754 DEFAULT CHARSET=utf8 COMMENT='Personel attendance book.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_Users`
--

DROP TABLE IF EXISTS `stg_Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stg_Users` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `HealthUnitId` int(11) NOT NULL,
  `RequireReset` tinyint(4) DEFAULT NULL,
  `email` varchar(100) DEFAULT 'null',
  `name` varchar(255) DEFAULT NULL,
  `HealthDistrictId` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `idx_username` (`username`) USING BTREE,
  KEY `fk_Users_HealthUnit1_idx` (`HealthUnitId`)
) ENGINE=InnoDB AUTO_INCREMENT=1026 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testHealthUnitRel_`
--

DROP TABLE IF EXISTS `testHealthUnitRel_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testHealthUnitRel_` (
  `HealthDistrictId` int(11) DEFAULT NULL,
  `HealthUnitId` int(11) DEFAULT NULL,
  `UnitL2` int(11) DEFAULT NULL,
  `UnitL3` int(11) DEFAULT NULL,
  UNIQUE KEY `uq_testHealthUnitRel` (`HealthUnitId`,`UnitL2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Health Unit Relation in district and other unit.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testHealthunit_`
--

DROP TABLE IF EXISTS `testHealthunit_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testHealthunit_` (
  `HealthUnitId` int(11) NOT NULL,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `StatusId` tinyint(4) NOT NULL DEFAULT '1',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1',
  `UserId` int(11) NOT NULL DEFAULT '0',
  `HealthUnitTypeId` int(11) DEFAULT NULL,
  `DescEL` varchar(254) DEFAULT NULL,
  `DescShortEL` varchar(100) DEFAULT NULL,
  `HealthDistrictId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testPersonel_`
--

DROP TABLE IF EXISTS `testPersonel_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testPersonel_` (
  `PersonelId` int(11) NOT NULL AUTO_INCREMENT,
  `AutoDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `StatusId` int(11) NOT NULL DEFAULT '1',
  `trn` char(9) DEFAULT NULL,
  `amka` char(11) NOT NULL,
  `HealthUnitId` int(11) NOT NULL,
  `PersonelCategoryId` int(11) NOT NULL,
  `PersonelEducationId` int(11) NOT NULL,
  `PersonelSpecialityId` int(11) NOT NULL,
  `PersonelPositionId` int(11) DEFAULT NULL,
  `PersonelStatusId` int(11) NOT NULL DEFAULT '1',
  `PersonelDepartmentId` int(11) DEFAULT NULL,
  `LastName` varchar(45) NOT NULL,
  `FirstName` varchar(45) NOT NULL,
  `FatherName` varchar(45) NOT NULL,
  `BirthDate` date DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `Gender` char(1) DEFAULT NULL,
  `personel_id` int(10) unsigned zerofill NOT NULL,
  PRIMARY KEY (`PersonelId`),
  KEY `fk_Personel_PersonelCategory_idx` (`PersonelCategoryId`),
  KEY `fk_Personel_HealthUnitId` (`HealthUnitId`),
  KEY `fk_Personel_PersonelPositionId_idx` (`PersonelPositionId`)
) ENGINE=InnoDB AUTO_INCREMENT=4556 DEFAULT CHARSET=utf8 COMMENT='Personel Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `vw_ClinicalTransaction`
--

DROP TABLE IF EXISTS `vw_ClinicalTransaction`;
/*!50001 DROP VIEW IF EXISTS `vw_ClinicalTransaction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_ClinicalTransaction` (
  `ClinicDepartmentId` tinyint NOT NULL,
  `ClinicTypeId` tinyint NOT NULL,
  `ClinicIncidentId` tinyint NOT NULL,
  `Quantity` tinyint NOT NULL,
  `HealthUnitTypeId` tinyint NOT NULL,
  `HealthUnit` tinyint NOT NULL,
  `ClinicDepartment` tinyint NOT NULL,
  `Clinic` tinyint NOT NULL,
  `ClinicIncident` tinyint NOT NULL,
  `HealthUnitId` tinyint NOT NULL,
  `RefDate` tinyint NOT NULL,
  `StatusId` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_ExamByHU`
--

DROP TABLE IF EXISTS `vw_ExamByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_ExamByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_ExamByHU` (
  `Monada` tinyint NOT NULL,
  `Examination` tinyint NOT NULL,
  `Quantity` tinyint NOT NULL,
  `Date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_HU_Address`
--

DROP TABLE IF EXISTS `vw_HU_Address`;
/*!50001 DROP VIEW IF EXISTS `vw_HU_Address`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_HU_Address` (
  `DescEL` tinyint NOT NULL,
  `Address` tinyint NOT NULL,
  `PostalCode` tinyint NOT NULL,
  `Phone` tinyint NOT NULL,
  `Fax` tinyint NOT NULL,
  `Email` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_HealthUnitClinicRel`
--

DROP TABLE IF EXISTS `vw_HealthUnitClinicRel`;
/*!50001 DROP VIEW IF EXISTS `vw_HealthUnitClinicRel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_HealthUnitClinicRel` (
  `HealthUnitTypeId` tinyint NOT NULL,
  `ClinicTypeId` tinyint NOT NULL,
  `ClinicType` tinyint NOT NULL,
  `HealthUnit` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_HealthUnitDetail`
--

DROP TABLE IF EXISTS `vw_HealthUnitDetail`;
/*!50001 DROP VIEW IF EXISTS `vw_HealthUnitDetail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_HealthUnitDetail` (
  `HealthUnitId` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `description_short` tinyint NOT NULL,
  `address` tinyint NOT NULL,
  `postcode` tinyint NOT NULL,
  `phone` tinyint NOT NULL,
  `fax` tinyint NOT NULL,
  `email` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_MedicalActTypeRel`
--

DROP TABLE IF EXISTS `vw_MedicalActTypeRel`;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalActTypeRel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_MedicalActTypeRel` (
  `DescEL` tinyint NOT NULL,
  `MedicalTypeId` tinyint NOT NULL,
  `MedicalActId` tinyint NOT NULL,
  `medicalAction` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_MedicalCategoryActTypeRel`
--

DROP TABLE IF EXISTS `vw_MedicalCategoryActTypeRel`;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalCategoryActTypeRel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_MedicalCategoryActTypeRel` (
  `MedicalCategoryId` tinyint NOT NULL,
  `CatDesc` tinyint NOT NULL,
  `MedicalActId` tinyint NOT NULL,
  `ActDesc` tinyint NOT NULL,
  `MedicalTypeId` tinyint NOT NULL,
  `TypDesc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_MedicalTypeHUType`
--

DROP TABLE IF EXISTS `vw_MedicalTypeHUType`;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalTypeHUType`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_MedicalTypeHUType` (
  `DescEL` tinyint NOT NULL,
  `HealthUnitTypeId` tinyint NOT NULL,
  `MedicalTypeId` tinyint NOT NULL,
  `medicaltype` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_Personel`
--

DROP TABLE IF EXISTS `vw_Personel`;
/*!50001 DROP VIEW IF EXISTS `vw_Personel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_Personel` (
  `HealthUnit` tinyint NOT NULL,
  `Category` tinyint NOT NULL,
  `Educatution` tinyint NOT NULL,
  `EducatutionShort` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `Position` tinyint NOT NULL,
  `Department` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `Age` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_PersonelAttendanceBookByHU`
--

DROP TABLE IF EXISTS `vw_PersonelAttendanceBookByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_PersonelAttendanceBookByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_PersonelAttendanceBookByHU` (
  `PersonelAttendanceBookId` tinyint NOT NULL,
  `RefDate` tinyint NOT NULL,
  `OrigHUId` tinyint NOT NULL,
  `OrigHU` tinyint NOT NULL,
  `PersonelId` tinyint NOT NULL,
  `EducationId` tinyint NOT NULL,
  `Education` tinyint NOT NULL,
  `SpecialityId` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `PersonelStatusId` tinyint NOT NULL,
  `PersonelStatus` tinyint NOT NULL,
  `AttendanceHUId` tinyint NOT NULL,
  `AttendanceHU` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_PersonelScheduleByHU`
--

DROP TABLE IF EXISTS `vw_PersonelScheduleByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_PersonelScheduleByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_PersonelScheduleByHU` (
  `PersonelId` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `Committee` tinyint NOT NULL,
  `FromDate` tinyint NOT NULL,
  `ToDate` tinyint NOT NULL,
  `ScheduleId` tinyint NOT NULL,
  `CommitteeId` tinyint NOT NULL,
  `MemoColor` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_Test`
--

DROP TABLE IF EXISTS `vw_Test`;
/*!50001 DROP VIEW IF EXISTS `vw_Test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_Test` (
  `ClinicTypeId` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstClinicByHU`
--

DROP TABLE IF EXISTS `vw_lstClinicByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_lstClinicByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstClinicByHU` (
  `ClinicId` tinyint NOT NULL,
  `Clinic` tinyint NOT NULL,
  `Tooltip` tinyint NOT NULL,
  `DepartmentId` tinyint NOT NULL,
  `Department` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstClinicByHUType`
--

DROP TABLE IF EXISTS `vw_lstClinicByHUType`;
/*!50001 DROP VIEW IF EXISTS `vw_lstClinicByHUType`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstClinicByHUType` (
  `ClinicTypeId` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstDoctorByHU`
--

DROP TABLE IF EXISTS `vw_lstDoctorByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_lstDoctorByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstDoctorByHU` (
  `HealthUnitId` tinyint NOT NULL,
  `HealthUnit` tinyint NOT NULL,
  `PersonelId` tinyint NOT NULL,
  `CategoryId` tinyint NOT NULL,
  `PersonelCategory` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstHealthCommitee`
--

DROP TABLE IF EXISTS `vw_lstHealthCommitee`;
/*!50001 DROP VIEW IF EXISTS `vw_lstHealthCommitee`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstHealthCommitee` (
  `CommitteeId` tinyint NOT NULL,
  `Committee` tinyint NOT NULL,
  `MemoColor` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstIncidentByHU`
--

DROP TABLE IF EXISTS `vw_lstIncidentByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_lstIncidentByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstIncidentByHU` (
  `IncidentId` tinyint NOT NULL,
  `Incident` tinyint NOT NULL,
  `Tooltip` tinyint NOT NULL,
  `DepartmentId` tinyint NOT NULL,
  `Department` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstIncidentByHUType`
--

DROP TABLE IF EXISTS `vw_lstIncidentByHUType`;
/*!50001 DROP VIEW IF EXISTS `vw_lstIncidentByHUType`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstIncidentByHUType` (
  `ClinicIncidentId` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstLabTestByMedicalAct1`
--

DROP TABLE IF EXISTS `vw_lstLabTestByMedicalAct1`;
/*!50001 DROP VIEW IF EXISTS `vw_lstLabTestByMedicalAct1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstLabTestByMedicalAct1` (
  `MedicalTypeId` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstMedTypeByMedCat`
--

DROP TABLE IF EXISTS `vw_lstMedTypeByMedCat`;
/*!50001 DROP VIEW IF EXISTS `vw_lstMedTypeByMedCat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstMedTypeByMedCat` (
  `MedicalCategoryId` tinyint NOT NULL,
  `CatDesc` tinyint NOT NULL,
  `MedicalActId` tinyint NOT NULL,
  `ActDesc` tinyint NOT NULL,
  `MedicalTypeId` tinyint NOT NULL,
  `MedDesc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstMedicalTypeByHUType`
--

DROP TABLE IF EXISTS `vw_lstMedicalTypeByHUType`;
/*!50001 DROP VIEW IF EXISTS `vw_lstMedicalTypeByHUType`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstMedicalTypeByHUType` (
  `HealthUnitTypeId` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL,
  `MedicalTypeId` tinyint NOT NULL,
  `medicaltype` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelAttendanceStatus`
--

DROP TABLE IF EXISTS `vw_lstPersonelAttendanceStatus`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelAttendanceStatus`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelAttendanceStatus` (
  `PersonelStatusId` tinyint NOT NULL,
  `description` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelByHU`
--

DROP TABLE IF EXISTS `vw_lstPersonelByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelByHU` (
  `HealthUnitId` tinyint NOT NULL,
  `HealthUnit` tinyint NOT NULL,
  `AFM` tinyint NOT NULL,
  `AMKA` tinyint NOT NULL,
  `PersonelId` tinyint NOT NULL,
  `CategoryId` tinyint NOT NULL,
  `PersonelCategory` tinyint NOT NULL,
  `EducationId` tinyint NOT NULL,
  `Education` tinyint NOT NULL,
  `SpecialityId` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `DepartmentId` tinyint NOT NULL,
  `Department` tinyint NOT NULL,
  `PositionId` tinyint NOT NULL,
  `Position` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `BirthDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelByHUCat`
--

DROP TABLE IF EXISTS `vw_lstPersonelByHUCat`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByHUCat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelByHUCat` (
  `HealthUnit` tinyint NOT NULL,
  `Category` tinyint NOT NULL,
  `Count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelByUser`
--

DROP TABLE IF EXISTS `vw_lstPersonelByUser`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByUser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelByUser` (
  `HealthUnitId` tinyint NOT NULL,
  `HealthUnit` tinyint NOT NULL,
  `AFM` tinyint NOT NULL,
  `AMKA` tinyint NOT NULL,
  `PersonelId` tinyint NOT NULL,
  `CategoryId` tinyint NOT NULL,
  `PersonelCategory` tinyint NOT NULL,
  `EducationId` tinyint NOT NULL,
  `Education` tinyint NOT NULL,
  `SpecialityId` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `DepartmentId` tinyint NOT NULL,
  `Department` tinyint NOT NULL,
  `PositionId` tinyint NOT NULL,
  `Position` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `BirthDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelScheduleByHU`
--

DROP TABLE IF EXISTS `vw_lstPersonelScheduleByHU`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelScheduleByHU`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelScheduleByHU` (
  `PersonelId` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `Committee` tinyint NOT NULL,
  `FromDate` tinyint NOT NULL,
  `ToDate` tinyint NOT NULL,
  `ScheduleId` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_lstPersonelTableau`
--

DROP TABLE IF EXISTS `vw_lstPersonelTableau`;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelTableau`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_lstPersonelTableau` (
  `HealthUnit` tinyint NOT NULL,
  `Category` tinyint NOT NULL,
  `Educatution` tinyint NOT NULL,
  `Speciality` tinyint NOT NULL,
  `Position` tinyint NOT NULL,
  `Department` tinyint NOT NULL,
  `LastName` tinyint NOT NULL,
  `FirstName` tinyint NOT NULL,
  `FatherName` tinyint NOT NULL,
  `Age` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vw_school`
--

DROP TABLE IF EXISTS `vw_school`;
/*!50001 DROP VIEW IF EXISTS `vw_school`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vw_school` (
  `school_id` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `username` tinyint NOT NULL,
  `DescEL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'pedy'
--
/*!50003 DROP FUNCTION IF EXISTS `fn_CountHUPersonel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `fn_CountHUPersonel`(`cnt` INT) RETURNS int(11)
    SQL SECURITY INVOKER
BEGIN
	declare c int;

	select count(*) into c
	from pedy.Personel
	where Personel.HealthUnitId = cnt;

RETURN c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_GetUserHUId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `fn_GetUserHUId`(`p1` INT) RETURNS int(11)
    SQL SECURITY INVOKER
BEGIN
	
	

declare usrhuid int;

select HealthUnitId into usrhuid
from pedy.Users
where UserId = p1;

RETURN @usrhuid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_GetUserHUType` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `fn_GetUserHUType`(`uid` INT) RETURNS varchar(10) CHARSET utf8
    SQL SECURITY INVOKER
BEGIN

declare hutype varchar(10);

select c.DescShortEL into hutype
from pedy.Users a
join pedy.HealthUnit b
	on a.HealthUnitId = b.HealthUnitId
join pedy.HealthUnitType c
	on b.HealthUnitTypeId = c.HealthUnitTypeId
where 
	a.UserId = uid;

RETURN hutype;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fn_UserHUTypeId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `fn_UserHUTypeId`(`user` INT) RETURNS int(11)
    SQL SECURITY INVOKER
BEGIN

DECLARE hutypeid INT;

SELECT HealthUnit.HealthUnitTypeId INTO hutypeid
FROM pedy.HealthUnit
INNER JOIN pedy.Users
ON HealthUnit.HealthUnitId = Users.HealthUnitId
WHERE Users.UserId=user;

RETURN hutypeid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `p1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `p1`() RETURNS int(11)
    NO SQL
    DETERMINISTIC
    SQL SECURITY INVOKER
return @p1 ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `p2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` FUNCTION `p2`() RETURNS int(11)
    NO SQL
    DETERMINISTIC
    SQL SECURITY INVOKER
return @p2 ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `duplicateRows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `duplicateRows`(`_schemaName` TEXT, `_tableName` TEXT, `_whereClause` TEXT, `_omitColumns` TEXT)
    SQL SECURITY INVOKER
BEGIN
  SELECT IF(TRIM(_omitColumns) <> '', CONCAT('id', ',', TRIM(_omitColumns)), 'id') INTO @omitColumns;

  SELECT GROUP_CONCAT(COLUMN_NAME) FROM information_schema.columns 
  WHERE table_schema = _schemaName AND table_name = _tableName AND FIND_IN_SET(COLUMN_NAME,@omitColumns) = 0 ORDER BY ORDINAL_POSITION INTO @columns;

  SET @sql = CONCAT('INSERT INTO ', _tableName, '(', @columns, ')',
  'SELECT ', @columns, 
  ' FROM ', _schemaName, '.', _tableName, ' ',  _whereClause);

  PREPARE stmt1 FROM @sql;
  EXECUTE stmt1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `make_healthunit_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `make_healthunit_history`()
BEGIN
	declare finished integer default 0;
    declare vPersonelId, vHealthUnitId integer;
    declare vamka varchar(50);
    declare vAutodate datetime; 
	DECLARE pcur CURSOR FOR SELECT PersonelId, amka, HealthUnitId, Autodate from Personel where StatusId = 1;
    declare continue handler for not found set finished = 1;
    
    open pcur;
    get_p: loop
		
        fetch pcur into vPersonelId, vamka, vHealthUnitId, vAutodate;
          if finished = 1 then
			leave get_p;
		end if;
			update PersonelAttendanceBook pa inner join Personel p on pa.PersonelId = p.PersonelId and p.amka = vamka and p.StatusId != 1  set pa.PersonelId = vPersonelId;
			insert into PersonelHealthUnitHistory
            (PersonelId, HealthUnitId, StartDate, EndDate, StatusId)
            select 
            vPersonelId, HealthUnitId, Autodate, null, StatusId
            from Personel where amka = vAmka and StatusId != 1;
			
            insert into PersonelHealthUnitHistory
            (PersonelId, HealthUnitId, StartDate, EndDate, StatusId)
            values
            (vPersonelId, vHealthUnitId, vAutodate, null, 1);
            
            delete from Personel where amka = vamka and Statusid != 1;
      
    end loop get_p;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `make_special_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `make_special_history`()
BEGIN
	declare finished integer default 0;
    declare vPersonelId, vPersonelSpecialityId integer;
    declare vamka varchar(50);
    declare vAutodate datetime; 
	DECLARE pcur CURSOR FOR SELECT PersonelId, amka, PersonelSpecialityId, Autodate from Personel where StatusId = 1;
    declare continue handler for not found set finished = 1;
    
    open pcur;
    get_p: loop
		
        fetch pcur into vPersonelId, vamka, vPersonelSpecialityId, vAutodate;
          if finished = 1 then
			leave get_p;
		end if;
			update PersonelAttendanceBook pa inner join Personel p on pa.PersonelId = p.PersonelId and p.amka = vamka and p.StatusId != 1  set pa.PersonelId = vPersonelId;
			insert into PersonelSpecialityHistory
            (PersonelId, SpecialityId, StartDate, EndDate, StatusId)
            select 
            vPersonelId, PersonelSpecialityId, Autodate, null, StatusId
            from Personel where amka = vAmka and StatusId != 1;
			
            insert into PersonelSpecialityHistory
            (PersonelId, SpecialityId, StartDate, EndDate, StatusId)
            values
            (vPersonelId, vPersonelSpecialityId, vAutodate, null, 1);
            
            delete from Personel where amka = vamka and Statusid != 1;
      
    end loop get_p;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_AddClinicTransaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_AddClinicTransaction`(IN `user` INT, IN `hu` INT, IN `CliTypeId` INT, IN `CliIncId` INT, IN `rYear` YEAR, IN `rMonth` INT, IN `Quant` INT)
    SQL SECURITY INVOKER
BEGIN

INSERT INTO pedy.ClinicTransaction (UserId,HealthUnitId,ClinicTypeId,ClinicIncidentId,Quantity,RefYear,RefMonth)
VALUES (user,hu,CliTypeId,CliIncId,Quant,rYear,rMonth);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_AddMedicalTransaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_AddMedicalTransaction`(IN `user` INT, IN `hu` INT, IN `MedActId` INT, IN `MedTypId` INT, IN `rYear` YEAR, IN `rMonth` INT, IN `Quant` INT)
    SQL SECURITY INVOKER
BEGIN

INSERT INTO pedy.MedicalTransaction (UserId,HealthUnitId,MedicalActId,MedicalTypeId,Quantity,RefYear,RefMonth)
VALUES (user,hu,MedActId,MedTypId,Quant,rYear,rMonth);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_fix_Dental_Age` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_fix_Dental_Age`()
    SQL SECURITY INVOKER
BEGIN
	update pedy.dental_transaction
	set age = DATE_FORMAT(exam_date,"%Y")-DATE_FORMAT(birthday,'%Y')-(IF(DATE_FORMAT(exam_date,"%m-%d") < DATE_FORMAT(birthday,'%m-%d'),1,0))
	where age = null or age = 0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getUserHUId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_getUserHUId`(IN `UId` INT, IN `Al` INT)
    SQL SECURITY INVOKER
BEGIN




declare UserDistrictId int;
declare HUId int;
declare Admin boolean;

if Al = '' then
	set Al = 0; 
end if;


select HealthDistrictId into UserDistrictId from pedy.Users where UserId = UId;

select HealthUnitId into HUId from pedy.Users where UserId = UId;

select IsAdmin into Admin  from pedy.Users where UserId = UId;


if Admin = 0 and Al = 0 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId
	where HealthUnitRel.UnitL2 = HUId or HealthUnitRel.HealthUnitId = HUId
    order by HealthUnit.DescEL;
end if;


if Admin = 0 and Al = 1 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId
    where HealthUnit.HealthDistrictId=UserDistrictId
    union
    select HealthUnitId, DescShortEL, HUId, HealthUnitTypeId
    from pedy.HealthUnit
    where (HealthDistrictId <>UserDistrictId and HealthUnitTypeId=26) or (HealthDistrictId =UserDistrictId and HealthUnitTypeId=1) 
       order by DescEL;
end if;

if Admin = 1 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId
    where HealthUnit.HealthDistrictId=UserDistrictId
    union
    select HealthUnitId, DescShortEL, HUId, HealthUnitTypeId
    from pedy.HealthUnit
    where (HealthDistrictId <>UserDistrictId and HealthUnitTypeId=26) or (HealthDistrictId =UserDistrictId and HealthUnitTypeId=1) 
    order by DescEL;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getUserHUId_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_getUserHUId_new`(IN `UId` INT)
    SQL SECURITY INVOKER
BEGIN




declare HUId int;
declare Admin boolean;


select HealthUnitId into HUId from pedy.Users where UserId = UId;

select IsAdmin into Admin  from pedy.Users where UserId = UId;

if Admin = 0 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId
	where HealthUnitRel.UnitL2 = HUId or HealthUnitRel.HealthUnitId = HUId;
end if;

if Admin = 1 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId;
end if;

if Admin = 1 then
	select HealthUnitRel.HealthUnitId,HealthUnit.DescEL,HUId,HealthUnit.HealthUnitTypeId
	from pedy.HealthUnitRel
	JOIN pedy.HealthUnit
	on HealthUnitRel.HealthUnitId = HealthUnit.HealthUnitId
    order by HealthUnit.DescEL;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getUserHUType` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_getUserHUType`(IN `uid` INT)
    SQL SECURITY INVOKER
BEGIN

select c.DescShortEL AS HUType
from pedy.Users a
join pedy.HealthUnit b
	on a.HealthUnitId = b.HealthUnitId
join pedy.HealthUnitType c
	on b.HealthUnitTypeId = c.HealthUnitTypeId
where 
	a.UserId = uid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_HealthUnitRel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_HealthUnitRel`(IN `Hu` INT, IN `RefTo` INT)
    SQL SECURITY INVOKER
BEGIN


declare RefToTypeId int;
	

select 
	HealthUnitTypeId
from
	HealthUnit
where
	HealthUnitId = RefTo
into
	RefToTypeId;


select RefToTypeId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_pers_trans` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_pers_trans`(`uid` INT, `afm` TEXT, `huid` INT)
    SQL SECURITY INVOKER
BEGIN
  declare pers_id int;
  
  SELECT MAX(PersonelId) FROM pedy.Personel WHERE trn=afm INTO pers_id;
  
  UPDATE pedy.Personel SET StatusId=2 WHERE PersonelId=pers_id;
  
  CREATE TEMPORARY TABLE pedy.tmp_table ENGINE=MEMORY
  
  SELECT * FROM pedy.Personel WHERE PersonelId=pers_id;

  
  UPDATE pedy.tmp_table SET StatusId=1;	
  
  INSERT INTO pedy.Personel (UserId, StatusId, trn, amka, HealthUnitId, PersonelCategoryId, PersonelEducationId, PersonelSpecialityId, PersonelPositionId, PersonelStatusId, PersonelDepartmentId, LastName, FirstName, FatherName, BirthDate, Age, Gender)
							SELECT uid,
								StatusId,
                                trn,
                                amka,
                                huid,
                                PersonelCategoryId,
                                PersonelEducationId,
                                PersonelSpecialityId,
                                PersonelPositionId,
                                PersonelStatusId,
                                PersonelDepartmentId,
                                LastName,
                                FirstName,
                                FatherName,
                                BirthDate,
                                Age,
                                Gender
							FROM pedy.tmp_table;
  
  DROP TABLE pedy.tmp_table;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UidRefHUs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_UidRefHUs`(IN `UId` INT)
    SQL SECURITY INVOKER
BEGIN



declare HUId int;
declare HUTypeId int;
declare HUL2 int;


select HealthUnitId INTO HUId from pedy.Users where UserId = UId;

select HealthUnitTypeId into HUTypeId from pedy.HealthUnit where HealthUnitId = HUId;


if HUTypeId = 14 then
	select a.HealthUnitId,b.DescEL,HUId,b.HealthUnitTypeId
	from pedy.HealthUnitRel a
	JOIN pedy.HealthUnit b
		on a.HealthUnitId = b.HealthUnitId
	where a.UnitL2 = HUId or a.HealthUnitId = HUId;
end if;


if HUTypeId <> 14 then
	select UnitL2 into HUL2 from pedy.HealthUnitRel where HealthUnitId = HUId;
	select a.HealthUnitId,b.DescEL,HUId,b.HealthUnitTypeId
	from pedy.HealthUnitRel a
	JOIN pedy.HealthUnit b
		on a.HealthUnitId = b.HealthUnitId
	where a.UnitL2 = HUL2 or a.HealthUnitId = HUL2;

end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_age` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`jpedy`@`localhost` PROCEDURE `sp_upd_age`(IN `amka_check` CHAR(11))
    SQL SECURITY INVOKER
BEGIN
SELECT DISTINCT(amka)
FROM pedy.Personel;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `vw_ClinicalTransaction`
--

/*!50001 DROP TABLE IF EXISTS `vw_ClinicalTransaction`*/;
/*!50001 DROP VIEW IF EXISTS `vw_ClinicalTransaction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_ClinicalTransaction` AS select `ClinicTransaction`.`ClinicDepartmentId` AS `ClinicDepartmentId`,`ClinicTransaction`.`ClinicTypeId` AS `ClinicTypeId`,`ClinicTransaction`.`ClinicIncidentId` AS `ClinicIncidentId`,`ClinicTransaction`.`Quantity` AS `Quantity`,`HealthUnit`.`HealthUnitTypeId` AS `HealthUnitTypeId`,`HealthUnit`.`DescEL` AS `HealthUnit`,`ClinicDepartment`.`DescEL` AS `ClinicDepartment`,`ClinicType`.`DescEL` AS `Clinic`,`ClinicIncident`.`DescEL` AS `ClinicIncident`,`ClinicTransaction`.`HealthUnitId` AS `HealthUnitId`,`ClinicTransaction`.`RefDate` AS `RefDate`,`ClinicTransaction`.`StatusId` AS `StatusId` from ((((`ClinicTransaction` join `HealthUnit` on((`HealthUnit`.`HealthUnitId` = `ClinicTransaction`.`HealthUnitId`))) left join `ClinicDepartment` on((`ClinicDepartment`.`ClinicDepartmentId` = `ClinicTransaction`.`ClinicDepartmentId`))) join `ClinicType` on((`ClinicType`.`ClinicTypeId` = `ClinicTransaction`.`ClinicTypeId`))) left join `ClinicIncident` on((`ClinicIncident`.`ClinicIncidentId` = `ClinicTransaction`.`ClinicIncidentId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_ExamByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_ExamByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_ExamByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_ExamByHU` AS select `HealthUnit`.`DescEL` AS `Monada`,`MedicalType`.`DescEL` AS `Examination`,`MedicalTransaction`.`Quantity` AS `Quantity`,`MedicalTransaction`.`RefDate` AS `Date` from ((`MedicalTransaction` join `MedicalType` on((`MedicalTransaction`.`MedicalTypeId` = `MedicalType`.`MedicalTypeId`))) join `HealthUnit` on((`MedicalTransaction`.`HealthUnitId` = `HealthUnit`.`HealthUnitId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_HU_Address`
--

/*!50001 DROP TABLE IF EXISTS `vw_HU_Address`*/;
/*!50001 DROP VIEW IF EXISTS `vw_HU_Address`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_HU_Address` AS select `HealthUnit`.`DescEL` AS `DescEL`,`HealthUnitDetail`.`Address` AS `Address`,`HealthUnitDetail`.`PostalCode` AS `PostalCode`,`HealthUnitDetail`.`Phone` AS `Phone`,`HealthUnitDetail`.`Fax` AS `Fax`,`HealthUnitDetail`.`Email` AS `Email` from (`HealthUnitDetail` join `HealthUnit` on((`HealthUnitDetail`.`HealthUnitId` = `HealthUnit`.`HealthUnitId`))) where (`HealthUnitDetail`.`StatusId` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_HealthUnitClinicRel`
--

/*!50001 DROP TABLE IF EXISTS `vw_HealthUnitClinicRel`*/;
/*!50001 DROP VIEW IF EXISTS `vw_HealthUnitClinicRel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_HealthUnitClinicRel` AS select `hu`.`HealthUnitTypeId` AS `HealthUnitTypeId`,`ct`.`ClinicTypeId` AS `ClinicTypeId`,`ct`.`DescEL` AS `ClinicType`,`hu`.`DescEL` AS `HealthUnit` from ((`HealthUnitClinicRel` `hucr` join `ClinicType` `ct` on((`hucr`.`ClinicTypeId` = `ct`.`ClinicTypeId`))) join `HealthUnitType` `hu` on((`hu`.`HealthUnitTypeId` = `hucr`.`HealthUnitTypeId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_HealthUnitDetail`
--

/*!50001 DROP TABLE IF EXISTS `vw_HealthUnitDetail`*/;
/*!50001 DROP VIEW IF EXISTS `vw_HealthUnitDetail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_HealthUnitDetail` AS select `b`.`HealthUnitId` AS `HealthUnitId`,`b`.`DescEL` AS `description`,`b`.`DescShortEL` AS `description_short`,`a`.`Address` AS `address`,`a`.`PostalCode` AS `postcode`,`a`.`Phone` AS `phone`,`a`.`Fax` AS `fax`,`a`.`Email` AS `email` from (`HealthUnitDetail` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) where (`a`.`StatusId` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_MedicalActTypeRel`
--

/*!50001 DROP TABLE IF EXISTS `vw_MedicalActTypeRel`*/;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalActTypeRel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_MedicalActTypeRel` AS select `mt`.`DescEL` AS `DescEL`,`mt`.`MedicalTypeId` AS `MedicalTypeId`,`mar`.`MedicalActId` AS `MedicalActId`,`ma`.`DescEL` AS `medicalAction` from ((`MedicalActTypeRel` `mar` join `MedicalType` `mt` on((`mar`.`MedicalTypeId` = `mt`.`MedicalTypeId`))) join `MedicalAct` `ma` on((`ma`.`MedicalActId` = `mar`.`MedicalActId`))) order by `ma`.`DescEL`,`mt`.`DescEL` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_MedicalCategoryActTypeRel`
--

/*!50001 DROP TABLE IF EXISTS `vw_MedicalCategoryActTypeRel`*/;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalCategoryActTypeRel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_MedicalCategoryActTypeRel` AS select `b`.`MedicalCategoryId` AS `MedicalCategoryId`,`b`.`DescEL` AS `CatDesc`,`c`.`MedicalActId` AS `MedicalActId`,`c`.`DescEL` AS `ActDesc`,`d`.`MedicalTypeId` AS `MedicalTypeId`,`d`.`DescEL` AS `TypDesc` from (((`MedicalCategoryActTypeRel` `a` join `MedicalCategory` `b` on((`a`.`MedicalCategoryId` = `b`.`MedicalCategoryId`))) join `MedicalAct` `c` on((`a`.`MedicalActId` = `c`.`MedicalActId`))) join `MedicalType` `d` on((`a`.`MedicalTypeId` = `d`.`MedicalTypeId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_MedicalTypeHUType`
--

/*!50001 DROP TABLE IF EXISTS `vw_MedicalTypeHUType`*/;
/*!50001 DROP VIEW IF EXISTS `vw_MedicalTypeHUType`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_MedicalTypeHUType` AS select `HealthUnitType`.`DescEL` AS `DescEL`,`HealthUnitType`.`HealthUnitTypeId` AS `HealthUnitTypeId`,`MedicalType`.`MedicalTypeId` AS `MedicalTypeId`,`MedicalType`.`DescEL` AS `medicaltype` from ((`MedicalTypeHUTypeRel` join `HealthUnitType` on((`HealthUnitType`.`HealthUnitTypeId` = `MedicalTypeHUTypeRel`.`HealthUnitTypeId`))) join `MedicalType` on((`MedicalType`.`MedicalTypeId` = `MedicalTypeHUTypeRel`.`MedicalTypeId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_Personel`
--

/*!50001 DROP TABLE IF EXISTS `vw_Personel`*/;
/*!50001 DROP VIEW IF EXISTS `vw_Personel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_Personel` AS select `b`.`DescEL` AS `HealthUnit`,`c`.`DescEL` AS `Category`,`d`.`DescEL` AS `Educatution`,`d`.`DescShortEL` AS `EducatutionShort`,`e`.`DescEL` AS `Speciality`,`f`.`DescEL` AS `Position`,`g`.`DescEL` AS `Department`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName`,`a`.`Age` AS `Age` from ((((((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) join `PersonelCategory` `c` on((`a`.`PersonelCategoryId` = `c`.`PersonelCategoryId`))) join `PersonelEducation` `d` on((`a`.`PersonelEducationId` = `d`.`PersonelEducationId`))) join `PersonelSpeciality` `e` on((`a`.`PersonelSpecialityId` = `e`.`PersonelSpecialityId`))) left join `PersonelPosition` `f` on((`a`.`PersonelPositionId` = `f`.`PersonelPositionId`))) left join `PersonelDepartment` `g` on((`a`.`PersonelDepartmentId` = `g`.`PersonelDepartmentId`))) where ((`a`.`StatusId` = 1) and (`a`.`PersonelPositionId` = 10)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_PersonelAttendanceBookByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_PersonelAttendanceBookByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_PersonelAttendanceBookByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_PersonelAttendanceBookByHU` AS select `c`.`PersonelAttendanceBookId` AS `PersonelAttendanceBookId`,`c`.`RefDate` AS `RefDate`,`b`.`HealthUnitId` AS `OrigHUId`,`b`.`DescEL` AS `OrigHU`,`a`.`PersonelId` AS `PersonelId`,`f`.`PersonelEducationId` AS `EducationId`,`f`.`DescEL` AS `Education`,`g`.`PersonelSpecialityId` AS `SpecialityId`,`g`.`DescEL` AS `Speciality`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName`,`c`.`PersonelStatusId` AS `PersonelStatusId`,`d`.`DescEL` AS `PersonelStatus`,`e`.`HealthUnitId` AS `AttendanceHUId`,`e`.`DescEL` AS `AttendanceHU` from ((((((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) left join `PersonelAttendanceBook` `c` on((`a`.`PersonelId` = `c`.`PersonelId`))) left join `PersonelStatus` `d` on((`c`.`PersonelStatusId` = `d`.`PersonelStatusId`))) left join `PersonelEducation` `f` on((`a`.`PersonelEducationId` = `f`.`PersonelEducationId`))) left join `PersonelSpeciality` `g` on((`a`.`PersonelSpecialityId` = `g`.`PersonelSpecialityId`))) join `HealthUnit` `e` on((`b`.`HealthUnitId` = `e`.`HealthUnitId`))) where ((`b`.`HealthUnitId` = `p1`()) and (`a`.`StatusId` = 1) and (`c`.`StatusId` = 1)) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_PersonelScheduleByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_PersonelScheduleByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_PersonelScheduleByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_PersonelScheduleByHU` AS select `a`.`PersonelId` AS `PersonelId`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`b`.`DescEL` AS `Speciality`,`d`.`DescEL` AS `Committee`,`c`.`Start` AS `FromDate`,`c`.`End` AS `ToDate`,`c`.`PersonelScheduleId` AS `ScheduleId`,`d`.`HealthCommitteeId` AS `CommitteeId`,`d`.`MemoColor` AS `MemoColor` from (((`Personel` `a` join `PersonelSpeciality` `b` on((`a`.`PersonelSpecialityId` = `b`.`PersonelSpecialityId`))) join `PersonelSchedule` `c` on((`c`.`PersonelId` = `a`.`PersonelId`))) join `HealthCommittee` `d` on((`c`.`HealthCommitteeId` = `d`.`HealthCommitteeId`))) where ((`a`.`StatusId` = 1) and (`c`.`StatusId` = 1) and (`a`.`HealthUnitId` = `P1`())) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_Test`
--

/*!50001 DROP TABLE IF EXISTS `vw_Test`*/;
/*!50001 DROP VIEW IF EXISTS `vw_Test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_Test` AS select `ClinicType`.`ClinicTypeId` AS `ClinicTypeId`,`ClinicType`.`DescEL` AS `DescEL` from (`ClinicType` left join `HealthUnitClinicRel` on((`HealthUnitClinicRel`.`ClinicTypeId` = `ClinicType`.`ClinicTypeId`))) where (`HealthUnitClinicRel`.`HealthUnitTypeId` = `p1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstClinicByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstClinicByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstClinicByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstClinicByHU` AS select `a`.`ClinicTypeId` AS `ClinicId`,`a`.`DescEL` AS `Clinic`,`a`.`DescShortEL` AS `Tooltip`,`b`.`ClinicDepartmentId` AS `DepartmentId`,`c`.`DescEL` AS `Department` from ((`ClinicType` `a` left join `HealthUnitClinicRel` `b` on((`a`.`ClinicTypeId` = `b`.`ClinicTypeId`))) join `ClinicDepartment` `c` on((`b`.`ClinicDepartmentId` = `c`.`ClinicDepartmentId`))) where (`b`.`HealthUnitId` = `P1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstClinicByHUType`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstClinicByHUType`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstClinicByHUType`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstClinicByHUType` AS select `ClinicType`.`ClinicTypeId` AS `ClinicTypeId`,`ClinicType`.`DescEL` AS `DescEL` from (`ClinicType` left join `HealthUnitClinicRel` on((`HealthUnitClinicRel`.`ClinicTypeId` = `ClinicType`.`ClinicTypeId`))) where (`HealthUnitClinicRel`.`HealthUnitTypeId` = `p1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstDoctorByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstDoctorByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstDoctorByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstDoctorByHU` AS select `b`.`HealthUnitId` AS `HealthUnitId`,`b`.`DescEL` AS `HealthUnit`,`a`.`PersonelId` AS `PersonelId`,`c`.`PersonelCategoryId` AS `CategoryId`,`c`.`DescEL` AS `PersonelCategory`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName` from ((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) left join `PersonelCategory` `c` on((`a`.`PersonelCategoryId` = `c`.`PersonelCategoryId`))) where ((`a`.`HealthUnitId` = `P1`()) and (`c`.`PersonelCategoryId` = 1) and (`a`.`StatusId` = 1)) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstHealthCommitee`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstHealthCommitee`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstHealthCommitee`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstHealthCommitee` AS select `a`.`HealthCommitteeId` AS `CommitteeId`,`a`.`DescEL` AS `Committee`,`a`.`MemoColor` AS `MemoColor` from `HealthCommittee` `a` where ((`a`.`HealthCommitteeId` <> `P1`()) or (`a`.`HealthCommitteeId` = `P1`())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstIncidentByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstIncidentByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstIncidentByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstIncidentByHU` AS select `a`.`ClinicIncidentId` AS `IncidentId`,`a`.`DescEL` AS `Incident`,`a`.`tooltip` AS `Tooltip`,`d`.`ClinicDepartmentId` AS `DepartmentId`,`d`.`DescEL` AS `Department` from (((`ClinicIncident` `a` left join `HUIncidentRel` `b` on((`b`.`ClinicIncidentId` = `a`.`ClinicIncidentId`))) join `HealthUnit` `c` on((`b`.`HealthUnitTypeId` = `c`.`HealthUnitTypeId`))) join `ClinicDepartment` `d` on((`b`.`ClinicDepartmentId` = `d`.`ClinicDepartmentId`))) where (`c`.`HealthUnitId` = `P1`()) order by `a`.`form_index` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstIncidentByHUType`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstIncidentByHUType`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstIncidentByHUType`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstIncidentByHUType` AS select `ClinicIncident`.`ClinicIncidentId` AS `ClinicIncidentId`,`ClinicIncident`.`DescEL` AS `DescEL` from (`ClinicIncident` left join `HUIncidentRel` on((`HUIncidentRel`.`ClinicIncidentId` = `ClinicIncident`.`ClinicIncidentId`))) where (`HUIncidentRel`.`HealthUnitTypeId` = `p1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstLabTestByMedicalAct1`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstLabTestByMedicalAct1`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstLabTestByMedicalAct1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstLabTestByMedicalAct1` AS select `MedicalActTypeRel`.`MedicalTypeId` AS `MedicalTypeId`,`MedicalType`.`DescEL` AS `DescEL` from (`MedicalActTypeRel` join `MedicalType` on((`MedicalActTypeRel`.`MedicalTypeId` = `MedicalType`.`MedicalTypeId`))) where (`MedicalActTypeRel`.`MedicalActId` = `p1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstMedTypeByMedCat`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstMedTypeByMedCat`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstMedTypeByMedCat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstMedTypeByMedCat` AS select `MedicalCategory`.`MedicalCategoryId` AS `MedicalCategoryId`,`MedicalCategory`.`DescEL` AS `CatDesc`,`MedicalAct`.`MedicalActId` AS `MedicalActId`,`MedicalAct`.`DescEL` AS `ActDesc`,`MedicalType`.`MedicalTypeId` AS `MedicalTypeId`,`MedicalType`.`DescEL` AS `MedDesc` from (((`MedicalCategoryActTypeRel` join `MedicalCategory` on((`MedicalCategoryActTypeRel`.`MedicalCategoryId` = `MedicalCategory`.`MedicalCategoryId`))) join `MedicalAct` on((`MedicalCategoryActTypeRel`.`MedicalActId` = `MedicalAct`.`MedicalActId`))) join `MedicalType` on((`MedicalCategoryActTypeRel`.`MedicalTypeId` = `MedicalType`.`MedicalTypeId`))) where (`MedicalCategoryActTypeRel`.`MedicalCategoryId` = `p1`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstMedicalTypeByHUType`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstMedicalTypeByHUType`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstMedicalTypeByHUType`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstMedicalTypeByHUType` AS select `HealthUnitType`.`HealthUnitTypeId` AS `HealthUnitTypeId`,`HealthUnitType`.`DescEL` AS `DescEL`,`MedicalType`.`MedicalTypeId` AS `MedicalTypeId`,`MedicalType`.`DescEL` AS `medicaltype` from ((`MedicalTypeHUTypeRel` join `HealthUnitType` on((`HealthUnitType`.`HealthUnitTypeId` = `p1`()))) join `MedicalType` on((`MedicalType`.`MedicalTypeId` = `MedicalTypeHUTypeRel`.`MedicalTypeId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelAttendanceStatus`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelAttendanceStatus`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelAttendanceStatus`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelAttendanceStatus` AS select `a`.`PersonelStatusId` AS `PersonelStatusId`,`a`.`DescEL` AS `description` from `PersonelStatus` `a` where (`a`.`PersonelStatusId` in (7,8,9,10,11,12)) order by `a`.`DescEL` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelByHU` AS select `b`.`HealthUnitId` AS `HealthUnitId`,`b`.`DescEL` AS `HealthUnit`,`a`.`trn` AS `AFM`,`a`.`amka` AS `AMKA`,`a`.`PersonelId` AS `PersonelId`,`c`.`PersonelCategoryId` AS `CategoryId`,`c`.`DescEL` AS `PersonelCategory`,`d`.`PersonelEducationId` AS `EducationId`,`d`.`DescEL` AS `Education`,`e`.`PersonelSpecialityId` AS `SpecialityId`,`e`.`DescEL` AS `Speciality`,`f`.`PersonelDepartmentId` AS `DepartmentId`,`f`.`DescEL` AS `Department`,`g`.`PersonelPositionId` AS `PositionId`,`g`.`DescEL` AS `Position`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName`,`a`.`BirthDate` AS `BirthDate` from ((((((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) left join `PersonelCategory` `c` on((`a`.`PersonelCategoryId` = `c`.`PersonelCategoryId`))) left join `PersonelEducation` `d` on((`a`.`PersonelEducationId` = `d`.`PersonelEducationId`))) left join `PersonelSpeciality` `e` on((`a`.`PersonelSpecialityId` = `e`.`PersonelSpecialityId`))) left join `PersonelDepartment` `f` on((`a`.`PersonelDepartmentId` = `f`.`PersonelDepartmentId`))) left join `PersonelPosition` `g` on((`a`.`PersonelPositionId` = `g`.`PersonelPositionId`))) where ((`a`.`HealthUnitId` = `P1`()) and (`a`.`StatusId` = 1)) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelByHUCat`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelByHUCat`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByHUCat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelByHUCat` AS select `HealthUnit`.`DescEL` AS `HealthUnit`,`PersonelCategory`.`DescEL` AS `Category`,count(0) AS `Count` from ((`Personel` join `HealthUnit` on((`Personel`.`HealthUnitId` = `HealthUnit`.`HealthUnitId`))) join `PersonelCategory` on((`Personel`.`PersonelCategoryId` = `PersonelCategory`.`PersonelCategoryId`))) group by `HealthUnit`.`DescEL`,`PersonelCategory`.`DescEL` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelByUser`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelByUser`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelByUser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelByUser` AS select `b`.`HealthUnitId` AS `HealthUnitId`,`b`.`DescEL` AS `HealthUnit`,`a`.`trn` AS `AFM`,`a`.`amka` AS `AMKA`,`a`.`PersonelId` AS `PersonelId`,`c`.`PersonelCategoryId` AS `CategoryId`,`c`.`DescEL` AS `PersonelCategory`,`d`.`PersonelEducationId` AS `EducationId`,`d`.`DescEL` AS `Education`,`e`.`PersonelSpecialityId` AS `SpecialityId`,`e`.`DescEL` AS `Speciality`,`f`.`PersonelDepartmentId` AS `DepartmentId`,`f`.`DescEL` AS `Department`,`g`.`PersonelPositionId` AS `PositionId`,`g`.`DescEL` AS `Position`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName`,`a`.`BirthDate` AS `BirthDate` from (((((((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) left join `PersonelCategory` `c` on((`a`.`PersonelCategoryId` = `c`.`PersonelCategoryId`))) left join `PersonelEducation` `d` on((`a`.`PersonelEducationId` = `d`.`PersonelEducationId`))) left join `PersonelSpeciality` `e` on((`a`.`PersonelSpecialityId` = `e`.`PersonelSpecialityId`))) left join `PersonelDepartment` `f` on((`a`.`PersonelDepartmentId` = `f`.`PersonelDepartmentId`))) left join `PersonelPosition` `g` on((`a`.`PersonelPositionId` = `g`.`PersonelPositionId`))) join `Users` `i` on((`i`.`HealthUnitId` = `a`.`HealthUnitId`))) where ((`i`.`UserId` = `P1`()) and (`a`.`StatusId` = 1)) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelScheduleByHU`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelScheduleByHU`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelScheduleByHU`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelScheduleByHU` AS select `a`.`PersonelId` AS `PersonelId`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`b`.`DescEL` AS `Speciality`,`d`.`DescEL` AS `Committee`,`c`.`Start` AS `FromDate`,`c`.`End` AS `ToDate`,`c`.`PersonelScheduleId` AS `ScheduleId` from (((`Personel` `a` join `PersonelSpeciality` `b` on((`a`.`PersonelSpecialityId` = `b`.`PersonelSpecialityId`))) join `PersonelSchedule` `c` on((`c`.`PersonelId` = `a`.`PersonelId`))) join `HealthCommittee` `d` on((`c`.`HealthCommitteeId` = `d`.`HealthCommitteeId`))) where ((`a`.`StatusId` = 1) and (`c`.`StatusId` = 1) and (`a`.`HealthUnitId` = `P1`())) order by `a`.`LastName`,`a`.`FirstName` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_lstPersonelTableau`
--

/*!50001 DROP TABLE IF EXISTS `vw_lstPersonelTableau`*/;
/*!50001 DROP VIEW IF EXISTS `vw_lstPersonelTableau`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_lstPersonelTableau` AS select `b`.`DescEL` AS `HealthUnit`,`c`.`DescEL` AS `Category`,`d`.`DescEL` AS `Educatution`,`e`.`DescEL` AS `Speciality`,`f`.`DescEL` AS `Position`,`g`.`DescEL` AS `Department`,`a`.`LastName` AS `LastName`,`a`.`FirstName` AS `FirstName`,`a`.`FatherName` AS `FatherName`,`a`.`Age` AS `Age` from ((((((`Personel` `a` join `HealthUnit` `b` on((`a`.`HealthUnitId` = `b`.`HealthUnitId`))) join `PersonelCategory` `c` on((`a`.`PersonelCategoryId` = `c`.`PersonelCategoryId`))) join `PersonelEducation` `d` on((`a`.`PersonelEducationId` = `d`.`PersonelEducationId`))) join `PersonelSpeciality` `e` on((`a`.`PersonelSpecialityId` = `e`.`PersonelSpecialityId`))) left join `PersonelPosition` `f` on((`a`.`PersonelPositionId` = `f`.`PersonelPositionId`))) left join `PersonelDepartment` `g` on((`a`.`PersonelDepartmentId` = `g`.`PersonelDepartmentId`))) where (`a`.`StatusId` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_school`
--

/*!50001 DROP TABLE IF EXISTS `vw_school`*/;
/*!50001 DROP VIEW IF EXISTS `vw_school`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jpedy`@`localhost` SQL SECURITY INVOKER */
/*!50001 VIEW `vw_school` AS select `a`.`school_id` AS `school_id`,`b`.`description` AS `description`,`a`.`user_id` AS `user_id`,`c`.`username` AS `username`,`d`.`DescEL` AS `DescEL` from (((`dental_transaction` `a` join `school` `b` on((`a`.`school_id` = `b`.`school_id`))) join `Users` `c` on((`a`.`user_id` = `c`.`UserId`))) join `HealthUnit` `d` on((`a`.`health_unit_id` = `d`.`HealthUnitId`))) where (`a`.`status_id` = 1) group by `a`.`school_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-10 13:14:51
