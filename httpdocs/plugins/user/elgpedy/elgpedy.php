<?php 
defined('JPATH_BASE') or die;

class plgUserelgpedy extends JPlugin
{
	function onContentPrepareData($context, $data)
	{
		// Check we are manipulating a valid form.
		if (!in_array($context, array('com_users.profile','com_users.registration','com_users.user','com_admin.profile'))){
			return true;
		}
 
		$userId = isset($data->id) ? $data->id : 0;
 
		// Load the profile data from the database.
		$db = JFactory::getDbo();
		$db->setQuery(
			'SELECT profile_key, profile_value FROM #__user_profiles' .
			' WHERE user_id = '.(int) $userId .
			' AND profile_key LIKE \'elgpedy.%\'' .
			' ORDER BY ordering'
		);
		$results = $db->loadRowList();
                
		// Check for a database error.
		if ($db->getErrorNum()) {
			$this->_subject->setError($db->getErrorMsg());
			return false;
		}
 
		// Merge the profile data.
		$data->elgpedy = array();
		foreach ($results as $v) {
			$k = str_replace('elgpedy.', '', $v[0]);
			$data->elgpedy[$k] =json_decode($v[1], true);
		}
 
		return true;
	}
 
	function onContentPrepareForm($form, $data)
	{
		// Load user_profile plugin language
		$lang = JFactory::getLanguage();
		$lang->load('plg_user_elgpedy', JPATH_ADMINISTRATOR);
 
		if (!($form instanceof JForm)) {
			$this->_subject->setError('JERROR_NOT_A_FORM');
			return false;
		}
		// Check we are manipulating a valid form.
		if (!in_array($form->getName(), array('com_users.profile', 'com_users.registration','com_users.user','com_admin.profile'))) {
			return true;
		}

		// Add the profile fields to the form.
		JForm::addFormPath(dirname(__FILE__).'/profiles');
		$form->loadFile('profile', false);
	}
 
	function onUserAfterSave($data, $isNew, $result, $error)
	{
		$userId	= JArrayHelper::getValue($data, 'id', 0, 'int');
		$healthUnitId = 0;
		$healthDistrictId = 0;
		
		
		if ($userId && $result && isset($data['elgpedy']) && (count($data['elgpedy'])))
		{
			try
			{
				$db = JFactory::getDbo();
				$db->setQuery('DELETE FROM #__user_profiles WHERE user_id = '.$userId.' AND profile_key LIKE \'elgpedy.%\'');
				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
 
				$tuples = array();
				$order	= 1;
				foreach ($data['elgpedy'] as $k => $v) {
					if ($k == 'HealthUnitId') :
						$healthUnitId = $v;	
					endif;
					if ($k == 'HealthDistrictId') :
						$healthDistrictId = $v;	
					endif;					
					$tuples[] = '('.$userId.', '.$db->quote('elgpedy.'.$k).', '.$db->quote(json_encode($v)).', '.$order++.')';
				}
 
				$db->setQuery('INSERT INTO #__user_profiles VALUES '.implode(', ', $tuples));
				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
			}
			catch (JException $e) {
				$this->_subject->setError($e->getMessage());
				return false;
			}
		}
		
		
		if($healthUnitId > 0 ):
			$this->deleteRemote($userId);
			$this->storeRemote($userId, $healthUnitId, $healthDistrictId);
		endif;
		return true;
	}
	
	private function storeRemote($userID, $hUID, $healthDistrictId)
	{
		$this -> getPedyDB() -> setQuery("insert into Users (UserId, HealthUnitId, HealthDistrictId) values ($userID, $hUID, $healthDistrictId)") -> execute();
	}
	
	function onUserAfterDelete($user, $success, $msg)
	{
		if (!$success) {
			return false;
		}
 
		$userId	= JArrayHelper::getValue($user, 'id', 0, 'int');
 
		if ($userId)
		{
			try
			{
				$db = JFactory::getDbo();
				$db->setQuery(
					'DELETE FROM #__user_profiles WHERE user_id = '.$userId .
					" AND profile_key LIKE 'elgpedy.%'"
				);
 
				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
			}
			catch (JException $e)
			{
				$this->_subject->setError($e->getMessage());
				return false;
			}
		}
		$this->deleteRemote($userId);
		return true;
		
	}
	
	private function deleteRemote($userID)
	{		
		$this->getPedyDb() -> setQuery ("delete from Users where UserId = $userID ") -> execute();
	}
	
	
	/**
	public static function getRemoteCon($con = '')
    {
		if ($con === ''):
			$con = 'pedy';
		endif;
        $className = $con . 'con';
        include_once JPATH_SITE . "/components/com_elgpedy/$className.php";
		$class = new $className();
		return $class::getDB();
        
    }	
	**/
	//**** stauro auto to connection **/
	
	public static function getPedyDB()
	{
		$option = array();
		$option['driver']   = 'mysqli'; 		
		$option['host']     = 'localhost';
		$option['user']     = 'pedy';
		$option['password'] = 'pedy@PP';
		$option['database'] = 'pedy';
		$option['prefix']   = ''; 
		return JDatabase::getInstance( $option );
	}
	
  
}
?>
