<?php
/*----------------------------------------------------------------------
 EL Lib E-logism's library plugin.
 -----------------------------------------------------------------------
 author    E-Logism 
 copyright Copyright (C) 2018 e-logism.gr. All Rights Reserved.
 @license - E-Logism propriatery library
 Websites: www.e-logism.gr
-----------------------------------------------------------------------*/

defined('_JEXEC') or die;


class plgSystemEllib extends JPlugin
{
    /**
     * Method to register e-logism's library.
     *
     */
    public function onAfterInitialise()
    {
        JLoader::registerNamespace('elogism', JPATH_LIBRARIES . '/elogism' , false, false, 'psr4');
        JLoader::registerNamespace('components', JPATH_SITE . '/components' , false, false, 'psr4');
        JLoader::registerNamespace('comadmin', JPATH_ADMINISTRATOR . '/components' , false, false, 'psr4');
    }
}

