<?php
defined('_JEXEC') or die;
// --------------------------------------------------------------------
// Software: eorisis jQuery
// Type: Joomla! System Plugin
// Author: eorisis https://eorisis.com
// Copyright (C) 2011-2017 eorisis. All Rights Reserved.
// License: GNU General Public License version 2; See /misc/licence.txt
// Terms of Service: https://eorisis.com/tos
// --------------------------------------------------------------------

if (!version_compare(JVERSION, 3, '>='))
{
	jimport('joomla.plugin.plugin');
}

class plgSystemEorisis_jquery extends JPlugin
{
	protected $app;
	protected $doc;
	private $app_type;
	private $app_name;
	private $lib_path;
	private $J3;
	private $J37;
	private $user_id;
	private $webroot;
	private $webroot_relative;
	private $scheme;
	private $is_html;
	private $is_admin;
	private $external_url;
	private $media_url;
	private $media_url_relative;
	private $main_url;
	private $url;
	private $cdn;
	private $loaded_media = array();
	private $head;
	private $css_media_type = false;
	private $css_first;
	private $css_last = array();
	private $custom_cdn;
	private $abort;
	private $lang_loaded;
	private $msg_set;
	private $file_check;
	private $task;
	private $custom_js_async_defer;
	private $js_attr = array();
	private $uri_version;
	private $media_version;
	private $media_options;
	private $sufix;

	// --------------------------------------------------

	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->app_type = $config['type'];
		$this->app_name = $config['name'];
		$this->lib_path = JPATH_SITE.'/plugins/'.$this->app_type.'/'.$this->app_name.'/framework/lib/';
	}

	// --------------------------------------------------

	public function onAfterInitialise()
	{
		$this->doc = JFactory::getDocument();
		$this->is_html = ($this->doc->getType() == 'html');

		if (!$this->is_html)
		{
			return;
		}

		// --------------------------------------------------

		JLoader::register($this->app_name, $this->lib_path.$this->app_name.'.php');
		$this->J3 = version_compare(JVERSION, 3, '>=');
		$J32 = ($this->J3 ? version_compare(JVERSION, '3.2', '>=') : null);
		$this->J37 = ($J32 ? version_compare(JVERSION, '3.7', '>=') : null);
		$this->uri_version = (int)$this->params->get('uri_version', 1);
		$this->media_version = (($this->uri_version and $J32) ? $this->doc->getMediaVersion() : null);
		$this->media_options = (($this->media_version and $this->J37) ? array('version' => 'auto') : array());
		$this->sufix = $this->media_version ? '?'.$this->media_version : '';

		if (!$this->app)
		{
			$this->app = JFactory::getApplication();
		}

		$this->is_admin = $this->app->isAdmin();
		$this->set_urls();

		$this->file_check = (int)$this->params->get('file_check', 1);
		if ($this->file_check)
		{
			$this->user_id = (int)JFactory::getUser()->get('id');
		}

		// --------------------------------------------------
		// Load Library and Scripts in Order

		// jQuery Library, Migrate
		// $lib_version_default, $lib_version_latest, $migrate_version_default, $migrate_version_latest
		$this->jquery_lib('1.12.4','1.12.4', '1.4.1','1.4.1');

		// All scripts depend on jQuery library
		// They will not load if the lib is not loaded in the same area
		if ($this->url->jq_lib)
		{
			$this->ui('1.9.2', '1.12.1'); // default, latest
			$this->custom_media();
			$this->twitter_bootstrap('2.3.2', '3.3.7'); // default, latest
			$this->no_conflict();
		}
	}

	// --------------------------------------------------

	public function onBeforeCompileHead()
	{
		if (!$this->is_html)
		{
			return;
		}

		$this->css_order_last();
		$this->head = $this->doc->getHeadData();
		$this->async_defer_adv(); // for "Add Files" (Joomla < 3.7)
		$remove = $this->remove_media();
		$remove = $this->remove_media_other($remove);
		$this->head_reset($remove);
	}

	// --------------------------------------------------

	private function jquery_lib($lib_version_default, $lib_version_latest, $migrate_version_default, $migrate_version_latest)
	{
		if (!$this->area('jq_lib', 1))
		{
			return;
		}

		$this->task = 'jq_lib';

		// --------------------------------------------------

		$type = '.min';
		$is_media = true;

		// Version Usage
		switch ($this->params->get('jq_lib_version_choice', 1))
		{
			case 1: // Auto
				$lib_version = eorisis_jquery::auto_version('jq_lib', $lib_version_default, $lib_version_latest, $this->J3); // $version_default, $version_latest
				$url_check = 'lib/jquery-'.$lib_version.'.min.js';
				$url_local = $this->media_url.$url_check;
				break;

			case 2: // List
				$lib_version = $this->clean_version('jq_lib', $lib_version_default);
				$url_check = 'lib/jquery-'.$lib_version.'.min.js';
				$url_local = $this->media_url.$url_check;
				break;

			case 3: // Custom
				$lib_version = $this->clean_version('jq_lib_custom', $lib_version_default);
				if (!$this->params->get('jq_lib_custom_version_type', 1))
				{
					$type = '';
				}
				$url_check = $this->custom_local_path('jq_lib', 'jquery-'.$lib_version.$type.'.js');
				$url_local = $url_check;
				$is_media = false;
				break;
		}

		$url = null;

		// Source: CDN
		if ($lib_version and ($this->params->get('jq_lib_source', 1) == 1))
		{
			$is_local = false;
			$default = $this->cdn->jquery.'jquery-'.$lib_version.$type.'.js';
			switch ($this->params->get('jq_lib_source_cdn', 1))
			{
				case 1: $url = $default; break;
				case 2: $url = $this->cdn->google.'jquery/'.eorisis_jquery::version_correction($lib_version).'/jquery'.$type.'.js'; break;
				case 3: $url = $this->cdn->cloudflare.'jquery/'.eorisis_jquery::version_correction($lib_version).'/jquery'.$type.'.js'; break;
				default: $url = $default;
			}

			$url = $this->cdn_fallback('jq_lib', $url, $url_local);
		}
		// Source: Local
		else
		{
			$is_local = true;
			$url = $this->check_file($url_check, $is_media);
		}

		if ($this->abort or !$url)
		{
			return;
		}

		$this->url->jq_lib = $url;
		$this->load('js', $url, $is_local);
		eorisis_jquery::set_loaded('lib', true);

		// --------------------------------------------------
		// Migrate

		$migrate = $this->params->get('migrate', 1);
		if (($migrate == 2) or (($migrate == 1) and version_compare($lib_version, '1.9.0', '>='))) // Always Load or Auto Load
		{
			// Version Usage
			switch ($this->params->get('migrate_version_choice', 1))
			{
				// Auto
				case 1: $migrate_version = eorisis_jquery::auto_version('migrate', $migrate_version_default, $migrate_version_latest, $this->J3); break;

				// List
				case 2: $migrate_version = $this->clean_version('migrate', $migrate_version_default); break;
			}

			$url_check = 'plugins/migrate/jquery-migrate-'.$migrate_version.'.min.js';
			$url_local = $this->media_url.$url_check;

			// Source
			switch ((int)$this->params->get('migrate_source', 1))
			{
				case 1: // CDN
				{
					$is_local = false;
					$default = $this->cdn->jquery.'jquery-migrate-'.$migrate_version.'.min.js';
					switch ($this->params->get('migrate_source_cdn', 1))
					{
						case 1: $url = $default; break;
						case 2: $url = $this->cdn->cloudflare.'jquery-migrate/'.$migrate_version.'/jquery-migrate.min.js'; break;
						default: $url = $default;
					}

					$url = $this->cdn_fallback('migrate', $url, $url_local);
				} break;

				case 2: // Local
					$is_local = true;
					$url = $this->check_file($url_check);
					break;
			}

			if ($this->abort or !$url)
			{
				return;
			}

			$this->url->migrate = $url;
			$this->load('js', $url, $is_local);
		}
	}

	// --------------------------------------------------

	private function ui($version_default, $version_latest)
	{
		if (!$this->area('ui', 0))
		{
			return;
		}

		$this->task = 'ui';

		// --------------------------------------------------

		// Use Preset UI Files
		if (!$this->params->get('ui_custom', 0))
		{
			$file_js = null;
			$file_css = null;
			$url_js = null;
			$url_css = null;

			$elem = $this->params->get('ui_elem', 'js');
			$file_min = 'jquery-ui.min';

			// --------------------------------------------------

			// Version Usage
			switch ($this->params->get('ui_version_choice', 1))
			{
				// Auto
				case 1: $version = eorisis_jquery::auto_version('ui', $version_default, $version_latest, $this->J3); break;

				// List
				case 2: $version = $this->clean_version('ui', $version_default); break;
			}

			// --------------------------------------------------

			if ($elem != 'css')
			{
				$file_js = $file_min.'.js';
				$url_js_part = $version.'/'.$file_js;
				$url_js_check = 'ui/'.$url_js_part;
				$url_js_local = $this->media_url.$url_js_check;
			}

			if ($elem != 'js')
			{
				$theme_default = 'ui-lightness';
				$theme = preg_replace('/[^a-z-]+/', '', $this->params->get('ui_theme', $theme_default));

				// UI versions below 1.10.1 ..
				if (version_compare($version, '1.10.1', '<='))
				{
					if ($theme == 'base') // ..have a 'base' theme
					{
						$file_css = 'minified/'.$file_min; // It's minified css file is inside a /minified/ dir
					}
					else
					{
						// UI versions below 1.10.0 have no minified CSS theme files (except the 'base' theme which is checked above)
						$file_css = version_compare($version, '1.10.0', '<=') ? 'jquery-ui' : $file_min;
					}
				}
				else
				{
					$file_css = $file_min;
					if ($theme == 'base')
					{
						// UI version 1.11.3 brought back the 'base' theme
						// UI version 1.11.4 removed the 'base' theme again
						// UI version 1.12.0 brought back the 'base' theme
						// So versions 1.11.4, or 1.11.2 and older will use the default
						if (($version == '1.11.4') or version_compare($version, '1.11.2', '<='))
						{
							$theme = $theme_default;
						}
					}
				}

				$url_css_part = $version.'/themes/'.$theme.'/'.$file_css.'.css';
				$url_css_check = 'ui/'.$url_css_part;
				$url_css_local = $this->media_url.$url_css_check;
			}

			// --------------------------------------------------

			// Source
			switch ((int)$this->params->get('ui_source', 1))
			{
				case 1: // CDN
				{
					$is_local = false;
					$default = $this->cdn->jquery.'ui/';
					switch ($this->params->get('ui_source_cdn', 1))
					{
						case 1: $main_url = $default; break;
						case 2: $main_url = $this->cdn->google.'jqueryui/'; break;
						default: $main_url = $default;
					}

					if ($file_js)
					{
						$url_js = $main_url.$url_js_part;
						$url_js = $this->cdn_fallback('ui', $url_js, $url_js_local);
					}

					if ($file_css)
					{
						$url_css = $main_url.$url_css_part;
						$url_css = $this->cdn_fallback('ui', $url_css, $url_css_local);
					}
				} break;

				case 2: // Local
				{
					$is_local = true;

					if ($file_js)
					{
						$url_js = $this->check_file($url_js_check);
					}

					if ($file_css)
					{
						$url_css = $this->check_file($url_css_check);
					}
				} break;
			}
		}
		// Use Custom UI Files
		else
		{
			$is_local = null;
			$url_js = $this->urls('js', $this->params->get('ui_custom_js'));
			$url_css = $this->urls('css', $this->params->get('ui_custom_css'));
		}

		// --------------------------------------------------

		if ($this->abort)
		{
			return;
		}

		// Load JS
		if ($url_js and $this->url->jq_lib)
		{
			$this->url->ui_js = $url_js; // $url_js is an array when Use Custom UI Files
			$this->load('js', $url_js, $is_local);
		}

		// Load CSS
		if ($url_css)
		{
			$this->url->ui_css = $url_css;
			$this->css_order($url_css, $is_local);
		}
	}

	// --------------------------------------------------

	private function twitter_bootstrap($version_default, $version_latest)
	{
		if (!$this->area('tb', 0))
		{
			return;
		}

		$this->task = 'tb';

		// --------------------------------------------------

		// Version Usage
		switch ($this->params->get('tb_version_choice', 1))
		{
			// Auto
			case 1: $version = eorisis_jquery::auto_version('tb', $version_default, $version_latest, $this->J3);break;

			// List
			case 2: $version = $this->clean_version('tb', $version_default); break;
		}

		$elem = $this->params->get('tb_elem', 'js');
		$dir = 'bootstrap/';
		$file_js = null;
		$file_css = null;
		$file_theme_css = null;

		if ($elem != 'css')
		{
			$file_js = 'bootstrap.min.js';
			$url_js_part = $version.'/js/'.$file_js;
			$url_js_check = $dir.$url_js_part;
			$url_js_local = $this->media_url.$url_js_check;
		}

		if ($elem != 'js')
		{
			if ($version == '2.3.2')
			{
				$file_css = 'bootstrap-combined.min.css';
			}
			else // >= 3.0.0
			{
				$file_css = 'bootstrap.min.css';
				if ($this->params->get('tb_theme', 0))
				{
					$file_theme_css = 'bootstrap-theme.min.css';
					$url_theme_css_part = $version.'/css/'.$file_theme_css;
					$url_theme_css_check = $dir.$url_theme_css_part;
					$url_theme_css_local = $this->media_url.$url_theme_css_check;
				}
			}

			$url_css_part = $version.'/css/'.$file_css;
			$url_css_check = $dir.$url_css_part;
			$url_css_local = $this->media_url.$url_css_check;
		}

		// Source
		switch ((int)$this->params->get('tb_source', 1))
		{
			case 1: // CDN
			{
				$is_local = false;
				$default = $this->cdn->bootstrap_maxcdn;
				switch ($this->params->get('tb_source_cdn', 1))
				{
					case 1: $main_url = $default; break;
					case 2: $main_url = $this->cdn->bootstrap_netdna; break;
					default: $main_url = $default;
				}

				if ($file_js)
				{
					$url_js = $main_url.$url_js_part;
					$url_js = $this->cdn_fallback('tb', $url_js, $url_js_local);
				}

				if ($file_css)
				{
					$url_css = $main_url.$url_css_part;
					$url_css = $this->cdn_fallback('tb', $url_css, $url_css_local);

					if ($file_theme_css)
					{
						$url_theme_css = $main_url.$url_theme_css_part;
						$url_theme_css = $this->cdn_fallback('tb', $url_theme_css, $url_theme_css_local);
					}
				}
			} break;

			case 2: // Local
			{
				$is_local = true;

				if ($file_js)
				{
					$url_js = $this->check_file($url_js_check);
				}

				if ($file_css)
				{
					$url_css = $this->check_file($url_css_check);
					if ($file_theme_css)
					{
						$url_theme_css = $this->check_file($url_theme_css_check);
					}
				}
			} break;
		}

		// --------------------------------------------------

		if ($this->abort)
		{
			return;
		}

		// Load JS
		if ($file_js and $this->url->jq_lib)
		{
			$this->url->tb_js = $url_js;
			$this->load('js', $url_js, $is_local);
		}

		// Load CSS
		if ($file_css)
		{
			$this->url->tb_css = $url_css;
			$this->css_order($url_css, $is_local);

			if ($file_theme_css)
			{
				$this->url->tb_theme_css = $url_theme_css;
				$this->css_order($url_theme_css, $is_local);
			}
		}
	}

	// --------------------------------------------------

	private function custom_media()
	{
		if (!$this->area('custom', 0))
		{
			return;
		}

		$this->task = 'custom';
		$this->custom_js_async_defer = (int)$this->params->get('custom_js_async_defer', 0);

		// --------------------------------------------------

		foreach (array('js','css') as $type)
		{
			if (!$this->params->get('custom_'.$type, 0))
			{
				continue;
			}

			$urls = $this->urls($type, $this->params->get('custom_'.$type.'_urls'));
			if (!$urls)
			{
				continue;
			}

			switch ($type)
			{
				case 'js': $this->load($type, $urls); break;
				case 'css': $this->css_order($urls); break;
			}
		}
	}

	// --------------------------------------------------

	private function no_conflict()
	{
		$nc = (int)$this->params->get('no_conflict', 1);
		if (!$nc or (($nc == 1) and !$this->url->jq_lib))
		{
			return;
		}

		$this->task = 'no_conflict';
		$url = $this->media_url.'jquery-noconflict.js';
		$this->url->noconflict = $url;
		$this->load('js', $url, true);
		eorisis_jquery::set_loaded('noconflict', true);
	}

	// --------------------------------------------------

	private function area($param, $default)
	{
		$area = (int)$this->params->get($param.'_area', $default);
		if (!$area)
		{
			return;
		}

		if (($area == 3) or
			(($area == 1) and !$this->is_admin) or
			(($area == 2) and $this->is_admin))
		{
			return true;
		}
	}

	// --------------------------------------------------

	private function head_isset($files, $jui)
	{
		foreach ($files as $file)
		{
			if (isset($this->head['scripts'][$jui.'js/'.$file]))
			{
				return true;
			}
		}
	}

	// --------------------------------------------------

	private function head_reset($remove)
	{
		$changes = null;
		$arr = array(
			'js' => 'scripts',
			'css' => 'styleSheets'
		);

		foreach ($arr as $type => $elem)
		{
			if (empty($remove->$type))
			{
				continue;
			}

			$urls = $this->head[$elem];
			foreach ($remove->$type as $key => $val)
			{
				if (isset($urls[$val]))
				{
					unset($urls[$val]);
				}
				elseif (isset($urls[$val.$this->sufix]))
				{
					unset($urls[$val.$this->sufix]);
				}
			}

			$this->head[$elem] = $urls;
			$changes = true;
		}

		if ($changes)
		{
			$this->doc->setHeadData($this->head);
		}
	}

	// --------------------------------------------------

	private function clean_version($param, $default)
	{
		$version = $this->params->get($param.'_version', $default);
		$version = preg_replace('/[^0-9a-zA-Z.-]+/', '', $version);
		$version = str_replace('..', '', $version);

		return ($version != '.') ? $version : '';
	}

	// --------------------------------------------------

	private function cdn_fallback($param, $url, $url_local)
	{
		$fallback = false;
		if ($this->params->get($param.'_cdn_fallback', 0))
		{
			if (function_exists('curl_init'))
			{
				$cURL = curl_init($this->scheme.':'.$url);
				curl_setopt($cURL, CURLOPT_NOBODY, true);

				// Send User-Agent
				if ($this->params->get('curl_useragent', 0))
				{
					curl_setopt($cURL, CURLOPT_USERAGENT, eorisis_jquery::clean($this->params->get('curl_useragent_txt', 'CDN Fallback Check')));
				}

				$result = curl_exec($cURL);
				if (!$result or ($result and (curl_getinfo($cURL, CURLINFO_HTTP_CODE) !== 200)))
				{
					$fallback = true;
				}

				curl_close($cURL);
			}
		}

		return $fallback ? $url_local : $url;
	}

	// --------------------------------------------------

	private function custom_local_path($param, $filename)
	{
		$path = $this->params->get($param.'_custom_local_path');
		if (!$path)
		{
			return '';
		}

		if (strpos($path, '.') === false)
		{
			$path = str_replace('\\', '/', $path);
			$path = trim($path, '/');

			$url = $path.'/'.$filename;
			$url = $this->clean_url($url);

			if (!$url)
			{
				return '';
			}

			if (!$this->external_url)
			{
				return $this->main_url.$url;
			}
		}
	}

	// --------------------------------------------------

	private function scheme()
	{
		$scheme = $this->params->get('scheme', 'auto');
		$current = JUri::getInstance($this->webroot)->getScheme();

		if ($scheme == 'auto')
		{
			return $current;
		}

		return eorisis_jquery::valid_scheme($scheme, $current);
	}

	// --------------------------------------------------

	private function fqdn()
	{
		if ($this->params->get('domain_fqdn', 1) == 1)
		{
			return parse_url($this->webroot, PHP_URL_HOST);
		}

		return eorisis_jquery::extract_fqdn($this->params->get('domain_fqdn_custom'));
	}

	// --------------------------------------------------

	private function local_url()
	{
		$default = $this->webroot_relative;
		switch ($this->params->get('local_urls', 1))
		{
			case 1: return $default;
			case 2: return str_replace(array('https:','http:'), '', $this->webroot); // Scheme Relative
			case 3: return $this->webroot; // Absolute
			default: return $default;
		}
	}

	// --------------------------------------------------

	private function custom_cdn_url()
	{
		$default = '//'.$this->custom_cdn; // Scheme Relative
		switch ($this->params->get('custom_cdn_urls', 1))
		{
			case 1: return $default;
			case 2: return $this->scheme.':'.$default; // Absolute
			default: return $default;
		}
	}

	// --------------------------------------------------

	private function custom_cdn()
	{
		if (!$this->params->get('custom_cdn', 0))
		{
			return;
		}

		$url = trim($this->params->get('custom_cdn_url'), '/').'/';
		$url = eorisis_jquery::remove_scheme($url);

		if (eorisis_jquery::valid_url('http://'.$url)) // a check
		{
			return $url;
		}
	}

	// --------------------------------------------------

	private function main_url()
	{
		$this->custom_cdn = $this->custom_cdn();
		return $this->custom_cdn ? $this->custom_cdn_url() : $this->local_url();
	}

	// --------------------------------------------------

	private function set_urls()
	{
		$this->webroot = JUri::root();
		$this->webroot_relative = rtrim(JUri::root(true), '/').'/';
		$this->scheme = $this->scheme();
		$host = $this->scheme.'://'.$this->fqdn();
		$this->webroot = eorisis_jquery::valid_url(strtolower($host).$this->webroot_relative);
		$this->main_url = $this->main_url();
		$this->media_url_relative = 'media/eorisis-jquery/';
		$this->media_url = $this->main_url.$this->media_url_relative;

		// CDN URLs
		$this->cdn = (object)array(
			'jquery'			=> '//code.jquery.com/',					// jQuery
			'cloudflare'		=> '//cdnjs.cloudflare.com/ajax/libs/',		// CDNJS (CloudFlare)
			'google'			=> '//ajax.googleapis.com/ajax/libs/',		// Google Ajax API
			'bootstrap_maxcdn'	=> '//maxcdn.bootstrapcdn.com/bootstrap/',	// Bootstrap CDN by MaxCDN
			'bootstrap_netdna'	=> '//netdna.bootstrapcdn.com/bootstrap/'	// Bootstrap CDN by NetDNA
		);

		$this->url = (object)array(
			'jq_lib'		=> null,
			'migrate'		=> null,
			'noconflict'	=> null,
			'tb_js'			=> null,
			'tb_css'		=> null,
			'tb_theme_css'	=> null,
			'ui_js'			=> null,
			'ui_css'		=> null
		);
	}

	// --------------------------------------------------

	private function urls($type, $data)
	{
		$strings = eorisis_jquery::extract_textarea($data);
		if (!$strings)
		{
			return;
		}

		// --------------------------------------------------

		$urls = array();
		switch ($type)
		{
			case 'js':  $chars = 3; break;
			case 'css': $chars = 4; break;
		}

		foreach ($strings as $url)
		{
			$url = $this->async_defer($url, $type);
			$uri_tmp = (is_object($url) ? $url->url : $url);
			$uri_tmp = $this->clean_url($uri_tmp);

			if (!$uri_tmp)
			{
				continue;
			}

			if (substr($uri_tmp, -$chars) == '.'.(string)$type)
			{
				if ($this->external_url)
				{
					$urls[] = array('url' => $url, 'is_local' => false);
				}
				else
				{
					$u = ltrim($uri_tmp, '/');
					if ($this->check_file('/'.$u, false))
					{
						$final = $this->main_url.$u;
						if (is_object($url))
						{
							$url->url = $final;
						}
						else
						{
							$url = $final;
						}

						$urls[] = array('url' => $url, 'is_local' => true);
					}
				}
			}
		}

		return $urls;
	}

	// --------------------------------------------------

	private function clean_url($url)
	{
		$url = str_replace(array(
		"\r\n",
		"\r",
		"\n",
		"\t",
		"\s",
		" ",
		"	",
		"*",
		"\"",
		"'",
		".."
		), '', $url);

		if (eorisis_jquery::external_url($url))
		{
			$this->external_url = true;
		}
		else
		{
			$this->external_url = false;
			$url = preg_replace('/[^a-zA-Z0-9\/._=?&-]+/', '', $url);
		}

		$url = filter_var($url, FILTER_SANITIZE_URL);
		return eorisis_jquery::clean($url);
	}

	// --------------------------------------------------

	private function remove_media()
	{
		$remove = (object)array(
			'js' => array(),
			'css' => array()
		);

		foreach (array('jq_lib','ui','tb') as $elem)
		{
			$plugin_css = null;
			$files_js_more = null;
			$files_css_main = null;
			$files_css_more = null;

			switch ($elem)
			{
				case 'jq_lib': // jQuery Library, Migrate
				{
					$plugin_main = $this->url->jq_lib;

					$plugin_js = array(
						$plugin_main,
						$this->url->migrate,
						$this->url->noconflict
					);

					$files_js_main = array(
						'jquery.min.js',
						'jquery.js'
					);

					$files_js_more = array(
						'jquery-noconflict.js',
						'jquery-migrate.min.js',
						'jquery-migrate.js'
					);
				} break;

				case 'ui':
				{
					$plugin_js = $this->url->ui_js;
					$plugin_css = $this->url->ui_css;

					$plugin_main = array(
						$plugin_js,
						$plugin_css
					);

					$files_js_main = array(
						'jquery.ui.core.min.js',
						'jquery.ui.core.js'
					);

					$files_js_more = array(
						'jquery.ui.sortable.min.js',
						'jquery.ui.sortable.js'
					);
				} break;

				case 'tb':
				{
					$plugin_js = $this->url->tb_js;

					$plugin_css = array(
						$this->url->tb_css,
						$this->url->tb_theme_css
					);

					$plugin_main = array(
						$this->url->tb_js,
						$this->url->tb_css
					);

					$files_js_main = array(
						'bootstrap.min.js',
						'bootstrap.js'
					);

					$files_css_main = array(
						'bootstrap.min.css',
						'bootstrap.css'
					);

					$files_css_more = array(
						'bootstrap-responsive.min.css',
						'bootstrap-responsive.css',
						'bootstrap-extended.css',
						'bootstrap-rtl.css'
					);
				} break;
			}

			// --------------------------------------------------

			$continue = null;
			if (is_array($plugin_main))
			{
				foreach ($plugin_main as $url)
				{
					if ($url)
					{
						$continue = true;
						break;
					}
				}
			}
			elseif ($plugin_main)
			{
				$continue = true;
			}

			// --------------------------------------------------

			if ($continue)
			{
				$jui = $this->webroot_relative.'media/jui/';
				$load = true;

				// Auto Load
				if ($this->params->get($elem.'_state', 2) == 1)
				{
					if (!$this->head_isset($files_js_main, $jui))
					{
						$load = false;
						$remove->js  = eorisis_jquery::url_into_array($plugin_js, $remove->js);
						$remove->css = eorisis_jquery::url_into_array($plugin_css, $remove->css);
					}
				}

				if ($load)
				{
					$files_js = eorisis_jquery::merge_arr($files_js_main, $files_js_more);
					$remove->js = $this->add_jui('js', $files_js, $remove->js, $jui);

					if ($files_css_main)
					{
						$files_css = eorisis_jquery::merge_arr($files_css_main, $files_css_more);
						$remove->css = $this->add_jui('css', $files_css, $remove->css, $jui);
					}
				}
			}
		}

		return $remove;
	}

	// --------------------------------------------------

	private function remove_media_other($remove)
	{
		if ($this->area('remove', 1))
		{
			foreach (array('js','css') as $type)
			{
				if ($this->params->get('remove_'.$type))
				{
					$remove->$type = $this->remove_media_strings($type, $remove->$type, $this->textarea('remove_'.$type.'_urls'));
				}
			}
		}

		return $remove;
	}

	// --------------------------------------------------

	private function remove_media_strings($type, $arr, $strings)
	{
		if ($strings)
		{
			switch ($type)
			{
				case 'js':  $urls = $this->head['scripts']; break;
				case 'css': $urls = $this->head['styleSheets']; break;
			}

			foreach ($urls as $url => $attr)
			{
				if (eorisis_jquery::in_arr($url, $this->loaded_media, $this->sufix))
				{
					continue;
				}

				foreach ($strings as $str)
				{
					if (strpos($url, (string)$str) !== false)
					{
						$arr[] = $url;
					}
				}
			}
		}

		return $arr;
	}

	// --------------------------------------------------

	private function css_media_type()
	{
		if ($this->css_media_type === false)
		{
			$media = $this->params->get('css_media_type', 'null');
			if ($media == 'null')
			{
				$this->css_media_type = null;
			}
			elseif (in_array($media, array('all','screen')))
			{
				$this->css_media_type = $media;
			}
		}

		return $this->css_media_type;
	}

	// --------------------------------------------------

	private function load($type, $path, $is_local = null, $media = null)
	{
		if (is_array($path))
		{
			foreach ($path as $r)
			{
				$this->load($type, $r['url'], $r['is_local'], $media);
			}
		}
		else
		{
			switch ($type)
			{
				case 'js' :
				{
					if (is_object($path))
					{
						if ($this->media_options)
						{
							$this->doc->addScript($path->url, ($is_local ? $this->media_options : array()), array('defer' => $path->defer, 'async' => $path->async));
						}
						elseif ($this->media_version)
						{
							$this->doc->addScriptVersion($path->url, ($is_local ? $this->media_version : array()), 'text/javascript', $path->defer, $path->async);
						}
						else
						{
							$this->doc->addScript($path->url, 'text/javascript', $path->defer, $path->async);
						}
					}
					else
					{
						if ($this->media_options)
						{
							$this->doc->addScript($path, $this->media_options);
						}
						elseif ($this->media_version)
						{
							$this->doc->addScriptVersion($path, ($is_local ? $this->media_version : array()));
						}
						else
						{
							$this->doc->addScript($path);
						}
					}
				} break;

				case 'css':
				{
					if ($this->media_options)
					{
						$this->doc->addStyleSheet($path, ($is_local ? $this->media_options : array()), ($media ? array('media' => $media) : array()));
					}
					elseif ($this->media_version)
					{
						$this->doc->addStyleSheetVersion($path, ($is_local ? $this->media_version : array()), 'text/css', $media);
					}
					else
					{
						$this->doc->addStyleSheet($path, 'text/css', $media);
					}
				} break;
			}

			$this->loaded_media[] = $path;
		}
	}

	// --------------------------------------------------

	private function textarea($param)
	{
		$data = $this->params->get($param);
		if (!$data)
		{
			return;
		}

		$strings = eorisis_jquery::extract_textarea($data);
		if ($strings)
		{
			return array_unique($strings);
		}
	}

	// --------------------------------------------------

	private function add_jui($type, $files, $arr, $jui)
	{
		foreach ($files as $file)
		{
			$arr[] = $jui.$type.'/'.$file;
		}

		return $arr;
	}

	// --------------------------------------------------

	private function css_order($css, $is_local = null)
	{
		if ($this->css_first === null)
		{
			$this->css_first = ((int)$this->params->get('css_order', 1) == 1);
		}

		if ($this->css_first)
		{
			$this->load('css', $css, $is_local, $this->css_media_type());
		}
		elseif (is_array($css))
		{
			foreach ($css as $arr)
			{
				$this->css_last[] = $arr;
			}
		}
		else
		{
			$this->css_last[] = array('url' => $css, 'is_local' => $is_local);
		}
	}

	// --------------------------------------------------

	private function css_order_last()
	{
		if ($this->css_last)
		{
			$this->load('css', $this->css_last, false, $this->css_media_type());
		}
	}

	// --------------------------------------------------

	private function load_lang()
	{
		if (!$this->lang_loaded)
		{
			$e = 'plg_'.$this->app_type.'_'.$this->app_name;
			$l = JFactory::getLanguage();
			$l->load($e, JPATH_ADMINISTRATOR, 'en-GB', true);
			$l->load($e, JPATH_ADMINISTRATOR, null, true);
			$this->lang_loaded = true;
		}
	}

	// --------------------------------------------------

	private function async_defer($url, $type)
	{
		// Add Files
		if ($this->custom_js_async_defer) // On, Ignore
		{
			if ($this->task == 'custom')
			{
				if ($type == 'js')
				{
					$p = strpos($url, '|');
					if ($p !== false)
					{
						if ($this->custom_js_async_defer == 1) // On
						{
							$attr = array();
							$arr = explode('|', $url);
							if (!empty($arr[1])) { $attr[] = $arr[1]; }
							if (!empty($arr[2])) { $attr[] = $arr[2]; }

							$o = array(
								'url'	=> $arr[0],
								'async'	=> (in_array('async', $attr) ? 1 : 0),
								'defer'	=> (in_array('defer', $attr) ? 1 : 0)
							);
							$this->js_attr[] = (object)$o;

							return (object)$o;
						}
						else // Ignore: $this->custom_js_async_defer == 2
						{
							return substr($url, 0, $p);
						}
					}
				}
			}
			else
			{
				$this->js_attr = array();
			}
		}

		return $url;
	}

	// --------------------------------------------------

	private function async_defer_adv()
	{
		// Joomla 3.7 doesn't need this function
		if ($this->J37)
		{
			return;
		}

		// --------------------------------------------------

		if (!$this->js_attr or ($this->custom_js_async_defer != 1))
		{
			return;
		}

		foreach ($this->head['scripts'] as $url => $a)
		{
			foreach ($this->js_attr as $obj)
			{
				$uri_tmp = ltrim($obj->url, '/');

				if ($this->main_url.$uri_tmp == $url)
				{
					// With URL Versioning
					if ($this->sufix)
					{
						unset($this->head['scripts'][$url]);
					}
					// Without URL Versioning
					else
					{
						if (empty($a['async'])) { $this->head['scripts'][$url]['async'] = $obj->async; }
						if (empty($a['defer'])) { $this->head['scripts'][$url]['defer'] = $obj->defer; }
					}

					break;
				}

				if ($this->main_url == '/')
				{
					continue;
				}

				// Automatically remove a previously set url from the head
				// It will work only if the url in the Add Files have either async or defer set
				// Otherwise the user must manually set it in Remove Files
				// TODO:
				// Maybe remove this part so the user can manually remove the path using the Remove Files
				if ($this->webroot_relative.$uri_tmp == str_replace($this->sufix, '', $url))
				{
					unset($this->head['scripts'][$url]);
				}
			}
		}
	}

	// --------------------------------------------------

	private function check_file($path, $is_media = true)
	{
		if ($this->file_check)
		{
			$root = JPATH_SITE.'/';
			if ($is_media)
			{
				if (is_file($root.$this->media_url_relative.$path))
				{
					return $this->media_url.$path;
				}
			}
			elseif (is_file($root.$path))
			{
				return $path;
			}

			$this->abort = true;
			if ($this->is_admin and $this->user_id) // Administrator area, logged in user only
			{
				if (!$this->msg_set)
				{
					$this->load_lang();
					$this->app->enqueueMessage(JText::_('EO_FILESYSTEM_WARN'), 'warning');
					$this->msg_set = true;
				}
			}

			return;
		}

		if ($is_media)
		{
			return $this->media_url.$path;
		}

		return $path;
	}
}
