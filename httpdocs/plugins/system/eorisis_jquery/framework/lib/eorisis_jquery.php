<?php
defined('_JEXEC') or die;
// --------------------------------------------------------------------
// Software: eorisis jQuery
// Type: Joomla! System Plugin
// Author: eorisis https://eorisis.com
// Copyright (C) 2011-2017 eorisis. All Rights Reserved.
// License: GNU General Public License version 2; See /misc/licence.txt
// Terms of Service: https://eorisis.com/tos
// --------------------------------------------------------------------

class eorisis_jquery
{
	protected static $loaded = array(
		'lib' => null,
		'noconflict' => null
	);

	// --------------------------------------------------

	public static function set_loaded($key, $value)
	{
		if (in_array($key, array('lib','noconflict')))
		{
			$value = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
			self::$loaded[$key] = $value;
		}
	}

	// --------------------------------------------------

	public static function loaded($value = null)
	{
		if ($value)
		{
			return self::$loaded[$value];
		}

		return (object)self::$loaded;
	}

	// --------------------------------------------------

	public static function version_correction($version)
	{
		if (strlen($version) == 3)
		{
			$version .= '.0';
		}

		return $version;
	}

	// --------------------------------------------------

	private static function starts_with($haystack, $needle, $case_sensitive = true)
	{
		if ($case_sensitive)
		{
			return (strpos($haystack, $needle) === 0);
		}

		return (stripos($haystack, $needle) === 0);
	}

	// --------------------------------------------------

	public static function external_url($url)
	{
		foreach (array('//','https:','http:') as $str)
		{
			if (self::starts_with($url, $str, true))
			{
				return true;
			}
		}
	}

	// --------------------------------------------------

	public static function clean($str)
	{
		$str = trim($str);
		return filter_var($str, FILTER_SANITIZE_STRING);
	}

	// --------------------------------------------------

	public static function remove_scheme($str)
	{
		return str_replace(array('https://','http://'), '', $str);
	}

	// --------------------------------------------------

	public static function extract_fqdn($str)
	{
		$str = trim($str, '/');
		$str = filter_var($str, FILTER_SANITIZE_URL);
		$str = self::remove_scheme($str);

		$fqdn = strstr($str, '/', true);
		if ($fqdn)
		{
			$str = $fqdn;
		}

		return strtolower($str);
	}

	// --------------------------------------------------

	public static function valid_url($str)
	{
		if (filter_var($str, FILTER_VALIDATE_URL) !== false)
		{
			return self::clean($str);
		}

		return '';
	}

	// --------------------------------------------------

	public static function valid_scheme($scheme, $default)
	{
		if (in_array($scheme, array('http','https')))
		{
			return $scheme;
		}

		return $default;
	}

	// --------------------------------------------------

	public static function url_into_array($path, $arr)
	{
		if ($path)
		{
			if (is_array($path))
			{
				foreach ($path as $url)
				{
					return self::url_into_array($url, $arr);
				}
			}
			elseif (strlen($path) > 3) // example: s.js as minimum
			{
				$arr[] = $path;
			}
		}

		return $arr;
	}

	// --------------------------------------------------

	public static function extract_textarea($data)
	{
		$strings = array();
		$lines = preg_split('/\s+/', $data); // line and gaps on each

		foreach ($lines as $s)
		{
			$s = trim($s);
			if ($s)
			{
				$strings[] = $s;
			}
		}

		return $strings;
	}

	// --------------------------------------------------

	public static function merge_arr($arr_a, $arr_b)
	{
		return $arr_b ? array_merge($arr_a, $arr_b) : $arr_a;
	}

	// --------------------------------------------------

	public static function in_arr($url, $arr, $sufix)
	{
		if (!$arr)
		{
			return;
		}

		if (in_array($url, $arr))
		{
			return true;
		}

		$possible = array($url, $url.$sufix);
		foreach ($arr as $key => $val)
		{
			if (is_object($val) or is_array($val))
			{
				$val = (array)$val;
				if (isset($val['url']) and in_array($val['url'], $possible))
				{
					return true;
				}
			}
			elseif (in_array($val, $possible))
			{
				return true;
			}
		}
	}

	// --------------------------------------------------

	public static function auto_version($elem, $version_default, $version_latest, $j3)
	{
		if ($j3)
		{
			// https://eorisis.com/blog/javascript-frameworks
			switch ($elem)
			{
				// --------------------------------------------------
				// jQuery library (Since Joomla 3.0.0)

				case 'jq_lib':
				{
					// jQuery 1.12.4 : Joomla 3.6.0-beta1 to Joomla 3.7.2
					if (version_compare(JVERSION, '3.6.0-beta1', '>='))
					{
						return $version_default;
					}

					// jQuery 1.11.3 : Joomla 3.4.4-rc2 to 3.6.0-alpha
					if (version_compare(JVERSION, '3.4.4-rc2', '>='))
					{
						return '1.11.3';
					}

					// jQuery 1.11.2 : Joomla 3.4.0-beta1 to 3.4.4-rc
					if (version_compare(JVERSION, '3.4.0-beta1', '>='))
					{
						return '1.11.2';
					}

					// jQuery 1.11.1 : Joomla 3.3.1 to 3.4.0-alpha
					if (version_compare(JVERSION, '3.3.1', '>='))
					{
						return '1.11.1';
					}

					// jQuery 1.11.0 : Joomla 3.2.3 to 3.3.0
					if (version_compare(JVERSION, '3.2.3', '>='))
					{
						return '1.11.0';
					}

					// jQuery 1.10.2 : Joomla 3.2.0 to 3.2.2
					if (version_compare(JVERSION, '3.2.0', '>='))
					{
						return '1.10.2';
					}

					// jQuery 1.8.3 : Joomla 3.1.0 to 3.1.6
					if (version_compare(JVERSION, '3.1.0', '>='))
					{
						return '1.8.3';
					}

					// jQuery 1.8.1 : Joomla 3.0.0 to 3.0.4
					return '1.8.1';
				} break;

				// --------------------------------------------------
				// jQuery Migrate (Since Joomla 3.2.0)
				// Note: Joomla 3.0.0 to 3.1.6 has no Migrate in the core because it uses jQuery library up to version 1.8.3

				case 'migrate':
				{
					// jQuery Migrate 1.4.1 : Joomla 3.6.0-beta1 to 3.7.2
					if (version_compare(JVERSION, '3.6.0-beta1', '>='))
					{
						return $version_default;
					}

					// jQuery Migrate 1.2.1 : Joomla 3.2.0 to 3.6.0-alpha
					if (version_compare(JVERSION, '3.2.0', '>='))
					{
						return '1.2.1';
					}
				} break;

				// --------------------------------------------------
				// jQuery UI (Since Joomla 3.0.0)

				case 'ui':
				{
					// jQuery UI 1.9.2 : Joomla 3.2.0 to 3.7.2
					if (version_compare(JVERSION, '3.2.0', '>='))
					{
						return $version_default;
					}

					// jQuery UI 1.8.23 : Joomla 3.0.0 to 3.1.6
					return '1.8.23';
				} break;

				// --------------------------------------------------
				// Twitter Bootstrap (Since Joomla 3.0.0)

				case 'tb':
				{
					// Bootstrap 2.3.2 [Custom Modified] (Joomla 3.1.4 to 3.7.2)
					// Bootstrap 2.1.0 [Custom Modified] (Joomla 3.0.0 to 3.1.1)

					// Bootstrap 2.3.2 appears to work fine in all cases
					return $version_default; // Twitter Bootstrap 2.3.2
				} break;
			}
		}

		return $version_latest;
	}
}
