<?php
/*------------------------------------------------------------------------
# E-Logism Libraries
# ------------------------------------------------------------------------
# @author    E-Logism
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\storeimplementation;


use elogism\interfaces\Storer;
use Joomla\CMS\Table\Table;
/**
 * Store data using a Joomla Table object
 *
 * @author User
 */
class StorerTable implements Storer{
    /**
     *
     * @var Joomla\CMS\Table
     */
    private $storeMedium = null;
    
    function __construct(Table $storeMedium) {
        $this -> storeMedium = $storeMedium; 
    }
    
    public function store(array $data): array {
        $this -> storeMedium -> reset();
        $this -> storeMedium -> bind ( $data );
        $this -> storeMedium -> store();
        return $this -> getValues ( $this -> storeMedium );
        
    }
    
    protected function getValues( $storeMedium ) {
       return array_map( function( $val ) use ($storeMedium ){
            return $storeMedium -> { $val -> Field } ;
        }, $storeMedium -> getFields()  ); 
    }

}
