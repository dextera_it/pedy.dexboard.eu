<?php
namespace elogism\views;
/*------------------------------------------------------------------------
# com_elergon - E-Logism
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
 defined( '_JEXEC' ) or die( 'Restricted access' );
 use JViewHtml;
 use SplPriorityQueue;
 use Joomla\Registry\Registry;
 use JModel;

 /**
  * Base class for all views.
  * @author Christoforos J. Korifidis
  * 
  */
 class   View extends  JViewHtml
{   
    protected $state = null;
    protected $option = '';
    protected $componentName = '';
    protected $Itemid = 0;
   
    public function __construct(JModel $model, SplPriorityQueue $paths = null) 
    {
        parent::__construct($model, $paths);        
        $this -> state = $model -> getState();
        $this -> option = $this -> state -> get('option', '');
        $this -> componentName = $this -> state -> get('componentName', '');
        $this -> Itemid = $this -> state -> get('Itemid', '');
        $this -> layout = $this -> state -> get('layout', ''); // (overrides joolma's default layout which is 'default'.
        
    }   
    
    
     /**
     *  Messages already added to the state  are pushed to the application message queue. In order
     * for this to happen, exceptions must have been stored in the state in the form of 
     * key: messages, value: [ message => 'The message itself goes here', 'type' => 'warning' ] . 
     * Message type may be one of the message types supported by joomla: success, warning, danger, notice etc
     * @param Registry $state
     */
    protected function enqueueMessages( Registry $state)
    {
      
        foreach( $state -> get('messages', []) as $message):
            $this -> app -> enqueueMessage($message['message'], $message['type']);
            JLog::add($message['message'], JLog::INFO);
        endforeach;
        unset( $message );       
    }
    
//    public static function getRouteBySEFConfiguration($Itemid, $state)
//    {
//        return Route::_( 'index.php?option=' . $state -> get('option') .'&Itemid=' . $Itemid ). ( $state -> get('sef') === '0' ? '?' : '' );
//    }
}