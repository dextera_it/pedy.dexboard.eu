<?php
namespace elogism\views;
/**
 * @copyright (C) 2017, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
use JModelBase;
use SplPriorityQueue;
use JLoader;

 /**
  * Factory methods from views.
  * @author Christoforos J. Korifidis
  * 
  */


class ElJoomlaViewFactory
{
        /**
          * Factory method that return the proper JView object. If the vew we specify does not exists returns the default framework JView object.
          * @param JModelBase $model The JModel object to pass into the view constructor.
          * @param String $component The component to which  to the view name.  
          * @param String $viewName The name of the view we want.
         *  @param String $format The format of the view.
          * @param SplPriorityQueue $layoutPaths An array with the paths where we will search to find the view file. Defaults to the result of the getComponentLayoutPath. 
          * @return JView The JView object created.
          */    
        public static function getView(JModelBase $model, $component, $viewName, $format = 'html',   SplPriorityQueue  $layoutPaths = null)
        {
            if ( $layoutPaths === null ):
                $layoutPaths = self::getComponentLayoutPath( $component ) ;
           endif;
           $className =  '\components\\' .  $component . '\views\\'  .  $viewName   . '\\' .  ucfirst( $format  ) ;
           if ( JLoader::loadByPsr4($className) ):
                   return new $className( $model, $layoutPaths );
           else:
                    $className = 'elogism\views\\' . ucfirst( $format );
                    return new $className($model, $layoutPaths);
           endif;
        }      
        
        /**
         *  Returns default layouts paths for a component.
         * @param String $componentName The name of the component for which we want the layout path without the com_ prefix. e.g content not com_content.
         * @return SplPriorityQueue Paths were we will look for layout files.
         */
        public static function getComponentLayoutPath( $componentName )
        {
                $layoutPaths = new SplPriorityQueue();
                $layoutPaths -> insert( JPATH_BASE . '/components/' . $componentName . '/layouts', 'normal' );  
                $layoutPaths -> insert( JPATH_BASE . '/components/com_elgfrm/layouts', 'normal' ); 
                return $layoutPaths;
        }
}