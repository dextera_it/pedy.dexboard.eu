<?php
/*------------------------------------------------------------------------
# com_elgpedy - e-logism, dexteraconsulting  application
# ------------------------------------------------------------------------
# copyright Copyright (C) 2014 e-logism, dexteraconsulting. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr, http://dexteraconsulting.com
----------------------------------**/
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * Base class for all views.
  * @package libraries.e-logism.php.joomla;
  * @subpackage views
  * @author Christoforos J. Korifidis
  * 
  */
 class  ViewModelArray extends JViewBase
{   
    private static $excludes = ['componentName' => 0, 'option' => 0, 'controller' => 0, 'view' => 0, 'model' => 0
        , 'layout' => 0, 'format' => 0, 'Itemid' => 0, 'lang' => 0, 'ctype' => 0, 'limit' => 0, 'limit_start' => 0
        ,'filter_order' => 0, 'filter_order_Dir' => 0]; 
    public function render() 
    {
        return array_diff_key ($this -> model -> getState() ->toArray(), self::$excludes );
    }
}