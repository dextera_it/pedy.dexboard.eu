<?php
/**
 * @copyright (C) 2017, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 /**
  * Factory methods from views.
  * @package libraries.e-logism.php.joomla
  * @subpackage views
  * @author Christoforos J. Korifidis
  * 
  */


class ElgJoomlaViewFactory
{    

   /**
     * Factory method that return the proper JView object. If the vew we specify does not exists returns the default framework JView object.
     * @param JModel $model The JModel object to pass into the view constructor.
     * @param string $prefix The prefix to the view name (Usually the name of the component without the 'com_' ).  
     * @param string $viewName The name of the view we want. 
     * @param string $format The format of the view. Default is html.    
     * @param SplPriorityQueue $layoutPaths An array with the paths where we will search to find the view file. Defaults to the result of the getComponentLayoutPath. 
     * @return JView The JView object created.
     */
    
    public static function getView(JModel $model, $prefix, $viewName, $format = 'html',   SplPriorityQueue  $layoutPaths = null)
    {
        $viewPath = JPATH_COMPONENT . '/views/' ;      
        if( file_exists( $viewPath . strtolower($viewName) ) ):
                require_once $viewPath . strtolower($viewName) . '/view.' . $format . '.php';
                $view = $prefix . 'View' . $viewName;
        endif;
        if ( $layoutPaths === null ):
             $layoutPaths = self::getComponentLayoutPath( $prefix ) ;
        endif;
        return ( isset( $view  ) ?  new $view( $model, $layoutPaths ) : self::getDefaultView( $model, $layoutPaths )  );
    }
      
        /**
         * Factory for the default view
         * @param JModelBase $model The JModel object passed to the contructor.
         * @param JRegistry $foramt Current state.
         * @return  The default view.
         * @todo Take account the format
         */
        public static function getDefaultView( JModelBase $model, $prefix,  SplPriorityQueue $paths = null )
        {
                require_once JPATH_SITE . '/components/com_elgfrm/views/view.php';
                return new View( $model, ( $paths === null ? self::getComponentLayoutPath( $prefix ) : $paths ) );
        }    
    
        public static function getComponentLayoutPath( $componentName )
        {
                $layoutPaths = new SplPriorityQueue();
                $layoutPaths -> insert( JPATH_BASE . '/components/com_' . strtolower($componentName) . '/layouts', 'normal' );  
                $layoutPaths -> insert( JPATH_BASE . '/components/com_elgfrm/layouts', 'normal' ); 
                return $layoutPaths;
        }
}