<?php
/*------------------------------------------------------------------------
# E-Logism Joomla library
# ------------------------------------------------------------------------
# @author   E-Logism
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\views;
defined( '_JEXEC' ) or die( 'Restricted access' );
use JViewBase;
use Joomla\CMS\Response\JsonResponse;

 /**
  * Default json view class. I returns model's   getState() method result in json format.
  * 
  */
    
 class  JSON extends JViewBase
{      
    public function render() 
    {
        return new JsonResponse( $this -> model -> getState()  );
    }
}