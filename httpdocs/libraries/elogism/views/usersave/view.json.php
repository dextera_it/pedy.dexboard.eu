<?php
/**
 * e-logism's com_elgcomponent.
 * @copyright (C) 2016, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_BASE . '/components/com_elgfrm/views/viewjsondata.php';
 /**
  
  * @package e-logism.joomla.com_elgcomponents.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  */
 
 
class ELGEmporioViewCustomerSave extends ViewJSONData
{}