<?php
/*------------------------------------------------------------------------
# com_elgpedy - e-logism, dexteraconsulting  application
# ------------------------------------------------------------------------
# copyright Copyright (C) 2014 e-logism, dexteraconsulting. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr, http://dexteraconsulting.com
----------------------------------**/
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * Base class for all mail views.
  * @package libraries.e-logism.php.joomla;
  * @subpackage views
  * @author Christoforos J. Korifidis
  * 
  */
  
 
  
 class  ViewMail extends JViewBase
{   
    public function render() 
    {
        return $this ->getMailer( $this -> model -> getState()['data'] );
    }   
    
    /**
     * Prepares the mailer object
     * @param Array $mailData The data for the mailer object. mailFrom: senderEmalAddress, $mailFomName: senderEmailName, mailTo: subArray of recipients in the from [recipientAddress, recipientName],
     */
    public function getMailer($mailData)
    {
        $mailer = JFactory::getMailer();
        $mailer -> setSender([$mailData['mailFrom'], $mailData['mailFromName']]);
        $mailer -> addrAppend('To', $mailData['mailTo']);
        $mailer -> setSubject($mailData['subject']);
        $mailer -> setBody($mailData['body']);
        return $mailer ;
    }
	
}