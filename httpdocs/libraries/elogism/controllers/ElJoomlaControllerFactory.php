<?php
namespace elogism\controllers;
/*------------------------------------------------------------------------
# E Logism libraries.
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/

defined( '_JEXEC' ) or die( 'Restricted access' );
use Joomla\CMS\Application\CMSApplication;
use JLoader;
use Joomla\DI\Container;

 /**
  * Factory methods for controllers
  * @author E-Logism 
  */

class ElJoomlaControllerFactory
{    
     /**
     * Factory method that return the proper JController object. If the controller we request does not exists returns the default controller object
     * @param CMSApplication $app The JApplicationBase object needed by controller constructor.
     * @return ElController
     */
    public static function getController(CMSApplication $app )
    {
        $input = $app -> input;
        $controllerName = $input -> getCMD('controller');
        $className =  '\components\\' .  $input -> get('option') . '\controllers\\'  . ucfirst( $controllerName );
    
        if ( JLoader::loadByPsr4($className)  ) : 
            return new $className($input, $app);
        else:
            return new \elogism\controllers\ElController ($input, $app);
        endif;
    }   
}