<?php
/**
 * @copyright (c) 2016, e-logism.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 
 /**
  * Controller for sanitizing parameters from wenzhinxin bootstraptable.
  * @package e-logism.joomla.librareis;
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  */


class ElgControllerBSTableWenzhixin
{
    public static function normalizeInput(JInput $input)
    {
        $input -> set('search', $input -> getString('search', '') );
        $input -> set('filter_order_Dir', $input -> getCmd('order', 'asc') );
        $input -> set('filter_order', $input -> getCmd('sort', '') );
        $input -> set('limit', $input -> getCmd('limit', '') );
        $input -> set('limit_start', $input -> getCmd('offset', '') );
    }
}
