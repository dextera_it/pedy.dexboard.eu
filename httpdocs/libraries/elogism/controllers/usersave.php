<?php

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once JPATH_BASE . '/components/com_elgfrm/controllers/elgcontrollersave.php';

class ElgFrmControllerUserSave extends ElgControllerSave
{
    public function validatePassword($p1, $p2)
    {
        if( strlen($p1) < 6 || strlen($p1) < 6)
        {
            throw new Exception(JText::_('COM_ELG_ERROR_10101'), 10101 );
        }
        
        if( trim($p1) !== trim($p2) )
        {
            throw new Exception(JText::_('COM_ELG_ERROR_10102'), 10102 );
        }
    }
    
    public function execute()
    {
        if($this -> input -> getInt('id') === 0):
            $this -> validatePassword ( $this -> input -> getString('password1'), $this -> input -> getString('password2') );
        endif;
        parent::execute();
    }
}
