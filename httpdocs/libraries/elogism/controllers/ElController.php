<?php
namespace elogism\controllers;
/*------------------------------------------------------------------------
# E Logism libraries.
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
defined('_JEXEC') or die('Restricted access');

use elogism\models\ElJoomlaModelFactory;
use elogism\views\ElJoomlaViewFactory;
use Joomla\Registry\Registry;
use Joomla\Input\Input;
use JControllerBase;
use Joomla\CMS\Application\CMSApplication;
use JModelBase;
use JViewBase;


class ElController extends JControllerBase 
{

    
    /**
     * Default execute method for all request to show a view. Subclasses should override the response method and not this one.
     * 
     * @return mixed. If input ctype parameter is not return it echo the result of the view normally. Otherwise it just returns the result of the view.
     */
    public function execute()
    {
        $state = $this -> createState( $this -> app -> input );
        $state -> set( 'sef' , $this -> app -> get('sef') );
        try
        {
            $model = ElJoomlaModelFactory::getModel($state -> get('model'), $state -> get('option') );
            $model -> setState ( $state );
            $view = ElJoomlaViewFactory::getView( $model , $state -> get('option'), $state -> get('view'), $state -> get('format') );
            if ( $this -> getInput() -> get('ctype', '') === 'return' ):
                return  $view -> render();
            else:
                echo  $view -> render();
            endif;
        }
        catch ( Exception $ex )
        {
             $this -> getApplication() -> enqueueMessage( $ex ->getMessage(), 'warning' );
        }        
        
    }
    
    
    /**
     * Creates  with the basic data of a joomla component
     * @param Input $input Data send by the user.
     * @return Registry The state that created
     */
    protected function createState(Input $input = null) : Registry
    {
        return new Registry( ( $input === null ? $this -> input -> getArray() : $input -> getArray() ) );
    }
    
    /**
     * Makes the work of the action execution. By default it shows the view. 
     * @param Registry $state
     * @return String The response
     */
//     protected function response(JModelBase $model, JViewBase $view, Registry $state = null )
//    {
//        try 
//        {
//            $model -> setState($state);
//        } catch (Exception $ex) {
//            $this -> getApplication() -> enqueueMessage( $ex ->getMessage(), 'warning' );
//        }
//        finally {
//            return $view -> render();
//        }          
//    }
       
    /**
     * Merge the data of state to input key by key. Overwrites same keys.
     * @param Input $input The input into which the data will be merged.
     * @param JRegistry $state The state to merge
     
     */
    public static function mergeStateToInput(Input $input, JRegistry $state )
    {
        foreach ( $state -> getIterator() as $key => $value ):
            $input -> set($key, $value);
        endforeach;
        unset($key);
        unset($value);
    }
    
    /**
     * Custom input filter for accepting html tags.
     * @return JFilterInput
     */
    public static function getHTMLFilter() {
        return  JFilterInput::getInstance(
            [
                'img','p','a','u','i','b','strong','span','div','ul','li','ol','h1','h2','h3','h4','h5',
                'table','tr','td','th','tbody','theader','tfooter','br'
            ],
            [
                'src','width','height','alt','style','href','rel','target','align','valign','border','cellpading',
                'cellspacing','title','id','class'
            ]
        );
    }  
}