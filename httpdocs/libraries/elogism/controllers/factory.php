<?php

/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 /**
  * Factory methods for controllers
  * @package libraries.e-logism.php.joomla
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */

class ElgJoomlaControllerFactory
{    
     /**
     * Factory method that return the proper JController object. If the controller we request does not exists returns the default controller object
     * @param JApplicatonCms $app The JApplicationBase object needed by controller constructor.
     * @param JInput $input The JInput object needed by controller constructor.
     * @param array $paths An array with the paths where we will search to find the controller file.
     * @return JControllerBase The JControllerBase object created.
     */
    public static function getController(JApplicationCms $app, JInput $input, array $paths = null)
    {
        $controller = null;
        $controllerName = $input -> get('controller');
        if( $paths === null ):
            $paths = array( JPATH_BASE . '/components/' . strtolower($input -> get('option')) . '/controllers/');
        endif;
        foreach($paths as $path):
            if( file_exists( $path  . strtolower( $controllerName ) . '.php' ) ):
                require_once $path  . strtolower(  $controllerName  ) . '.php';
                $controller =  $input -> get('componentName')  . 'Controller' .  $controllerName ;
                break;
            endif;
        endforeach;
        unset( $path );
        return ( $controller === null ? self::getDefaultController( $app, $input  ) : new $controller( $input, $app ) );
    }
       
    /**
     * Factory method for the default controller php/joomla/e-logism/controllers/default.php.
     * @param JApplicationBase $app     
     * @param JInput $input
     * @return JController The default controller.
     */
    public static function getDefaultController( JApplicationCms $app, JInput $input  )
    { 
        require_once JPATH_SITE . '/components/com_elgfrm/controllers/elgcontroller.php';
        return new ElgController($input, $app);    
    }   
}