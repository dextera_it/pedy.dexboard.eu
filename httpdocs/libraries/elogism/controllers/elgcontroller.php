<?php


/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3. 
 */
defined('_JEXEC') or die('Restricted access');
require_once JPATH_SITE . '/components/com_elgfrm/models/factory.php';
require_once JPATH_SITE . '/components/com_elgfrm/views/factory.php';
require_once JPATH_SITE . '/components/com_elgfrm/controllers/factory.php';


/**
 * Father controler of all controllers.
 * @package libraries.e-logism.php.joomla;
 * @subpackage controllers
 * @author Christoforos J. Korifidis
 */
class ElgController extends JControllerBase 
{
     public function __construct(\JInput $input = null, JApplicationCms $app = null) 
    {
        $lang = JFactory::getLanguage();
        $lang -> load( 'com_elgfrm', JPATH_SITE . '/components/com_elgfrm', $lang ->getTag() , true);
//        $formatter = $input -> getCmd('dft', '');   // coment out for better implementatin of data format. Actual the data format must be in view responsibility.
//        if ( $formatter !== ''):
//            require_once JPATH_BASE . "/components/com_elgfrm/controllers/frm" . strtolower($formatter) . ".php";
//            $formatterClass = "ElgController$formatter";
//            $formatterClass::normalizeInput( $input );
//        endif;     
        parent::__construct($input, $app);
    }
    
    /**
     * Default execute method for all request to show a view. Subclasess should override the response method instead of execute method.  
     */
    public function execute()
    {
        if ( $this -> getInput() -> get('ctype', '') === 'return' ):
                return    $this -> response( 
                                $this -> createState( $this ->getInput() ) 
                            );
        else:
                echo  $this -> response( 
                                $this -> createState( $this ->getInput() ) 
                            );
        endif;
    }
    
    /**
     * Creates a default state by the data send by the user
     * @param JInput $input Data send by the user.
     * @return \JRegistry The state that created
     */
    protected function createState(JInput $input = null)
    {
        return new JRegistry( ( $input === null ? $this -> input -> getArray() : $input -> getArray() ) );
    }
    
   
    /**
     * Makes the work of the action execution. By default it shows the view. 
     * @param Registry $state
     * @return String The response
     */
     protected function response(Joomla\Registry\Registry $state = null)
    {
        $model = ElgJoomlaModelFactory::getModel($state -> get('model'), $state -> get('componentName') );
        try 
        {
            $model -> setState($state);
        } catch (Exception $ex) {
                $this -> getApplication() -> enqueueMessage($ex ->getMessage(), 'warning');
        }
        finally {
                return ElgJoomlaViewFactory::getView( $model , $state -> get('componentName'), $state -> get('view'), $state -> get('format') ) -> render();
        }
          
    }
       
    /**
     * Merge the data of state to input key by key. Overwirites same keys.
     * @param JRegistry $state The state to merge
     * @param JInput $input The input into which the data will be merged.
     */
    public static function mergeStateToInput(JInput $input, JRegistry $state )
    {
        foreach ( $state -> getIterator() as $key => $value ):
            $input -> set($key, $value);
        endforeach;
        unset($key);
        unset($value);
    }
    
    public static function getHTMLFilter() {
        return  JFilterInput::getInstance(
            [
                'img','p','a','u','i','b','strong','span','div','ul','li','ol','h1','h2','h3','h4','h5',
                'table','tr','td','th','tbody','theader','tfooter','br'
            ],
            [
                'src','width','height','alt','style','href','rel','target','align','valign','border','cellpading',
                'cellspacing','title','id','class'
            ]
        );
    }
}