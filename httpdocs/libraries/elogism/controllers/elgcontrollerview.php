<?php

/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3. 
 */
defined('_JEXEC') or die('Restricted access');
require_once JPATH_BASE . '/components/com_elgfrm/controllers/elgcontroller.php';

/**
 * Strategy controller that execute the execute method of the right subclass.
 * @package libraries.e-logism.php.joomla;
 * @subpackage controllers
 * @author Christoforos J. Korifidis
 * 
 */
class ElgControllerView extends ElgController 
{   
    protected $view = null;
 
    protected function setView($view = null) {
        if ( $this -> view == null):
            $this -> view = $this -> createView($this -> getInput() );
        else:
            $this -> view  = $view;
        endif;
    }
    
    protected function getView()
    {
        if($this -> view === null):
            $this -> setView();
        endif;
        return $this -> view;
    }
    
    private function createView($input) {
        $state = new JRegistry( $input -> getArray() );
        return ElgJoomlaViewFactory::getView(
                $this -> getModel( $state ), 
                $input -> get('format'), 
                $input -> get('componentName'), 
                $input -> get('view') );
    }
}
