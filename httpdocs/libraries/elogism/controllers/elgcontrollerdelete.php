<?php

/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3. 
 */
defined('_JEXEC') or die('Restricted access');
require_once JPATH_BASE . '/components/com_elgfrm/controllers/elgcontroller.php';

/**
 * Strategy controller that execute the execute method of the right subclass.
 * @package libraries.e-logism.php.joomla;
 * @subpackage controllers
 * @author Christoforos J. Korifidis
 * 
 */
class ElgControllerDelete extends ElgController 
{  
    public function execute() 
    {
        $input = $this -> getInput();        
        $model =  ElgJoomlaModelFactory::getModel( $input -> get('model'), $input -> get('componentName') );
        try
        {
            $model -> setState ( new JRegistry( $input -> getArray() ) );
        }    
        catch(Exception $e) {
            $this -> getApplication() -> enqueueMessage($e ->getMessage());
        }
        $state = $model -> getState();
        $this -> enqueueMessages($state);      
        return $this ->  responseAction( $input, $state, ElgJoomlaViewFactory::getView($model, $state -> get('format'), $state -> get('componentName'), $state -> get('view') ) ) ;              
    }
    
}
