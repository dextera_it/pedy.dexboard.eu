<?php

/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3. 
 */
defined('_JEXEC') or die('Restricted access');
require_once JPATH_BASE . '/components/com_elgfrm/controllers/elgcontroller.php';

/**
 * Strategy controller that execute the execute method of the right subclass.
 * @package libraries.e-logism.php.joomla;
 * @subpackage controllers
 * @author Christoforos J. Korifidis
 * 
 */
class ElgControllerSave extends ElgController 
{  
    private $model = null;
    public function __construct(\JInput $input = null, \JApplicationCms $app = null) 
    {
        parent::__construct($input, $app);
    }
//    public function execute() 
//    {
//        $this -> model ->setState( $this -> state );
//        $newState = $this -> model -> getState();
//        $this ->enqueueMessages( $newState );
//        $newState -> set('view', 'viewjsondata');
//        ElgJoomlaViewFactory::getView($this -> model, $state );
//        try {
//            $this -> doAction( $this -> getInput() );
//            $state = $this -> model -> getState();
//            $this -> enqueueMessages($state); 
//        } catch (Exception $ex) {
//            $this -> getApplication() -> enqueueMessage( ' (' . $ex -> getCode() . ') ' . $ex -> getMessage(), 'warning' );
//            $state = new JRegistry ( $this -> getInput() -> getArray() );
//            if ( $this -> model === null ):
//                $this -> model = ElgJoomlaModelFactory::getDefaultModel( $state );
//            endif;
//        }
//        finally {
//                $input = $this -> getInput();
//                $input -> set('view', 'viewjsondata');
//                ;
//                $this -> responseAction ($input
//                        , $state
//                        , ElgJoomlaViewFactory::getView($this -> model, $state )
//                        );
//        }
//        
//    }    
//    
    
    protected function responseAction(Joomla\Registry\Registry $state = null) {
        $model = ElgJoomlaModelFactory::getModel( $state -> get('model'), $state -> get('componentName') ); 
        $model -> setState( $state );
        echo ElgJoomlaViewFactory::getView( 
                $model
                , $state 
                ) -> render();
    }
    
    
    
    /**
     * Instantiates the model and executes the requested action. By default action is model's setState method.
     * @param JInput $input The Variables send to server.
     */
    protected function doAction(JInput $input)
    {
        
        $this -> model =  ElgJoomlaModelFactory::getModel( $input -> getCmd('model'), $input -> getCmd('componentName') );
        $this -> model -> setState ( $this -> createState( $input ) );
    }
    
    
}