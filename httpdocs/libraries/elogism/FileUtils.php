<?php
namespace elogism;
defined('_JEXEC') or die('Restricted access');


class FileUtils 
{

  
 
    /**
     * Returns decode json file data from a json file.
     * @param String $filePath The file to open e.g. 2015/2015_01_01.json
     * @param Boolean $assoc if true data will be returned as an associative array otherwise they will be returned as stdClass.
     * @return mixed An array or stdClass depend on the $assoc argument.
     */
    public function getFileJsonDecodedData( $filePath, $assoc = false )
    {
        if ( is_file ( $filePath ) ):
            return json_decode( file_get_contents( $filePath ), $assoc );
        endif;
    }
        
       
    public function storeToKeyFile( $keyFile, $key, $value)
    {
        if ( file_exists ( $keyFile ) ):
            $data = $this -> getFileJsonDecodedData( $keyFile );
        else:
            $data = new stdClass();
        endif;
        $data -> $key = $value;
        $this -> storeFile ( $keyFile, json_encode( $data ), 'w', true  );
    }
    
     /**
     * Write data to file.
     * @param String $path The full path to the file.
     * @param String $data The data to be stored in the file.
     * @param String $mode The file write mode file open default is w (write) Premitted values are those of fopen function.
     * @param boolean $makePath If true, construct the directory tree if does not exists.
     * @param int $zipMode  0: The file will not be zipped. 1: The file will be stored zipped. 3: The file will be stored as zipped archive and as a regular file
     */
    public function storeFile($path, $data, $mode = 'w', $makePath = true, $zipMode = 0) 
    {
        if ( $makePath ):
            $this -> makePath( dirname( $path ) );
        endif;
        $handle = fopen($path, $mode);
        fwrite($handle, $data);	
        fclose($handle);		
        if ( $zipMode === 1 || $zipMode === 2 ):
            $this -> zipFile( $path, "$path.zip", basename( $path ) );
        endif;
        if ( $zipMode === 1 ):
            unlink ( $path );
        endif;
    }
    
    public static function storeDownloadedFile($requestFieldData, $storeDir, $storeFileName = null )
    {   
        $fileName = JFile::makeSafe( JFile::makeSafe($requestFieldData['name']) );
        $src = $requestFieldData['tmp_name'];
        $filePath = ($storeFileName === null ? "$storeDir/$fileName": "$storeDir/$storeFileName");
        if ( file_exists($filePath) ):
            unlink($filePath);
        endif;
        JFile::upload($src, $filePath);        
        return $filePath;
    }
    
    
    
    
    /**
     * Contrucats a directory tree is does not exists
     * @param String $path The directory path
     */
    public function makePath($path) 
    {
        if ( !file_exists ( $path ) ):
            mkdir($path, null, true);
        endif;
    }
    
    /**
     * Zips the contents of a directory.
     * @param String $sourcePath  Directory's path of which the contents will be ziped.
     * @param String $zipFile The name of the zip file that will be created.
     */
    public  function zipFolder($sourcePath, $zipFile) {
        $zipArchive = new ZipArchive();
        if ( !$zipArchive -> open( $zipFile, ZIPARCHIVE::OVERWRITE ) ):
            JLog::add( "Failed to create archive\n" . $zipFile, JLog::ERROR, 'zip' );
            return;
        endif;
        $all = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $sourcePath ) );
        foreach ( $all as $f ) :
            $fileName = $f -> getFileName();
            if ( $fileName != "." && $fileName != ".." ):
                $addPath = str_replace( JPATH_SITE . '/', '', $f->getPathName() );
                $res = $zipArchive -> addFile( $addPath );
                if ( !$res ) :
                    JLog::add( "Unable to add file: $f", JLog::ERROR, 'zip' );
                endif;
            endif;
        endforeach;
        unset( $f );
        if ( !$zipArchive -> status == ZIPARCHIVE::ER_OK ):
            JLog::add( "Failed to write local files to zip\n" . $zipFile );
        endif;
        $res = $zipArchive -> close();
    }
    
    /**
     * Zips a single file.
     * @param String $sourcePath  File path to be ziped.
     * @param String $zipFile The name of the zip file to be created.
     * @param Sting same as local name of zipArrchive::addFile $localName parameter
     */
    public  function zipFile( $sourcePath, $zipFile, $localName = null ) {
        if ( !is_File ( $sourcePath ) ):
            JLog::add("Not vile file for zip: $sourcePath", JLog::ERROR, 'zip' );
            return;
        endif;
        $zipArchive = new ZipArchive();
        if ( !$zipArchive -> open( $zipFile,  ZipArchive::CREATE | ZipArchive::OVERWRITE ) ):
            JLog::add( "Failed to create archive\n" . $zipFile, JLog::ERROR, 'zip' );
            return;
        endif;
        $res = $zipArchive -> addFile( $sourcePath, $localName );
        if ( !$res ):
            JLog::add( "Unable to add file: $sourcePath", JLog::ERROR, 'zip' );
        else:
            if ( !$zipArchive -> status == ZIPARCHIVE::ER_OK ):
                JLog::add( "Failed to write local files to zip\n" . $zipFile );
            endif;
        endif;
        $zipArchive -> close();
    }
    
    
     /**
     * Compares 2 files for equality
     * @param String $a The First file path
     * @param String $b The Second file path
     *
     * */
    public static function areFileEqual($a, $b) {
        if (filesize($a) !== filesize($b)) :
            return false;
        endif;

        $ah = fopen($a, 'rb');
        $bh = fopen($b, 'rb');

        $result = true;
        while (!feof($ah)):
            if (fread($ah, 8192) != fread($bh, 8192)):
                $result = false;
                break;
            endif;
        endwhile;
        fclose($ah);
        fclose($bh);
        return $result;
    }
    
    
    /**
     * This function reads a file row by row and returns an array that its rows corresponds
     * in each row in the file. It is suitable for reding property files.
     * @param String $filePath the path of the file to read
     * @param boolean $rmBlanks wether or not to remove blank rows. Defaults to true.
     * @param boolean $addNewLine whether to add a new line character at the end of array. Defaults to true.
     * @param boolean $rmComments wether or not to return rows that begins with "#" or ";". Defaults to true.
     */
    function getFileRows($filePath, $rmBlanks = true, $adNewLine = true, $rmComments = true) {
        if (!JFile::exists($filePath)) {
            JError::raiseWarning('500', JText::_('FILE_NOT_FOUND') . " : \"" .  JFile::getName($filePath) . "\"");
            return false;

        } else {
            /** FILE_SKIP_EMPTY_LINES not working (php 5.3.1) **/
            if ($rmBlanks && ! $adNewLine){
                $content = file($filePath, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            }elseif($rmBlanks && $adNewLine){
                $content = file($filePath,FILE_SKIP_EMPTY_LINES);
            }elseif(!$rmBlanks && !$adNewLine){
                $content = file($filePath,FILE_IGNORE_NEW_LINES);
            }else{
                $content = file($filePath);
            }

            if ($rmComments){
                $data = array();
                $recs = count($content);
                for ($row = 0; $row < $recs; $row++) {
                    if (substr($content[$row], 0, 1) != "#" and substr($content[$row], 0, 1) != ";" ) {
                        $data[] = $content[$row];
                    }
                }
                return $data;
            }else{
                return $content;
            }
        }
    }
    
        
    /**
     * Deletes a directory and its contents
     * @param string $dirPath The directory to delete.
     * @throws InvalidArgumentException
     */
     public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            JLog::add("$dirPath must be a directory", JLog::ERROR, 'importer');
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    /**
     * Resizes the dimensions of an Image
     * @param String originalFile Path to the original fiel to resize. 
     * @param String destinationFile Full image path where the new resized image will be stored.
     * @param Int width The new width of the image.
     * @param Int height The new height of the image.
     */
    public static function resizeImage($originalFile, $destinationFile, $width, $height) {
      
        $image = new JImage($originalFile);
        
        if($image->getWidth() > 0)
        {
            $properties = JImage::getImageFileProperties($originalFile);
            $resizedImage = $image->resize($width, $height, true);
            $resizedImage->toFile($destinationFile, $properties->type);
        }
    }  
    
    
    /**
     * Returns values from *.ini file for the requested keys in the from of key => value. On error it returns an empty array.
     * @param array $keys The Requested keys for which we want the values
     * @param string $iniFile The iniFile to look up for values.
     * @return array Found values
     */ 
    public static function getIniFileItems(array $keys, string $iniFile) : array 
    {
        try {
            return array_filter( 
                            parse_ini_file( $iniFile)
                            , function ( $key ) use ( $keys ) {
                               return in_array ( $key, $keys ); 
                            }
                            ,ARRAY_FILTER_USE_KEY
                );
        }
        catch ( Exception $ex ) {
            return [];
        }
    }
}
