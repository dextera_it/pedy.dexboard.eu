<?php
/*------------------------------------------------------------------------
# com_elergon - e-logism
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\datatemplates; 
use JDatabaseDriver;
use JDatabaseQuery;
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class FrmBsTable 
{
        public static function getData(JDatabaseDriver $db, JDatabaseQuery $query, $page, $pageSize) 
        {
            $db -> setQuery($query);
            $res = [ 'rows' => $db -> setQuery($query, $page, $pageSize) -> loadAssocList()];
            $query -> clear('select') -> clear('order') -> clear('group') -> select(' count(*) ');
            $db -> setQuery($query);
            $res ['total'] = $db -> loadResult();
            return $res;
        }
}