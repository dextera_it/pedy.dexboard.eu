<?php

namespace elogism\interfaces;
/**
 * @copyright (C) 2018, E-Logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */

/**
 * Interface for all classes that stores data
 
 */
Interface Storer {
    /**
     * Store data to the store.
     * @param array The data to store.
     * @return array Returns the data stored.
     */
    public function store(array $data): array;
}
