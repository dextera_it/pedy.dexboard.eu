<?php
/*------------------------------------------------------------------------
# E-Logism library
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\inputtemplates;
defined( '_JEXEC' ) or die( 'Restricted access' );
use Joomla\Input\Input;
 
 /**
  * Controller for sanitizing parameters from wenzhinxin bootstraptable.
  * @author E-Logism.
  */


class InpBSTable
{
    public static function normalizeInput(Input $input)
    {
        $input -> set('search', $input -> getString('search', '') );
        $input -> set('filter_order_Dir', $input -> getCmd('order', 'asc') );
        $input -> set('filter_order', $input -> getCmd('sort', '') );
        $input -> set('limit', $input -> getCmd('limit', 50) );
        $input -> set('limit_start', $input -> getCmd('offset', 0) );
    }
}
