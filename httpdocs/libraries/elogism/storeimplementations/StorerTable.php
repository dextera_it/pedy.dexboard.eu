<?php
namespace elogism\storeimplementation;
/**
 * @copyright (C) 2018, E-Logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */

use elogism\interfaces\Storer;
use Joomla\CMS\Table\Table;
/**
 * Store data using a Joomla Table object
 *
 * @author User
 */
class StorerTable implements Storer{
    /**
     *
     * @var Joomla\CMS\Table
     */
    private $storeMedium = null;
    
    function __construct(Table $storeMedium) {
        $this -> storeMedium = $storeMedium; 
    }
    
    public function store(array $data): array {
        $this -> storeMedium -> reset();
        $this -> storeMedium -> bind ( $data );
        $this -> storeMedium -> store();
        return $this -> getValues ( $this -> storeMedium );
        
    }
    
    protected function getVaues( $storeMedium ) {
       return array_map( function( $val ) use ($storeMedium ){
           return $storeMedium -> $val;
        }, $storeMedium -> getFields() ); 
    }

}
