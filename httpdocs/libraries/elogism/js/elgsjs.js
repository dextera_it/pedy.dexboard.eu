var elgsJS = {
   
    /**
     * Show a busy indicator for a given section of page
     * @param {String} sectionSelector The css selector of the page's section we want show as busy. This section will become hidden.
     * @param {String} busySelector The css selector of the section of the page that hides thw bysy indicator animation. This animation will becaome visilble.
     */
    showAsBusy: function (sectionSelector, busySelector) {
        var sSel = typeof sectionSelector !== 'undefined' ? sectionSelector : '.elg';
        var bSel = typeof busySelector !== 'undefined' ? busySelector : '.loader-128';
        document.querySelector(sSel).style.display = 'none';
        document.querySelector(bSel).style.display = 'block';
    },
    
    /**
     * Hides a busy indicator for a given section of page and makes the contect of this sections visibnle to the user.
     * @param {String} sectionSelector The css selector of the page's section we want to reveal. This section was hidden and was replaced by the busy indicator.
     * @param {String} busySelector The css selector of the busy indicator animation. This animation will dissapear and it will bew replaced by actual content.
     */
    showAsReady: function (sectionSelector, busySelector) {
        var sSel = typeof sectionSelector !== 'undefined' ? sectionSelector : '.elg';
        var bSel = typeof busySelector !== 'undefined' ? busySelector : '.loader-128';
        document.querySelector(bSel).style.display = 'none';
        document.querySelector(sSel).style.display = 'block';
    },
    
    /**
     * Checks to see if the response from server contians wanrings or error messages
     * @param XMLHTTPRequest response
     * @returns true if no warnigs or errors found false if everything wend ok.
     */
    hasErrors: function(response) {
        if (response.messages !== null ) {
            var keys = Object.keys(response.messages);
            for(var i = keys.length-1; i >=0; i --) 
                if( keys[i] !== 'success') return true;
        }
        return false;
    },
    
    /**
     * Contructs an html message string from a joomla json response.
     * @param {JSON} response The xhr response.
     * @param {string} messageType It is case sensitive. Currently allowed : HTMLBootstrap.
     * @param {object} options Options for the message type function. This options object will be passed on any custom function asas the second argument. Custom functions must adhere to the interface (response, options). 
     * @returns {String} The constacted html string.
     */
    renderAppMessages: function (response, messageType, options) {
        var f = 'renderAppMessage' + messageType;
        try {
            return elgsJS[f](response, options);
        }
        catch (e) {
            alert(e.message);
        }
    },
    /**
     * Renders a message as a bootstrap alert.
     * @param {JSON} response The remote json response that contains the messages property.
     * @param {object} options An object containing the messageArea node and the timeout id any.
     */
    renderAppMessageHTMLBootstrap: function (response, options) {
        elgsJS.clearChildren(options.messageArea);
        var msg = '';
        if (response.messages !== null )
            Object.keys(response.messages).forEach(function (val) {
                msg += '<div class="alert alert-' + val + '" ><a href="#" class="close" data-dismiss="alert">&times;</a>' + response.messages[val].join('<br />') + '</div>';
            });
        options.messageArea.innerHTML = msg;
        if (options.timeout) {
            setTimeout(function () {
               elgsJS.clearChildren(options.messageArea)
            }, options.timeout);
        }
    },
    
    /**
     * Returns a message as an html string.
     * @param {JSON} response The remote json response that contains the messages property.     
     */
    renderAppMessageHTMLReturned: function (response) {
       
        var msg = '';
        if (response.messages !== null )
            Object.keys(response.messages).forEach(function (val) {
                msg += '<p class="' + val + '" >' + response.messages[val].join('<br />') + '</p>';
            });
        return msg;
    },
    
    /**
     * Returns the max value of an array
     * @param {Array} arr. The array of which we want it's value.
     * @returns {Array Item}. The max array item.
     */
    arrayMax: function (arr) {
        return arr.reduce(function (p, v) {
            return (p > v ? p : v);
        });
    },
    
    arrayUnique: function (value, index, self) { 
        return self.indexOf(value) === index;
    },

    
    /**
     * Clear a dom nod of it's children
     * @param {domNode} parent The dom node of which we want to remove it's children.
     */
    clearChildren: function (parent) {
        while (parent.firstChild !== null)
            parent.removeChild(parent.firstChild);
    },
    
    /**
     * clears values from a form elements
     * @param domNode form The form element
     * @param Array exceptions The ids of the elements that we don't want to be clerared from this procedure.
    */
    clearForm: function(form, exceptions)
    {
        var formData = new FormData(form);
        var keys = formData.keys();
        
        for( var key in keys) 
            if( exceptions.indexOf(key) === -1)
                document.getElementById(key).value = ''; 
    },
    
    /**
     * Convert collection to array
     * @param {collection} The collection to convert to array
     */
    collection2Array: function (collection) {
        for (var a = [], i = collection.length; i; )
            a[--i] = collection[i];
        return a;
    },

       
    /**
     * Returns a value from the Query String
     * @param {String} The key you are looking for
     * @returns {String} The Value or empty String if the value not found
     */
    getQSValue: function(key) {
        var val = elgsJS.getUrlVars()[key];
       return (typeof val === 'undefined' ? '' : val);
    },
    
    
    createFragmentFromString: function(strHTML) {
        return document.createRange().createContextualFragment(strHTML);
    },
    
    roundNumber: function(num) {
        return +(Math.round(num + "e+2")  + "e-2");
    },
    
    /**
     * Retunrs a value parsed as float or the defValue. If defValue is null return false
     * @param String value The value to parse as float.
     * @param Mixed defVal Teh defautl that will be returned if the clearedValue could not be parsed as float. if defautl value is null returns false.
     * @returns Mixed The clearedValue parsed as float. If cleard value is not numeric it return the defauilt value. if defauilt value is null it return false.
     */
    getValueAsNum: function(value, defVal) {
        var numVal = (value + '').replace(/ /g, '');
        if( numVal === '') return(defVal === null ? false: defVal);
        var numVal = parseFloat(numVal);
        if(isNaN(numVal))
            return (defVal === null ? false: defVal);
        else
            return numVal;
    },
    
    /**
     * Removes all listenres from element by cloning the passed elemenet to a new one without listeners.
     * @param HTMLDOMElement elm
     * @returns HTMLDOMELement The new cloned element 
     */
    removeListeners: function(elm){
        var new_element = elm.cloneNode(true);
        elm.parentNode.replaceChild(new_element, elm);
        return new_element;
    }
    
};
