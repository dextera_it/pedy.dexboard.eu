<?php
namespace elogism;
/*------------------------------------------------------------------------
# E Logism libraries.
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/

class PHPUtils {

   


    public static function array_group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    public static function getObjectListGrouped($objectList, $key) {
        $lType = "";
        $result = array();
        foreach ($objectList as $item) {
            if ($lType != $item->$key) {
                $lType = $item->$key;
                $result[$lType] = array();
            }
            $result[$lType][] = $item;
        }
        unset($item);
        return $result;
    }

    
    public static function getMonths1stDate($month = null, $year = null) {
        $year = ($year == null) ? date('Y') : $year;
        $month = ($month == null) ? date('m') : $month;
        return mktime(0, 0, 0, $month, 1, $year);
    }

    public static function getMonthsLasttDate($month = null, $year = null) {
        $year = ($year == null) ? date('Y') : $year;
        $month = ($month == null) ? date('m') + 1 : $month;
        return mktime(23, 59, 59, $month, 0, $year);
    }

    public static function getWeekRange($date) {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 6) ? $ts : strtotime('last saturday', $ts);
        return array(date('Y-m-d', $start),
            date('Y-m-d', strtotime('next friday', $start)));
    }

    /**
     * Formats a datetime string 
     * @param String date. The date to be formated.
     * @param String inFormat. The format of the input date.
     * @param String outFormat. The new format of the date.
     * @param String timezone. The datetime zone to use.
     * @param String $localeCategory. The locale category to infect.
     * @param String $localeLang. The locale lang to use.
     * @return String. The new formated date.
     */
    public static function getDateFormated($date = '0000-00-00', $inFormat = 'Y-m-d', $outFormat = 'd/m/Y', $timezone = 'Europe/Athens', $localeCategory = null, $localeLang = null) {
        $d = DateTime::createFromFormat($inFormat, $date);
        if ($d === false) {
            return '';
        }
        $tz = new DateTimeZone($timezone);
        $d->setTimeZone($tz);
        return $d->format($outFormat);
    }

    /**
     * Returns all dates between two intervals inclusive. 
     * @param String startDate. The start date in the format 'Y-m-d'.
     * @param String endDate. The end date in the format 'Y-m-d'.
     * @param String dateInterval. The date interval that will be the steps to output between the start and end date. Default to step by day ('P1D'). 
     * @return Array . An array of dates string in the form 'Y-m-d'.
     */
    public static function getDateRange($startDate, $endDate, $dateInterval = 'P1D') {
        $period = new DatePeriod(
                new DateTime($startDate), new DateInterval($dateInterval), new DateTime($endDate)
        );
        $allDates = array();
        foreach ($period as $date) {
            $allDates[] = $date->format('Y-m-d');
        }
        unset($date);
        return $allDates;
    }


    /**
     * This is similar to php array_search so as to search recursively in multidimensional arrays. Parameters are same as original array_search function. 
     * 
     * IS NTO TESTED YET
     * 
     * @param Mixed $needle The value to dearch for
     * @param array $haystack The array into which we will search for the value
     * @param boolean $strict. If true the comparison of the value takes in consideration the type of the value also, not just the value.
     * @return The key of the value id found otherwise false.
     */
    public static function recursive_array_search($needle, $haystack, $strict = false) {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if (($strict ? $needle === $value : $needle == $value) OR ( is_array($value) && recursive_array_search($needle, $value, $strict) !== false)) {
                return $current_key;
            }
        }
        return false;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
//    public static function getArrayValue(array $data, $key) 
//    {
//        try
//        {
//            return $data[ $key ];
//        } catch (Exception $ex) {
//            return null;
//        }
//    }
    
  
    
    /**
     * Returns all strored end points.
     * @return array
     */
    protected function getEndPoints( ) : array
    {
        try {
            return parse_ini_file( JPATH_COMPONENT_ADMINISTRATOR . '/endpoints.ini');                    
        }
        catch ( Exception $ex ) {
            return [];
        }
    }
    
    
    protected function getEndPointByData(array $data, string $key )
    {
        return PHPUtils::getArrayValue($data, $key);
    }
}
