
<?php
/*------------------------------------------------------------------------
# E Logism libraries.
# ------------------------------------------------------------------------
# author    Christoforos J. Korifidis
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
defined('_JEXEC') or die('Restricted access');
echo new JResponseJson($this->data);