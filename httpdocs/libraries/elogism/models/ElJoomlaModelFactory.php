<?php
namespace elogism\models;
/**
 * @copyright (c) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */

 defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;
use JLoader;
use elogism\models\ElModel;

 /**
  * Factory methods from models views and controllers
  * @author Christoforos J. Korifidis
  * 
  */


class ElJoomlaModelFactory
{    
         
    
    /**
     * Creates a model without a predifined state
     * @param String $modelName Name of model
     * @param String $componentName Name of component eg com_component.
     * @return type
     */
    public static function getModel ($modelName, $componentName )
    {
        $className =  '\components\\' .  $componentName . '\models\\'  . ucfirst( $modelName );
        $found = JLoader::loadByPsr4($className);
        if ( $found ):
                return new $className( );
        else:
                return new ElModel();
        endif;
    }
    
}