<?php
/*------------------------------------------------------------------------
# E-Logism Libraries
# ------------------------------------------------------------------------
# @author    E-Logism
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\models;
defined( '_JEXEC' ) or die( 'Restricted access' );

use elogism\interfaces\Storer;
use Joomla\Registry\Registry;
use JModelBase;

 /**
  * Base Model for storing data.
  * 
  */

    class ElModelStore extends JModelBase
    {
        /**
         * @var Storer
         */
        private $storer = null;
       
        public function __construct( Storer $storer, Registry $state = null  ) {
            $this -> storer = $storer;
            parent::__construct( $state );
        }
        
        public function getStorer() {
            return $this -> storer;
        }
        
        public function setState(Registry $state)
        {
                
            try
            {
                //$storedData = $this -> storer -> store( $state -> toArray() );
                $sendedData = $state -> toArray() ;
                $messages = true; //$this -> validate ( $this -> getForm($sendedData), $sendedData );
                if ( $messages === true ):
                    $returnedData = $this -> storer -> store( $sendedData );  //$this -> store( $this -> normalizeData( $sendedData, $this -> getNormRules() ) ) ;
                    $state = new Registry($returnedData);
                    if (isset($returnedData['messages'])):
                        $this -> mergeMessages ( $state, $returnedData['messages']);
                    endif;                    
                else:
                    $this -> mergeMessages ( $state, $messages);
                endif;
                
            }
            catch(Exception $e) {
                $this -> addMessage($state, ['message' => $e -> getMessage(), 'type' => 'warning']);
            }
            parent::setState( $state );
        } 
        
        protected function validate(JForm $form,  $data)
        {
            if( $form -> validate($data) ):
                return true;
            else:
                return $this -> getErrors($form);
            endif;
        }
        
        protected function getErrors(JForm $form)
        {
             $return = [];
             $errors = $form -> getErrors();
             for ($i = 0, $n = count($errors); $i < $n && $i < 1; $i++):
                 if ($errors[$i] instanceof Exception):
                     $return[] = [ 'message' => $errors[$i] -> getMessage(), 'type' => 'warning' ];
                 else:
                     $return[] = [ 'message' => $errors[$i], 'type' => 'warning' ];
                 endif;
             endfor;
             return $return;
        }
      
        protected function normalizeData(Array $sendedData, Array $normRules)
        {
            foreach ( $normRules as $field => $rules ):
                $sendedData[ $field ] = $this -> getUtils() -> normData( $sendedData[ $field ], $rules );
            endforeach;
            unset( $rules );
            unset( $field );
            return $sendedData;
        }        
        
        protected function getForm($formData)
        {            
            return $this -> modifyFormRules( JForm::getInstance($this -> form, $this -> form ), $formData);
        }
        
        protected function modifyFormRules(JForm $form , $formData)
        {
            return $form;
        }
     
        protected function getNormRules()
        {
            return [];
        }
}