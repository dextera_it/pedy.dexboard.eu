<?php
/**
 * E-Logism Component
 *
 * @package       Emporio.Site
 * @subpackage    Models
 *
 * @copyright (C) 2016 E-Logism Team. All rights reserved.
 * @license       http://www.e-logism.gr/elgsoftware.txt
 * @link          http://www.e-logism.gr
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Product View Details
 * @since 1.0
 */

require_once JPATH_BASE . '/components/com_elgfrm/models/elgmodelsave.php';
class ElgFrmModelUserSave extends ElgModelSave
{
       
     protected $form = 'user';
    protected function store(Array $sendedData)
    {
        $user = new JUser();
        $user -> name = $sendedData['name'];
        $user -> username = $sendedData['email'];
        $user -> email = $sendedData['email'];
        $user -> password = $sendedData['password'];
        if( !isset($sendedData['groups']) ):
            $sendedData['groups'] = [1];
        endif;
        $user -> groups = $sendedData['groups'];
        $db = $this -> getDb();
        $oldId = $db -> setQuery( 'select id from #__users where username = ' . $db ->quote($sendedData['email']) ) -> loadResult();
        if ($oldId > 0):
            $user -> id = $oldId;
        endif;
        $user -> save();
        $sendedData['idUser'] = $user -> id;
        return  $sendedData;  
    }
    
    protected function getNormRules()
    {
        return [
            'name' => ['nogreekaccent', 'trim', 'siglewhitespace', 'upper'] ,
            'email' => ['lower']
        ];
    }
    
   
}