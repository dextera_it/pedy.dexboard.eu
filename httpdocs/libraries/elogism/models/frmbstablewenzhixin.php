<?php
/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 
 
 /**
  * The default model class.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */

    class ElgModelFrmBSTableWenzhixin extends JModelDatabase
    {

        public function getData(JDatabaseDriver $db, JDatabaseQuery $query, $page, $pageSize) 
        {
            $res = [ 'rows' => $db -> setQuery($query, $page, $pageSize) -> loadAssocList()];
            $query -> clear('select');
            $query -> clear('order');
            $query -> clear('group');
            $query -> select(' count(*) ');
            $res ['total'] = $db -> loadResult();
            return $res;
        }
    }