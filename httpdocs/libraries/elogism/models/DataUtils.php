<?php
namespace elogism\models;
/**
 * @copyright (c) 2016 e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

class DataUtils 
{
     /**
     * This function gets a value and normalizes it according to given rules. 
     * Normalization rules are given in the array of the function's second argument (see below on the arguments explaination. 
     * @param primitive $value The value to be normalized.
     * @param Array $normRules An associative array that contains the rules for normalization. 
     * Currently the rules that supported are:
     * 1. upper: convert data to upper case.
     * 2. lower: conert data to lower case.
     * 3. nospace: elimenates all spaces from the data.
     * 4. nowhitespace: eliminates all white space (not just spaces but also tabs etc) from the data.
     * 5. singlewhitespace: Converts two and more subsequent white space (tab, newline, space ect) to single space.
     * 6. numeric: Convert value to zero if is not numeric
     * 7. trim: Trim spaces.
     * 8. nogreekaccent: Eliminates greek accents
     * 9. similar2Greek: Converts similar looking lettters to correspoding greeks e.g. latin H to Greek Η.
     * @param String encoding. The encoding to be used when needed on string operations.
     * @return The $value normalized
     * 

     */
    public static function normData($value, $normRules, $encoding = 'utf8') {
        foreach ($normRules as $normRule) {
            switch ($normRule) {
                case 'upper' : $value = mb_strtoupper($value, $encoding);
                    break;
                case 'lower' : $value = mb_strtolower($value, $encoding);
                    break;
                case 'nowhitespace' : $value = preg_replace('/\s+/', '', $value);
                    break;
                case 'siglewhitespace' : $value = preg_replace('/\s{2,}/', ' ', $value);
                    break;
                case 'nospaces' : $value = str_replace(' ', '', $value);
                    break;
                case 'numeric' : $value = (is_numeric($value) ? $value : 0);
                    break;
                case 'trim' : $value = trim($value);
                    break;
                case 'nogreekaccent' : $value = str_replace('ώ', 'ω', 
                        str_replace('ύ', 'υ', 
                                str_replace('ί', 'ι', 
                                        str_replace('ή', 'η', 
                                                str_replace('έ', 'ε', 
                                                        str_replace('ό', 'ο', 
                                                                str_replace('ά', 'α', 
                                                                        str_replace('Ά', 'Α', 
                                                                                str_replace('Έ', 'Ε', 
                                                                                        str_replace('Ό', 'Ο', 
                                                                                                str_replace( 'Ή','Η', 
                                                                                                        str_replace('Ύ', 'Υ', 
                                                                                                                str_replace('Ί', 'Ι', 
                                                                                                                        str_replace('Ώ', 'Ω', $value )
                                                                                                                        )
                                                                                                                )
                                                                                                        )
                                                                                                )
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        );
                    break;
                case 'similar2Greek' : $value = strtr($value, ['A' => 'Α', 'B' => 'Β', 'H' =>'Η', 'I' => 'Ι', 'K' => 'Κ', 'M' => 'Μ', 'N' => 'Ν', 'O' => 'Ο', 'P' => 'Ρ', 'T' => 'Τ', 'Y' => 'Υ', 'Z' => 'Ζ', 'k' => 'κ', 'n' => 'η', 'o' => 'ο', 'i' => 'ι', 'p' => 'ρ', 'v' => 'ν', 'x' => 'χ']);
                    break;
                case 'escapeQuotes' : $value = str_replace("'", "", $value);
            }
        }
        unset($normRule);
        return $value;
    }
    
    
    /**
     * Inserts a new row into a db table and return the new inserted row.
     * Optionally checks if the row already exists. If the row exists it return the existing row.
     * If en error occurs it return null.
     * 
     * @param JTable $dbTable The table into which we want to insert the value
     * @param array $values The values of the the row's columns. It must by in the form [columnName=>[column Value, [normRule1, normRule2]]
     * @param boolean $checkExists If true it will check if the row already exists. Default is true.
     * @return mixed The new Row.
     */
    
    public static function insertNewValue($dbTable, $values=array(), $checkExists=true)
    {
        $cnt = count($values);
        if( $cnt <= 0) 
        {
            return null;
        }
        $rV = [];
        foreach($values as $key=>$value):
            $normValue = self::normData($value[0], $value[1]);
            $rV[$key] = $normValue;
        endforeach;
        unset($value);
        unset($key);
    
       
        if($checkExists)
        {
            if($dbTable->load($rV))
            {
                return $dbTable;
            }
        }
        foreach($rV as $key=>$value)
        {
            $dbTable->$key = $value;
        }
        unset($key);
        unset($value);
        
        if($dbTable->store())
        {
            
            return $dbTable;
        }
        else
        {
            return null;
        }        
    }
    
    
    public static function getOrderByClause(JDatabaseDriver $db, Array $options =[] )
    {
        $c = '';
        foreach ($options as $option):
            if( isset($option['field'] ) ):
                $c .= $db -> quoteName($option['field']) . ( isset($option['dir']) ? $option['dir'] : '' ) . ',';
            endif;
        endforeach;
        unset($option);
        if ($c !== ''):
            return ' order by ' . trim($c, ',');
        else:
            return '';
        endif;
    }
    
    public static function getDataWithPaging(JDatabaseDriver $db, $query, $limit_start, $limit)
    {
        if ($limit > 0 && $limit_start != ''):
            return $db -> setQuery($query, $limit_start, $limit) -> loadAssocList();
        else:
            return $db -> setQuery($query) -> loadAssocList();
        endif;
    }
    
    
    /**
      * This function converts a value to literally "Yes" or "No" or "Not set". The value is converted 
      * according to these rules:
      * 1. If the $conVal is -1 or "" then is converted to "Not set", (Component translatable constant COM_ELG_NOTSET).
      * 2. 0 or false then is converted to "No", (CMS translatable constant JNO).
      * 2. Anything else is converted to "Yes", (CMS translatable constant JYES).
      * @param Mixed $intVal The value we want to convert.      
      * @return String. The litteraly "Yes" or "No" or "Not set".
      */
     public static function getYesNo($conVal) {
        if ($conVal == -1 || $conVal == "") {
            return JText::_('COM_ELG_NOT_SET');
        }
        elseif ($conVal == 0 || $conVal == "" || $conVal === false ) {
            return JText::_('JNO');
        }
        else {
            return JText::_('JYES');
        }
    }
    
    /**
     * Returns current component specific parameters in the form of json data.
     * @return JSON Data
     */
    public static function getAppConfig() {
        if ( JFactory::getSession() -> get('appConfig','') === '' ):
            JFactory::getSession() ->set('appConfig', file_get_contents( JPATH_COMPONENT .'/appconfig.json') );
        endif;
        return json_decode( JFactory::getSession() -> get('appConfig') );
    }
    
    /**
     *  returns value from the current component 
     * @param type $val
     * @return type
     */
    public function getAppConfigValue( $val )
    {
       return   self::getAppConfig()[$val];
    }
}
