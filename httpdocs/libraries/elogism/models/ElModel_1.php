<?php
namespace elogism\models;
/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 use JModelBase;
 use JFormHelper;
 // require_once __DIR__ .'/utils.php';
 /**
  * The default model class.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */

    class ElModel extends JModelBase
    {
        //private $utils = null;
        public function __construct(\Joomla\Registry\Registry $state = null) {
            parent::__construct($state);
            JFormHelper::addFormPath( JPATH_SITE . '/components/com_elgfrm/models/forms');
            JFormHelper::addFormPath( JPATH_COMPONENT . '/models/forms');
            JFormHelper::addFieldPath(JPATH_ADMINISTRATOR . '/components/com_elgfrm/models/fields');
            JFormHelper::addRulePath(JPATH_ADMINISTRATOR .  '/components/com_elgfrm/models/rules');
            JFormHelper::addRulePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/rules');
            // JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/tables');
            // $this -> utils = new DataUtils();
        }
        
        /**
         * Singleton for DataUtils object
         * @return DataUtils
         */
        protected function getUtils() 
        {
//                if ( $this -> utils === null ):
//                        $this -> utils = new DataUtils();
//                endif;
//                return $this -> utils;
        }
        
        /**
         * Adds a message to the state so as to be shown 
         * @param Joomla\Registry\Registry $state
         * @param String $message
         * @param String $messageType (success, warning, notice)
         */
        protected function addMessage (Joomla\Registry\Registry $state, String $message, String  $messageType )
        {
                $state -> set('messages',  $state -> get('messages')[] = [$message, $messageType] ) ;
        }
        
        /**
         * 
         * @param JRegistry $state
         * @param array $messages [[message, messagetype], [message, messagetype] ...]
         */
        protected function mergeMessages(JRegistry $state, Array $messages)
        {
            $sm = $state -> get('messages');
            $state -> set( 'messages', array_merge(($sm == null ? [] : $sm) , $messages) );
        }
        
//        protected function getDataFormated(JDatabaseDriver $db, $query, $state)
//        {
//            $formater = $this -> getFormater( $state );
//            require_once JPATH_BASE . '/components/com_elgfrm/models/frm' . strtolower($formater) . '.php';
//            $formatClass = 'ElgModelFrm' . $formater;
//            $orderCol = $state -> get('filter_order', '');
//            if ( $orderCol !== ''):
//                $query -> order( $orderCol . ' ' . $state -> get('filter_order_Dir') );
//            endif;
//            return (new $formatClass()) -> getData($db, $query, $state -> get('limit_start'), $state -> get('limit'));
//        }
//        
//        protected function getFormater($state) {
//            return $state -> get('dft', 'raw');
//        }
        
//        protected static function storeDataHistory ($category, $action, $data, $fileCategory = 'history', $datetime = null, $user = null )
//        {
//            if ($datetime === null):
//                $data['dt'] = date('Y-m-d H:i:s');
//            endif;
//            if($user === null):
//                $user = JFactory::getUser();
//            endif;
//            $data ['user'] = $user -> name;
//            $data['userId'] = $user -> id;
//            $data['action'] = $action;
//            $file = self::getChangesStorage() . "/$fileCategory.$category.dat";
//            file_put_contents($file, json_encode($data), FILE_APPEND);
//            return $data;
//        }
        
//        protected static  function getChangesStorage()
//        {
//            return JPATH_SITE. '/media/com_elgemporio/data';
//        }
    }