<?php
/*------------------------------------------------------------------------
# E-Logism Libraries
# ------------------------------------------------------------------------
# @author    E-Logism
# @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
# Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\models;
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require_once JPATH_SITE .'/components/com_elgfrm/models/elgmodel.php';
 
 /**
  * Base Model class for manipulating Data.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */

    class ElgModelSave extends JModelBase
    {
        private $updateMethod = null;
       
        protected function setStoreMethod(String $storeMethod) {
            $this -> updateMethod = $storeMethod;
        }
        
        protected function getStoreMethod() {
            return ( $this -> updateMethod === null? 'JTable': $this -> updateMethod );
        }
        
        public function setState(JRegistry $state)
        {
            try
            {
                $sendedData = $state -> toArray() ;
                $messages = $this -> validate ( $this -> getForm($sendedData), $sendedData );
                if ( $messages === true ):
                    $returnedData = $this -> store( $this -> normalizeData( $sendedData, $this -> getNormRules() ) ) ;
                    $state = new JRegistry($returnedData);
                    if (isset($returnedData['messages'])):
                        $this -> mergeMessages ( $state, $returnedData['messages']);
                    endif;                    
                else:
                    $this -> mergeMessages ( $state, $messages);
                endif;
                
            }
            catch(Exception $e) {
                $this -> addMessage($state, ['message' => $e -> getMessage(), 'type' => 'warning']);
            }
            parent::setState( $state );
        } 
        
        protected function validate(JForm $form,  $data)
        {
            if( $form -> validate($data) ):
                return true;
            else:
                return $this -> getErrors($form);
            endif;
        }
        
        protected function getErrors(JForm $form)
        {
             $return = [];
             $errors = $form -> getErrors();
             for ($i = 0, $n = count($errors); $i < $n && $i < 1; $i++):
                 if ($errors[$i] instanceof Exception):
                     $return[] = [ 'message' => $errors[$i] -> getMessage(), 'type' => 'warning' ];
                 else:
                     $return[] = [ 'message' => $errors[$i], 'type' => 'warning' ];
                 endif;
             endfor;
             return $return;
        }
      
        protected function normalizeData(Array $sendedData, Array $normRules)
        {
            foreach ( $normRules as $field => $rules ):
                $sendedData[ $field ] = $this -> getUtils() -> normData( $sendedData[ $field ], $rules );
            endforeach;
            unset( $rules );
            unset( $field );
            return $sendedData;
        }        
        
        protected function getForm($formData)
        {            
            return $this -> modifyFormRules( JForm::getInstance($this -> form, $this -> form ), $formData);
        }
        
        protected function modifyFormRules(JForm $form , $formData)
        {
            return $form;
        }
     
        protected function getNormRules()
        {
            return [];
        }
}