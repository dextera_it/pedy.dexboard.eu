<?php
/*------------------------------------------------------------------------
; E-Logism Libraries
;  ------------------------------------------------------------------------
; @author    E-Logism
; @license - E Logism Proprietary Software Licence http://www.e-logism.gr/licence.pdf
; @Website: http://www.e-logism.gr
----------------------------------**/
namespace elogism\models\updateworkers;
defined( '_JEXEC' ) or die( 'Restricted access' );
use Joomla\CMS\Table\Table;
use Joomla\Registry\Registry;
 

        class JoomlaTable
        {
            private $storeMedium = null;
            public function __construct( String $storeMediumIdentifier) {
                    JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/tables');
                    $this -> setStoreMediumByIdentifier( $storeMediumIdentifier);
            }

            public function getStoreMedium() {
                return $this -> storeMedium;
            }

            public function setStoreMediumByIdentifier( String $storeMediumIdentifier ) {
                $this -> storeMedium = Table::getInstance($storeMediumIdentifier);
                checkStoreMediumSet();
            }
            
            public function setStoreMedium( Table $storeMedium) {
                $this -> storeMedium = $storeMedium;
                checkStoreMediumSet();
            }
                      
            public function store( Registry $data) {
                checkStoreMedium();
                $this -> storeMedium ->bind( $data ->toArray() );
            }
            
            protected function checkStoreMedium() {
                if ( !$this -> storeMedium instanceof  Table ):
                    throw new Exception('COM_EL_ERROR_10201');
                endif;
            }
        }