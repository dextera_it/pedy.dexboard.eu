<?php
/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 
 require_once __DIR__ .'/utils.php';
 /**
  * The default model class.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */
    
    class ElgModel extends JModelDatabase
    {
        public static $STORE_MEDIUM_DB_TABLE = 0;
        private $utils = null;
        public function __construct(\Joomla\Registry\Registry $state = null) {
            parent::__construct($state);
            JFormHelper::addFormPath( JPATH_COMPONENT . '/models/forms');
            JFormHelper::addFieldPath(JPATH_ADMINISTRATOR . '/components/com_elgfrm/models/fields');
            JFormHelper::addRulePath(JPATH_ADMINISTRATOR .  '/components/com_elgfrm/models/rules');
            JFormHelper::addRulePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/rules');
            JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/tables');
            $this -> utils = new DataUtils();
        }
        
        /**
         * 
         * @return DataUtils
         */
        protected function getUtils() 
        {
            return $this -> utils;
        }
        
        /**
         * 
         * @param JRegistry $state
         * @param array $message [message, messagetype]
         */
        protected function addMessage (JRegistry $state, Array $message)
        {
            $sm = $state -> get('messages');
            $sm[] = $message;
            $state -> set('messages', $sm);
        }
        
        /**
         * 
         * @param JRegistry $state
         * @param array $messages [[message, messagetype], [message, messagetype] ...]
         */
        protected function mergeMessages(JRegistry $state, Array $messages)
        {
            $sm = $state -> get('messages');
            $state -> set( 'messages', array_merge($sm, $messages) );
        }
        
        protected function getDataFormated(JDatabaseDriver $db, $query, $state)
        {
            $formater = $this -> getFormater( $state );
            require_once JPATH_BASE . '/components/com_elgfrm/models/frm' . strtolower($formater) . '.php';
            $formatClass = 'ElgModelFrm' . $formater;
            $orderCol = $state -> get('filter_order', '');
            if ( $orderCol !== ''):
                $query -> order( $orderCol . ' ' . $state -> get('filter_order_Dir') );
            endif;
            return (new $formatClass()) -> getData($db, $query, $state -> get('limit_start'), $state -> get('limit'));
        }
        
        protected function getFormater($state) {
            return $state -> get('dft', 'raw');
        }
        
        protected function storeDataHistory ($action, $data, $category = null, $datetime = null, $user = null )
        {
            if($category === null):
                $category = $this -> getHistoryCategory();
            endif;
            if ($datetime === null):
                $data['dt'] = date('Y-m-d H:i:s');
            endif;
            if($user === null):
                $data['user'] = JFactory::getUser() -> name;
                $data['userId'] = JFactory::getUser() -> id;
            endif;     
            $data['action'] = $action;
            $file = $this -> getChangesStorage() . "/changes$category.dat";
            file_put_contents($file, json_encode($data) . ',', FILE_APPEND);
        }
        
        protected function getChangesStorage()
        {
            return JPATH_BASE. '/media/com_elgemporio/data';
        }
    }