<?php
/**
 * @copyright (c) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 /**
  * Factory methods from models views and controllers
  * @package libraries.e-logism.php.joomla
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */


class ElgJoomlaModelFactory
{    
       
     /**
     * Factory method that return the proper JModel object whe we already have the state Object. It 's actually a wrapper for getModel.
     * @param JRegistry $state The JRegistry object to pass into the model constructor.
     * @param array $paths An array with the paths where we will search to find the model file.
     * @return JModel The JModel object created.
     */
    public static function getModelByState(JRegistry $state = null, Array  $paths = null)
    {
        return self::getModel($state -> get('model'), $state -> get('componentName'), $state, $paths );
    }    
    
    /**
     * Creates a model without a predifined state
     * @param String $modelName Name of model
     * @param String $componentName Name of component without the com_ prefix.
     * @param JRegistry $state The state if we  have one
     * @param array $paths Array of paths where the model file will be searched
     * @return type
     */
    public static function getModel ($modelName, $componentName, JRegistry $state = null,  Array $paths = null)
    {
        $model = null;
        if($paths === null)
        {
            $paths = array(JPATH_BASE . '/components/com_' . strtolower($componentName) . '/models/');
        }
        foreach($paths as $path)
        {
            if(file_exists($path  . strtolower($modelName) . '.php'))
            {
                require_once $path  . strtolower($modelName) . '.php';
                $model = $componentName . 'Model' . $modelName;
                break;
            }
        }
        unset($path);
        return ($model === null ? self::getDefaultModel($state) : new $model($state));
    }
    
	/**
     * Factory method for the default model
     * @param JRegistry $state The JRegistry object passed to objecet contructor
     */
    public static function getDefaultModel(JRegistry $state = null)
    {
        require_once __DIR__ .  '/default.php';
        return new ModelDefault($state);
    }
}