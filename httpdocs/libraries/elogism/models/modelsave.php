<?php
/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require_once JPATH_BASE . '/components/com_elgfrm/models/model.php';
 /**
  * The default model class.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */

    class ElgModelSave extends ElgModel
    {
        
        public function setState(JRegistry $state)
        {
            try
            {
                $state = $this -> doAction($state);
            }
            catch(Exception $e) {
                $this -> addMessage($state, ['message' => $e -> getMessage(), 'type' => 'warning']);
            }
            parent::setState( $state );
        }  
        
        protected function doAction($state)
        {       
            $sendedData = $state -> toArray() ;
            $messages = $this -> validate ( $this -> getForm(), $sendedData );
            if ( $messages === true ):
                $normSendedData = $this -> store( $this -> normalizeData( $sendedData, $this -> getNormRules() ) ) ;
                $state = new JRegistry($normSendedData);
                $this -> addMessage($state, ['message' => JText::_('COM_ELG_SUBMIT_SUCCESS'), 'type' => 'success']);
            else:
                $this -> mergeMessages ( $state, $messages);
            endif;
            return $state;
        }
        
        protected function validate(JForm $form,  $data)
        {
            if( $form -> validate($data) ):
                return true;
            else:
                return $this -> getErrors($form);
            endif;
        }
        
       protected function getErrors(JForm $form)
       {
            $return = [];
            $errors = $form -> getErrors();
            for ($i = 0, $n = count($errors); $i < $n && $i < 1; $i++):
                if ($errors[$i] instanceof Exception):
                    $return[] = [ 'message' => $errors[$i] -> getMessage(), 'type' => 'warning' ];
                else:
                    $return[] = [ 'message' => $errors[$i], 'type' => 'warning' ];
                endif;
            endfor;
            return $return;
       }
      
        protected function normalizeData(Array $sendedData, Array $normRules)
        {
            foreach ( $normRules as $field => $rules ):
                $sendedData[ $field ] = $this -> getUtils() -> normData( $sendedData[ $field ], $rules );
            endforeach;
            unset( $rules );
            unset( $field );
            return $sendedData;
        }
        
        
        protected function getForm()
        {
            return JForm::getInstance( $this -> form, $this -> form );
        }
    }