<?php
/**
 * @copyright (C) 2016, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * This file containes null object implementation for the admin side.
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require_once __DIR__ . '/model.php';
 /**
  * The default model class.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */

    class ElgModelDelete extends ElgModel
    {
        public function setState( JRegistry $state )
        {
            $tb = $this -> getStoreMedium()[1];
            try
            {
                if( $tb -> load( ['id' => $state -> get('id') ] ) ) :
                    $data = $this -> getExistingData( ['tb' => $tb] );
                    $tb -> delete();
                    $this -> storeDataHistory('DEL', $data);
                    $state -> set('data', ['deleted' => $state -> get('id')]);
                    $this -> addMessage( $state,  ['message' => JText::_('COM_ELG_DELETE_SUCCESS'), 'type' => 'success']);
                else:
                    throw new Exception(JText::_('COM_ELG_ERROR_10055_RECORD_NOT_FOUND'), 10055);
                endif;
            }
            catch(Exception $e) {
                $state -> set('data', ['deleted' => 0]);
                $this -> addMessage( $state, ['message' => $e -> getMessage(), 'type' => 'warning']);
            }
            parent::setState( $state );
        }
        
        
        protected function getExistingData($params) {
            return $params['tb'] -> getProperties(true);
        }
    }