<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:6981:"
<div class="yjsgarticle">
			<h1 class="pagetitle">
		Πολιτική Απορρήτου	</h1>
								<div class="newsitem_text">
												
<div class="row">
<div class="span6">
<h3 class="hightlight-heading"><strong>Προσωπικά Δεδομένα</strong></h3>
<div class="box">
<p>Η εταιρεία μας δίνει μεγάλη προσοχή στην προστασία των προσωπικών πληροφοριών των επισκεπτών στην ιστοσελίδα μας και λαμβάνει όλα τα απαραίτητα μέτρα για να εξασφαλίσει την ασφάλειά σας όσο το δυνατόν περισσότερο.</p>
<p>Οποιεσδήποτε πληροφορίες παρέχετε στην DEXTERA μέσω αυτής της ιστοσελίδας θα διατηρούνται και θα είναι προσβάσιμες μόνο από την DEXTERA. Η DEXTERA δεν θα αποκαλύψει πληροφορίες για οποιονδήποτε χρήστη σε καμία οντότητα εκτός από την συμμόρφωση με τους ισχύοντες νόμους ή τις έγκυρες νομικές διαδικασίες.
</p>
<p>Ορισμένα τμήματα του ιστότοπου ενδέχεται να σας επιτρέψουν να ανεβάσετε πληροφορίες που θα σας αναγνωρίσουν. Εάν αποφασίσετε να χρησιμοποιήσετε αυτά τα τμήματα του ιστότοπου και να υποβάλετε πληροφορίες, η DEXTERA θα χρησιμοποιήσει μόνο τις προσωπικές πληροφορίες που παρέχετε για την παράδοση των συγκεκριμένων πληροφοριών ή υπηρεσιών που έχετε ζητήσει. Χωρίς τη ρητή συγκατάθεσή σας, η DEXTERA δεν θα χρησιμοποιήσει τα στοιχεία επικοινωνίας σας για οποιονδήποτε άλλο σκοπό.</p>
<p>Είναι σημαντικό για εμάς να είναι θετική η εμπειρία σας από την ιστοσελίδα της DEXTERA. Προς το σκοπό αυτό, υποσχόμαστε να κάνουμε τα πάντα για να σεβαστούμε τα δικαιώματά σας.</p>
</div>
<h3 class="hightlight-heading"><strong>Cookies</strong></h3>
<div class="box">
<p>Τα cookies είναι μικρά αρχεία κειμένου που τοποθετούνται στον υπολογιστή σας από ιστότοπους που επισκέπτεστε. Χρησιμοποιούνται ευρέως για να λειτουργούν οι ιστότοποι ή να εργάζονται πιο αποτελεσματικά, καθώς και για να παρέχουν πληροφορίες στους ιδιοκτήτες του ιστότοπου. Αυτός ο ιστότοπος χρησιμοποιεί cookies για να βελτιώσει τη λειτουργικότητα, τη φιλικότητα προς το χρήστη και την εξατομίκευση.
</p>
</div>
<h3 class="hightlight-heading"><strong>Cookies Τρίτων</strong></h3>
<div class="box">
<p>Εκτός από τα δικά μας cookies, μπορεί επίσης να χρησιμοποιηθούν cookies τρίτων, όπως τα cookies του Google Analytics, για στατιστικούς λόγους.</p>
<p>Επιφυλάσσετε το δικαίωμα να ρυθμίσετε το πρόγραμμα περιήγησής σας για να σας προειδοποιήσει προτού αποδεχτείτε τα cookies ή μπορείτε απλά να το ρυθμίσετε για να τα απορρίψετε, παρόλο που ενδέχεται να μην έχετε πρόσβαση σε όλες τις δυνατότητες αυτής της ιστοσελίδας εάν το κάνετε. Δείτε το κουμπί βοήθειας του προγράμματος περιήγησης για το πώς μπορείτε να το κάνετε αυτό. Δεν χρειάζεται να έχετε τα cookies ενεργοποιημένα για να χρησιμοποιήσετε ή να πλοηγηθείτε σε πολλά μέρη αυτού του ιστοτόπου. Θυμηθείτε ότι εάν χρησιμοποιείτε διαφορετικούς υπολογιστές σε διαφορετικές τοποθεσίες, θα πρέπει να βεβαιωθείτε ότι κάθε πρόγραμμα περιήγησης έχει προσαρμοστεί ώστε να ταιριάζει στις προτιμήσεις σας για τα cookies .</p>
</div>
</div>
<div class="span6">
<h3 class="hightlight-heading"><strong>Νόμοι και Δικαιοδοσία</strong></h3>
<div class="box">Τα δικαστήρια των Αθηνών είναι αρμόδια να διευθετήσουν τυχόν διαφορές που προκύπτουν από το παρόν, σύμφωνα με τον ελληνικό νόμο.</div>
</div>
<div class="span6">
<h3 class="hightlight-heading"><strong>Πληροφορίες Επικοινωνίας</strong></h3>
<div class="box">
<p><span style="font-size: 12pt;"><strong>DEXTERA CONSULTING</strong></span></p>
<p>Διεύθυνση: Ρωμανού Μελωδού 3, Μαρούσι, Αθήνα</p>
<p>Τ.Κ.: 15125</p>
<p>Τηλέφωνο: +30 210 6390727</p>
<p>e-mail: <span id="cloak3261bdfe3c5bd6314a618d5bc99a6832">Αυτή η διεύθυνση ηλεκτρονικού ταχυδρομείου προστατεύεται από τους αυτοματισμούς αποστολέων ανεπιθύμητων μηνυμάτων. Χρειάζεται να ενεργοποιήσετε τη JavaScript για να μπορέσετε να τη δείτε.</span><script type='text/javascript'>
				document.getElementById('cloak3261bdfe3c5bd6314a618d5bc99a6832').innerHTML = '';
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				var addy3261bdfe3c5bd6314a618d5bc99a6832 = '&#105;nf&#111;' + '&#64;';
				addy3261bdfe3c5bd6314a618d5bc99a6832 = addy3261bdfe3c5bd6314a618d5bc99a6832 + 'd&#101;xt&#101;r&#97;c&#111;ns&#117;lt&#105;ng' + '&#46;' + 'c&#111;m';
				var addy_text3261bdfe3c5bd6314a618d5bc99a6832 = '&#105;nf&#111;' + '&#64;' + 'd&#101;xt&#101;r&#97;c&#111;ns&#117;lt&#105;ng' + '&#46;' + 'c&#111;m';document.getElementById('cloak3261bdfe3c5bd6314a618d5bc99a6832').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy3261bdfe3c5bd6314a618d5bc99a6832 + '\'>'+addy_text3261bdfe3c5bd6314a618d5bc99a6832+'<\/a>';
		</script></p>
</div>
</div>
</div>													</div>
	</div>
<!--end news item -->
";s:4:"head";a:12:{s:5:"title";s:50:"Π.Ε.Δ.Υ. - Πολιτική Απορρήτου";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:4:"name";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:10:"Super User";}}s:5:"links";a:0:{}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:27:"/media/jui/js/jquery.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:34:"/media/jui/js/jquery-noconflict.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;s:11:"detectDebug";b:1;}}s:35:"/media/jui/js/jquery-migrate.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";b:0;s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}s:30:"/media/jui/js/bootstrap.min.js";a:2:{s:4:"type";s:15:"text/javascript";s:7:"options";a:6:{s:7:"version";s:4:"auto";s:8:"relative";b:1;s:11:"detectDebug";s:1:"0";s:9:"framework";b:0;s:8:"pathOnly";b:0;s:13:"detectBrowser";b:1;}}}s:6:"script";a:1:{s:15:"text/javascript";s:242:"jQuery(function($){ initTooltips(); $("body").on("subform-row-add", initTooltips); function initTooltips (event, container) { container = container || document;$(container).find(".hasTooltip").tooltip({"html": true,"container": "body"});} });";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}s:13:"scriptOptions";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:35:"Πολιτική Απορρήτου";s:4:"link";s:58:"index.php?option=com_content&view=article&id=38&Itemid=180";}}s:6:"module";a:0:{}}