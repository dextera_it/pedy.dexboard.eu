 var elgCEFD = {};
    $(document).ready(function() {
		
		
		$('#personelsComittees').fullCalendar({
			
			now:  moment(), //[$('#RefYear').val(), $('#RefMonth').val() -1, 1],
			locale: 'el',
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
                elgInitCommitteeModal();
				$('#StartDateCommittee').val(start.format('YYYY-MM-DD' ));
				$('#EndDateCommittee').val(end.subtract(1, 'seconds').format('YYYY-MM-DD' ));
				$('#PersonelScheduleId').val('');                        
				jQuery('#personelsCommitteesForm').modal({});
			},
			
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: function(start, end, timezone, callback) {
				$.ajax({
					url: 'index.php?option=com_elgpedy&view=' + elgview + '&format=json&Itemid=' + elgItemid,
					dataType: 'json',
					data: {
						HealthUnitId :  document.getElementById('HealthUnitId').value,
						start:  start.format('YYYY-MM-DD'),
						end: end.subtract(1, 'seconds').format('YYYY-MM-DD')  
					},
					success: function(response) {
						showDataArea();
						callback(response);
					}
				});	
			},
			eventClick: function(calEvent, jsEvent, view) {
				elgInitCommitteeModal();
				if(calEvent.end === null ) calEvent.end = calEvent.start;
				$('#StartDateCommittee').val(calEvent.start.format('YYYY-MM-DD'));
				$('#EndDateCommittee').val( calEvent.end.format('YYYY-MM-DD') );                    
				$('#PersonelScheduleId').val(calEvent.PersonelScheduleId);
				if(calEvent.PersonelScheduleId != '') {
					elgCEFD[calEvent.PersonelScheduleId] = calEvent._id;
				}
				$('#HealthCommitteeId').val(calEvent.HealthCommitteeId);                                                    
				$('#PersonelId').val(calEvent.PersonelId);                        
				$('#delTitle').text(calEvent.title);
				$('#delDates').text(calEvent.start.format('dddd DD/M/YYYY') + ' - ' + calEvent.end.format('dddd DD/M/YYYY'));                                        
				$('#committeAskDel').show();
				jQuery('#personelsCommitteesForm').modal({});
			},
		});
		
		
} );


function   elgCommitteeCloseDel() {
    $('#committeeDelSection').hide();
    $('#personelsCommitteesForm .buttons-del').hide();
    $('#updateSection').slideDown();    
    $('#personelsCommitteesForm .buttons').show();
}

function elgCommitteeAskDel() {
    $('#updateSection').slideUp();
    $('#personelsCommitteesForm .buttons').hide();
    $('#committeeDelSection').show();
    $('#personelsCommitteesForm .buttons-del').show();
}

function elgEvalCommitteeModalElm(elm) {
    if(elm.val() == '') {
        elgCommitteModalShowError(elm);
        return false;
    }
    else {
        elgCommitteModalShowOk(elm);
        return true;
    }
}

function elgCommitteModalShowError(elm)
{
    var p = elm.parent();
    if(!p.hasClass('has-error')) p.addClass('has-error');
}

function elgCommitteModalShowOk(elm)
{
    elm.parent().removeClass('has-error');
}

function elgInitCommitteeModal() {
    $('#updateSection').show();
    var e1 = $('#HealthCommitteeId');
    e1.val('');
    elgCommitteModalShowOk(e1);
    var e2 = $('#PersonelId');
    e2.val('');
    elgCommitteModalShowOk(e2);    
    $('#committeAskDel').hide();
    $('#committeeDelSection').hide();
    $('#personelsCommitteesForm .buttons-del').hide();
    $('#personelsCommitteesForm .buttons-result').hide();
    $('#personelsCommitteesForm .buttons').show();
}

function elgCommitteeUpdateEvent() {
    $('#personelsCommitteesForm .modal-body-data').slideUp();
    $('#personelsCommitteesForm .loading-indicator').show();
    
}


function elgCommitteeSetEvent(){
	var isOK = true;
     var subData = {};
	subData.PersonelScheduleId = $('#PersonelScheduleId').val();
    subData.StartDateCommittee = $('#StartDateCommittee').val();
	subData.EndDateCommittee = $('#EndDateCommittee').val();
	var e1 = $('#PersonelId');
        if(elgEvalCommitteeModalElm(e1)) {
            subData.PersonelId = e1.val();
        }
        else {
            isOK = false;
        }
        var e2 = $('#HealthCommitteeId');
        if(elgEvalCommitteeModalElm(e2)) {
            subData.HealthCommitteeId = e2.val();
        }
        else {
            isOK = false;
        }
        if(!isOK)
        {
            $('#committeeMesage').show();
            return false;
        }
        $('#personelsCommitteesForm .modal-body-data').slideUp();
        elgHideButtonsGroups();
	$('#personelsCommitteesForm .loading-indicator').show();
	
	
	$.post(saveUrl
	, subData
    , 	function(data, textStatus){
            var oldId = data.data.data.oldId;
            if(elgCEFD[oldId]) {
               $('#personelsComittees').fullCalendar( 'removeEvents', elgCEFD[oldId]); 
            }
                eventData = {
                    title: data.data.data.LastName + ' ' + data.data.data.FirstName,
                    start: data.data.data.FromDate,
                    end: data.data.data.ToDate,
                    PersonelId: data.data.data.PersonelId,
                    PersonelScheduleId: data.data.data.ScheduleId,
                    HealthCommitteeId: data.data.data.CommitteeId,
                    backgroundColor: '#' + data.data.data.MemoColor

                };
                $('#personelsComittees').fullCalendar('renderEvent', eventData, true)
                jQuery('#personelsCommitteesForm').modal('hide');
                $('#personelsCommitteesForm .loading-indicator').hide();
                $('#personelsCommitteesForm .modal-body-data').slideDown();		
            }
	);
}

function elgCommitteeDelEvent() {
    $('#committeeDelSection').slideUp();
    elgHideButtonsGroups();
    $('#personelsCommitteesForm .loading-indicator').show();
    var subData = {};
    subData.PersonelScheduleId = $('#PersonelScheduleId').val();
    $.post( deleteUrl
    , subData
    , function(data, textStatus){
        if(data.messages != null)
        {
            var  s = '';
            for(var i in data.messages) {
                s += '<br  />' + i;
            }
            $('#resultMessage').text(s);
        }
         var oldId = data.data.data.oldId;
            if(elgCEFD[oldId]) {
               $('#personelsComittees').fullCalendar( 'removeEvents', elgCEFD[oldId]); 
        }
        elgHideButtonsGroups();
        jQuery('#personelsCommitteesForm').modal('hide');
        $('#personelsCommitteesForm .loading-indicator').hide();
        $('#personelsCommitteesForm .buttons-result').show();   
        
    });
}

function elgHideButtonsGroups() {
    $('#personelsCommitteesForm .buttons').hide();
    $('#personelsCommitteesForm .buttons-del').hide();
    $('#personelsCommitteesForm .buttons-result').hide();
}