var attendancyTableObj;
var myTable;

function getPersonnelShownStatus(personelData, selectedHUId)
{

    if (personelData.PersonelStatusId == null)
        if (selectedHUId == personelData.tmpRefHealthUintId)
            return [null, null];
        else
            return [personelData.tempStatusId, personelData.tmpStatus];
    else
    if (selectedHUId == personelData.tmpRefHealthUintId)
        return [personelData.PersonelStatusId, personelData.PersonelStatus];
    else
        return [personelData.tempStatusId, personelData.tmpStatus];

}
function getAttendancyData() {
    showAsBusy();

    var hu = document.getElementById('HealthUnitId');
    var refDate = document.getElementById('RefDate');

    jQuery.get(location.href + '&format=json&HealthUnitId=' + hu.value + '&RefDate=' + moment(refDate.value, 'D-M-YYYY').format('YYYYMMDD'), function (data) {
        attendancyTableObj = new attendanceTable(hu);
        var tb = attendancyTableObj.getTable();
        var rData = attendancyTableObj.reformPersonelData(data.data, hu.value);
        var data = attendancyTableObj.initializeData(rData);
      
        tb.bootstrapTable('load', data);
        showUncommitted();
        makeEdits(data, hu.value);
        showDataArea();
    });
}

/**
 * Show the legend with the sum of uncommited rows
 */
function showUncommitted() {
    unCommittedBadge.innerHTML = attendancyTableObj.getUncommittedNo();
    unContainer.style.display = (attendancyTableObj.getUncommittedNo() > 0 ? '' : 'none');
}


getAttendancyData();

/**
 * Individual row save
 * @param index Table row index
 */
function saveAttendacyRow(index) {
    var tb = attendancyTableObj.getTable();
    var huId;
    showAsBusy();
    var data = attendancyTableObj.getData();
    if (data.length > 0) {
        huId = document.getElementById('HealthUnitId').value;;
        pD = {
            HealthUnitId: huId,
            RefDate: moment(document.getElementById('RefDate').value, 'D-M-YYYY').format('YYYYMMDD'),
            aD: [data[index]]
        };
        jQuery.post('index.php?option=com_elgpedy&view=personelattendancybooksave&format=json&Itemid=144', JSON.stringify(pD),
                function (response) {
                    var rData = attendancyTableObj.reformPersonelData( response.data , huId);
                    var data = attendancyTableObj.initializeData( rData );
                    tb.bootstrapTable('removeAll');                   
                    tb.bootstrapTable('load', data);
                    showUncommitted();
                    makeEdits(huId);
                    showDataArea();
                },
                'json');
    }

}

function attendanceTable(healthUnitIn /**, refDate **/) {
    var elmTable = null;
    var uncommittedNo = 0;
   
    var healthUnit = healthUnitIn;
   
   
    
    this.reformPersonelData = function (data, selectedHUId) {
        var shown;
        for (var i = data.length - 1; i >= 0; i--) {
            shown = getPersonnelShownStatus(data[i], selectedHUId)
            data[i].finalStatusId = shown[0];
            data[i].finalStatus = shown[1];
        }
        return data;
    };


    
    
    this.getTable = function () {
        if ( elmTable === null ) {
            elmTable = jQuery('#atTable').bootstrapTable({
                idField: 'PersonelAttendanceBookId',
                send: 'never',
                rowStyle: function (row, index) {
                    rowClasses = '';
                    if (row.PersonelAttendanceBookId == null)
                        rowClasses += ' warning ';
                    return {classes: rowClasses};
                },

                showPaginationSwitch: false,
                pagination: true,
                pageSize: 50,
                pageList: "[10, 25, 50, 100, 'ALL']",
                classes: 'table table-hover',

                columns: [
                    {
                        field: 'PersonelAttendanceBookId',
                        visible: false
                    },
                    {
                        field: 'HealthUnit',
                        title: Joomla.JText._('COM_ELGPEDY_ORG_UNIT'),
                        align: 'left',
                        sortable: true,
                        cellStyle: function (value, row, index) {
                            if (row.HealthUnitId != healthUnit.value)
                                return {classes: ' text-success '};
                            else if (row.HealthUnitId != row.tmpRefHealthUnitId)
                                return {classes: ' text-danger '};
                            else
                                return {};
                        }

                    },
                    {
                        field: 'LastName',
                        title: Joomla.JText._('COM_ELGPEDY_LASTNAME'),
                        align: 'left',
                        sortable: true
                    },
                    {
                        field: 'FirstName',
                        title: Joomla.JText._('COM_ELGPEDY_FIRSTNAME'),
                        align: 'left',
                        sortable: true
                    },
                    {
                        field: 'FatherName',
                        title: Joomla.JText._('COM_ELGPEDY_FATHERNAME'),
                        align: 'left',
                        sortable: true
                    },
                    {
                        field: 'PersonelSpeciality',
                        title: Joomla.JText._('COM_ELGPEDY_SPECIALITY'),
                        sortable: true,

                    },
                    {
                        field: 'PersonelEducation',
                        title: Joomla.JText._('COM_ELGPEDY_EDUCATION'),
                        sortable: true,

                    },
                    {
                        field: 'finalStatus',
                        title: Joomla.JText._('COM_ELG_PEDY_PERSONEL_STATUS_ID'),
                        width: 200,
                        sortable: false,
                        cellStyle: function (value, row, index) {
                            cellClasses = ' ';
                            if (row.dutyStatusId == 8)
                                cellClasses += ' danger';
                            if (row.tmpRefHealthUnitId == healthUnit.value)
                                cellClasses += ' personel-status '

                            return {classes: cellClasses}
                        },

                    },
                    {
                        field: 'tmpRefHealthUnit',
                        title: Joomla.JText._('COM_ELGPEDY_UNIT_WORK'),
                        sortable: false,

                        cellStyle: function (value, row, index) {
                            cellCalsses = '';
                            if (row.HealthUnitId != healthUnit.value)
                                return {classes: ' text-success attendance-healthunit'};
                            else if (row.tmpRefHealthUnitId != healthUnit.value)
                                return {classes: ' attendance-healthunit text-danger '};
                            else
                                return {classes: ' attendance-healthunit'};

                        },

                        width: 280
                    },
                    {
                        field: 'PersonelAttendanceBookId',
                        title: '',
                        formatter: function (value, row, index) {
                            if (value == null) {
                                return '<button class="btn btn-default" onclick="saveAttendacyRow(' + index + ')" >Αποθήκευση</button>';
                            } else {
                                return '<button class="btn btn-default hidden" onclick="saveAttendacyRow(' + index + ')" >Αποθήκευση</button>';
                            }
                        }
                    }

                ]
            })            
            .on('page-change.bs.table', function (e, page, rowsPerPage) {
                var start = (page - 1) * rowsPerPage;
                makeEdits( attendancyTableObj.getData().slice(start, start + rowsPerPage - 1), healthUnit.value);
            })
            .on('after-sort.bs.table', function (e, name, order) {
                var start = (page - 1) * rowsPerPage;
                makeEdits( attendancyTableObj.getData().attendancyData.slice(start, start + rowsPerPage - 1), healthUnit.value);
            });
            
        }
        return elmTable;
    }; // end getTable
    
    
    
    /**
     * initializes statuses for personel and drity status for row
     * @param rawData
     * @returns Initialized Data
     */
    this.initializeData = function( rawData ){
        uncommittedNo = 0;
        return rawData.map(function(item){
            if ( item.PersonelAttendanceBookId != null) { // saved personel
                item.finalStatusId = item.dutyStatusId;
                item.finalStatus = item.dutyStatus;
                item.isDirty = false;
            } 
            else if (item.tmpRefHealthUnitId != healthUnit.value) { // unsaved personel from different untis
                item.finalStatusId = item.tmpStatusId;
                item.finalStatus = item.tmpStatus;
                item.isDirty = true;
            } 
            else { //rest unsaved personel rows
                item.finalStatusId = 7;
                item.finalStatus = 'Παρών';
                item.isDirty = true;
                uncommittedNo++;
            }
            return item;
        });
    };
    
//    
    this.getData = function () {
        return this.getTable().bootstrapTable('getData');
    };
    
    this.getHealthUnitId = function () {
        return document.getElementById('HealthUnitId').value;
    };
    
    this.getUncommittedNo = function () {
        return uncommittedNo;
    };
    
    this.setUncommittedNo = function (unocmmittedNoIn) {
        uncommittedNo = unocmmittedNoIn;
    };
    
    this.getRefDate = function() {
        return moment(document.getElementById('RefDate').value, 'D-M-YYYY').format('YYYYMMDD');
    };
    
    this.setAttendancyData = function( data ){
        attendancyData = data;
        this.getTable().bootstrapTable('load', attendancyData);
    };
    
    
    
    /**
     * total save
     */
    jQuery('#btnSave').on('click', function () {
        var data = attendancyTableObj.getData();
        if ( data.length > 0 ) {
            showAsBusy();
            var pD = {
                HealthUnitId: attendancyTableObj.getHealthUnitId(),
                RefDate: attendancyTableObj.getRefDate(),
                aD: data
            };
            jQuery.post('index.php?option=com_elgpedy&view=personelattendancybooksave&format=json&Itemid=144', JSON.stringify(pD),
                function (response) {
                    var huId = attendancyTableObj.getHealthUnitId();
                    var tb = attendancyTableObj.getTable();
                    var rData = attendancyTableObj.reformPersonelData(response.data, huId );
                    var data = attendancyTableObj.initializeData(rData);
                    tb.bootstrapTable('removeAll');                   
                    tb.bootstrapTable('load', data)
                    showUncommitted();
                    makeEdits( huId);
                    showDataArea();
                },
            'json');   
        }
    });
    
     this.getRowIndex = function(cell) {
        return jQuery(cell).parent().data('index');
    };
    
} // attendancy Table


function makeEdits(healthUnitId) {
    var tb = document.querySelector('#atTable tbody');
    var statuses = tb.querySelectorAll('tr td:nth-child(7)'); //.personel-status');
    var statusLen = statuses.length;

    for (var i = statusLen - 1; i > -1; i--) {
            jQuery(statuses[i]).editable({
                mode: 'inline', 
                showbuttons: false, 
                type: 'select', 
                source: optStatuses, 
                value: statuses[i].PersonelStatusId, send: 'never'
            })
            .on('save', function(e, params){
                var rowIndex = e.target.parentNode.rowIndex;
                var ind = attendancyTableObj.getRowIndex( e.target );
                var data = attendancyTableObj.getData();
                var row = data[ind];
                var newStatus = optStatuses.find(function(val){
                     return val.value === params.newValue;
                });
                row.finalStatusId = params.newValue;
                row.finalStatus = newStatus.text;
                attendancyTableObj.getTable().bootstrapTable('updateRow', { index: ind, row: row} );
                makeEdits (healthUnitId);
                tb.querySelector('tr:nth-child(' + (rowIndex) + ')  td:nth-child(9)').classList.remove('hidden');
                tb.querySelector('tr:nth-child(' + (rowIndex) + ')  td:nth-child(9) button').classList.remove('hidden');
                // e.target.parentNode.cells[8].children[0].classList.remove('hidden');
            });
          
    }


    jQuery('#atTable td.attendance-healthunit').editable({mode: 'inline', showbuttons: false, type: 'select2', source: optHealthUnits, value: healthUnitId, send: 'never',
        select2: {placeholder: 'Επιλέξτε μονάδα...'}
    })
    .on('save', function(e, params){
        var rowIndex = e.target.parentNode.rowIndex;
        var ind = attendancyTableObj.getRowIndex( e.target );
        var data = attendancyTableObj.getData();
        var row = data[ind];
        var newHealthUnit = optHealthUnits.find(function(val){
            return val.id === params.newValue;
        });
        row.tmpRefHealthUnitId = params.newValue;
        row.tmpRefHealthUnit = newHealthUnit.text;
        attendancyTableObj.getTable().bootstrapTable('updateRow', { index: ind, row: row} );
        makeEdits (healthUnitId);
        tb.querySelector('tr:nth-child(' + (rowIndex) + ') td:nth-child(9)').classList.remove('hidden');
        tb.querySelector('tr:nth-child(' + (rowIndex) + ')  td:nth-child(9) button').classList.remove('hidden');
    });
    

   
};


function makeEditableItem(item, attItem) {
    item.editable({mode: 'inline', showbuttons: false, type: 'select', source: optStatuses,  /**attItem.PersonelStatusId,**/ send: 'never'});
    item.on('save', function (e, params) {
        var r = jQuery.grep(optStatuses, function (e) {
            return e.value == params.newValue
        });
        if (r.length > 0) {
            var ind = getTargetIndex(e.currentTarget);
            attItem.finalStatusId = params.newValue;
            attItem.finalStatus = r[0].text;
            makeDataDirty(attItem, e.target.parentNode);
            return;
        }
        return false;
    });
}


function makeDataDirty(dataItem, tr) {
    if (!(dataItem.isDirty === true)) {
        dataItem.isDirty = true;
        var unc = attendancyTableObj.getUncommittedNo();
        attendancyTableObj.setUncommittedNo(++unc);
        showUncommitted();
        tr.lastChild.firstChild.classList.remove('hidden');
    }
}



