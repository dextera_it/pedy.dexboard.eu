
/**
 * Finds the correct size of the dashboard in this screen.
 */
function findDashboardSize() {
    var minHeight = 600;
    var maxHeight = 768;
    var minWidth = 1024;

    // The minus 100px leaves us room for the header/footer. Adjust as needed.
    var height = jQuery("body").height() - 100;

    if (height < minHeight) {
        height = minHeight;
    }
    if (height > maxHeight) {
        height = maxHeight;
    }

    var width = Math.round(height / (minHeight / minWidth));
    return {
        "width": width,
        "height": height
    };
}

/**
 * Resizes our viz.
 */
function resize() {
    var size = findDashboardSize();
    jQuery('#viz').width('90%');
    jQuery('#viz').height(size.height);
    jQuery('#viz iframe').attr("scrolling", "yes");
    jQuery('#viz iframe').css('overflow', 'hidden');
    jQuery('#viz iframe').width('90%');
    jQuery('#viz iframe').height(size.height + 20);
}

/**
 * Hook into the window resize event to resize when the user resizes the screen.
 */
jQuery(window).resize(function () {
    resize();
});


/***************************************************************/
function switchView(sheetName) {
	workBook = viz.getWorkbook();
	workBook.activateSheetAsync(sheetName);
}

function showOnly(filterName, values) {
	sheet = viz.getWorkbook().getActiveSheet();
	if(sheet.getSheetType() === 'worksheet') {
		sheet.applyFilterAsync(filterName, values, 'REPLACE');
	} else {
		worksheetArray = sheet.getWorksheets();
		for(var i = 0; i < worksheetArray.length; i++) {
			worksheetArray[i].applyFilterAsync(filterName, values, 'REPLACE');
		}
	}
}

function alsoShow(filterName, values) {
	sheet = viz.getWorkbook().getActiveSheet();
	if(sheet.getSheetType() === 'worksheet') {
		sheet.applyFilterAsync(filterName, values, 'ADD');
	} else {
		worksheetArray = sheet.getWorksheets();
		for(var i = 0; i < worksheetArray.length; i++) {
			worksheetArray[i].applyFilterAsync(filterName, values, 'ADD');
		}
	}	
}