var elgClickedButton = null;
var removeDocRow = null;
jQuery(document).ready(function(){
		
    jQuery('#elgdata tbody td.editable-text').editable( //common editables
    {
            url: prolepsisPhysioTherapistSaveUrl,
            type: 'text',
            pk: 1,
            name: 'pk',			
            mode: 'inline',
            showbuttons: false,
            savenochange: true,
            title: 'Click to edit',


            params: function(params){
                    var dataSub = {};				
                    dataSub.hid = this.id;
                    var t = dataSub.hid.replace('elgmedtid','');
                    dataSub.actid = t.charAt(0);
                    dataSub.mtid = t.replace(dataSub.actid + '-', '');
                    dataSub.Quantity = params.value;
                    dataSub.RefDate =moment(document.getElementById('RefDate').value, 'D-M-YYYY').format('YYYYMMDD');
                    dataSub.HealthUnitId = document.getElementById('HealthUnitId').value;
                    return dataSub;
            },
            validate: function(value) {
                    if(isNaN(value.replace(' ', ''))){
                            jQuery('#modalValidate').modal('show');
                            return '-';
                    }

            },
            success: elgShowResultMessage		
    });	//end of common editables	
		
        var trs = document.querySelectorAll('table.medicalTransaction-7 tbody tr');
        for (var i = trs.length -1; i >= 0; i --) {
                initPhysioTherapistRow( trs[i] );
        }		

        var addDBts = document.querySelectorAll('button.addDoctor'); 
        for( var i = addDBts.length -1; i >=0; i -- ){
                addDBts[i].addEventListener('click',  addDoctor);
        } 

});
	
	function initPhysioTherapistRow( row ) {
		/*
		* row.children:
		* 0: doctor name
		* 1: amka 
		* 2: insurance
		* 3: patient origination
		* 4: Medical Type (peristatiko)
		* 5: Municipality
		* 6: Quantity
		* 7: [0]: add button, [1]: remove buton
		**/
		jQuery( row.children[1] ).editable( {mode: 'inline', showbuttons: false, type: 'text', send: 'never', emptytext: '', value:''}).editable('setValue', row.children[1].textContent);
		jQuery( row.children[2] ).editable( {mode: 'inline', showbuttons: false, type: 'select', source: attr1, send: 'never', emptytext: '', value:''}).editable('setValue', row.children[2].textContent);
		jQuery( row.children[3] ).editable( {mode: 'inline', showbuttons: false, type: 'select', source: attr2, send: 'never', emptytext: '', value:''} ).editable('setValue', row.children[3].textContent);	
		jQuery( row.children[4] ).editable( {mode: 'inline', showbuttons: false, type: 'select', source: meds, send: 'never', emptytext: '', value:''} ).editable('setValue', row.children[4].textContent).on('save', incidentTypeChanged);	
		if ( row.children[5].textContent !== '' ) {
			jQuery( row.children[5]).editable( {mode: 'inline', showbuttons: false, type: 'select', source: municipalities, send: 'never', emptytext: '', value: ''} ).editable('setValue', row.children[5].textContent );	
		}
		makePhysioTherapistQuantityEditable( row.children[6] );
		row.children[7].children[0].addEventListener('click', moreIncidents);
		row.children[7].children[1].addEventListener('click', removeDoctorAsk);
	}
		
	function incidentTypeChanged(e, editableData) {
		if ( editableData.newValue === '50' || editableData.newValue === '49' ) {
			jQuery(e.target.parentNode.querySelectorAll( 'td' )[4]).editable( {mode: 'inline', showbuttons: false, type: 'select', source: municipalities, send: 'never', emptytext: '', value: ''} ).editable('setValue', '').editable('show');	
		}
		else {
			var tds = e.target.parentNode.querySelectorAll( 'td' )
			jQuery( tds[4] ).editable('destroy');
			tds[4].textContent = '';
		}
	}	
	
	function moreIncidents( e ) {
		var row = e.target.parentNode.parentNode;
		var newRow = row.cloneNode( true );
                
                newRow.dataset.id = '';
                var children = newRow.children;
                var msg = children[0].querySelector('small');
                if ( msg !== null ) children[0].removeChild( msg );
                
                children[1].textContent = '';
				children[2].textContent = '';
                children[3].textContent = '';
                children[4].textContent = '';
                children[5].textContent = '';
				children[6].textContent = 0;
                for (var i = 1; i <7; i ++) {
                    if ( children[i].classList.contains('warning') ) continue;
                    children[i].classList.remove('success');
                    children[i].classList.add('warning');
                    children[i].style.backgroundColor = '';
                }
                initPhysioTherapistRow( newRow );
		row.parentNode.insertBefore( newRow, row.nextSibling);
	}	
	
	function makePhysioTherapistQuantityEditable( cell )
	{
		jQuery( cell ).editable(
		{
			url: prolepsisPhysioTherapistSaveUrl,
			type: 'text',
			pk: 1,
			name: 'pk',			
			mode: 'inline',
			showbuttons: false,
			savenochange: true,
			emptytext: '',
			title: '',
                        
			params: function(params){
				return getDataToSubmit( this, params );
			},
			
			validate: function(value) {
				var val = parseInt( value.replace( / /g, '') );
                                if( isNaN( val ) 
					|| ! requiredCheck( this ) 
					|| ! isRowUnique( this )
                                        || ( val< 0 || val > 1 ) 
				) {
					jQuery('#modalValidate').modal('show');
					
					return '-';
				}
                        },
                        
			success: function(xhr) {
				elgShowResultMessage2 (cell, xhr);
			}				
		}).editable('setValue', cell.textContent);
	}	
	function requiredCheck( cell ) {
		var row = cell.parentNode;
		if ( getPatientInsurance( row ) === '' ) return false;
		if ( getPatientOrigination( row ) === '' ) return false;
		var medicalType = getMedicalTypeId( row ); 
		if ( medicalType === '' ) return false;
		if ( ( medicalType === '45' || medicalType === '46' ) &&  getMunicipality( row ) === '' ) return false;
	
		return true
	}
	function isRowUnique( cell ) {
		var row = cell.parentNode;
		var tbody = row.parentNode;
		var isOk = true;
		var trows = tbody.children;
		for ( var i = trows.length - 1; i >=0; i -- ) {
			if ( trows[i] === row ) continue;
			if ( 
				trows[i].children[0].dataset.id === row.children[0].dataset.id
				&& trows[i].children[1].textContent === row.children[1].textContent
				&& trows[i].children[2].textContent === row.children[2].textContent
				&& trows[i].children[3].textContent === row.children[3].textContent
				&& trows[i].children[4].textContent === row.children[4].textContent
				&& trows[i].children[5].textContent === row.children[5].textContent	
				) {
					isOk = false;
					break;
			}
			
		}
		return isOk;			
	}
	
	function getDataToSubmit( trg, params ){
		var dataSub = {};				
		var row = trg.parentNode;
		dataSub.id = row.dataset.id;
		dataSub.actid =  row.parentNode.dataset.id;
		dataSub.pid = row.children[0].dataset.id;
		dataSub.Quantity = params.value;
		dataSub.RefDate =moment(document.getElementById('RefDate').value, 'D-M-YYYY').format('YYYYMMDD');
		dataSub.HealthUnitId = document.getElementById('HealthUnitId').value;
		dataSub.mtid = getMedicalTypeId( row );
		dataSub.pai = getPatientInsurance ( row );
		dataSub.pao = getPatientOrigination ( row );
		dataSub.munic = getMunicipality ( row );
		dataSub.amka = row.children[1].textContent;
		return dataSub;
	}
	
	function getDDValue( cell ) {
		return jQuery( cell ).editable('getValue', true);
	}		
	
	function getPatientInsurance( row ) {
		return getDDValue( row.children[2] ); // return jQuery( cell ).editable('getValue', true);
	}
	
	function getPatientOrigination( row ) {
		return getDDValue( row.children[3] ); //return jQuery( cell ).editable('getValue', true);
	}
	
	function getAmka( row ) {
		return row.children[1].textContent; //return jQuery( cell ).editable('getValue', true);
	}
	
	function getMunicipality( row ) {
		try {
			return getDDValue( row.children[5] ); //return jQuery( cell ).editable('getValue', true);
		}
		catch(e) {
			return '';
		}
	}
	
	function getMedicalTypeId( row ) {
		return getDDValue( row.children[4] ); //  jQuery( cell ).editable('getValue', true);
	}
	
	
	
	function removeDoctorAsk(e) {
		
		removeDocRow = e.target.parentNode.parentNode;
		document.querySelector('#remDocModal .modal-body' ).innerHTML = '<strong>' + removeDocRow.children[0].textContent + '</strong><br />' 
		+ ( removeDocRow.children[1].textContent === '' ? '' : removeDocRow.children[1].textContent) 
		+ (removeDocRow.children[2].textContent === '' ? '' :  ', ' + removeDocRow.children[2].textContent)  
		+ ( removeDocRow.children[3].textContent === '' ? '': ', ' + removeDocRow.children[3].textContent)
		+ ( removeDocRow.children[4].textContent === '' ? '': ', ' + removeDocRow.children[4].textContent)
		+ ( removeDocRow.children[5].textContent === '' ? '': ', ' + removeDocRow.children[5].textContent);
		document.getElementById('remDocBt').style.visibility = 'visible';
		jQuery('#remDocModal').modal();
	}
	
	function hideModal(mdIdentifier) {
		jQuery(mdIdentifier).modal('hide');
	}
	
	function hideRemoveDocModal() {
		hideModal('#remDocModal');
	}
	
	function removeDoctor() {
		//var data = {};
		// var cells = removeDocRow.children; 
		jQuery.post(
		prolepsisRemoveDoctorUrl
		, 
		/**
		'pid=' + cells[0].dataset.id 
		+ '&HealthUnitId=' + document.getElementById('HealthUnitId').value 
		+'&RefDate=' +  moment(document.getElementById('RefDate').value, 'DD/MM/YYYY').format('YYYY-MM-DD') 
		+ '&mai=7'		
		+ '&mti=' + getMedicalTypeId( removeDocRow )		
		+ '&pai=' + getPatientInsurance ( removeDocRow ) 
		+ '&pao=' + getPatientOrigination ( removeDocRow ) 
		+ '&munic=' + getMunicipality ( removeDocRow ) 
		+ '&amka=' + getAmka ( removeDocRow ) **/
		'id=' + removeDocRow.dataset.id
		, removeDoctorResponse);
		
	}
	
	function removeDoctorResponse(data, status, xhr){
		document.getElementById('remDocBt').style.visibility = 'hidden';
		if(data.success == true) {
			var errors = '';
			if(typeof data.messages.error !== 'undefined') {
				errors += removeErrors(data.messages.error);
			}
			
			if(typeof data.messages.warning !== 'undefined') {
				errors += removeWarnings(data.messages.error);
			}
			if(errors !== '') {
				document.querySelector('#remDocModal .modal-body' ).innerHTML = errors;
				return;
			}
			else {
				removeSuccess(data.messages.success, data.data);
			}
			window.setTimeout(hideRemoveDocModal, 3000);
		}
		else {
			alert('Communication error');
		}
		
		
	}
	
	function removeSuccess(messages, data)
	{
		var html = '<p class="alert alert-success">';
		messages.forEach(function(val){
			html += val;
		});
		html += '</p>';
		document.querySelector('#remDocModal .modal-body' ).innerHTML =  html;
		var table = document.querySelector('table.medicalTransaction-7 tbody');
		table.removeChild( removeDocRow );
	}
	
	function removeErrors(errors) {
		var html = '';
		html += '<p class="alert alert-danger">';
		errors.forEach(function(val){
			html += val; 
		});
		html += '</p>';
		return html;
	}
	
	
	function removeWarnings(warnings) {
		var html = '';
		html += '<p class="alert alert-warning">';
		warnings.forEach(function(val){
			html += val; 
		});
		html += '</p>';
		return html;
		
	}	
	
	function addDoctor(e) {
		elgClickedButton = e.target;
		jQuery('#docModal').modal();
	}
	
	function insertDoctor() {
		var row = elgClickedButton.parentNode.parentNode.parentNode.parentNode.children[2].insertRow();
		var docsDrop = document.getElementById('docsDrop');
		var selectedDoctorId = docsDrop.value;
		var selectedDoctorName = docsDrop.options[docsDrop.selectedIndex].text;
		row.dataset.id = selectedDoctorId;
		row.innerHTML = '<th data-id="' + selectedDoctorId + '" >' + selectedDoctorName + '<td class="editable-text warning"></td><td class="editable-drop1 warning"></td><td class="editable-drop2 warning"></td><td class="editable-med-type warning"></td><td class="editable-munic warning"></td><td class="editable-doctor-quantity warning">0</td><td><button type="button" class="btn btn-default more-incidents">+</button><button type="button" class="btn btn-warning remove-record">-</button></td>';
		initPhysioTherapistRow( row );
		row.parentNode.insertBefore( row, row.nextSibling);
	}