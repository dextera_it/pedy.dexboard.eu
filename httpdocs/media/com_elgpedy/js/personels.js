var personels = (function (options) {
    var frmAction = function (id, row, ind) {
        return '<a class="text-info" href="index.php?option=com_elgpedy&view=personeledit&Itemid=' + options.elgItemid + '&id=' + id + '" >' + Joomla.JText._('COM_ELG_EDIT') + '</a>'
                + ' <span class="text-danger" onclick="personels.removePersonel(' + id + ',' + ind + ')" >' + Joomla.JText._('COM_ELG_DELETE') + '</span>';
    };

    var removePersonel = function (id, row) {

        jQuery('#personelDelete').on('show.bs.modal',
                function (e) {
                    var modal = jQuery(this);
                    this.querySelector('.btn-danger').setAttribute('href', 'index.php?option=com_elgpedy&controller=personeleditdelete&Itemid=' + options.elgItemid + '&id=' + id);
                    var tds = document.querySelectorAll('table.personels-table tbody tr:nth-child(' + (row + 1) + ') td ');
                    var text = '';
                    for (var i = 1; i < tds.length; i ++)
                        text += tds[i].textContent + ' ';
                    this.querySelector('.modal-body p').textContent = text;
                }
        );
        jQuery('#personelDelete').modal('show');
    };

    return {
        frmAction: frmAction,
        removePersonel: removePersonel
    };

}(elgPeronelsOptions));
