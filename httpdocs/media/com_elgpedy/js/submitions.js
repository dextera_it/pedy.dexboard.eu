
var elgSubSections = {};
var elgMessages = new Array();
var elgOldVal = null;
var elgEditOptions= {tooltip: 'Κλικ για επεξεργασία' };
jQuery(document).ready(function(){
	jQuery('#clinicTransaction tbody td').each(function(index, item){
		elgSubSections[item.id] = item;
	});
	
	jQuery('#medicalTransaction tbody').each(function(index, item){
		elgSubSections[item.id] = {};
		jQuery(item).find('td').each(function(indexin, itemin){
			elgSubSections[item.id][itemin.id] = itemin;
		});
	});
	
	jQuery('#prolepsisTransaction tbody td').each(function(index, item){
		elgSubSections[item.id] = item;
	});
	
        jQuery('table.clinicTransaction tbody td').each(function(index, item){
            elgSubSections[item.id] = item;
        });
});

function checkState(trg) {
    jQuery(trg).siblings('input[name="state"')
}

function elgGetFormData(){
  
        if(elgCheckChrono()) {
            //jQuery('.elg .pedy-form-data').slideUp();
            //jQuery('.elg div.loading-indicator').show();
            showAsBusy();
            if(elgview !== 'personelscommittees') {    
                jQuery('.form_datetime input').each(function(index, item){item.value= elgReformDate(item.value);});
                var url = unitsUrls[ jQuery('#HealthUnitId').val() ];
              
                var form = document.getElementById('frmRedirect');
                
            form.setAttribute('action',url );
                document.getElementById('frmRedirect').submit();
            }
            else {
				//$('#personelsComittees').fullCalendar('gotoDate',  document.getElementById('RefYear') + '-' + document.getElementById('RefMonth') ;
				$('#personelsComittees').fullCalendar('removeEvents');
                $('#personelsComittees').fullCalendar('gotoDate', [jQuery('#RefYear').val(), jQuery('#RefMonth').val() -1, 1]);                
            }
            
        }
    
}

function elgReformDate(inDate)
{
	return  moment(inDate, 'D/M/YYYY').format('YYYYMMDD');
}

function elgShowData(response) {    
    if(response.success == true) {
        if(response.data.data)
        {
            switch(response.data.mc)
            {
                case 2: elgFillProlepsisFormData(response.data.data);
                break;
                case 3: elgFillMedicalFormData(response.data.data);
                break;
                case 4: elgFillMotherChildCare(response.data.data)
                break;
                case 5: elgFillPsychoChild(response.data.data)
                break;
                case 'r1': elgFillReportClinical(response.data.data);
                break;
                default: elgFillClinicFormData(response.data.data);
            }
            
        }
        else
        {
            clearDataForm();
           // jQuery('#elgActionType').text(Joomla.JText._('COM_ELG_PEDY_NEW_SUBMITION'));
        }
        if(response.data.checker)
        {
			var item = jQuery('#checkerValue');
			item.text(response.data.checker);
			if(response.data.checker > 0) {
				
				elgAddClass(item, 'success');
			}
			else {
				elgAddClass(item, 'success');
			}
		}
      // jQuery('#elgActionType').text('');
    }
    else
    {
        jQuery('#elgActionType').text(response.message); 
    }
    showDataArea();
}

function showDataArea() {
    jQuery('.elg div.loading-indicator').hide();
    jQuery('.elg .pedy-form-data').slideDown();
}

function showAsBusy () {
    jQuery('.elg .pedy-form-data').slideUp();
    jQuery('.elg div.loading-indicator').show();
}

function elgFillClinicFormData(data) {
	var t, pid, cid;
	jQuery.each(elgSubSections, function(index, item){
		t = index.replace('elginc', '');
		pid = t.substring(0, t.indexOf('-')); //t.charAt(0);
		if(isNaN(pid)) return;
		cid = t.replace(pid + '-', '');
		if(data[pid] == null){
			item.innerHTML = 0;
			elgAddClass(jQuery(item), 'warning');
		}
		else {
			if(data[pid][cid] == null) {
				item.innerHTML = 0;
				elgAddClass(jQuery(item), 'warning');
			}
			else {
                            item.innerHTML = data[pid][cid];
                            if(data[pid][cid] > 0 )
                            {
                                    elgAddClass(jQuery(item), 'success');
                            }
                            else
                            {
                                    elgAddClass(jQuery(item), 'warning');
                            }
			}
		}
	});   
	
}

function elgFillPsychoChild(data) {
	var t, pid, cid,  parts;
        elgZeroSubSections();
	jQuery.each(elgSubSections, function(index, item){
            jQuery.each(data,function(index, item){
                if(elgSubSections['elginc' + item.ClinicDepartmentId + '-' + item.ClinicTypeId + '-' + item.ClinicIncidentId])
                {
                    elgSubSections['elginc' + item.ClinicDepartmentId + '-' + item.ClinicTypeId + '-' + item.ClinicIncidentId].innerHTML = item.Quantity;
                    elgAddClass(elgSubSections['elginc' + item.ClinicDepartmentId + '-' + item.ClinicTypeId + '-' + item.ClinicIncidentId], 'success');

                }
            });
        });
}


function elgFillMotherChildCare(data)
{
	elgFillClinicFormData(data.data.clinics);
	elgFillProlepsisFormData(data.data.prolepsis);	
}

function elgFillMedicalFormData(data)
{
	var l1, l2;
	jQuery.each(elgSubSections, function(index1, item1){
		jQuery.each(item1, function(index2, item2) {
			l2= data[index2.replace('elgmtid', '')];
			if(l2 != null) {
				item2.innerHTML = data[index2.replace('elgmtid', '')]['Quantity'];
				elgAddClass(jQuery(item2),'success');
			}
			else {
				item2.innerHTML = 0;
				elgAddClass(jQuery(item2), 'warning');
			}
		});
	});   
}


function elgFillProlepsisFormData(data)
{
	var t, actid, medtid, dv;
	jQuery.each(elgSubSections, function(index1, item1){
		t = index1.replace('elgmedtid', '');
		actid = t.substring(0, t.indexOf('-')); //t.charAt(0);
		if(isNaN(actid)) return;
		medtid = t.replace(actid + '-', '');
		if(data[actid] == null){
			item1.innerHTML = 0;
			elgAddClass(jQuery(item1), 'warning');
		}
		else {
			if(data[actid][medtid] == null) {
				item1.innerHTML = 0;
				elgAddClass(jQuery(item1), 'warning');
			}
			else {
				item1.innerHTML = data[actid][medtid];
				if(data[actid][medtid] > 0 )
				{
					elgAddClass(jQuery(item1), 'success');
				}
				else
				{
					elgAddClass(jQuery(item1), 'warning');
				}
			}
		}
	});   
}


function clearDataForm()
{
    if(elgview !== 'personelscommittees') {
        jQuery('.elg input[name="Quantity[]"]').val('');
    }
}

function getSubmitedData() {
    jQuery.ajax({
        url: 'index.php?option=com_elgpedy&view=' + elgview + '&format=json&HealthUnitId=' + document.getElementById('HealthUnitId').value + '&RefYear=' + document.getElementById('RefYear').value + '&RefMonth=' + document.getElementById('RefMonth').value + '&Itemid=' + elgItemid,
        success: elgShowData,
        dataType: 'json'
    });
}

function elgCheckChrono(){
	//if(document.getElementById('RefYear').value === '' || document.getElementById('RefMonth').value === '')
    // if(document.getElementById('RefDate').value === '')
	var ret = true;
    jQuery('.form_datetime input').each(function(index, item){
		if(item.value === '') ret =  false;
	});
	return ret;
}

function elgShowResultMessage(data, textStatus)
{
	var same = jQuery('#' + data.data.hid);
	var textClass, tdClass;
	if(data.messages)
	{
            for (var key in data.messages) 
            {
                    if(key == null || key == 'message' ) {
                            textClass = 'text-success';
                            tdClass = 'success';
                            if(data.data.Quantity > 0 ) {
                                tdClass= 'success';
                            }
                            else {
                                tdClass= 'warning';
                            }
                    }
                    else {
                            if(key === 'error')
                            {
                                tdClass = 'danger';
                                textClass = 'text-danger';
                            }
                            else
                            {
                                textClass = 'text-'+ key;
                                tdClass = key;
                              //  sameClass=key;
                            }
                    }
                    var elm = jQuery('<small class="'+ textClass + '" ><br />' + data.messages[key] + '</small>');
                    elm.hide().appendTo(same.prev('th')).fadeIn().delay(3000).fadeOut();
            }
            elgAddClass(same, tdClass);
            same.css('background-color','');
	}
}

function elgZeroSubSections()
{
    jQuery.each(elgSubSections, function(index, item){
        item.innerHTML = 0;
        elgAddClass(item, 'warning');
    });    
}

function elgAddClass(elm, className)
{
	elgClearTDClasses(elm);
	elm.addClass(className);
}

function elgClearTDClasses(elm)
{
	var tdClasses= ['error', 'warning', 'success', 'notice', 'info', 'danger'];
	for(var i = tdClasses.length -1; i >=0; i --)
	{
		elm.removeClass(tdClasses[i]);
	}
}

function elgClearEditable() {
	jQuery('#elgdata tbody td').on('shown', function(e, editable) {	editable.input.clear()});
}



function elgShowResultMessage2(elmSame, data)
{
	//var same = jQuery(elmMes);
	var textClass, tdClass;
	if(data.messages)
	{
            for (var key in data.messages) 
            {
                    if(key == null || key == 'message' ) {
                            textClass = 'text-success';
                            tdClass = 'success';
                            if(data.data.Quantity > 0 ) {
                                tdClass= 'success';
                            }
                            else {
                                tdClass= 'warning';
                            }
                    }
                    else {
                            if(key === 'error')
                            {
                                tdClass = 'danger';
                                textClass = 'text-danger';
                            }
                            else
                            {
                                textClass = 'text-'+ key;
                                tdClass = key;
                            }
                    }
                    var elm = jQuery('<small class="'+ textClass + '" ><br />' + data.messages[key] + '</small>');
                    elm.appendTo( elmSame.parentNode.children[0] ).fadeIn().delay(3000).fadeOut();
            }
            var cl = elmSame.classList;
            cl.remove('warning');
            cl.remove('success');
            cl.add(tdClass);
            elmSame.style.backgroundColor = '';
	}
}

function elgShowMessages(messages,parentNode, timeout) {
    let messagesElement = parentNode.querySelector('div.messages-element');
    if ( messagesElement === null ) {
        messagesElement = document.createElement('div');
        messagesElement.setAttribute('class', 'messages-element');
        parentNode.appendChild( messagesElement );
    }
    Object.keys( messages ).forEach( function ( messageType ){
        messages[messageType].forEach( function( messageText){
            elgAppendMessage(messageType, messageText, messagesElement);
        });
    });
    if ( typeof timeout === 'undefined' )
        timeout = 3000;
    if ( timeout > 0 )
        setTimeout( elgClearNodeChildren, timeout, messagesElement);
}

function elgAppendMessage(messageType, messageText, messagesElement) {
   let messageElement = document.createElement('p');
    messageElement.setAttribute('class', 'text-' +messageType);
    messageElement.textContent = messageText;
    messagesElement.appendChild(  messageElement );
     
}


function elgClearNodeChildren(parentNode) {
    while ( parentNode.firstChild)
        parentNode.removeChild(parentNode.firstChild);
} 


