var personelEditMod = (function() {
        // validatoin states: 0: not validated yet, value is as it cames from db, 1: Validated and there are error,
        // 2: Validated and it is ok, 3: Is validating wright now.
        var valid = {
            trn: 0,
            amka: 0
        };
       
	var options = {
            ssnPending: false, 
            btSbPersonel: null, 
            
            srPerUrl: '', 
            personelId: 0,
            frmPersonnel: null,
            onSubmit: true,
            messages : [
                'Επιλέξτε μια τιμή',
                'Συμπληρώστε με τη μορφή HH/MM/EEEE',
              'To ΑΦΜ που δηλώσατε υπάρχει ήδη',
              'Το ΑΜΚΑ που δηλώσατε υπάρχει ήδη'
            ]
        };
        
        var init = function( optionsIn ) {
		document.getElementById('frmMovement').addEventListener('submit', movementsSubmited);
		if ( optionsIn.hasOpenMovement ) {
			document.getElementById('btSaveMovement').style.display = 'inline';
		}
		else {
			document.getElementById('btAddMovement').style.display = 'inline';
			document.getElementById('btAddMovement').addEventListener('click', newMovementClicked);
		}
		options.srPerUrl = optionsIn.srPerUrl;
                options.personelId = optionsIn.personelId.toString();
                jQuery('#RefHealthUnitId').select2({width: '100%'});
		jQuery('#HealthUnitId2').select2({width: '100%'}).val( document.getElementById('HealthUnitId').value ).trigger('change');
                options.frmPersonnel = document.getElementById('frmPersonnel');
                options.frmPersonnel.addEventListener('submit', personelSubmited);
                document.getElementById('trn').addEventListener('change', trnChanged);
                document.getElementById('amka').addEventListener('change', amkaChanged);
                options.btSbPersonel =  document.getElementById('btSbPersonel');
                
                
                
	};
        
	var movementsSubmited = function (e) {
		e.preventDefault();
		if ( validateMovements ( e.target ) ) {
			e.target.submit();
		}
	}
			
	var validateNewMovement = function( form ) {
		var elms, i, date = '', isOk = 1, val ;
		
		elms = form.querySelectorAll( 'foot select[name="HealthUnitId"]');
		for ( i = elms.length -1; i >= 0; i -- ) {
				if( elms[i].value == '' ) {
					isOk = 0;
					showInputAsInvalid( elms[i], options.messages[0] );
				}
                                else {
                                    showInputAsValid(elms[i]);
                                }
		}
		
		elms = form.querySelectorAll( 'tfoot select[name="RefHealthUnitId"]');
		for ( i = elms.length -1; i >= 0; i -- ) {
				if( elms[i].value == '' ) {
					isOk = 0;
					showInputAsInvalid( elms[i], options.messages[0] );
				}
                                else {
                                    showInputAsValid(elms[i]);
                                }
		}
		
		elms = form.querySelectorAll( 'tfoot select[name="PersonelStatusId"]');
		for ( i = elms.length -1; i >= 0; i -- ) {
			
			if( elms[i].value == ''  ) {
				isOk = 0;
				showInputAsInvalid( elms[i], options.messages[0]);
			}
                        else {
                            showInputAsValid(elms[i]);
                        }
		}
		
		elms = form.querySelectorAll( 'tfoot input[name="RefUnitStartDate"]');
		for ( i = elms.length -1; i >= 0; i -- ) {
			date = moment( elms[i].value, 'DD/MM/YYYY');
			if( elms[i].value.replace(/ /g, '') === '' || !date.isValid()) {
				isOk = 0;
				showInputAsInvalid(elms[i], options.messages[1]);
			}
                        else {
                            showInputAsValid(elms[i]);
                        }
		}
		
		var elms = form.querySelectorAll( 'tbody input[name="RefUnitEndDate"]');
		for ( i = elms.length - 1; i > -1; i -- ) {
			val = elms[i].value.replace(/ /g,'');
                        var endDate = moment( val, 'DD/MM/YYYY');
			if ( val !== '' || !endDate.isValid()) {
				date = moment(elms[i].value, 'DD/MM/YYYY');
				if ( !date.isValid() )
					isOk = 0;
			}
			else {
				elms[i].value = val;
                                showInputAsValid(elms[i]);
			}
		}
		
		return isOk;
	};
	
	var validateEndMovement = function ( form ) {
		var elms = form.querySelectorAll( 'tbody input[name="RefUnitEndDate"]');
		var isOk = 1, endDate = '';
		for ( var i = elms.length - 1 ; i >=0; i -- ) {
			endDate = moment( elms[i].value, 'DD/MM/YYYY');
			
			if ( elms[i].value.replace(/ /g, '' ) === '' || !endDate.isValid() ) {
				isOk[0] = 0 ;
				showInputAsInvalid(elms[i], options.messages[1]);
				
			}
			else {
				elms[i].value = endDate.format('YYYY-MM-DD');
                       	}
		}
		return isOk;
	};
	
	var validateMovements = function ( form ) {
		if ( document.getElementById('newMovement').style.display === 'none' )
			return validateEndMovement( form );
		else
			return validateNewMovement( form );
	};
	
	var newMovementClicked = function(e) {
		var row = document.getElementById('newMovement');
		// Query('#HealthUnitId2').val( document.getElementById('HealthUnitId').value ).trigger('change');
		// row.children[0].children[0].value = document.getElementById('HealthUnitId').value;
		row.style.display = 'table-row';
		e.target.style.display = 'none';
		document.getElementById('btSaveMovement').style.display = 'inline';
	};
	
	var showInputAsInvalid = function( elm, message ) {
		clearInputMessages(elm);
                elm.style.borderWidth = '1px';
		elm.style.borderStyle = 'solid';
		elm.style.borderColor = 'red';
		elm.parentNode.insertAdjacentHTML('beforeend', '<p class="text-danger ">' + message + '</p>' );
	};
        
        var showInputAsValid = function(elm) {
            clearInputMessages(elm);
            elm.style.borderColor = '#ccc';  
          
        };
        
        var clearInputMessages = function(elm) {
            var pNode = elm.parentNode;
            var msgs = pNode.querySelectorAll('p');
            for ( var i = msgs.length -1 ; i > -1; i -- )
                pNode.removeChild( msgs[i] );
        };
                
        var trnChanged = function(e) {
            checkTrn(e.target);
        };
        
        var amkaChanged = function(e) {
            checkAmka(e.target);
        };
        
        var checkAmka = function ( trg ) {
            var toSubmit = (valid.amka === 3 ? true: false);
            if ( !jQuery.isNumeric( trg.value)|| trg.value.length !== 11 ) {
                 showInputAsInvalid( trg, Joomla.JText._('COM_ELG_PEDY_50006_FILL_WITH_N_NUMBERS').replace('%d', 11) );
                 valid.amka = 1;
                 return;
            }
            else {
                valid.amka = 2;
                showInputAsValid(trg);
            }
            trg.style.display = 'none';
            jQuery(trg).next().css('display', 'block');
            jQuery.get(options.srPerUrl, {amka: trg.value, all:1, statusid: '1.3'}, function(data){
                if (data.total > 0 ) 
                    valid.amka = 1;
                validationResults(options.messages[3], trg, data);
                if ( toSubmit )
                    submitForm();
            }, 'json' );            
        };
        
        var checkTrn = function( trg ){
            var toSubmit = (valid.trn === 3 ? true: false);
            if ( !jQuery.isNumeric( trg.value) || trg.value.length !== 9 ) {
                 showInputAsInvalid( trg, Joomla.JText._('COM_ELG_PEDY_50006_FILL_WITH_N_NUMBERS').replace('%d', 9) );
                 valid.trn = 1;
                 return;
            }
            else {
                valid.trn = 2;
                showInputAsValid(trg);
            }
            trg.style.display = 'none';
            jQuery(trg).next().css('display', 'block');
            jQuery.get(options.srPerUrl, {trn: trg.value, all: 1, statusId: '1.3'}, function(data) {
                if (data.total > 0 ) 
                    valid.trn = 1;
                validationResults( options.messages[2], trg, data);
                if ( toSubmit )
                    submitForm();
                
            }, 'json' );
           
        };
        
        var validationResults = function (message, elm, response) {
            var personels = response.rows;
            
            var othersData = personels.filter(function(val){
                return val.PersonelId !== options.personelId; 
            }); // removes personel itself from the list so as not to check himself as similar.
            
            var catPersonels = getPersonelsByCategory(othersData, response.huids);
            if ( catPersonels.editable.length > 0 || catPersonels.noEditable.length > 0 ) {
               // message= message + ' <i class="fa fa-arrow-circle-o-down" onclick="personelEditMod.toggleSimilar(this)" > </i>';
                showInputAsInvalid( elm, message );
                elm.parentNode.insertAdjacentHTML('beforeend'
                , '<p class="similar-links text-info">' 
                + getSimilarLinks(catPersonels.editable) 
                + '</p>'
                + '<p class="similar-links text-danger" >'
                + getSimilarNoLinks(catPersonels.noEditable) 
                +'</p>' );
            }
            if ( othersData.length > 0) {
//                message= message + ' <i class="fa fa-arrow-circle-o-down" onclick="personelEditMod.toggleSimilar(this)" > </i>';
//                showInputAsInvalid( elm, message );
//                elm.parentNode.insertAdjacentHTML('beforeend', '<p class="similar-links text-info hidden">' + getSimilarLinks(othersData) +'</p>')
            }
            else {
                showInputAsValid(elm);
            }    
            elm.style.display ='block';
            jQuery(elm).next().css('display','none');
        };
        
        var getSimilarLinks = function(similarData) {
            return similarData.reduce(function(text, val){
                if (val.statusId === '3' )
                    return text + '<del>' + getPersonelLink(val) + '</del><br />';
                else
                    return text + getPersonelLink(val) + '<br />';
            }, '');
        };
               
        var getSimilarNoLinks = function(similarData) {
            return similarData.reduce(function(text, val){
                if (val.statusId === '3' )
                    return text + '<del>' + getPersonelLink(val) + '</del> <--Κλικ για συμπλήρωση μονάδας (Ιστορικό Υπηρεσίας)<br />';
                else
                    return text + val.LastName + ' ' + val.FirstName +  ', '+ val.amka + ', ' +val.trn + ', ' + val.HealthUnit +'<br />';
            }, '');
//            return similarData.reduce(function(text, val){
//               return text + val.LastName + ' ' + val.FirstName +  ', '+ val.amka + ', ' +val.trn + ', ' + val.HealthUnit +'<br />'; 
//           }, '');
        };
        
         
        var getPersonelLink = function (val) {
            return '<a href="index.php?option=com_elgpedy&view=personeledit&Itemid=109&id=' + val.PersonelId + '" >'
               + val.LastName + ' ' + val.FirstName +  ', '+ val.amka + ', ' +val.trn + ', ' + val.HealthUnit + '</a>'; 
           
        };
        
        var getPersonelsByCategory = function(personels, healthUnits) {
            var res = { editable:[], noEditable: [] };
            personels.forEach(function(val){
                if( healthUnits.indexOf(val.HealthUnitId) > - 1 )
                    res.editable.push(val);
                else
                    res.noEditable.push(val);
            });
            return res;
        };
        
//        var toggleSimilar = function(trg) {
//            if ( trg.classList.contains('fa-arrow-circle-o-down') ) {
//                trg.parentNode.nextSibling.classList.remove('hidden');
//                trg.classList.remove('fa-arrow-circle-o-down');
//                trg.classList.add('fa-arrow-circle-o-up');
//            }
//            else {
//                trg.parentNode.nextSibling.classList.add('hidden');
//                trg.classList.remove('fa-arrow-circle-o-up');
//                trg.classList.add('fa-arrow-circle-o-down');
//            }
//        };
//          
        var personelSubmited = function(e) { 
            e.preventDefault();
            submitForm();
        };
        
        /**
         * submit forms and check for errors
         * @returns {undefined}
         */
        var submitForm = function(){
            
            if (valid.trn === 0 ) { //validate only if it now validated yet
                valid.trn = 3;
                checkTrn ( document.getElementById('trn') );
            }
            
            if (valid.amka === 0 ) { //validate only if it now validated yet
                valid.amka = 3;
                checkAmka ( document.getElementById('amka') );
              
            }
         
            var isOk = true;
            Object.keys( valid ).forEach(function(key){
                if (valid[key] === 1 ) { // there is error. 
                    showInputAsInvalid( options.btSbPersonel, Joomla.JText._('COM_ELGPEDY_VALIDATE_BEFORE_SUMIT') );
                    isOk = false;
                } 
                else if ( valid[key] === 3 ) { // validation has not finished yet.
                    return;
                }
            });
            
            if ( isOk )
              options.frmPersonnel.submit();
        };
        
	
	return {
		init: init,
                // toggleSimilar: toggleSimilar
	};
}());