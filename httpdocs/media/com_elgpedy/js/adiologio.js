jQuery('#PersonelId').select2({data: personelData, multiple:true});
        function formSubmitReport(){
            var msgf = '';
            var d1 = moment(document.getElementById('StartDate').value, 'DD/MM/YYYY');
            var d2 = moment(document.getElementById('EndDate').value, 'DD/MM/YYYY');    
            if(!d1.isValid()) 
                msgf += ' Lathos Dedomena imerominia enarxis\n';
            if(!d2.isValid()) 
                msgf += ' Lathos Dedomena imerominia lixi\n';
            
            if(msgf !== '') {
                alert('Problima filtron\n' + msgf);
                return false;
            }
            else {
                return true;
            }
        };
        
        function getPDF() {
            var f = document.getElementById('adiaFilters');
            f.setAttribute('target','_blank');
            f.setAttribute('action', location.href +'&format=pdf');
            f.submit();
            f.setAttribute('target','_self');
            f.setAttribute('action', '');
        }
var delId = 0;   
function confirmDeleteAdiologio(trg, id){
    delId = id;
    var row = trg.parentNode.parentNode;
    jQuery('.modal .modal-body').html('Είστε σίγουροι για τη διαγραφή της άδειας:<br/>' + row.cells[1].textContent + '<br />' + row.cells[2].children[0].textContent + ' ' + row.cells[3].textContent + '-' + row.cells[4].textContent);
    jQuery('#myModal').modal({show: true});
}
    
function deleteAdiologioRecord() {
    document.querySelector('#myModal div.modal-body').innerHTML = 'Παρακαλώ περιμένετε ...';
    var   xhr = new XMLHttpRequest();
    xhr.open('GET',  delUrl.replace('#', delId), true);
    xhr.responseType = 'json';
    xhr.send(null);    
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                if(!xhr.response.messages.error) {
                    var tb = document.getElementById('attendanceTable');
                    var rows = tb.rows;
                    for(var i = rows.length -1; i >=0; i --) {
                        if(rows[i].id === 'r' + delId)
                        {
                            tb.deleteRow(i);
                            break;
                        }
                    }
                }
                renderAppMessages(xhr.response, document.querySelector('#myModal div.modal-body'));

            }
        }
    };
}

/**
 * Contructs an html messahe string from a joomla json response.
 * @param {JSON} response The xhr response.
 * @param {node} messageArea The dom node where the message will be rendered.
 * @returns {String} The constacted html string.
 */
function renderAppMessages(response, messageArea){
    var msg='';
    clearChildren(messageArea);
    if(response.messages)
        Object.keys(response.messages).forEach(function(val){
            msg += '<div class="alert alert-' + val + '" ><a href="#" class="close" data-dismiss="alert">&times;</a>' + response.messages[val].join('<br />') + '</div>';
        });
    messageArea.innerHTML = msg;
}

function clearChildren(parent) {
    while(parent.firstChild) parent.removeChild(parent.firstChild);
}

         /**
        jQuery('#attendanceTable').bootstrapTable(
        {
            data: rafinaData,
            pagination: true,
            columns: [
                
                {
                    field: 'PersonelattendanceBookRafinaId',
                    title: 'A/A',
                    formatter: runningFormatter                    
                },
               
                {
                  field: 'LastName',
                  title: Joomla.JText._('COM_ELG_PEDY_PERSONEL'),
                  formatter: nameFormatter
                },
                {
                  field: 'PersonelStatus',
                  title: 'Αδεια'                 
                },
                {
                  field: 'StartDate',
                  title: 'Έναρξη' ,
                  formatter: dateFormatter
                },
                {
                  field: 'EndDate',
                  title: 'Λήξη' 
                },
                {
                  field: 'Duration',
                  title: 'Ημέρες <br>(Χωρίς αργίες)' 
                
                }
            ]
        });
        **/
       /**
        function nameFormatter(value, row) {
            return row.LastName + ' ' + row.FirstName;
        }
        
        function dateFormatter(val) {
            var d = moment(val);
            if(d.isValid) 
                return d.format('DD/MM/YYYY');
            else
                return d.format('');
        }
        
        function runningFormatter(value, row, index) {
            //return '<a href="' + editURL + '&id=' + value + '">' + (index + 1) + '</a>';
            return index + 1;
        }
        
        **/