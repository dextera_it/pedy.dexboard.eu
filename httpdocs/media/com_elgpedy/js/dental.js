var areaTypeElm;
document.addEventListener("DOMContentLoaded", function(){
    
    dentalConditions.forEach(function(curValue, index, arr){
       document.querySelector('dl.dental-who').innerHTML +=  '<dt>' + curValue[2] + '</dt><dd>' + curValue[1] + '</dd>';
    });
    toothTransaction.forEach(function(curValue, index, arr){
        document.querySelector('input[name="tooth[' + curValue[0] + ']"]').value = curValue[1]; 
    });
    areaTypeElms = document.querySelectorAll('input.area_type');
    var areasData = areas.map(function(area){return {id: area[0], text: area[2]};});
    var ar = jQuery("#area").select2({data: areasData, width:'80%'}).on("change", function(e){changeArea(e.val)});    
    jQuery('#school_level_id').select2({data: schoolLevels.map(function(item){ return { id: item[0], text:item[1] } ; }), width: '80%' })
            .on("change", function(e){
                changeArea(
                    jQuery("#school_id").val());
                });
    jQuery("#school_id").select2({data:[], width:'80%'}).on('change', function(e){ changeSchool(e.val) });
    jQuery("#school_class_id").select2({data: [], width:'80%'});
    jQuery('#nationality_id').select2({width: '80%'});
    jQuery("#info_level").select2({width: '80%'});
    
    
    var dentalConditionsData = dentalConditions.map(function(elm){return {id: elm[0], text: elm[2]};});
    jQuery('.tooth').each(function(i,e){
        var el = jQuery(e);
       el.select2({data: dentalConditionsData,width:'6rem', allowClear: true, placeholder: ' '}).on('change', function(e){ changeTooth(e) });
       el.select2('val', e.value);            
      
    });  
    
   
    jQuery('.form_datetime').datetimepicker({
        format: "dd/mm/yyyy",
        startView: 2,
        minView: 2,
        autoclose: true,
        todayHighlight: true,
        language: 'el',
        forceParse: false,
        endDate: new Date()
    })
    .on('hide',function(e){ 
        var a = jQuery(this);
        setTimeout(function(){a.show();},2);
    });
    
    var school_id = document.getElementById('school_id').value;
    if(school_id > 0) {
        var school = schools.filter(function(elm){return elm[0]===school_id;});
        ar.select2('val', school[0][2]);
        changeArea(school[0][2]);
        changeSchool(school_id);
    }
    if(document.getElementById('area').value > 0) {
        changeArea(document.getElementById('area').value);
    }
});


function changeTooth(e) {
    if(e.val === '') return;
    var dId = document.getElementById(e.target.id).id.replace("t","");
    var relId = 0;
    for(var i = dentalTooths.length -1; i >= 0; i --){
        if(dentalTooths[i][0] === dId) {
            relId = dentalTooths[i][1];
            break;
        }
    }
    if(relId > 0) {
        if(document.getElementById('t' + relId).value > 0){
            alert('Εχετε ήδη καταχωρήσει το δόντι ' + relId);
        }
    }
}


function changeArea(selIdArea) {
    
    var curArea = areas.filter(function(item){ return item[0] == selIdArea });
    var schoolFiltered = schools;
    var selSchoolLevelId = jQuery('#school_level_id').val();
    
    try {
       var curAreaType = curArea[0][1];
    }
    catch(ex) {
        var curAreaType = 0;
    }
    for( var i =0; i < 3; i ++ ) {
        if(areaTypeElms[i].value == curAreaType){
            areaTypeElms[i].checked = true;
        }
        else {
            areaTypeElms[i].checked = false;
        }
    }
    
    var schoolFiltered = schools.filter(function(element){
        return element[2] == selIdArea;
    });
    
   if ( schoolFiltered.length === 0 ) {
       schoolFiltered = schools;
   }
   
   if ( selSchoolLevelId > 0 ){
       schoolFiltered = schoolFiltered.filter(function(element){
        return element[1] === selSchoolLevelId;
    });
   }
    
   //if ( schoolFiltered.length > 0 ){
        var schoolData = schoolFiltered.map(function(school){ return {id: school[0], text: school[3]}; });
   // }
   // else{
   //    var schoolData = schools.map(function(school){ return {id: school[0], text: school[3]}; });
   // }     
    jQuery("#school_id").select2({data: schoolData, width: '80%'});
    jQuery("#school_id").select2('val',schoolData[0].id);
    changeSchool(schoolData[0].id);
    document.getElementById('school-container').style.display = 'block'; 
    document.getElementById('area-type-container').style.display = 'block'; 
}

function changeSchool(selIdSchool){
    var curSchool = schools.filter(function(val){ return val[0] == selIdSchool});
    try {
        var classesData = levelClasses.filter(function(val){ 
            return  val[0] == curSchool[0][1];            
        }).map(function(item){return {id: item[2], text:item[3]};});
    }
    catch(e) {
        var classesData = [];
    }
    finally {
        jQuery("#school_class_id").select2({data: classesData, width: '80%'});
        document.getElementById('class-container').style.display = 'block';
    } 
    
    
   
}