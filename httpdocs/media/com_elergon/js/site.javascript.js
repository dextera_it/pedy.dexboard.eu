elgsJS = {
    /**
     * Show a busy indicator for a given section of page
     * @param {String} sectionSelector The css selector of the page's section we want show as busy. This section will become hidden.
     * @param {String} busySelector The css selector of the section of the page that hides thw bysy indicator animation. This animation will becaome visilble.
     */
    showAsBusy: function (sectionSelector, busySelector) {
        var sSel = typeof sectionSelector !== 'undefined' ? sectionSelector : '.elg';
        var bSel = typeof busySelector !== 'undefined' ? busySelector : '.loader-128';
        document.querySelector(sSel).style.display = 'none';
        document.querySelector(bSel).style.display = 'block';
    },
    /**
     * Hides a busy indicator for a given section of page and makes the contect of this sections visibnle to the user.
     * @param {String} sectionSelector The css selector of the page's section we want to reveal. This section was hidden and was replaced by the busy indicator.
     * @param {String} busySelector The css selector of the busy indicator animation. This animation will dissapear and it will bew replaced by actual content.
     */
    showAsReady: function (sectionSelector, busySelector) {
        var sSel = typeof sectionSelector !== 'undefined' ? sectionSelector : '.elg';
        var bSel = typeof busySelector !== 'undefined' ? busySelector : '.loader-128';
        document.querySelector(bSel).style.display = 'none';
        document.querySelector(sSel).style.display = 'block';
    },
    /**
     * Contructs an html message string from a joomla json response.
     * @param {JSON} response The xhr response.
     * @param {string} messageType It is case sensitive. Currently allowed : HTMLBootstrap.
     * @param {object} options Options for the message type function. This options object will be passed on any custom function asas the second argument. Custom functions must adhere to the interface (response, options). 
     * @returns {String} The constacted html string.
     */
    renderAppMessages: function (response, messageType, options) {
        var f = 'renderAppMessage' + messageType;
        try {
            window[f](response, options);
        }
        catch (e) {
            alert(e.message);
        }
    },
    /**
     * Renders a message as a bootstrap alert.
     * @param {JSON} response The remote json response that contains the messages property.
     * @param {object} options An object containing the messageArea node and the timeout id any.
     */
    renderAppMessageHTMLBootstrap: function (response, options) {
        elgsJS.clearChildren(options.messageArea);
        var msg = '';
        if (response.messages)
            Object.keys(response.messages).forEach(function (val) {
                msg += '<div class="alert alert-' + val + '" ><a href="#" class="close" data-dismiss="alert">&times;</a>' + response.messages[val].join('<br />') + '</div>';
            });
        options.messageArea.innerHTML = msg;
        if (options.timeout) {
            setTimeout(function () {
               elgsJS.clearChildren(options.messageArea)
            }, options.timeout);
        }
    },
    /**
     * Returns the max value of an array
     * @param {Array} arr. The array of which we want it's value.
     * @returns {Array Item}. The max array item.
     */
    arrayMax: function (arr) {
        return arr.reduce(function (p, v) {
            return (p > v ? p : v);
        });
    },
    /**
     * Clear a dom nod of it's children
     * @param {domNode} parent The dom node of which we want to remove it's children.
     */
    clearChildren: function (parent) {
        while (parent.firstChild)
            parent.removeChild(parent.firstChild);
    },
    /**
     * Convert collection to array
     * @param {collection} The collection to convert to array
     */
    collection2Array: function (collection) {
        for (var a = [], i = collection.length; i; )
            a[--i] = collection[i];
        return a;
    },

    /**
     * Returns hash array with query string values
     * @returns {hash} 
     */
    getUrlVars: function() {
    
        var i, len, params = {},
        hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
       // Also Crockford recommends not using ++ on account of it being a silly construct that can lead to bad practices
        for(i = 0, len = hashes.length, len; i < len; i+=1) {
            hash = hashes[i].split('=');
            params[hash[0]] = hash[1];
        }
        return params;
    },
    /**
     * Returns a value from the Query String
     * @param {String} The key you are looking for
     * @returns {String} The Value or empty String if the value not found
     */
    getQSValue: function(key) {
        var val = elgsJS.getUrlVars()[key];
       return (typeof val === 'undefiend' ? '' : val);
    },
    
    xhrCall: function(url, options) {
        if(typeof options === 'undefined') options = {};
        var method = ( typeof options.method === 'undfined' ? 'GET': ( ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'].indexOf( options.method) > -1 ? options.method : 'GET') );
        var   xhr = new XMLHttpRequest();
        xhr.open( method,  url, ( typeof options.async === 'undefined' ? true : options.async ) );
        if (typeof options.responseType !== 'undefined') xhr.responseType = options.responseType;
        xhr.send( (method === 'GET') ? null: ( typeof options.data !== 'undefined' ? options.data : null ) );    
        xhr.onreadystatechange = function(){
            if(xhr.readyState === 4){
                if(xhr.status === 200){
                    if (typeof options.success !== 'undefined') options.success(xhr); 
                }
            }
        };
    },

};