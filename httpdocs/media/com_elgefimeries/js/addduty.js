 //jQuery.noConfilct();
 jQuery(document).ready(function() {
     kendo.culture('el-GR');
    jQuery("#elgAvailableStuff").kendoGrid({
            dataSource: {
                data: [{fullName:'Doctor 1',rank:"Rank 1",speciality:"Speciality 1",position:'position 1',status:'yes'},{fullName:'Doctor 2',rank:"Rank 2",speciality:"Speciality 2",position:'position 2',status:'yes'},{fullName:'Doctor 3',rank:"Rank 3",speciality:"Speciality 3",position:'position 3',status:'yes'},{fullName:'Doctor 4',rank:"Rank 4",speciality:"Speciality 4",position:'position 4',status:'yes'},{fullName:'Doctor 5',rank:"Rank 5",speciality:"Speciality 5",position:'position 5',status:'yes'},{fullName:'Doctor 6',rank:"Rank 6",speciality:"Speciality 6",position:'position 6',status:'yes'},{fullName:'Doctor 7',rank:"Rank 7",speciality:"Speciality 7",position:'position 7',status:'yes'}],
                pageSize: 5
            },
            sortable: {mode: "single"},
            pageable: {buttonCount: 5},
            scrollable: false,
            columns: [{field: "fullName",title: "Full Name", width: 80},{field: "rank",title: "Rank",width: 80},{field: "speciality",title: "Speciality",width:80},{field: "position",title: "Position",width:80},{field: "status",title: "Status",width:80}]
        });
       jQuery("#dutyDate").kendoDateTimePicker({value:new Date()});

       jQuery("#elgSelectedStuff").kendoGrid({
            dataSource: {
                data: [{fullName:'Doctor 1',rank:"Rank 1",speciality:"Speciality 1",position:'position 1',status:'yes'},{fullName:'Doctor 2',rank:"Rank 2",speciality:"Speciality 2",position:'position 2',status:'yes'},{fullName:'Doctor 3',rank:"Rank 3",speciality:"Speciality 3",position:'position 3',status:'yes'},{fullName:'Doctor 4',rank:"Rank 4",speciality:"Speciality 4",position:'position 4',status:'yes'},{fullName:'Doctor 5',rank:"Rank 5",speciality:"Speciality 5",position:'position 5',status:'yes'},{fullName:'Doctor 6',rank:"Rank 6",speciality:"Speciality 6",position:'position 6',status:'yes'},{fullName:'Doctor 7',rank:"Rank 7",speciality:"Speciality 7",position:'position 7',status:'yes'}],
                pageSize: 3
            },
            sortable: {mode: "single"},
            pageable: {buttonCount: 3},
            scrollable: false,
            columns: [{field: "fullName",title: "Full Name", width: 80},{field: "rank",title: "Rank",width: 80},{field: "speciality",title: "Speciality",width:80},{field: "position",title: "Position",width:80},{field: "status",title: "Status",width:80}]
        });
       
       jQuery("#elgNotOkSelected").kendoGrid({
            dataSource: {
                data: [{fullName:'Doctor 1',rank:"Rank 1",speciality:"Speciality 1",position:'position 1',status:'yes'},{fullName:'Doctor 2',rank:"Rank 2",speciality:"Speciality 2",position:'position 2',status:'yes'},{fullName:'Doctor 3',rank:"Rank 3",speciality:"Speciality 3",position:'position 3',status:'yes'},{fullName:'Doctor 4',rank:"Rank 4",speciality:"Speciality 4",position:'position 4',status:'yes'},{fullName:'Doctor 5',rank:"Rank 5",speciality:"Speciality 5",position:'position 5',status:'yes'},{fullName:'Doctor 6',rank:"Rank 6",speciality:"Speciality 6",position:'position 6',status:'yes'},{fullName:'Doctor 7',rank:"Rank 7",speciality:"Speciality 7",position:'position 7',status:'yes'}],
                pageSize: 3
            },
            sortable: {mode: "single"},
            pageable: {buttonCount: 3},
            scrollable: false,
            columns: [{field: "fullName",title: "Full Name", width: 80},{field: "rank",title: "Rank",width: 80},{field: "speciality",title: "Speciality",width:80},{field: "position",title: "Position",width:80},{field: "status",title: "Status",width:80}]
        });
        
        jQuery("#dutyType").kendoDropDownList({
            optionLabel: Joomla.JText._('COM_ELGEFIMERIES_CLINICS_SELECT'),
            dataTextField: "cN",
            dataValueField: "cI",
            dataSource:[{cN:'Etimotita',cI:'1'},{cN:'Energeis',cI:'2'},{cN:'Miktes', cI:'3'}],
            change: function() {jQuery('#infoDate').value=this.value();}
    
        });
        jQuery("#dutyPlace").kendoDropDownList({
            optionLabel: Joomla.JText._('COM_ELGEFIMERIES_DUTY_PLACE'),
            dataTextField: "cN",
            dataValueField: "cI",
            dataSource:clinics
        });
        
        jQuery("#infoType").kendoDropDownList({
            cascadeFrom: 'dutyType',
            dataTextField: "iN",
            dataValueField: "iI",
            dataSource:[{iN:'Etimotita',iI:'1', cI:'1'},{iN:'Energeis',iI:'2', cI:'2'},{iN:'Miktes',iI:'3', cI:'3'}]
        });
        jQuery("#infoDate").kendoDateTimePicker({value:new Date(), selectedDate: jQuery('#dutyDate').selectedDate});
        jQuery("#infoPlace").kendoDropDownList({
            cascadeFrom:'dutyPlace',
            dataTextField: "iN",
            dataValueField: "iI",
            dataSource:[{iN:'Clinic 1',iI:'1',cI:'1'},{iN:'Clinic 2',iI:'2',cI:'2'},{iN:'Clinic 3', iI:'3',cI:'3'},{iN:'Clinic 4', iI:'4',cI:'4'},{iN:'Clinic 5', iI:'5',cI:'5'},{iN:'Clinic 6', iI:'6',cI:'5'}]
        });
 
 });