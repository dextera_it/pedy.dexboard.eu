 //jQuery.noConfilct();
 jQuery(document).ready(function() {
        jQuery("#dC").kendoDropDownList({
            optionLabel: Joomla.JText._('COM_ELGEFIMERIES_CLINICS_SELECT'),
            dataTextField: "cN",
            dataValueField: "cI",
            dataSource: clinics
        });

        jQuery("#dD").kendoDropDownList({
            cascadeFrom: 'dC',
            optionLabel: Joomla.JText._('COM_ELGEFIMERIES_DOCTORS_SELECT'),
            dataTextField: "rN",dataValueField: "rI",
            dataSource: [{rI:'1',rN:'Doctor 1',cI:'1'},{rI:'2',rN:'Doctor 2',cI:'1'},{rI:'3', rN:'Doctor 3',cI:'3'},{rI:'4', rN:'Doctor 4',cI:'3'},{rI:'5', rN:'Doctor 5',cI:'6'},{rI:'6', rN:'Doctor 6',cI:'6'}]
        });

        jQuery("#dH").kendoDropDownList({
                    cascadeFrom: "dC",
                    optionLabel: Joomla.JText._('COM_ELGEFIMERIES_HIERARCHY_SELECT'),
                    dataTextField: "rN",dataValueField: "rI",cascaseFrom: 'dC',
                    dataSource: [{rI:'1',rN:'Rank 1',cI:'1'},{rI:'2',rN:'Rank 2',cI:'1'},{rI:'3', rN:'Rank 3',cI:'3'},{rI:'3', rN:'Rank 4',cI:'3'},{rI:'5', rN:'Rank 5',cI:'6'},{rI:'6', rN:'Rank 6',cI:'6'}]
        });
        jQuery("#dS").kendoDropDownList({
                    cascadeFrom: "dC",
                    optionLabel: Joomla.JText._('COM_ELGEFIMERIES_SPECIALIZATION_SELECT'),
                    dataTextField: "rN",dataValueField: "rI",cascaseFrom: 'dC',
                    dataSource: [{rI:'1',rN:'Speciality 1',cI:'1'},{rI:'2',rN:'Speciality 2',cI:'1'},{rI:'3',rN:'Speciality 3',cI:'3'},{rI:'3',rN:'Speciality 4',cI:'3'},{rI:'5',rN:'Speciality 5',cI:'6'},{rI:'6',rN:'Speciality 6',cI:'6'}]
        });

//                    $("#get").click(function() {
//                        var categoryInfo = "\nCategory: { id: " + categories.value() + ", name: " + categories.text() + " }",
//                            productInfo = "\nProduct: { id: " + products.value() + ", name: " + products.text() + " }",
//                            orderInfo = "\nOrder: { id: " + orders.value() + ", name: " + orders.text() + " }";
//
//                        alert("Order details:\n" + categoryInfo + productInfo + orderInfo);
//                    });
               
 
 
    jQuery('#scheduler').kendoScheduler({
     date: new Date("2013/6/13"),
        startTime: new Date("2013/6/13 07:00 AM"),
        height: 600,
        views: [
        "day",
            { type: "week", selected: true },
            "month",
            "agenda"
        ],
        timezone: "Etc/UTC",
        dataSource: {
            batch: true,
            
            schema: {
                model: {
                    id: "taskId",
                    fields: {
                        taskId: { from: "TaskID", type: "number" },
                        title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                        start: { type: "date", from: "Start" },
                        end: { type: "date", from: "End" },
                        startTimezone: { from: "StartTimezone" },
                        endTimezone: { from: "EndTimezone" },
                        description: { from: "Description" },
                        recurrenceId: { from: "RecurrenceID" },
                        recurrenceRule: { from: "RecurrenceRule" },
                        recurrenceException: { from: "RecurrenceException" },
                        ownerId: { from: "OwnerID", defaultValue: 1 },
                        isAllDay: { type: "boolean", from: "IsAllDay" }
                    }
                }
            },
            filter: {
                logic: "or",
                filters: [
                    { field: "ownerId", operator: "eq", value: 1 },
                    { field: "ownerId", operator: "eq", value: 2 }
                ]
            }
        },
        resources: [
            {
                field: "ownerId",
                title: "Owner",
                dataSource: [
                    { text: "Alex", value: 1, color: "#f8a398" },
                    { text: "Bob", value: 2, color: "#51a0ed" },
                    { text: "Charlie", value: 3, color: "#56ca85" }
                ]
            }
        ]
    });
 
 
 
 
 
 
 
 
 });