 
 jQuery(document).ready(function() {
     jQuery('a[data-toggle="tab"]').on('shown', function (e) {
         var tb = e.target.hash;
         var year = jQuery('#year').val();
		 var url = '';
         var data = {data:{}};
         
         if(tb === '#tab-1'){
             data.data.idFileGroup = 1;
             data.data.year = year;
			url = 'index.php?option=com_elgefimeries&view=submitionsums&controller=ajaxproxy&format=ajax'; 
         }
         else{
            //data.data.idFileGroup = 2;
           // data.data.year = year;
			url = 'index.php?option=com_elgefimeries&view=organogramslist&format=ajax'; 
         }
		 jQuery('div.tab-pane.active').css('min-height', 0);
         getData(data, url);
            //alert(e.target); // activated tab
            //alert(e.relatedTarget); // previous tab
    });
   
    function getData(data, url) {
        //var url = 'index.php?option=com_elgefimeries&view=submitionsums&controller=ajaxproxy&format=ajax';
        jQuery('div.tab-pane.active').html('');
       
        jQuery.get(url,data).done(function(data){
            jQuery('div.tab-pane.active').css('min-height', '100px');
			jQuery('div.tab-pane.active').html(data);
            
        });
        
    }
 });