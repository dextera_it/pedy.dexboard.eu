
 var elms= {};
 var submitData, isCalculating = false;
 jQuery(document).ready(function() {

     jQuery('#costDataSection input[type!=button]').each(function(ind){
        if(this.id != 'start_date') {
            if (this.addEventListener) {
                this.addEventListener('change', valueChanged,false);
            } else if(this.attachEvent) {
                this.attachEvent('onchange',valueChanged);
            } else {
                this.onchange = valueChanged;
            }
        }       
        elms[this.id] = this;
     });
      var dd = document.getElementById('idVathmos');
        if (dd.addEventListener) {
                dd.addEventListener('change', getCostData,false);
        } else if(dd.attachEvent) {
            dd.attachEvent('onchange',getCostData);
        } else {
            dd.onchange = getCostData;
        }
        submitData = jQuery('#submitData');
        submitData.click(updateData);
               
 });
   
 function updateData() {
     jQuery('div.loader-hide').css('display', 'none');
     var data = jQuery('#costForm').serialize();
     jQuery.post( "index.php?option=com_elgefimeries&controller=costsave&model=costsave&format=ajax", 
     data,
     function( data ) {
           
            jQuery('div.loader-hide').css('display', 'block');
             alert(  Joomla.JText._('COM_ELGEFIMERIES_SUBMIT_COMPLETED') );
        });
 }  
   
 function getCostData() {
    var idVathmos = jQuery('#idVathmos').val();
    if(idVathmos != '')
    {   
        jQuery('div.loader-hide').css('display', 'none');
        jQuery.getJSON( "index.php?option=com_elgefimeries&view=cost&controller=ajaxproxy&model=costajax&format=ajax&data[idBathmos]=" + idVathmos, function( data ) {
            fillData(data);
            calcCost();
            jQuery('div.loader-hide').css('display', 'block');
        });
    }
 }
 //var basicSalary, indexhourlyDay;
 
 

function fillData(data) {
    elms.token.name =  data.token;
    elms.token.value = 1;
    elms.proelefsi.value = data.coef.proelefsi;
    elms.old_start_date.value = data.coef.old_start_date; 
    elms.start_date.value = data.coef.start_date;
    elms.basicsalary.value = data.coef.basicsalary;
    elms.indexhourlyDay.value = data.coef.indexhourlyDay;
    elms.indexhourlyNight.value = data.coef.indexhourlyNight;
    elms.indexhourlyNightArgia.value = data.coef.indexhourlyNightArgia;
    elms.indexhourlyDayArgia.value = data.coef.indexhourlyDayArgia;
    
    elms.EnergiDayFactor_1.value = data.active.EnergiDayFactor_1;
    elms.EnergiNightFactor_1.value = data.active.EnergiNightFactor_1;
    elms.EnergiNightArgiaFactor_1.value = data.active.EnergiNightArgiaFactor_1;
    elms.EnergiDayArgiaFactor_1.value = data.active.EnergiDayArgiaFactor_1;
    elms.EnergiAdditional_1.value  = data.active.prostheti_1;
     
    elms.EnergiDayFactor_2.value   = data.active.EnergiDayFactor_2;
    elms.EnergiNightFactor_2.value  = data.active.EnergiDayFactor_2;
    elms.EnergiNightArgiaFactor_2.value = data.active.EnergiDayFactor_2;
    elms.EnergiDayArgiaFactor_2.value = data.active.EnergiDayFactor_2;
    elms.EnergiAdditional_2.value =  data.active.prostheti_2;
     
    elms.EnergiDayFactor_3.value = data.active.EnergiDayFactor_3;
    elms.EnergiNightFactor_3.value = data.active.EnergiDayFactor_3;
    elms.EnergiNightArgiaFactor_3.value = data.active.EnergiDayFactor_3;
    elms.EnergiDayArgiaFactor_3.value = data.active.EnergiDayFactor_3;
    elms.EnergiAdditional_3.value = data.active.prostheti_3;
     
    elms.EnergiDayFactor_4.value = data.active.EnergiDayFactor_4;
    elms.EnergiNightFactor_4.value = data.active.EnergiDayFactor_4;
    elms.EnergiNightArgiaFactor_4.value = data.active.EnergiDayFactor_4;
    elms.EnergiDayArgiaFactor_4.value = data.active.EnergiDayFactor_4;
    elms.EnergiAdditional_4.value = data.active.prostheti_4;
     
    elms.EnergiDayFactor_5.value = data.active.EnergiDayFactor_5;
    elms.EnergiNightFactor_5.value = data.active.EnergiDayFactor_5;
    elms.EnergiNightArgiaFactor_5.value = data.active.EnergiDayFactor_5;
    elms.EnergiDayArgiaFactor_5.value = data.active.EnergiDayFactor_5;
    elms.EnergiAdditional_5.value = data.active.prostheti_5;
     
    elms.ready_1.value = data.ready.Etoimotitasfactor_1;
    elms.ready_2.value = data.ready.Etoimotitasfactor_2;
    elms.ready_3.value = data.ready.Etoimotitasfactor_3;
    elms.ready_4.value = data.ready.Etoimotitasfactor_4;
    elms.ready_5.value = data.ready.Etoimotitasfactor_5;
     
    elms.MixedDayFactor_1.value = data.mixed.MiktiDayFactor_1;
    elms.MixedNightFactor_1.value = data.mixed.MiktiNightFactor_1;
    elms.MixedNightArgiaFactor_1.value = 0 ; //data.mixed.MiktiNightArgiaFactor_1;
    elms.MixedDayArgiaFactor_1.value = 0 ; //data.mixed.MiktiDayArgiaFactor_1;
    elms.MixedAdditional_1.value =  0; //data.mixed.prostheti_1;
     
    elms.MixedDayFactor_2.value = data.mixed.MiktiDayFactor_2;
    elms.MixedNightFactor_2.value = data.mixed.MiktiNightFactor_2;
    elms.MixedNightArgiaFactor_2.value = 0; //data.mixed.MiktiNightArgiaFactor_2;
    elms.MixedDayArgiaFactor_2.value = 0; //data.mixed.MiktiDayArgiaFactor_2;
    elms.MixedAdditional_2.value = 0; //data.mixed.prostheti_2;
     
    elms.MixedDayFactor_3.value = data.mixed.MiktiDayFactor_3;
    elms.MixedNightFactor_3.value = data.mixed.MiktiNightFactor_3;
    elms.MixedNightArgiaFactor_3.value = 0; // data.mixed.MiktiNightArgiaFactor_3;
    elms.MixedDayArgiaFactor_3.value = 0; ///data.mixed.MiktiDayArgiaFactor_3;
    elms.MixedAdditional_3.value = 0; //data.mixed.prostheti_3;
     
    elms.MixedDayFactor_4.value = data.mixed.MiktiDayFactor_4;
    elms.MixedNightFactor_4.value = data.mixed.MiktiNightFactor_4;
    elms.MixedNightArgiaFactor_4.value = 0; /// data.mixed.MiktiNightArgiaFactor_4;
    elms.MixedDayArgiaFactor_4.value = 0; //data.mixed.MiktiDayArgiaFactor_4;
    elms.MixedAdditional_4.value = 0; //data.mixed.prostheti_4;
     
    elms.MixedDayFactor_5.value = data.mixed.MiktiDayFactor_5;
    elms.MixedNightFactor_5.value = data.mixed.MiktiNightFactor_5;
    elms.MixedNightArgiaFactor_5.value = 0; // data.mixed.MiktiNightArgiaFactor_5;
    elms.MixedDayArgiaFactor_5.value = 0; //data.mixed.MiktiDayArgiaFactor_5;
    elms.MixedAdditional_5.value = 0; //data.mixed.prostheti_5;
    
     jQuery(".start_date").datetimepicker({
        format: 'dd/mm/yyyy',
        minView: 2,
        autoclose: true,
        language: 'el',
        startDate: new Date()
    });
   
     
 }
   
   function valueChanged() {
       submitData.removeAttr('disabled');
       if(!isCalculating) calcCost();
   }
   function calcCost() {
       isCalculating = true;
       var day = (parseFloat(elms.indexhourlyDay.value) * parseFloat(elms.basicsalary.value)).toFixed(2),
               night = (parseFloat(elms.indexhourlyNight.value * day)).toFixed(2),
               nightArgia = (parseFloat(elms.indexhourlyNightArgia.value * day)).toFixed(2),
               dayArgia = (parseFloat(elms.indexhourlyDayArgia.value * day)).toFixed(2);
       
       elms.indexhourlyDayCalc.value = day;
       elms.indexhourlyNightCalc.value = night;
       elms.indexhourlyNightArgiaCalc.value = nightArgia;
       elms.indexhourlyDayArgiaCalc.value =  dayArgia;
       
       elms.EnergiDayFactor_1Calc.value = (parseFloat(elms.EnergiDayFactor_1.value) * day).toFixed(2);
       elms.EnergiDayFactor_2Calc.value = (parseFloat(elms.EnergiDayFactor_2.value) * day).toFixed(2);
       elms.EnergiDayFactor_3Calc.value = (parseFloat(elms.EnergiDayFactor_3.value) * day).toFixed(2);
       elms.EnergiDayFactor_4Calc.value = (parseFloat(elms.EnergiDayFactor_4.value) * day).toFixed(2);
       elms.EnergiDayFactor_5Calc.value = (parseFloat(elms.EnergiDayFactor_5.value) * day).toFixed(2);
       
       elms.EnergiNightFactor_1Calc.value = (parseFloat(elms.EnergiNightFactor_1.value) * night).toFixed(2);
       elms.EnergiNightFactor_2Calc.value = (parseFloat(elms.EnergiNightFactor_2.value) * night).toFixed(2);
       elms.EnergiNightFactor_3Calc.value = (parseFloat(elms.EnergiNightFactor_3.value) * night).toFixed(2);
       elms.EnergiNightFactor_4Calc.value = (parseFloat(elms.EnergiNightFactor_4.value) * night).toFixed(2);
       elms.EnergiNightFactor_5Calc.value = (parseFloat(elms.EnergiNightFactor_5.value) * night).toFixed(2);
       
       elms.EnergiNightArgiaFactor_1Calc.value = (parseFloat(elms.EnergiNightArgiaFactor_1.value) * nightArgia).toFixed(2);
       elms.EnergiNightArgiaFactor_2Calc.value = (parseFloat(elms.EnergiNightArgiaFactor_2.value) * nightArgia).toFixed(2);
       elms.EnergiNightArgiaFactor_3Calc.value = (parseFloat(elms.EnergiNightArgiaFactor_3.value) * nightArgia).toFixed(2);
       elms.EnergiNightArgiaFactor_4Calc.value = (parseFloat(elms.EnergiNightArgiaFactor_4.value) * nightArgia).toFixed(2);
       elms.EnergiNightArgiaFactor_5Calc.value = (parseFloat(elms.EnergiNightArgiaFactor_5.value) * nightArgia).toFixed(2);
       
       elms.EnergiDayArgiaFactor_1Calc.value = (parseFloat(elms.EnergiDayArgiaFactor_1.value) * dayArgia).toFixed(2);
       elms.EnergiDayArgiaFactor_2Calc.value = (parseFloat(elms.EnergiDayArgiaFactor_2.value) * dayArgia).toFixed(2);
       elms.EnergiDayArgiaFactor_3Calc.value = (parseFloat(elms.EnergiDayArgiaFactor_3.value) * dayArgia).toFixed(2);
       elms.EnergiDayArgiaFactor_4Calc.value = (parseFloat(elms.EnergiDayArgiaFactor_4.value) * dayArgia).toFixed(2);
       elms.EnergiDayArgiaFactor_5Calc.value = (parseFloat(elms.EnergiDayArgiaFactor_5.value) * dayArgia).toFixed(2);
       
       elms.EnergiAdditional_1Calc.value = (parseFloat(elms.EnergiDayFactor_1Calc.value) + parseFloat(elms.EnergiNightFactor_1Calc.value) +  parseFloat(elms.EnergiNightArgiaFactor_1Calc.value) + parseFloat(elms.EnergiDayArgiaFactor_1Calc.value) + parseFloat(elms.EnergiAdditional_1.value)).toFixed(2);
       elms.EnergiAdditional_2Calc.value = (parseFloat(elms.EnergiDayFactor_2Calc.value) + parseFloat(elms.EnergiNightFactor_2Calc.value) +  parseFloat(elms.EnergiNightArgiaFactor_2Calc.value) + parseFloat(elms.EnergiDayArgiaFactor_2Calc.value) + parseFloat(elms.EnergiAdditional_2.value)).toFixed(2);
       elms.EnergiAdditional_3Calc.value = (parseFloat(elms.EnergiDayFactor_3Calc.value) + parseFloat(elms.EnergiNightFactor_3Calc.value) +  parseFloat(elms.EnergiNightArgiaFactor_3Calc.value) + parseFloat(elms.EnergiDayArgiaFactor_3Calc.value) + parseFloat(elms.EnergiAdditional_3.value)).toFixed(2);
       elms.EnergiAdditional_4Calc.value = (parseFloat(elms.EnergiDayFactor_4Calc.value) + parseFloat(elms.EnergiNightFactor_4Calc.value) +  parseFloat(elms.EnergiNightArgiaFactor_4Calc.value) + parseFloat(elms.EnergiDayArgiaFactor_4Calc.value) + parseFloat(elms.EnergiAdditional_4.value)).toFixed(2);
       elms.EnergiAdditional_5Calc.value = (parseFloat(elms.EnergiDayFactor_5Calc.value) + parseFloat(elms.EnergiNightFactor_5Calc.value) +  parseFloat(elms.EnergiNightArgiaFactor_5Calc.value) + parseFloat(elms.EnergiDayArgiaFactor_5Calc.value) + parseFloat(elms.EnergiAdditional_5.value)).toFixed(2);
 
       elms.ready_1Calc.value = (parseFloat(elms.ready_1.value) * parseFloat(elms.EnergiAdditional_1Calc.value)).toFixed(2);
       elms.ready_2Calc.value = (parseFloat(elms.ready_1.value) * parseFloat(elms.EnergiAdditional_2Calc.value)).toFixed(2);
       elms.ready_3Calc.value = (parseFloat(elms.ready_1.value) * parseFloat(elms.EnergiAdditional_3Calc.value)).toFixed(2);
       elms.ready_4Calc.value = (parseFloat(elms.ready_1.value) * parseFloat(elms.EnergiAdditional_4Calc.value)).toFixed(2);
       elms.ready_5Calc.value = (parseFloat(elms.ready_1.value) * parseFloat(elms.EnergiAdditional_5Calc.value)).toFixed(2);
       
       
       elms.MixedDayFactor_1Calc.value = (parseFloat(elms.MixedDayFactor_1.value) * day).toFixed(2);
       elms.MixedDayFactor_2Calc.value = (parseFloat(elms.MixedDayFactor_2.value) * day).toFixed(2);
       elms.MixedDayFactor_3Calc.value = (parseFloat(elms.MixedDayFactor_3.value) * day).toFixed(2);
       elms.MixedDayFactor_4Calc.value = (parseFloat(elms.MixedDayFactor_4.value) * day).toFixed(2);
       elms.MixedDayFactor_5Calc.value = (parseFloat(elms.MixedDayFactor_5.value) * day).toFixed(2);
       
       elms.MixedNightFactor_1Calc.value = (parseFloat(elms.MixedNightFactor_1.value) * night).toFixed(2);
       elms.MixedNightFactor_2Calc.value = (parseFloat(elms.MixedNightFactor_2.value) * night).toFixed(2);
       elms.MixedNightFactor_3Calc.value = (parseFloat(elms.MixedNightFactor_3.value) * night).toFixed(2);
       elms.MixedNightFactor_4Calc.value = (parseFloat(elms.MixedNightFactor_4.value) * night).toFixed(2);
       elms.MixedNightFactor_5Calc.value = (parseFloat(elms.MixedNightFactor_5.value) * night).toFixed(2);
       
       elms.MixedNightArgiaFactor_1Calc.value = (parseFloat(elms.MixedNightArgiaFactor_1.value) * nightArgia).toFixed(2);
       elms.MixedNightArgiaFactor_2Calc.value = (parseFloat(elms.MixedNightArgiaFactor_2.value) * nightArgia).toFixed(2);
       elms.MixedNightArgiaFactor_3Calc.value = (parseFloat(elms.MixedNightArgiaFactor_3.value) * nightArgia).toFixed(2);
       elms.MixedNightArgiaFactor_4Calc.value = (parseFloat(elms.MixedNightArgiaFactor_4.value) * nightArgia).toFixed(2);
       elms.MixedNightArgiaFactor_5Calc.value = (parseFloat(elms.MixedNightArgiaFactor_5.value) * nightArgia).toFixed(2);
       
       elms.MixedDayArgiaFactor_1Calc.value = (parseFloat(elms.MixedDayArgiaFactor_1.value) * dayArgia).toFixed(2);
       elms.MixedDayArgiaFactor_2Calc.value = (parseFloat(elms.MixedDayArgiaFactor_2.value) * dayArgia).toFixed(2);
       elms.MixedDayArgiaFactor_3Calc.value = (parseFloat(elms.MixedDayArgiaFactor_3.value) * dayArgia).toFixed(2);
       elms.MixedDayArgiaFactor_4Calc.value = (parseFloat(elms.MixedDayArgiaFactor_4.value) * dayArgia).toFixed(2);
       elms.MixedDayArgiaFactor_5Calc.value = (parseFloat(elms.MixedDayArgiaFactor_5.value) * dayArgia).toFixed(2);
       
       
       elms.MixedAdditional_1Calc.value = (parseFloat(elms.MixedDayFactor_1Calc.value) + parseFloat(elms.MixedNightFactor_1Calc.value) +  parseFloat(elms.MixedNightArgiaFactor_1Calc.value) + parseFloat(elms.MixedDayArgiaFactor_1Calc.value) + parseFloat(elms.MixedAdditional_1.value)).toFixed(2);
       elms.MixedAdditional_2Calc.value = (parseFloat(elms.MixedDayFactor_2Calc.value) + parseFloat(elms.MixedNightFactor_2Calc.value) +  parseFloat(elms.MixedNightArgiaFactor_2Calc.value) + parseFloat(elms.MixedDayArgiaFactor_2Calc.value) + parseFloat(elms.MixedAdditional_2.value)).toFixed(2);
       elms.MixedAdditional_3Calc.value = (parseFloat(elms.MixedDayFactor_3Calc.value) + parseFloat(elms.MixedNightFactor_3Calc.value) +  parseFloat(elms.MixedNightArgiaFactor_3Calc.value) + parseFloat(elms.MixedDayArgiaFactor_3Calc.value) + parseFloat(elms.MixedAdditional_3.value)).toFixed(2);
       elms.MixedAdditional_4Calc.value = (parseFloat(elms.MixedDayFactor_4Calc.value) + parseFloat(elms.MixedNightFactor_4Calc.value) +  parseFloat(elms.MixedNightArgiaFactor_4Calc.value) + parseFloat(elms.MixedDayArgiaFactor_4Calc.value) + parseFloat(elms.MixedAdditional_4.value)).toFixed(2);
       elms.MixedAdditional_5Calc.value = (parseFloat(elms.MixedDayFactor_5Calc.value) + parseFloat(elms.MixedNightFactor_5Calc.value) +  parseFloat(elms.MixedNightArgiaFactor_5Calc.value) + parseFloat(elms.MixedDayArgiaFactor_5Calc.value) + parseFloat(elms.MixedAdditional_5.value)).toFixed(2);
       
       isCalculating = false;
       
   }