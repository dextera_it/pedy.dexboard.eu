<?php
/*------------------------------------------------------------------------
# com_elgpedy - e-logism  application
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (c) 2010-2020 e-logism.com. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr

 
----------------------------------**/

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once JPATH_COMPONENT_SITE . '/models/pedy.php';

 
class ElgPedyModelPersonelEdit extends Pedy
{
	
	
    function getState() 
    {

		$state = parent::getState();
		JFormHelper::addFieldPath(	JPATH_ADMINISTRATOR . '/components/com_elgpedy/models/fields');
        $form = JForm::getInstance('personel', ComponentUtils::getDefaultFormPath() .'/personel.xml');
		JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');
		$personel = JTable::getInstance('Personel');
		$stateData = JFactory::getApplication()->getUserState('personel.edit.data');
		$personelId = $state -> get( 'id', 0 );
		 if(isset($stateData)):
			$form->bind($stateData);
		elseif( $personelId > 0 ):
			$personel -> load($state->get('id', 0));
			if($personel !== false)
			{
				$form->bind($personel);
			}
			$form -> setValue('PersonelSpecialityId', null, $this -> getSpeciality($state -> get('id')) );
			$movements = $this -> getMovements( $state -> get('id') );
			
			$lastMovement = $movements[ count($movements) - 1 ];
			$form -> setValue('HealthUnitId', null, $lastMovement[1] );
			
			$state -> set('storedData', 
				[
					'movements' => $movements,
					'HealthUnit' => $lastMovement[8]
				] );
		else:
			$form -> setValue( 'PersonelSpecialityId', null, '' );
			$form -> setValue('HealthUnitId', '' );
                       
			$state -> set('storedData', 
				[
					'movements' => [],
					'HealthUnit' => ''
				] );
		endif;
		/** Data for speciality and health unit must be only on their history tables. These are the steps towrads it **/

		
		$state->set('form', $form);		
		return $state;
    }
	
	private function getSpeciality( $PersonelId )
	{
		return $this -> pedyDB -> setQuery("select SpecialityId from PersonelSpecialityHistory ph where PersonelId = $PersonelId and endDate is null") -> loadResult();
	}
	
	// private function getHealthUnit( $PersonelId )
	// {
		
		// return $this -> pedyDB -> setQuery("select HealthUnitId, DescEL as HealthUnit from PersonelHealthUnitHistory ph inner join HealthUnit h on ph.HealthUnitId = h.HealthUnitId where PersonelId = $PersonelId and endDate is null") -> loadAssoc();
	// }
	
	private function getMovements( $personelId ) 
	{
		return $this -> pedyDB -> setQuery("select huh.id, huh.HealthUnitId, huh.RefHealthUnitId, hu.DescEL, huh.EndDate, huh.StartDate
		, huh.PersonelStatusId, ps.DescEL as personelStatus, οhu.DescEL as HealthUnit 
		from PersonelHealthUnitHistory huh 
		inner join HealthUnit hu on huh.RefHealthUnitId = hu.HealthUnitId and huh.PersonelId = $personelId 
		inner join HealthUnit οhu on huh.HealthUnitId = οhu.HealthUnitId 
		inner join PersonelStatus ps on huh.PersonelStatusId = ps.PersonelStatusId
		order by id ") -> loadRowList();
	}
	
}