<?php
/*------------------------------------------------------------------------
# com_elgalocrm - e-logism library.
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
?>
<div class="elg-column" >
    <div class="formelm" >
        <button>
            <?php echo JText::_('COM_ELG_SUBMIT') ; ?>
        </button>
    </div>
</div>
<div class="elg-column <?php echo $this->returnClass ?>" >
    <div class="formelm returnTo" >
            <a href ="<?php echo $this->retTo ?>" ><?php echo $this->returnToText; ?></a>
    </div>
</div>

