<?php
/*------------------------------------------------------------------------
# com_elgalocrm - e-logism project managment
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
?>
 <h2>
     <?php
     if ($this->idProject > 0 ) {
        echo JText::_('COM_ELGERGON_PROJECT_ID') . ' ' . CommonUtils::getNumWithZeros($this->idProject) ;
     }
     else {
        echo JText::_('COM_ELGERGON_NEW_PROJECT'); 
     }
            ?>
 </h2>
