<?php
/**
 * e-logism's ergon
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require_once JPATH_COMPONENT_SITE . '/libraries/joomla/views/itemvieweditable.php';
 /**
  * Techincal's data loader for plugins files .
  * @package e-logism.joomla.ergon.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */  

class Plugins
{
   public static function getPluginsFiles($doc) {
      $doc->addStyleSheet('media/com_elgergon/css/site.stylesheettf.css');
      $doc->addStyleSheet('media/com_elgergon/css/jquery.fileupload.css');
      $doc->addStyleSheet('media/com_elgergon/css/jquery.fileupload-ui.css');
      $doc->addStyleSheet('media/com_elgergon/bigallery/css/blueimp-gallery.min.css');
      
      
      
   }  
}
 ?>