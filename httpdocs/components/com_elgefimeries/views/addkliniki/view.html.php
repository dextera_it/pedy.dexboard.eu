<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
 class ElgEfimeriesViewAddKliniki  extends ItemViewPlugins
 {
	 public function render() 
	 {
		$data = $this->model->getState()->get('data')->tomeis;
		foreach($data as $key=>$value) :
			$options[] = JHTML::_('select.option', $value->PK_Tomeas, $value->Tomeas);
		endforeach;
		
		$this->tomeas = JHTML::_('select.genericlist', $options, 'idTomeas', 'class="inputbox"', 'value', 'text', $default);
		return parent::render();
	}
 }
 ?>