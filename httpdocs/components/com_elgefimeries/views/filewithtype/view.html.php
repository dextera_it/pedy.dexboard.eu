<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  class ElgEfimeriesViewFileWithType extends ItemViewPlugins
 {
    public function render() {
       
        $this->importedDirectory = $this->state->get("importedDirectory","");
        $this->filesDirectory = $this->state->get("filesDirectory","");
        $this->importedData = $this->state->get("importedData", array());
//        $this->filter_order = $this->state->get('filter_order','');
//        $this->filter_order_Dir = $this->state->get('filter_order_Dir','');
	JHtml::stylesheet('media/com_elgefimeries/css/bootstrap-datetimepicker.min.css');
	JHtml::script('media/com_elgefimeries/js/bootstrap-datetimepicker.min.js');
	JHtml::script('media/com_elgefimeries/js/locales/bootstrap-datetimepicker.el.js');
        $this->user = JFactory::getUser();
        
        return parent::render();
    }
 }
 ?>