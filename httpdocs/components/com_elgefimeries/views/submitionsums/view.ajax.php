<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  jimport ('joomla.html.html.bootstrap');
  class ElgEfimeriesViewSubmitionSums extends ItemViewPlugins{
    public function render() {
        
        $doc = JFactory::getDocument();
        $doc->addStyleSheet('media/com_elgefimeries/css/submitions.css');
        $state = $this->model->getState();
        // $idFileGroup = $state->get('idFileGroup', 1);
		$this->dbg = $state->get('dbg');
		//echo $this->dbg;
        $this->setLayout('submitionsums_ajax');
        return parent::render();
       
       
    }
 }
 ?>