<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 require_once JPATH_COMPONENT_SITE . '/models/nullfactory.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  class ElgEfimeriesViewCost extends ItemViewPlugins{
    public function render() {
        $data = $this->model->getState()->get('data');
        $vathmoi = array();
        $vathmoi[] = array('PK_Bathmoi'=>'', 'Bathmoi'=>JText::_('COM_ELG_SELECT'));
        $vathmoi = array_merge($vathmoi, $data->vathmoi);
        $this->vathmoi = $vathmoi;
        $coef = array();
        $active = array();
        $ready = array();
        $mixed = array();
//        if(count($data->cost) > 0)
//        {
//            $coef['start_date'] = $data->cost[0]->start_date;
//            $coef['basicsalary'] = $data->cost[0]->basicsalary;
//            $coef['indexhourlyDay'] = $data->cost[0]->indexhourlyDay;
//            $coef['indexhourlyNight'] = $data->cost[0]->indexhourlyNight;
//            $coef['indexhourlyNightArgia'] = $data->cost[0]->indexhourlyNightArgia;
//            $coef['indexhourlyDayArgia'] = $data->cost[0]->indexhourlyDayArgia;
//            foreach($data->cost as $item)
//            {
//                if($item->EfimeriaType_id == 1)
//                {
//                    $active['EnergiDayFactor_' . $item->EfimeriaDayType_id] = $item->EnergiDayFactor;
//                    $active['EnergiNightFactor_' . $item->EfimeriaDayType_id] = $item->EnergiNightFactor;
//                    $active['EnergiNightArgiaFactor_' . $item->EfimeriaDayType_id] = $item->EnergiNightArgiaFactor;
//                    $active['EnergiDayArgiaFactor_' . $item->EfimeriaDayType_id] =  $item->EnergiDayArgiaFactor;
//                }
//                elseif($item->EfimeriaType_id == 2)
//                {
//                    $ready['Etoimotitasfactor_' . $item->EfimeriaDayType_id] = $item->Etoimotitasfactor;
//                }
//                elseif($item->EfimeriaType_id == 3)
//                {
//                    $mixed['MiktiDayFactor_' . $item->EfimeriaDayType_id] = $item->MiktiDayFactor ;
//                    $mixed['MiktiNightFactor_' . $item->EfimeriaDayType_id] = $item->MiktiNightFactor ;
//                }
//            }
//            unset($item);
//            
//        }
//        else {
            $coef = NullFactory::getNullCostCoef();
            $active = NullFactory::getNullCostActive();
            $ready = NullFactory::getNullCostReady();
            $mixed = NullFactory::getNullCostMixed();
//        }
        
        $this->coef = $coef;
        $this->active = $active;
        $this->ready = $ready;
        $this->mixed = $mixed;
        $doc = JFactory::getDocument();
        $doc->addStyleSheet('media/com_elgefimeries/css/cost.css');
      
        JHtml::_('script', 'media/com_elgefimeries/js/costdata.js');
        JHtml::stylesheet('media/com_elgefimeries/css/bootstrap-datetimepicker.min.css');
        return parent::render();
       
       
    }
 }
 ?>