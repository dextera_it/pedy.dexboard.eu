<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 require_once JPATH_COMPONENT_SITE . '/models/nullfactory.php';
 require_once JPATH_COMPONENT_SITE . '/libraries/joomla/commonutils.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  class ElgEfimeriesViewCost extends ItemViewPlugins{
    public function render() {
        
       $data = $this->model->getState()->get('data');
       $coef = array();
       $active = array();
       $ready = array();
       $mixed = array();
       if(count($data->cost) > 0)
       {
            $token = JSession::getFormToken(); 
            $coef['proelefsi'] = $data->cost[0]->proelefsi;
            $coef['old_start_date'] = CommonUtils::getDateFormated($data->cost[0]->start_date, 'd/m/Y');
            $coef['start_date'] = date('d/m/Y');
            $coef['basicsalary'] = $data->cost[0]->basicsalary;
            $coef['indexhourlyDay'] = $data->cost[0]->indexhourlyDay;
            $coef['indexhourlyNight'] = $data->cost[0]->indexhourlyNight;
            $coef['indexhourlyNightArgia'] = $data->cost[0]->indexhourlyNightArgia;
            $coef['indexhourlyDayArgia'] = $data->cost[0]->indexhourlyDayArgia;
            foreach($data->cost as $item)
            {
                if($item->EfimeriaType_id == 1)
                {
                    $active['EnergiDayFactor_' . $item->EfimeriaDayType_id] = $item->EnergiDayfactor;
                    $active['EnergiNightFactor_' . $item->EfimeriaDayType_id] = $item->EnergiNightfactor;
                    $active['EnergiNightArgiaFactor_' . $item->EfimeriaDayType_id] = $item->EnergiNightArgiafactor;
                    $active['EnergiDayArgiaFactor_' . $item->EfimeriaDayType_id] =  $item->EnegiDayArgiafactor;
                    $active['prostheti_' . $item->EfimeriaDayType_id ] =  $item->prostheti;
                }
                elseif($item->EfimeriaType_id == 2)
                {
                    $ready['Etoimotitasfactor_' . $item->EfimeriaDayType_id] = $item->Etoimotitasfactor;
                    $active['prostheti_' . $item->EfimeriaDayType_id ] =  $item->prostheti;
                }
                elseif($item->EfimeriaType_id == 3)
                {
                    $mixed['MiktiDayFactor_' . $item->EfimeriaDayType_id] = $item->MiktiDayfactor ;
                    $mixed['MiktiNightFactor_' . $item->EfimeriaDayType_id] = $item->MiktiNightfactor ;
                    $active['prostheti_' . $item->EfimeriaDayType_id ] =  $item->prostheti;
                }
            }
            unset($item);
            
        }
        else {
            $coef = NullFactory::getNullCostCoef();
            $active = NullFactory::getNullCostActive();
            $ready = NullFactory::getNullCostReady();
            $mixed = NullFactory::getNullCostMixed();
        }
        $ret = new stdClass();
        $ret->coef = $coef;
        $ret->active = $active;
        $ret->ready = $ready;
        $ret->mixed = $mixed;
        $ret->token = $token;
        return json_encode($ret);
        //return parent::render();
       
       
    }
 }
 ?>