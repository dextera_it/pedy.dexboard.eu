<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE . '/libraries/joomla/views/view.php';
 
 /**
  * Base class for all item views with kendo ui.
  * @package e-logism.joomla.efimeries.;
  * @subpackage views
  * @author Christoforos J. Korifidis
  * 
  */

 class  ItemViewPlugins extends View
 {
     
    
    public function __construct(JModel $model, SplPriorityQueue $paths = null) 
    {
		
        parent::__construct($model, $paths);
        
		$doc = JFactory::getDocument();
       
       // JHtml::stylesheet('media/com_elgefimeries/css/bootstrap-select.min.css');
        JHTML::stylesheet('media/com_elgefimeries/css/jquery.fileupload.css');
        JHtml::stylesheet('media/com_elgefimeries/css/jquery.fileupload-ui.css');
        
        //$doc->addStyleSheet('media/com_elgefimeries/css/jquery.fileupload.css');
        //$doc->addStyleSheet('media/com_elgefimeries/css/jquery.fileupload-ui.css');
        //$doc->addStyleSheet('media/com_elgefimeries/css/bootstrap-select.min.css');
        
         JHtml::_('bootstrap.framework');
        $doc->addScript('media/com_elgefimeries/js/jquery.ui.widget.js');
        $doc->addScript('media/com_elgefimeries/js/jquery.iframe-transport.js'); 
        $doc->addScript("media/com_elgefimeries/js/jquery.fileupload.js");
		$doc->addScript("media/com_elgefimeries/js/jquery.fileupload-process.js");
		$doc->addScript("media/com_elgefimeries/js/jquery.fileupload-validate.js");
      //  $doc->addScript("media/com_elgefimeries/js/bootstrap-select.min.js");
        
  
  
    }
     
    
    /**
     * This function redirect the user to the list associated with this view item if no value or zero value given for the id.
     */  
    
    protected function redirectOnNoId()
    {
      
        if($this->basicData['id'] == 0) 
        {
            CommonUtils::redirect($this->commonUrl . 'view=' . $this->basicData['view'] .'s');
        }
    }
 }
?>
