<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  class ElgEfimeriesViewAddFile  extends ItemViewPlugins
 {
    public function render() {
        //$this->data = $this->state->get('data');
       
		$this->importedDirectory = $this->state->get("importedDirectory","");
        $this->filesDirectory = $this->state->get("filesDirectory","");
        //$this->unimportedFiles = $this->state->get("data")->files;
        $this->importedData = $this->state->get("importedData", array());
        $this->user = JFactory::getUser();
         JHtml::stylesheet('media/com_elgefimeries/css/bootstrap-datetimepicker.min.css');
	JHtml::script('media/com_elgefimeries/js/bootstrap-datetimepicker.min.js');
	JHtml::script('media/com_elgefimeries/js/locales/bootstrap-datetimepicker.el.js');
                
        return parent::render();
    }
 }
 ?>