<?php
/**
 * e-logism's elgefimeries.
 * @copyright (c) 2013, e-logism.
 * 
 */
 
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE .'/views/itemviewplugins.php';
 /**
  
  * @package e-logism.joomla.elgefimeries.site
  * @subpackage views
  * @author Christoforos J. Korifidis.
  * 
  */
  class ElgEfimeriesViewAddDuty extends ItemViewPlugins{
    public function render() {
        $this->data = $this->state->get('data');
        $doc = JFactory::getDocument();
        $doc->addStyleSheet('media/com_elgefimeries/css/addduty.css');
        $doc->addScript('media/com_elgefimeries/js/addduty.js');
        $doc->addScriptDeclaration(' var userGroups = ' . json_encode(JFactory::getUser()->getAuthorisedGroups()) . ';');
        return parent::render();
       
       
    }
 }
 ?>