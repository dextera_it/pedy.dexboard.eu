<?php
/**
 * com_elgefimeries : elgefimeries application by e-logism.
 * @author e-logism <info@e-logism.gr>
 * @copyright (C)  2013, e-logism.gr
 */

JHtml::_('behavior.framework'); 
JHtml::_('bootstrap.framework');
defined( '_JEXEC' ) or die( 'Restricted access' );
JLoader::register('CommonUtils', JPATH_COMPONENT_SITE . '/libraries/joomla/commonutils.php');
JLoader::register('Utils', JPATH_COMPONENT_SITE . '/utils.php');
JLoader::register('DataUtils', JPATH_COMPONENT_SITE . '/models/datautils.php');
JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/models/tables/');
JForm::addFieldPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/fields');
$input = JFactory::getApplication()->input;
$basicData = Utils::getBasicStateData($input);
$controllerName = $basicData['controller'];

if ($basicData['controller'] == 'itemcontrollerview' ) 
{
    require JPATH_COMPONENT_SITE . '/libraries/joomla/controllers/' . $controllerName . '.php';
    $controllerClass = $basicData['controller'];
}
else
{

   require JPATH_COMPONENT_SITE . '/controllers/' . $controllerName . '.php';
   
  
	$controllerClass = Utils::COMPONENT_NAME . 'Controller' . $controllerName ;
	
}

$controller = new $controllerClass();

$controller->execute();

$controller->redirect();
?>