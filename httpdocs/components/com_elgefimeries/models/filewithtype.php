<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 include JPATH_COMPONENT_SITE . '/models/filelist.php';
 class ElgEfimeriesModelFileWithType extends JModelDatabase
 { 
    public function getState() {
        $profile = JUserHelper::getProfile();
      //  $idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
        $idUser = $profile->get('id');
		$db =  DataUtils::getDB2();
		$query = $db->getQuery(true);
		$db->setQuery($query);
        $query->select("EsynetFileTypeID, EsynetFileTypeName")->from("t_EsynetFileTypes")->order('EsynetFileTypeName');
        $fileTypes = $db->loadObjectList();
        $state = parent::getState();
        $data = new stdClass();
        $data->fileTypes = $fileTypes;
        $path = 'images/elgefimeries/uploaded/esynet';
        // $directory = JPATH_SITE . "/" .$path;
        $state->set("data", $data);
        $state->set("filesDirectory",$path );
        $state->set("importedDirectory", "images/elgefimeries/uploaded/esynet_done");
        $dataImp = new stdClass();
		// if(count($clinics) == 0) $clinics = 0;
        $keys = array('Upload_UserID'=>$idUser
                ,'Upload_EsynetFileTypeID' => $state->get('fltFileType', 0)
                ,'UploadYear'=>$state->get('fltYear', 0)
                , 'UploadMonth'=>$state->get('fltMonth', 0)
                ,'UploadDate'=>CommonUtils::reformDateString($state->get('fltUploadDate', '0000-00-00'), 'd/m/Y', 'Y-m-d')
            ); 
        $filters = array(
            'fltFileType'=>$state->get('fltFileType', 0),
            'fltYear'=>$state->get('fltYear', 0),
            'fltMonth'=>$state->get('fltMonth', 0),
            'fltUploadDate'=>$state->get('fltUploadDate', '')
            );        
        $dataImp->keys = $keys;
        $dataImp->queryType = 3;
        $dataImp->limit = $state->get('limit', 10);
        $dataImp->limitstart = $state->get('limitstart', 0);
//        $dataImp->filter_order_Dir = $state->get('filter_order_Dir','');
//        $dataImp->filter_order = $state->get('filter_order','');
        $dataImp->filters = $filters;
        
        $filesModel = new ElgEfimeriesModelFileList(new JRegistry($dataImp));
        $filesState = $filesModel->getState();
        $state->set('importedData', $filesState->get('importedData'));
        $state->set('pagination', $filesState->get('pagination'));
        return $state;
       
    }    
   
 }