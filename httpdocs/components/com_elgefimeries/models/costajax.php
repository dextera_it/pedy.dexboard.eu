<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelCostAjax extends JModelDatabase
 { 
  
    public function getState() {
        $data = new stdClass();
        $state = parent::getState();
        $db = DataUtils::getDB2(); //$this->getDb(); // 
        $query = $db->getQuery(true);
        $db->setQuery($query);
        $filters = array();
        $filters['t_EfimerevonBathmos_id'] = $state->get('idBathmos'); 
        $data->cost = DataUtils::getCost($filters, $db, $query);
	$state->set('data', $data);	
        return $state;
      }
 }
 
 ?>