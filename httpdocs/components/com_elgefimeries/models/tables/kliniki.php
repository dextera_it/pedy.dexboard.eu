<?php
/**
 * @author e-logism http://www.e-logism.gr
 * @copyright (C) 2013 e-logism. All rights reserved.
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

/**
 * JTable class for projects calls
 *
 * @author e-logism
 */
            
class TableCalls extends JTable {
    var $id = null;
    var $idProjectState = null;
    var $idUser = null;
    var $callNumber = null;
    var $callDate = null;
    var $callDuration = null;
    var $comments = null;
    
    function __construct($db) {
        parent::__construct( '#__elg_ergon_projectstates_calls', 'id', $db );
    } 
}

?>
