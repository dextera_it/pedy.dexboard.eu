<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require JPATH_COMPONENT_SITE . '/libraries/joomla/models/modelsave.php';

 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
*/
class ElgEfimeriesModelAddKlinikiSave extends ModelSave
{
	function setState(JRegistry $state) {
			
		parent::setState($state);  
		exit('ok');
		$profile = JUserHelper::getProfile();
		$idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
		$k = self::getNullKliniki();
		$k->Kliniki = $this->data->kliniki->kliniki;
	
		$db2 = DataUtils::getDB2();
		$query = $db2->getQuery(true);
		$db2->setQuery($query);
		$db2->insertObject('DIM_Klinikes', $k);
		$query->clear();
		$query->select('SCOPE_IDENTITY()');
		$idKliniki = $db->loadResult();
		
		$query->clear();
		$query->select('max(PK_Tomeas)')->from('Hospital_klinikes_Tomeis')->where('PK_Hospital=' . $idHospital);
		$idTomeas = $db2->loadResult();
		$hkt = self::getNullHospitalKlinikesTomeis();
		$hkt->PK_Hospital = $idHospital;
		$hkt->PK_kliniki = $idKliniki;
		$hkt->PK_Tomeas = $idTomeas;
		$db2->insertObject('Hospital_Klinikes_Tomeis', $hkt);
	}
	
	public static function getNullKliniki()
	{
		$o = new stdClass();
		$o->PK_Kliniki = null;
		$o->Kliniki = '';
		return $o;
	}
	
	public static function getNullHospitalKilnikesTomeis()
	{
		$o = new stdClass()
		$o->PK_Hospital = null;
		$o->PK_kliniki = null;
		$o->PK_Tomeas = null;
		return $o;
	}
}
?>