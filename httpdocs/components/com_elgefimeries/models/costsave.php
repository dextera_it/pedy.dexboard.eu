<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 require_once JPATH_COMPONENT_SITE . '/models/nullfactory.php';
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelCostSave extends JModelDatabase
 { 
  
    public function setState(JRegistry $state) {
        
        $importTable = 't_Cost';
        $nids = array();
        $db = DataUtils::getDB2(); //$this->getDb(); 
        $query = $db->getQuery(true);
        $db->setQuery($query);
        $dt = NullFactory::getNullCost();
		
		
		
        $dt->efimeria_cost_id = null;
        $dt->t_EfimerevonBathmos_id = $state->get('idVathmos');
        $dt->proelefsi = $state->get('proelefsi');
        $dt->start_date = CommonUtils::reformDateString($state->get('start_date'), 'd/m/Y', 'Y-m-d');
        $dt->end_date = '2040-12-31';
        $dt->basicsalary = $state->get('basicsalary');
        $dt->indexhourlyDay = $state->get('indexhourlyDay');
        $dt->hourlyDay = $state->get('indexhourlyDayCalc');
        $dt->indexhourlyNight = $state->get('indexhourlyNight');
        $dt->hourlyNight = $state->get('indexhourlyNightCalc');
        $dt->indexhourlyNightArgia = $state->get('indexhourlyNightArgia');
        $dt->hourlyNightArgia = $state->get('indexhourlyNightArgiaCalc');
        $dt->indexhourlyDayArgia = $state->get('indexhourlyDayArgia');
        $dt->hourlyDayArgia = $state->get('indexhourlyDayArgiaCalc');
        $dt->flag = 1;
        
        
        $dt->EfimeriaType_id = 1;
        $dt->EfimeriDayType_id = 1;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_1');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_1');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_1');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_1');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_1');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_1');
        $dt->Etoimotitasfactor =  $state->get('ready_1');
        
        $dt->prostheti= $state->get('EnergiAdditional_1');
        $dt->efimeria_cost_value2 = $state->get('EnergiAdditional_1Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 1;
        $dt->EfimeriDayType_id = 2;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_2');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_2');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_2');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_2');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_2');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_2');
        $dt->Etoimotitasfactor =  $state->get('ready_2');
        
        $dt->prostheti= $state->get('EnergiAdditional_2');
        $dt->efimeria_cost_value2 = $state->get('EnergiAdditional_2Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 1;
        $dt->EfimeriDayType_id = 3;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_3');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_3');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_3');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_3');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_3');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_3');
        $dt->Etoimotitasfactor =  $state->get('ready_3');
        
        $dt->prostheti= $state->get('EnergiAdditional_3');
        $dt->efimeria_cost_value2 = $state->get('EnergiAdditional_3Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 1;
        $dt->EfimeriDayType_id = 4;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_4');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_4');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_4');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_4');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_4');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_4');
        $dt->Etoimotitasfactor =  $state->get('ready_4');
        
        $dt->prostheti= $state->get('EnergiAdditional_4');
        $dt->efimeria_cost_value2 = $state->get('EnergiAdditional_4Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 1;
        $dt->EfimeriDayType_id = 5;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_5');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_5');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_5');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_5');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_5');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_5');
        $dt->Etoimotitasfactor =  $state->get('ready_5');
        
        $dt->prostheti= $state->get('EnergiAdditional_5');
        $dt->efimeria_cost_value2 = $state->get('EnergiAdditional_5Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
       
        /*--------------------------------etimotitas-------------------------------------------*/
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 2;
        $dt->EfimeriDayType_id = 1;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_1');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_1');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_1');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_1');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_1');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_1');
        $dt->Etoimotitasfactor =  $state->get('ready_1');
        
        $dt->prostheti= $state->get('EnergiAdditional_1');
        $dt->efimeria_cost_value2 = $state->get('ready_1Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 2;
        $dt->EfimeriDayType_id = 2;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_2');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_2');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_2');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_2');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_2');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_2');
        $dt->Etoimotitasfactor =  $state->get('ready_2');
        
        $dt->prostheti= $state->get('EnergiAdditional_2');
        $dt->efimeria_cost_value2 = $state->get('ready_2Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 2;
        $dt->EfimeriDayType_id = 3;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_3');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_3');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_3');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_3');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_3');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_3');
        $dt->Etoimotitasfactor =  $state->get('ready_3');
        
        $dt->prostheti= $state->get('EnergiAdditional_3');
        $dt->efimeria_cost_value2 = $state->get('ready_3Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 2;
        $dt->EfimeriDayType_id = 4;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_4');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_4');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_4');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_4');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_4');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_4');
        $dt->Etoimotitasfactor =  $state->get('ready_4');
        
        $dt->prostheti= $state->get('EnergiAdditional_4');
        $dt->efimeria_cost_value2 = $state->get('ready_4Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 2;
        $dt->EfimeriDayType_id = 5;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_5');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_5');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_5');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_5');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_5');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_5');
        $dt->Etoimotitasfactor =  $state->get('ready_5');
        
        $dt->prostheti= $state->get('EnergiAdditional_5');
        $dt->efimeria_cost_value2 = $state->get('ready_5Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
      
        /*--------------------------------- miktes ------------------------------------------*/
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 3;
        $dt->EfimeriDayType_id = 1;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_1');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_1');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_1');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_1');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_1');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_1');
        $dt->Etoimotitasfactor =  $state->get('ready_1');
        
        $dt->prostheti= $state->get('MixedAdditional_1');
        $dt->efimeria_cost_value2 = $state->get('MixedAdditional_1Calc');
        $db->insertObject($importTable, $dt);     
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 3;
        $dt->EfimeriDayType_id = 2;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_2');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_2');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_2');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_2');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_2');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_2');
        $dt->Etoimotitasfactor =  $state->get('ready_2');
        
        $dt->prostheti= $state->get('MixedAdditional_2');
        $dt->efimeria_cost_value2 = $state->get('MixedAdditional_2Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 3;
        $dt->EfimeriDayType_id = 3;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_3');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_3');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_3');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_3');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_3');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_3');
        $dt->Etoimotitasfactor =  $state->get('ready_3');
        
        $dt->prostheti= $state->get('MixedAdditional_3');
        $dt->efimeria_cost_value2 = $state->get('MixedAdditional_3Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 3;
        $dt->EfimeriDayType_id = 4;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_4');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_4');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_4');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_4');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_4');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_4');
        $dt->Etoimotitasfactor =  $state->get('ready_4');
        
        $dt->prostheti= $state->get('MixedAdditional_4');
        $dt->efimeria_cost_value2 = $state->get('MixedAdditional_4Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();
        
        
        $dt->efimeria_cost_id = null;
        $dt->EfimeriaType_id = 3;
        $dt->EfimeriDayType_id = 5;
        $dt->EnergiDayfactor = $state->get('EnergiDayFactor_5');
        $dt->EnergiNightfactor = $state->get('EnergiNightFactor_5');
        $dt->EnergiNightArgiafactor = $state->get('EnergiNightArgiaFactor_5');
        $dt->EnegiDayArgiafactor = $state->get('EnergiDayArgiaFactor_5');
               
        $dt->MiktiDayfactor =  $state->get('MiktiDayFactor_5');
        $dt->MiktiNightfactor = $state->get('MiktiNightFactor_5');
        $dt->Etoimotitasfactor =  $state->get('ready_5');
        
        $dt->prostheti= $state->get('MixedAdditional_5');
        $dt->efimeria_cost_value2 = $state->get('MixedAdditional_5Calc');
        $db->insertObject($importTable, $dt);
        $nids[] = $db->insertid();    
       
       // $db->setQuery($query);
        //$query->clear();
       // $query->update($importTable)->set('flag= 0')->where('t_EfimerevonBathmos_id = ' . $dt->t_EfimerevonBathmos_id . ' and efimeria_cost_id not in(' . implode(',',$nids) . ')');
       
      //  $db->execute();
      
      }
 }
 
 ?>