<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelSubmitionSumsClinics extends JModelDatabase
 {
     public function getState() {
         $state = parent::getState();
         $idFileGroup = $state->get('idFileGroup', 1);
         $year = $state->get('year', date('Y'));
         $idHospital = $state->get('idHospital',0);
         $data = new stdClass();
         $db = $this->getDb();
         $query = $db->getQuery();
         $query->clear();
         $db->setQuery($query);
         $query->select("idFileGroup, count(*) as cnt")->from("#__elg_efimeries_filetype")->group("idFileGroup");
         $data->fileGroups = $db->loadObjectList('idFileGroup');
         
         $query->clear();
         $query->select("k.id, k.kliniki as unit, u.Upload_year, u.Upload_month, ft.idFileGroup, count(*) as totalUploads")
                 ->from("#__elg_efimeries_kliniki k ")
                 ->innerJoin("t_uploads u on k.id = u.Hu_id " . ($year > 0 ? " and Upload_year = " . $year : "") . " " . ($idHospital >0 ? " and k.idNosokomio = " . $idHospital : "" ))
                 ->innerJoin("#__elg_efimeries_filetype ft on ft.id = u.UploadStatus_id")
                 ->innerJoin("#__elg_efimeries_filegroup fg on fg.id = ft.idFileGroup " . ($idFileGroup >0 ? " and idFileGroup=" . $idFileGroup : ""))
                 ->group("k.id, k.kliniki, u.Upload_year, u.Upload_month, ft.idFileGroup")
                 ->order("unit, u.Upload_year desc, u.Upload_month, ft.idFileGroup");
         
         
         $data->uploads = $db->loadObjectList();
         $query->clear();
         $query->select("distinct Upload_year")->from("t_uploads")->order('Upload_year desc');
         //$db->setQuery($query);
         $data->years = $db->loadColumn();
         $state->set('data', $data);
         return $state;
    }
 }