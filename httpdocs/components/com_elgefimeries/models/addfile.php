<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 include JPATH_COMPONENT_SITE . '/models/filelist.php';
 class ElgEfimeriesModelAddFile extends JModelDatabase
 { 
    public function getState() {
		
        $profile = JUserHelper::getProfile();
        $idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
        $idUser = $profile->get('id');
		$db =  DataUtils::getDB2();
		$query = $db->getQuery(true);
		$query->select('distinct k.PK_kliniki, k.kliniki')
		->from('Dim_klinikes k')
		->innerJoin('Hospital_Klinikes_Tomeis hkt on k.PK_kliniki = hkt.PK_kliniki and hkt.PK_Hospital = ' . $idHospital);
		$db->setQuery($query);
		$klinikes = $db->loadObjectList();
        $query->clear();
        $query->select("EfimeriaFileTypeID, EfimeriesFileTypeName")->from("t_EfimeriesFileTypes");
        $db->setQuery($query);
        $fileTypes = $db->loadObjectList();
        $state = parent::getState();
        $data = new stdClass();
        $data->klinikes = $klinikes;
        $data->fileTypes = $fileTypes;
        $path = 'images/elgefimeries/uploaded/efimeries';
        $directory = JPATH_SITE . "/" .$path;
        $query->clear();
		$query->select("distinct k.PK_kliniki")->from("Dim_klinikes k")->innerJoin('Hospital_Klinikes_Tomeis hkt on k.PK_kliniki = hkt.PK_kliniki and hkt.PK_Hospital = ' . $idHospital);
        $db->setQuery($query);
		$clinics = $db->loadColumn();
        $state->set("data", $data);
        $state->set("filesDirectory",$path );
        $state->set("importedDirectory", "images/elgefimeries/uploaded/efimeries_done");
        $dataImp = new stdClass();
		if(count($clinics) == 0) $clinics = 0;
		//$keys = array('Upload_UserID'=>$idUser); 
                 $keys = array('Upload_UserID'=>$idUser
                ,'UploadYear'=>$state->get('fltYear', 0)
                , 'UploadMonth'=>$state->get('fltMonth', 0)
                ,'UploadDate'=>CommonUtils::reformDateString($state->get('fltUploadDate', '0000-00-00'), 'd/m/Y', 'Y-m-d')
            ); 
        $dataImp->keys = $keys;
         $filters = array(
            'fltFileType'=>$state->get('fltFileType', 0),
            'fltYear'=>$state->get('fltYear', 0),
            'fltMonth'=>$state->get('fltMonth', 0),
            'fltUploadDate'=>$state->get('fltUploadDate', '')
            ); 
           $dataImp->limit = $state->get('limit', 10);
        $dataImp->limitstart = $state->get('limitstart', 0);
         $dataImp->filters = $filters;
        $filesModel = new ElgEfimeriesModelFileList(new JRegistry($dataImp));
        $filesState = $filesModel->getState();
        $state->set('importedData', $filesState->get('importedData'));
        $state->set('pagination', $filesState->get('pagination'));
        return $state;
       
    }    
   
 }