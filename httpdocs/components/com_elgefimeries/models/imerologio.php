<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelImerologio extends JModelBase
 { 
    public function render() {
        $state = $this->model->getState();
        $this->itemData = $state->get('itemData');
        $this->state = $state;
        return parent::render();
       
    }    
   
 }