<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 /**
  * Class f that holds static functions used by e-logism's components.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */


class NullFactory {
   
  public static function getNullCostCoef() {
        $coef = array();
        $coef['start_date'] = '';
        $coef['basicsalary'] = '';
        $coef['indexhourlyDay'] = '';
        $coef['indexhourlyNight'] = '';
        $coef['indexhourlyNightArgia'] = '';
        $coef['indexhourlyDayArgia'] = '';
        return $coef;
  }  

  public static function getNullCostActive() {
        $active = array();
        $active['EnergiDayFactor_1'] = '';
        $active['EnergiDayFactor_2'] = '';
        $active['EnergiDayFactor_3'] = '';
        $active['EnergiDayFactor_4'] = '';
        $active['EnergiDayFactor_5'] = '';
        $active['EnergiNightFactor_1'] = '';
        $active['EnergiNightFactor_2'] = '';
        $active['EnergiNightFactor_3'] = '';
        $active['EnergiNightFactor_4'] = '';
        $active['EnergiNightFactor_5'] = '';
        $active['EnergiNightArgiaFactor_1'] = '';
        $active['EnergiNightArgiaFactor_2'] = '';
        $active['EnergiNightArgiaFactor_3'] = '';
        $active['EnergiNightArgiaFactor_4'] = '';
        $active['EnergiNightArgiaFactor_5'] = '';
        $active['EnergiDayArgiaFactor_1'] = '';
        $active['EnergiDayArgiaFactor_2'] = '';
        $active['EnergiDayArgiaFactor_3'] = '';
        $active['EnergiDayArgiaFactor_4'] = '';
        $active['EnergiDayArgiaFactor_5'] = '';
        return $active;
  }
  
  public static function getNullCostReady()
  {
      $ready = array();
      $ready['Etoimotitasfactor_1'] = '';
      $ready['Etoimotitasfactor_2'] = '';
      $ready['Etoimotitasfactor_3'] = '';
      $ready['Etoimotitasfactor_4'] = '';
      $ready['Etoimotitasfactor_5'] = '';
      return $ready;
  }
  
  public static function getNullCostMixed()
  {
       $mixed = array();
       $mixed['MiktiDayFactor_1'] = '';
       $mixed['MiktiDayFactor_2'] = '';
       $mixed['MiktiDayFactor_3'] = '';
       $mixed['MiktiDayFactor_4'] = '';
       $mixed['MiktiDayFactor_5'] = '';
       $mixed['MiktiNightFactor_1'] = '';
       $mixed['MiktiNightFactor_2'] = '';
       $mixed['MiktiNightFactor_3'] = '';
       $mixed['MiktiNightFactor_4'] = '';
       $mixed['MiktiNightFactor_5'] = '';
       return $mixed;
        
  }
  
  public static function getNullCost()
  {
      $o = new stdClass();
      $o->efimeria_cost_id = null;
      $o->proelefsi='';
      $o->t_EfimerevonBathmos_id=0;
      $o->EfimeriaType_id=0;
      $o->EfimeriaDayType_id=0;
      $o->efimeria_cost_value2=0;
      $o->start_date='';
      $o->end_date='';
      $o->flag=0;
      $o->basicsalary=0;
      $o->indexhourlyDay=0;
      $o->hourlyDay=0;
      $o->EnergiDayfactor=0;
      $o->indexhourlyNight=0;
      $o->hourlyNight=0;
      $o->EnergiNightfactor=0;
      $o->indexhourlyNightArgia=0;
      $o->hourlyNightArgia=0;
      $o->EnergiNightArgiafactor=0;
      $o->indexhourlyDayArgia=0;
      $o->hourlyDayArgia=0;
      $o->EnegiDayArgiafactor=0;
      $o->prostheti=0;
      $o->MiktiDayfactor=0;
      $o->MiktiNightfactor=0;
      $o->Etoimotitasfactor=0;
      return $o;
  }
  
}    



?>
