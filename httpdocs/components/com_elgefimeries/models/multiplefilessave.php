<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
    defined( '_JEXEC' ) or die( 'Restricted access' );
    require JPATH_COMPONENT_SITE . '/libraries/joomla/models/modelsave.php';
    jimport('joomla.filesystem.file');
 /**
  * Photos save model.
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgergonModelMultipleFilesSave extends ModelSave
 { 
    function setState(JRegistry $state) {
        parent::setState($state);  
        $storePlace = JPATH_ROOT . '/media/com_elgefimeries/images';
        
       
       
        $uploadFile->name;
        exit();
        JFile::getExt($uploadFile->name);
        $fileExt = strtolower(JFile::getExt($uploadFile->name));
         
        $this->tables['photos']->id = $state->get('id');
        if($fileExt != '')
        {
            $this->tables['photos']->photoExt = $fileExt;
        }
        $res = $this->tables['photos']->store();
        if(!$res)
        {
             
             $state->set('errors', $this->tables['photos']->getErrors());
        }
        else 
        {
         
            if($fileExt != '')
            {
                $id= $this->tables['photos']->id;
                $originalFile =  $storePlace  . '/' . $id ;
                JFile::upload($uploadFile->tmp_name, $originalFile . '.' . $fileExt);   
                CommonUtils::resizeImage($originalFile . '.' .$fileExt, $originalFile . '_thumb.' . $fileExt, 250, 170);
                CommonUtils::resizeImage($originalFile . '.' . $fileExt, $originalFile . '_med.' . $fileExt , 400, 300);
                CommonUtils::resizeImage( $originalFile . '.' . $fileExt, $originalFile . '_large.' . $fileExt , 800, 620);
            }
        }
    }
}
?>