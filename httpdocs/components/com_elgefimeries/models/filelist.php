<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelFileList extends JModelDatabase
 {
      public function getState() {
	  
			
          $state = parent::getState();
          $keys = $state->get('keys');
          $queryType = $state->get('queryType', 1);
          $where = '';
          $andV = '';   
          $db =  DataUtils::getDB2(); 
          foreach($keys as $key=>$value) {
              if(is_array($value)) {
                  $where .= $andV . " " . $key  . " in (" . implode(",", $value) . ")"; 
                  $andV = " and ";
              }
              elseif($value != 0 && $value !='' && $value!= '0000-00-00') {
                  
                $where .= $andV . " " . $key . " = " . $db->quote($value);
                $andV = " and ";
              }
              
          }
		 // var_dump($keys);
          unset($key);
          unset($value);
		   
         
		  
            $query = $db->getQuery(true);
            $query->clear();
            
            $order = CommonUtils::getOrderClause($state->get('filter_order', ''), $state->get('filter_order_Dir', ''));
            if($queryType == 1)
            {
                $query->clear();
                $query->select('count(*) as cnt')->from('t_UploadsEfimeries');
                if($where != '')
                {
                    $query->where($where);
                }
                $db->setQuery($query);
                $cnt = $db->loadResult();
                $query->clear();
		$query->setQuery('select * from (select row_number() over (order by UploadDate desc) as rownumber,
                    UploadFileName, UploadDate, k.Kliniki, eft.EfimeriesFileTypeName,   
                    ef.UploadYear, ef.UploadMonth, ef.UploadIntegrationDate, ef.UploadIntegrationPass, Upload_UserID
                    FROM t_UploadsEfimeries ef
                    INNER JOIN Dim_klinikes k on k.PK_kliniki = ef.Upload_Kliniki_id
                    INNER JOIN t_EfimeriesFileTypes eft on ef.Upload_EfimeriaFileTypeID = eft.EfimeriaFileTypeID
                    ' . ($where != '' ? ' where ' . $where : '') . ' ' . $order . '
                    ) as tb1
                    where rownumber between ' . ($state->get('limitstart') + 1) . ' and ' . ( $state->get('limitstart') + $state->get('limit'))  
                          );
               //echo $query->dump();
                        
            }
		elseif($queryType == 2)
		{
                    $query->clear();
                    $query->select('count(*) as cnt')->from('t_UploadsOrgan');
                    if($where != '')
                    {
                        $query->where($where);
                    }
                    $db->setQuery($query);
                    $cnt = $db->loadResult();
                    $query->clear();
                    $query->setQuery(
                            'select * from (select row_number() over (order by UploadDate desc) as rownumber, UploadID, Upload_UserID, UploadFileName, UploadDate, UploadIntegrationDate, UploadDateValidFrom, UploadIntegrationPass 
                                            from t_UploadsOrgan ' . ($where != '' ? ' where ' . $where : '') . ' ' . $order . ') as tb1
                                            where rownumber between ' . ($state->get('limitstart') + 1) . ' and ' . ( $state->get('limitstart') + $state->get('limit'))  
                                            
                            
                            );
//			$query->select('UploadID, Upload_UserID, UploadFileName, UploadDate, UploadIntegrationDate, UploadDateValidFrom, UploadIntegrationPass')
//				->from('t_UploadsOrgan ') 
//				->order('UploadDate, UploadIntegrationDate');
                    
				
		}
		elseif($queryType == 3 ) //files with types
		{
                    $query->clear();
                    $query->select('count(*) as cnt')->from('t_UploadsEsynet');
                    if($where != '')
                    {
                        $query->where($where);
                    }
                    $db->setQuery($query);
                    $cnt = $db->loadResult();
                    $query->clear();
                    $query->setQuery('select * from(
                                        select  row_number() over (order by UploadDate desc) as rownumber,
                                        UploadID, Upload_UserID, Upload_EsynetFileTypeID, UploadDate, 
                                        UploadFileName, UploadFilePath, UploadYear, UploadMonth, 
                                        UploadIntegrationPass, UploadIntegrationDate, EsynetFileTypeName
                                        FROM t_UploadsEsynet u
                                        INNER JOIN t_EsynetFileTypes ft on u.Upload_EsynetFileTypeID = ft.EsynetFileTypeID '
                                        . ($where != '' ? ' where ' . $where : '') . 
                                        ' ' . $order . ' 
                                        ) as tb1
                                        where rownumber between ' . ($state->get('limitstart') + 1) . ' and ' . ( $state->get('limitstart') + $state->get('limit'))
                            );
                   
                    //$db->setQuery($query);
                    
		}
                //echo $query->dump();
                $db->setQuery($query);
                
                $importedData =  $db->loadObjectList();
                $filters = $state->get('filters', array());
                $additional = array();
                foreach($filters as $key=>$value)
                {
                    $additional[$key] = $value;
                }
				
//                $additional['filter_order'] = $state->get('filter_order', '');
//                $additional['filter_order_Dir'] = $state->get('filter_order_Dir', '');
                $pagination = CommonUtils::getPagination($cnt, $state->get('limitstart'), $state->get('limit'), $additional);
		$state->set('pagination', $pagination);
                $state->set('importedData', $importedData);
                 
		  //echo $query->dump();
		 
		  
		 
          return $state;
      }
      
 }