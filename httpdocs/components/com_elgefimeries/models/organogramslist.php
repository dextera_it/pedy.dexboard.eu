<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelOrganogramsList extends JModelDatabase
 {
     public function getState() {
		
        $state = parent::getState();
		$year = $state->get('year', date('Y'));
		
		$profile = JUserHelper::getProfile();
		$idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
		
		$data = new stdClass();
		$db =  DataUtils::getDB2(); //$this->getDb();
		$query = $db->getQuery(true);
		$db->setQuery($query);
		$query->select('max(UploadDate) as UploadDate, UploadFileName, Hospital')
		->from('t_UploadsOrgan o')
		->innerJoin('Dim_Hospital h on o.Upload_HuID = h.PK_Hospital ' .  ($year > 0 ? ' and year(o.UploadDate) = ' . $year : ''))
		
		->group('UploadFileName, Hospital') 
		->order('Hospital');
		
		$organograms = $db->loadAssocList();
		$data->organograms = $organograms;
		$state->set('data', $data); 
		return $state;
	}
 }