<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );

 /**
  * Class f that holds static functions used by e-logism's components.
  * @package e-logism.joomla;
  * @author Christoforos J. Korifidis
  * 
  */


class DataUtils {
   
  public static function getDB2() {
	$option = array(); 
	$option['driver']   = 'sqlsrv'; 
	$option['host']     =  '192.168.10.171';    
	$option['user']     = 'sa';       
	$option['password'] =  'xdR5$#@!';   
	$option['database'] = 'efimeries';      
	$option['prefix']   = 't_';      
	return JDatabase::getInstance( $option );
  }

   public static function getCost($filters, $dbIn = null, $queryIn = null)
  {
        
        if(count($filters) > 0)
        {
            $db = ($dbIn == null ? self::getDB2() : $dbIn);
            $query = ($queryIn == null ? $db->getQuery(true) : $queryIn);
            $db->setQuery($query);
            $query->clear();
            $and = ' and '; $where = ' end_date >= getdate() ';
            foreach($filters as $key=>$value)
            {
                $where .= $and . ' ' . $db->quoteName($key) . ' = ' . $db->quote($value);
                
            }
            unset($key);
            unset($value);
            $query = 'select efimeria_cost_id, proelefsi, t_EfimerevonBathmos_id
            , EfimeriaType_id, EfimeriaDayType_id, efimeria_cost_value2
            ,start_date, end_date, flag
            ,basicsalary, indexhourlyDay, hourlyDay
            ,EnergiDayfactor, indexhourlyNight, hourlyNight 
            ,EnergiNightfactor, indexhourlyNightArgia, hourlyNightArgia 
            ,EnergiNightArgiafactor, indexhourlyDayArgia, hourlyDayArgia
            ,EnegiDayArgiafactor, prostheti , MiktiDayfactor
            ,MiktiNightfactor, Etoimotitasfactor
            from t_Cost where ' . $where . ' order by start_date desc';
            /**
            $query->select('efimeria_cost_id, proelefsi, t_EfimerevonBathmos_id
            , EfimeriaType_id, EfimeriaDayType_id, efimeria_cost_value2
            ,start_date, end_date, flag
            ,basicsalary, indexhourlyDay, hourlyDay
            ,EnergiDayfactor, indexhourlyNight, hourlyNight 
            ,EnergiNightfactor, indexhourlyNightArgia, hourlyNightArgia 
            ,EnergiNightArgiafactor, indexhourlyDayArgia, hourlyDayArgia
            ,EnegiDayArgiafactor, prostheti , MiktiDayfactor
            ,MiktiNightfactor, Etoimotitasfactor')
            ->from('t_Cost')
            ->where($where)
            ->order('start_date desc');
             * 
             */
            //$dbg = $query->dump();
            $db->setQuery($query);
            return $db->loadObjectList();
        }
        else {
            return array();
        }
  }
 
  

}    



?>
