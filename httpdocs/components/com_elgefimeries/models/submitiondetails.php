<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 include JPATH_COMPONENT_SITE . '/models/filelist.php';
 class ElgEfimeriesModelSubmitionDetails extends JModelDatabase
 {
      public function getState() {
        $state = parent::getState();
        $year = $state->get('year',0);
        $month = $state->get('month', 0);
        $idFileGroup = $state->get('idFileGroup', 0);
        $idHospital = $state->get('idHospital',0);
        
        $data= new stdClass();
        switch($idFileGroup) {
            case 2: 
                $filesDirectory = "images/elgefimeries/uploaded/organ";
                $importedDirectory = "images/elgefimeries/uploaded/organ_done";
                $filePrefix = "";
                break;
            default :
                $filesDirectory = "images/elgefimeries/uploaded/efimeries";
                $importedDirectory = "images/elgefimeries/uploaded/efimeries_done";            
                $filePrefix = "";
        }
        $db = DataUtils::getDB2(); //$db = $this->getDb();
        $query = $db->getQuery(true);
        $db->setQuery($query);
        $query->select("PK_kliniki")->from("Hospital_Klinikes_Tomeis")->where("PK_Hospital=" . $idHospital);
        $clinics = $db->loadColumn();
        // $query->clear();
        // $query->select("EfimeriaFileTypeID")->from("t_EfimeriesFileTypes");
        // if($idFileGroup > 0) 
        // {
            // $query->where("idFileGroup = " . $idFileGroup);
        // }
        //$fileType = $db->loadColumn();
        $query->clear();
        $query->select("EfimeriaFileTypeID, EfimeriesFileTypeName")->from("t_EfimeriesFileTypes");
        $fileTypeName = $db->loadObjectList("EfimeriaFileTypeID");
        $fileTypeIDs = array();
		foreach($fileTypeName as $item)
		{
			$fileTypeIDs[] = $item->EfimeriaFileTypeID;
		}
		unset($item);
		$fileTypeFilter = implode($fileTypeIDs);
		
		// $allFiles = scandir(JPATH_SITE . "/" . $filesDirectory);
        // $userFiles = array();
        
		
		// foreach($allFiles as $item)
        // {
            // $parts = explode("_", $item);
            // $clinic = str_replace($filePrefix, "",$parts[0]);
            // if( in_array($clinic, $clinics))
            // {
                // if($parts[2] == $year )
                // {
                    // if($parts[3] == $month)
                    // {
                        // if(in_array($parts[1], $fileType)) {
                            // $userFiles[] = array($item, filemtime($filesDirectory), $fileTypeName[$parts[1]]);
                        // }
                        
                    // }
                // }
           // }
        // }
        
		// $data->files = $userFiles;
        // $state->set("data", $data);
        $state->set("filesDirectory",$filesDirectory );
        $state->set("importedDirectory", $importedDirectory);
		$filters = array(
            
            'fltUploadDate'=>$state->get('fltUploadDate', ''),
            'fltYear'=>$state->get('fltYear', ''),
			'fltMonth'=>$state->get('fltMonth', ''),
          ); 
		 //var_dump($filters);
        $dataImp = new stdClass();
        $keys = array('Upload_HuID'=>$idHospital, "UploadYear"=>$filters['fltYear'], "UploadMonth"=>$filters['fltMonth']);
        $dataImp->keys = $keys;
		$dataImp->limit = $state->get('limit', 10);
		$dataImp->limitstart = $state->get('limitstart', 0);
		$dataImp->filters = $filters;
        $filesModel = new ElgEfimeriesModelFileList(new JRegistry($dataImp));
        $filesState = $filesModel->getState();
        $state->set('importedData', $filesState->get('importedData'));
		
        $state->set('pagination', $filesState->get('pagination'));
        return $state;
      }
 }