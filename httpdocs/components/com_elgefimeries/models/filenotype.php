<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 include JPATH_COMPONENT_SITE . '/models/filelist.php';
 class ElgEfimeriesModelFileNoType extends JModelDatabase
 { 
    public function getState() {
        $profile = JUserHelper::getProfile();
        $idUser = $profile->get('id');
        $idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
        $path = "images/elgefimeries/uploaded/organ";
        $db = $this->getDb();
        $query = $db->getQuery(true);
        $query->select("id, kliniki")->from("#__elg_efimeries_kliniki")->where("idNosokomio=" . $idHospital)->order("kliniki desc");
        
        $db->setQuery($query);
        $klinikes = $db->loadObjectList();
        $query->clear();
        $query->select("id, fileType")->from("#__elg_efimeries_filetype")->order("fileType");
        $db->setQuery($query);
        $fileTypes = $db->loadObjectList();
        $state = parent::getState();
        $data = new stdClass();
        $data->klinikes = $klinikes;
        $data->fileTypes = $fileTypes;
        
        $state->set("data", $data);
        $state->set("filesDirectory",$path );
        $state->set("importedDirectory", "images/elgefimeries/uploaded/organ_done");
        $dataImp = new stdClass();
        $keys = array(
                'Upload_UserID'=>$idUser,               
                'UploadDate'=>CommonUtils::reformDateString($state->get('fltUploadDate', '0000-00-00'), 'd/m/Y', 'Y-m-d'),
                'UploadDateValidFrom'=>CommonUtils::reformDateString($state->get('fltValidDate', '0000-00-00'), 'd/m/Y', 'Y-m-d')
                );
         $filters = array(
            
            'fltUploadDate'=>$state->get('fltUploadDate', ''),
            'fltValidDate'=>$state->get('fltValidDate', '')
            ); 
        $dataImp->keys = $keys;
        $dataImp->queryType = 2;
        $dataImp->limit = $state->get('limit', 10);
        $dataImp->limitstart = $state->get('limitstart', 0);
        $dataImp->filters = $filters;
        $filesModel = new ElgEfimeriesModelFileList(new JRegistry($dataImp));
        $filesState = $filesModel->getState();
        $state->set('importedData', $filesState->get('importedData'));
        $state->set('pagination', $filesState->get('pagination'));
        return $state;
       
    }    
   
 }