<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */

 class ElgEfimeriesModelAddKliniki extends JModelDatabase
 {
     public function getState() {
	
	$state = parent::getState();
		$data = new stdClass();
		$db =  DataUtils::getDB2();
		$query = $db->getQuery(true);
		$query->select('PK_Tomeas, Tomeas')->from('Dim_Tomeis');
		$db->setQuery($query);
		$tomeis = $db->loadObjectList();
		$data->tomeis = $tomeis;
		$state->set('data', $data); 
		return $state;
	}
 }