<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 /**
  * @package e-logism.joomla.ergon.site
  * @subpackage models
  * @author Christoforos J. Korifidis.
  * 
  */
 
 class ElgEfimeriesModelSubmitionSums extends JModelDatabase
 {
     public function getState() {
	
         $state = parent::getState();
         $idFileGroup = $state->get('idFileGroup', 1);
         $year = $state->get('year', date('Y'));
         $data = new stdClass();
         $db =  DataUtils::getDB2(); //$this->getDb();
         $query = $db->getQuery(true);
        $db->setQuery($query);
		$query->clear();
        $query->select("count(*) as cnt")->from("t_EfimeriesFileTypes");
        $fileGroups = $db->loadResult();
		$data->fileGroups = ($fileGroups == null ? 0 : $fileGroups);
       
		$query->clear();
        $query->select("PK_Hospital, count(*) as cnt")->from("Hospital_Klinikes_Tomeis")->group("PK_Hospital");
        $data->clinics = $db->loadObjectList("PK_Hospital");
		
		// $data->clinics = array();
		
         $query->clear();
         $query->select("h.PK_Hospital, Hospital as unit, u.UploadYear, u.UploadMonth, /**ft.idFileGroup, **/ count(*) as totalUploads")
                ->from("Dim_Hospital h")
				->innerJoin('Hospital_Klinikes_Tomeis hkt on h.PK_Hospital = hkt.PK_Hospital')
                 ->innerJoin('Dim_Klinikes k on k.PK_Kliniki = hkt.PK_Kliniki')
                 ->innerJoin('t_UploadsEfimeries u on k.PK_Kliniki = u.Upload_Kliniki_id ' . ($year > 0 ? ' and UploadYear = ' . $year : ''))
                 ->innerJoin('t_EfimeriesFileTypes ft on ft.EfimeriaFileTypeID = u.Upload_EfimeriaFileTypeID')
                // ->innerJoin("#__elg_efimeries_filegroup fg on fg.id = ft.idFileGroup " . ($idFileGroup >0 ? " and idFileGroup=" . $idFileGroup : ""))
                 ->group("h.PK_Hospital, h.Hospital, u.UploadYear, u.UploadMonth/**,ft.idFileGroup **/")
                 ->order("unit, UploadYear desc, u.UploadMonth /.**, ft.idFileGroup **/");
         $dbg =  $query->dump();
       //   echo $dbg;
		//exit();
         $data->uploads = $db->loadObjectList();
		// var_dump($data->uploads);
		 //exit();
         $query->clear();
         $query->select("distinct UploadYear")->from("t_UploadsEfimeries")->order('UploadYear desc');
         //$db->setQuery($query);
         $data->years = $db->loadColumn();
         $state->set('data', $data);
		 $query->clear();
		 $query->select(" u.UploadYear, u.UploadMonth, u.Upload_EfimeriaFileTypeID" )
                //->from("Dim_Hospital h")
				->from('Hospital_Klinikes_Tomeis hkt')
                ->innerJoin('Dim_Klinikes k on k.PK_Kliniki = hkt.PK_Kliniki')
                ->innerJoin('t_UploadsEfimeries u on k.PK_Kliniki = u.Upload_Kliniki_id ' . ($year > 0 ? ' and UploadYear = ' . $year : ''))
                ->innerJoin('t_EfimeriesFileTypes ft on ft.EfimeriaFileTypeID = u.Upload_EfimeriaFileTypeID')
                // ->innerJoin("#__elg_efimeries_filegroup fg on fg.id = ft.idFileGroup " . ($idFileGroup >0 ? " and idFileGroup=" . $idFileGroup : ""))
                // ->group("h.PK_Hospital, h.Hospital, u.UploadYear, u.UploadMonth, u.Upload_EfimeriaFileTypeID /**,ft.idFileGroup **/")
				->where('hkt.PK_Hospital = 21 and u.UploadMonth = 5');
				// ->group("h.PK_Hospital, h.Hospital, u.UploadYear, u.UploadMonth, u.Upload_EfimeriaFileTypeID /**,ft.idFileGroup **/")
              //   ->order("unit, UploadYear desc, u.UploadMonth, u.Upload_EfimeriaFileTypeID /.**, ft.idFileGroup **/");
		//$dbg = $db->loadObjectList();
		//echo var_dump($dbg);
		$state->set('dbg', $dbg);
		//var_dump($state->get('dbg'));
         return $state;
    }
 }