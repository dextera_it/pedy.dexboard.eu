<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerDeleteFile extends JControllerBase {
    
    public function execute() {
        $referer = $this->input->server->getRaw('HTTP_REFERER');
        $file = $this->input->getString('fltd');
		$qt = $this->input->getInt('qt', 1);
		$deleteFileType = $this->input->get('deleteFileType', 0);
        if(file_exists(JPATH_SITE . '/' . $file)) {

            $db2 = DataUtils::getDB2(); //JDatabase::getInstance( $option );
            $query2 = $db2->getQuery(true);
            $db2->setQuery($query2);
			if($qt == 1) 
			{
				$query2->setQuery(' delete from t_UploadsEfimeries where UploadFileName = ' . $db2->quote(basename($file)) );  
			}
			else
			{
				$query2->setQuery(' delete from t_UploadsOrgan where UploadFileName = ' . $db2->quote(basename($file)) );  
			}
			
			if($deleteFileType == 3)
			{
				$query2->setQuery('delete from t_UploadsEsynet where UploadFileName = ' . $db2->quote(basename($file)) );  
			}
     
            $res = $db2->execute();
		
            if($res)
            {

				$res = unlink(JPATH_SITE . '/' . $file);
                if($res) {
                    $msg = JText::_('COM_ELG_FILE_DELETE_SUCCESS');
                    $msgType = 'message';
                }
                else {
                    $msg = JText::_('COM_ELG_FILE_DELETE_FAILURE');
                    $msgType = 'error';
                }
            }
            else {
                $msg = JText::_('COM_ELG_ERROR_DELETE_FILE_FROM_DB');
            }
        }
        else {
            $msg = JText::_('COM_ELG_FILE_NOT_FOUND') . '<br />' . JText::_('COM_ELGEFIMERIES_FILE_PROPABLY_IMPORTED');
            $msgType = 'warning';
        }
        JFactory::getApplication()->enqueueMessage($msg, $msgType);
        JFactory::getApplication()->redirect($referer);      
		
    }

    
    public function redirect(){}

}
?>
