<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerCostSave extends JControllerBase{
    
    public function execute()
    {   
        $input = $this->getInput();
        $data = $input->getArray();
        $state = new JRegistry($data);
        require_once JPATH_COMPONENT_SITE . '/models/costsave.php';
		
        $model = new ElgEfimeriesModelCostSave();
		
        $model->setState($state);
       
    }
    
    public function redirect(){}
    

}
?>
