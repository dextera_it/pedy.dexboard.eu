<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerMultipleFilesSaveOrganogram extends JControllerBase{
    
        public function execute()
        {
   
            $db2 = DataUtils::getDB2(); 
			$query2 = $db2->getQuery(true);
            $db2->setQuery($query2);
           
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $db->setQuery($query);
            $importTable = 't_UploadsOrgan';
            
            $input = $this->getInput();
            $uploadedFiles = $input->files->get('url',null, 'ARRAY');
            if (!is_array($uploadedFiles))
            {
                exit('not array');
            }
               $profile = JUserHelper::getProfile();
            $idHospital = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
            $id = 'H' . $idHospital . '_' . date('Ymd');
            $storePlace = JPATH_ROOT . '/images/elgefimeries/uploaded/organ';
			
			$dt = DateTime::createFromFormat('d/m/Y', $input->getString('uploadDateValidFrom'));
            $validDateFrom = $dt->format('Y-m-d');
         
         
            
            foreach($uploadedFiles as $file)
            {
                
                $fileExt = strtolower(JFile::getExt($file['name']));
                   
                   if($fileExt != '')
                   {
                        $time = time();
                        $dateUpl = date('Y-m-d H:i:s', $time);
                      
                        $uDB = self::getNullUploadDB();
						$uDB->Upload_UserID = JFactory::getUser()->id;
						$uDB->UploadFileName = $id . '.' . $fileExt;
						$uDB->UploadDate = date('Y-m-d H:i:s');
						$uDB->UploadDateValidFrom = $validDateFrom;
						$uDB->Upload_HuID = $idHospital;
					   
                        $query2->clear();
                        $query2->select('UploadID, UploadIntegrationPass')->from('t_UploadsOrgan')->where('UploadFileName=\'' . $uDB->UploadFileName . '\'');
                        
                        $res = $db2->loadObject();
                      
						$uplodedPath =  $storePlace  . '/' .  $uDB->UploadFileName;
                      
                      
                      
                        if($res != null) 
                        {
                         
                            $isDone = $res->UploadIntegrationPass;
						
                            if($isDone == 0)
                            {
								echo 'canupdate';
                                    // $uDB->UploadID = $res->UploadID;
                                    // $db2->updateObject($importTable, $uDB, 'UploadID');
                                    // JFile::upload($file['tmp_name'],  $uplodedPath);   
                            }
                            else
                            {   
                               
                                    echo 'imported';
                            }
                             
                        }
                        else
                        {
                       
                           try {
                            $db2->insertObject($importTable, $uDB);
                           }catch(Exception $e) {
                               echo $e;
                           }
                          //echo $uplodedPath;
                            JFile::upload($file['tmp_name'], $uplodedPath);   
                        }
                      
                        //JFile::upload($file['tmp_name'], $uplodedPath);   
                          
                   }
            }

            unset($file);
          

            exit();
          
      
    }

     public static function getNullUploadDB() {
		$o = new stdClass();
		$o->UploadID = null;
		$o->Upload_UserID = '';
		$o->UploadFileName = '';
		$o->UploadDate= '';
		$o->UploadDate= '';
		$o->UploadFilePath = '';
		$o->UploadIntegrationPass = 0;
		$o->UploadDateValidFrom = '';
		$o->Upload_HuID = '';
		
		return $o;
	}
    public function redirect(){}
    
}
?>
