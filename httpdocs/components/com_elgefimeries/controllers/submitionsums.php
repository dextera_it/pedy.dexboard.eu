<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerSubmitionSums extends ItemControllerView {
    
    public function __construct($input = null, $app = null) {
		parent::__construct($input, $app);
        $this->basicData['fltYear'] = $this->input->getInt('fltYear', 0);
        $this->basicData['idHospital'] = $this->input->getInt('idHospital', 0);
        $this->basicData['idFileGroup'] = $this->input->getInt('idFileGroup', 1);
        $this->basicData['fltMonth'] = $this->input->getInt('fltMonth', 1);
		$this->basicData['fltUploadDate'] = $this->input->getString('fltUploadDate', '');        
		
    }
    
    

    
    //public function redirect(){}

}
?>
