<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 /// require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerMultipleFilesSave extends JControllerBase{
    
        public function execute()
        {
		
      
        $input = $this->getInput();
        $uploadedFiles = $input->files->get('url',null, 'ARRAY');
        if (!is_array($uploadedFiles))
        {
            exit('not array');
        }
		
    
            $storePlace = JPATH_SITE . '/images/elgefimeries/uploaded/efimeries';
		
			
			$db2 = DataUtils::getDB2(); 
		
			
			$query2 = $db2->getQuery(true);
			$db2->setQuery($query2);
			$importTable = 't_UploadsEfimeries';
            foreach($uploadedFiles as $file)
            {
                
                $fileExt = strtolower(JFile::getExt($file['name']));
                   
                   if($fileExt != '')
				   {	
						$uploadFileName = '';
						$KlinikiID = $input->getInt('clinics',0);
						$query2->clear();
						$query2->select('PK_Hospital')->from('Hospital_Klinikes_Tomeis')->where('PK_kliniki=' . $KlinikiID);
						try
						{
							$HuID = $db2->loadResult();
							if ($HuID == '')
							{
								$profile = JUserHelper::getProfile();
								$HuID = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
							}
							
						}
						catch(Exception $e)
						{
							echo $e;
							exit();
						}
						if($KlinikiID == -1)
						{
							$HuID = (isset($profile->elgefimeries["idHospital"])? $profile->elgefimeries["idHospital"]: 0);
							$KlinikiID = $KlinikiID. $HuID;
						}
						
						$fileType = $input->getInt('fileTypes',0);
						$UploadFileYear = $input->get("years",0);
						$UploadFileMonth = $input->get("months",0);
						$id = 'K' . $KlinikiID . '_T'  . $fileType . '_' . $UploadFileYear . '_' . str_pad($UploadFileMonth,2,'0',STR_PAD_LEFT);
                        $originalFile =  $storePlace  . '/' . $id ;
						//echo $originalFile;
						//exit();
						$uplodedPath = $storePlace  . '/' . $id . '.' . $fileExt;
						
						$query2->clear();
						$query2->select('UploadID, UploadIntegrationPass')->from('t_UploadsEfimeries')->where('UploadFileName=\'' . $id . '.'. $fileExt . '\'');
						$uDB = $this->getNullUploadDB();
						$uDB->UploadFileName = $id . '.' . $fileExt;
						$uDB->Upload_UserID = JFactory::getUser()->id;
						$uDB->Upload_Kliniki_id = $KlinikiID;
						$uDB->Upload_HuID = $HuID;
						
						$uDB->Upload_EfimeriaFileTypeID = $fileType;
						$uDB->UploadDate = date('Y-m-d H:i:s');
						
						$uDB->UploadYear = $UploadFileYear;
						$uDB->UploadMonth = $UploadFileMonth;
						
						$res = $db2->loadObject();
						
						if($res != null) 
						{
							//echo 1;
							$isDone = $res->UploadIntegrationPass;
							
							if($isDone == 0)
							{
								// $uDB->UploadID = $res->UploadID;
								// $db2->updateObject($importTable, $uDB, 'UploadID');
								// JFile::upload($file['tmp_name'],  $uplodedPath);   
								echo 'canupdate';
							}
							else
							{
								echo 'imported';
							}
						}
						else
						{
							try
							{
								$db2->insertObject($importTable, $uDB);
							}
							catch(Exception $e)
							{
								echo $e;
							}
							
							JFile::upload($file['tmp_name'],$uplodedPath);   
						}
					
					}
				
            }
            unset($file);
      
            exit();
          
       
    }
	function getNullUploadDB() {
		$o = new stdClass();
		$o->UploadID = null;
		$o->Upload_UserID = '';
		$o->Upload_EfimeriaFileTypeID = 0;
		$o->Upload_Kliniki_id = 0;
		$o->Upload_HuID= 0;
		$o->UploadDate= 0;
		$o->UploadFileName = '';
		$o->UploadFilePath = '';
		$o->UploadYear = 0;
		$o->UploadMonth = 0;
		
		
		return $o;
	}
    
    public function redirect(){}

}
?>
