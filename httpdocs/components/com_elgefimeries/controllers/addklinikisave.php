<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 require_once JPATH_COMPONENT_SITE . '/models/addklinikisave.php';
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerAddKlinikiSave extends JControllerBase
{
    
	public function execute()
	{
		$input = $this->getInput();
		$kliniki = $input->getString('kliniki', '');
		$idTomeas = $input->getInt('idTomeas');
		$model = new ElgEfimeriesModelAddKlinikiSave();
		$state = new JRegistry(array('kliniki'=>$kliniki, 'idTomeas'=>$idTomeas));
		$model->setState($state);
	}
	
	public function redirect()
	{
		$this->redirectUrl = '?option=com_elgefimeries&view=addkliniki';
	}

}
?>
