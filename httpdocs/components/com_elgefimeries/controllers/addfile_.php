<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerAddFile extends JControllerForm {
    
    public function execute() {
        $doc = JFactory::getDocument();
        $doc->addStyleSheet('media/' . $this->basicData['option'] . '/css/site.stylesheet.css');
        $viewClass = Utils::COMPONENT_NAME . 'View' . $this->basicData['view'];
        parent::execute();
        $view = new $viewClass($this->model, $this->paths);
        $view->setLayout($this->basicData['layout']);
        $m = $this->getModel('filelist');
        $view->setModel($m);
        $view->basicData = $this->basicData;
        $this->view = $view;
           
        echo $this->view->render();
        
        
       
    }

    
    public function redirect(){}

}
?>
