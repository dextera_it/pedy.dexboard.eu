<?php
/**
 * @copyright (c) 2013, e-logism.
 * @license http://www.gnu.org/licenses/gpl.txt GPL version 3.
 * 
 */
 defined( '_JEXEC' ) or die( 'Restricted access' );
 jimport('joomla.filesystem.file');
 /// require JPATH_COMPONENT_SITE .'/libraries/joomla/controllers/itemcontrollerview.php';
 
 /**
  * elgergon controller that saves mulitple uploaded .
  * @package e-logism.joomla.ergon.site
  * @subpackage controllers
  * @author Christoforos J. Korifidis
  * 
  */
class ElgEfimeriesControllerFilesSaveEsynet extends JControllerBase{
    
        public function execute()
        {
		
      
        $input = $this->getInput();
        $uploadedFiles = $input->files->get('url',null, 'ARRAY');
		
		$answer=$input->getInt('a',0);
        if (!is_array($uploadedFiles))
        {
            exit('not array');
        }
		$storePlace = JPATH_SITE . '/images/elgefimeries/uploaded/esynet';
		$db2 = DataUtils::getDB2(); 
		$query2 = $db2->getQuery(true);
		$db2->setQuery($query2);
		$importTable ='t_UploadsEsynet';
		
		foreach($uploadedFiles as $file)
		{
		
			$fileExt = strtolower(JFile::getExt($file['name']));
			if($fileExt != '')
				{
				
					$fileType = $input->getInt('fileTypes',0);
					$UploadFileYear = $input->get("years",0);
					$UploadFileMonth = $input->get("months",0);
					$id = 'T' . $fileType . '_'  . $UploadFileYear . '_' . str_pad($UploadFileMonth,2,'0',STR_PAD_LEFT);
                    $originalFile =  $storePlace  . '/' . $id ;
					$uplodedPath = $storePlace  . '/' . $id . '.' . $fileExt;
					$query2->clear();
					
					$query2->select('UploadID, UploadIntegrationPass')->from($importTable)->where('UploadFileName=\'' . $id . '.'. $fileExt . '\'');
					$res = $db2->loadObject();
					$uDB = $this->getNullUploadDB();
					$uDB->UploadFileName = $id . '.' . $fileExt;
					$uDB->Upload_UserID = JFactory::getUser()->id;
					$uDB->Upload_EsynetFileTypeID = $fileType;
					$uDB->UploadDate = date('Y-m-d H:i:s');
					$uDB->UploadYear = $UploadFileYear;
					$uDB->UploadMonth = $UploadFileMonth;
					
					if($res != null) 
						{
							$isDone = $res->UploadIntegrationPass;
							if($isDone === 0)
							{
								if($a ==1)
								{
									$uDB->UploadID = $res->UploadID;
									$db2->updateObject($importTable, $uDB, 'UploadID');
									JFile::upload($file['tmp_name'],  $uplodedPath);   
								}
								else
								{
									echo 'canupdate';
								}
							}
							else
							{
								echo 'imported';
							}
						}
						else
						{
							try
							{
								$db2->insertObject($importTable, $uDB);
							}
							catch(Exception $e)
							{
								echo $e;
							}
							JFile::upload($file['tmp_name'],$uplodedPath);   
						}
					}
            }
            unset($file);
      
            exit();
          
       
    }
	function getNullUploadDB() {
		$o = new stdClass();
		$o->UploadID = null;
		$o->Upload_UserID = '';
		$o->Upload_EsynetFileTypeID = 0;
		$o->UploadDate = '';
		$o->UploadFileName= '';
		$o->UploadFilePath= '';
		$o->UploadYear = '';
		$o->UploadMonth = '';
		$o->UploadIntegrationPass = 0;
		return $o;
	}
    
    public function redirect(){}

}
?>
