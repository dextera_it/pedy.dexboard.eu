<?php
/* ------------------------------------------------------------------------
  # com_elgefimeries e-logism
  # ------------------------------------------------------------------------
  # author    e-logism
  # copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
  # @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
  # Websites: http://www.e-logism.gr

  ----------------------------------* */
defined('_JEXEC') or die('Restricted access');
$filesDirectory = $this->filesDirectory;
$importedDirectory = $this->importedDirectory;
// $unimportedFiles = $this->unimportedFiles;   
?>      
<div class="filters">
    <fieldset style="border-style: solid; border-width:1px; border-color:#DDDDDD; border-radius: 4px; margin: 1ex 0; ">
        <legend style="width:12ex;font-weight:bold; border: none;font-size: 14px;margin:1ex"><?php echo JText::_('COM_ELG_SEARCH'); ?></legend>

        <div class="col" >
            <label for="fltFileType" ><?php echo Jtext::_('COM_ELGEFIMERIES_FILE_TYPE_TO_ATTACH'); ?></label>
            <select id="fltFileType" name="fltFileType"  class="form-control"> </select>
        </div>
        <div class="col" >
            <label for="fltYear" ><?php echo Jtext::_('COM_ELGEFIMERIES_REL_YEAR'); ?></label>
            <select id="fltYear" name="fltYear" class="form-control" >
                <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                <?php for ($i = 2010; $i <= 2015; $i++): ?>
                    <option value="<?php echo $i; ?>"  ><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>
        </div>

        <div class="col">
            <label for="fltMonth" ><?php echo Jtext::_('COM_ELGEFIMERIES_REL_MONTH'); ?></label>
            <select id="fltMonth" name="fltMonth"  class="form-control">
                <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                <option value="1" ><?php echo JText::_('JANUARY'); ?></option>
                <option value="2" ><?php echo JText::_('FEBRUARY'); ?></option>
                <option value="3" ><?php echo JText::_('MARCH'); ?></option>
                <option value="4" ><?php echo JText::_('APRIL'); ?></option>
                <option value="5" ><?php echo JText::_('MAY'); ?></option>
                <option value="6" ><?php echo JText::_('JUNE'); ?></option>
                <option value="7" ><?php echo JText::_('JULY'); ?></option>
                <option value="8" ><?php echo JText::_('AUGUST'); ?></option>
                <option value="9" ><?php echo JText::_('SEPTEMBER'); ?></option>
                <option value="10" ><?php echo JText::_('OCTOBER'); ?></option>
                <option value="11" ><?php echo JText::_('NOVEMBER'); ?></option>
                <option value="12" ><?php echo JText::_('DECEMBER'); ?></option>
            </select>
        </div>
        <div class="col">
            <label for="fltUploadDate" ><?php echo Jtext::_('COM_ELGEFIMERIES_FILE_UPLOAD'); ?></label>
            <div class="input-append date" id="fltUploadDate"  data-date-format="dd/mm/yyyy">
                <input size="16" type="text" name="fltUploadDate" value="<?php echo ($this->state->get('fltUploadDate') == '0000-00-00' || $this->state->get('fltUploadDate') == '0' ? '' : $this->state->get('fltUploadDate')); ?>"  >
                <span class="add-on"><i class="icon-calendar"></i></span>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col">
            
            <button class="btn btn-primary" style="margin-top:1ex"><?php echo Jtext::_('COM_ELG_SEARCH'); ?></button>
        </div>

    </fieldset>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var uD = jQuery('#fltUploadDate').datetimepicker(
                {
                    todayHighlight: true,
                    language: 'el',
                    autoclose: true,
                    minView: 2,
                    startView: 2
                });
    });
    function fillFilterFileTypes(options, fltFileType) {
        var fltFileTypeElm = jQuery('#fltFileType');
        fltFileTypeElm.html(options);
        if (fltFileType == 0)
            fltFileType = '';
        fltFileTypeElm.val(fltFileType);
    }

    function setFilterMonth(fltMonth) {
        if (fltMonth == 0)
            fltMonth = '';
        jQuery('#fltMonth').val(fltMonth);
    }

    function setFilterYear(fltYear) {
        if (fltYear == 0)
            fltYear = '';
        jQuery('#fltYear').val(fltYear);
    }

    function setFilterUploadDate(fltUploadDate) {
        if (fltUploadDate == '0000-00-00')
            fltUploadDate = '';
        //jQuery('[name=fltUploadDate]').val(fltUploadDate);
    }
</script>
