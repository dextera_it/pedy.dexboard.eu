<?php

    defined('_JEXEC') or die('Restricted access');
   
?>

        <div class="user-options" >
			<input type="hidden" name="clinics" value="-1" />
            
            <div class="form-group">
                <label for="fileTypes" ><?php echo Jtext::_('COM_ELGEFIMERIES_FILE_TYPE_TO_ATTACH'); ?></label>
                <select id="fileTypes" name="fileTypes"  class="form-control">

                </select>
            </div>
            <div class="form-group" >
                <label for="clinics" ><?php echo Jtext::_('COM_ELGEFIMERIES_YEAR'); ?></label>
                <select id="years" name="years" class="form-control" >
                    <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                    <?php for($i = 2010; $i<= 2015; $i++):  ?>
                    <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="months" ><?php echo Jtext::_('COM_ELGEFIMERIES_MONTHS'); ?></label>
                <select id="months" name="months"  class="form-control">
                       <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                       <option value="1" ><?php echo JText::_('JANUARY'); ?></option>
                       <option value="2" ><?php echo JText::_('FEBRUARY'); ?></option>
                       <option value="3" ><?php echo JText::_('MARCH'); ?></option>
                       <option value="4" ><?php echo JText::_('APRIL'); ?></option>
                       <option value="5" ><?php echo JText::_('MAY');?></option>
                       <option value="6" ><?php echo JText::_('JUNE'); ?></option>
                       <option value="7" ><?php echo JText::_('JULY'); ?></option>
                       <option value="8" ><?php echo JText::_('AUGUST'); ?></option>
                       <option value="9" ><?php echo JText::_('SEPTEMBER'); ?></option>
                       <option value="10" ><?php echo JText::_('OCTOBER'); ?></option>
                       <option value="11" ><?php echo JText::_('NOVEMBER'); ?></option>
                       <option value="12" ><?php echo JText::_('DECEMBER'); ?></option>
                </select>
            </div>
        </div>
        </form>
		<script type="text/javascript" >
			clinics = new Array(1);
		</script>