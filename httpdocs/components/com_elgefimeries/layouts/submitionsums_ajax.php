<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
   
    $uploads = $this->data->uploads;
	
    $fileGroups = $this->data->fileGroups;
    if(isset($this->data->clinics)) $clinics = $this->data->clinics;
    $selectedYear = $this->state->get('year', date('Y'));
  
    
?>
<div style="display:none" >
<?php
echo $this->dbg;
	// foreach($this->dbg as $item)
	// {
		// var_dump($item);
// echo 		'<br />';
	// }		
?>
</div>
<table class="table table-bordered sum">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>ΙΑΝ</th>
            <th>ΦΕΒ</th>
            <th>ΜΑΡ</th>
            <th>ΑΠΡ</th>
            <th>ΜΑΙ</th>
            <th>ΙΟΥΝ</th>
            <th>ΙΟΥΛ</th>
            <th>ΑΥΓ</th>
            <th>ΣΕΠ</th>
            <th>ΟΚΤ</th>
            <th>ΝΟΕ</th>
            <th>ΔΕΚ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $currentYear = date('Y');
        $currentMonth = date('m');
        $units = array();
        $values = array();
        //var_dump($uploads);
		foreach ($uploads as $upload) {
		
			$upload->totalNedded = $fileGroups; //[$upload->idFileGroup]->cnt;
            $upload->totalClinics = (isset($clinics) ? $clinics[$upload->PK_Hospital]->cnt : 1);
            if (!isset($units[$upload->PK_Hospital]))
                $units[$upload->PK_Hospital] = array('id' => $upload->PK_Hospital, 'unit' => $upload->unit, 'Upload_year' => $upload->UploadYear);
            if (isset($values[$upload->PK_Hospital])) {
                $upload->totalNedded = $fileGroups; //[$upload->idFileGroup]->cnt;
                $values[$upload->PK_Hospital][$upload->UploadMonth] = $upload;
            } else {
                $values[$upload->PK_Hospital] = array();
                $values[$upload->PK_Hospital][$upload->UploadMonth] = $upload;
            }
        }
        unset($upload);
		
        foreach ($units as $unit) :
            ?>
            <tr>
                <td><?php echo $unit['unit']; ?></td>
                <?php
                for ($m = 1; $m <= 12; $m++):

                    if (isset($values[$unit['id']][$m])) :

                        $totalNedded = $values[$unit['id']][$m]->totalNedded * $values[$unit['id']][$m]->totalClinics;
                        $write = $values[$unit['id']][$m]->totalUploads . "/" . $totalNedded;
                        if ($values[$unit['id']][$m]->totalUploads < $totalNedded) :
                            $class = "badge-important";
                        elseif ($values[$unit['id']][$m]->totalUploads == $totalNedded) :
                            $class = "badge-success";
                        // elseif ($values[$unit['id']][$m]->totalUploads == $totalNedded) :
                            // $class = "badge-warning";
                        endif;
                        $link = JRoute::_(Juri::base() . $this->commonUrl . "&view=submitiondetails&controller=submitionsums&model=submitiondetails&fltYear=" . $selectedYear . "&idHospital=" . $unit['id'] . "&fltMonth=" . $m . "&idFileGroup=" . $values[$unit['id']][$m]->idFileGroup);
                    elseif ($unit['Upload_year'] == $currentYear && $m > $currentMonth) :
                        $write = "";
                        $class = "";
                        $link = "";
                    else :
                        $write = "0/" . $fileGroups[$this->state->get('idFileGroup', 1)]->cnt;
                        $class = "badge-inverse";
                        $link = "";
                    endif;
                    ?>

                    <td><a <?php echo ($link == "" ? "" : ' href="' . $link . '"'); ?> class="badge <?php echo $class ?>"><?php echo $write; ?></a></td>
                <?php endfor; ?>
            </tr>
        <?php endforeach;
        unset($unit); ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="13"><span class="badge badge-success">Ολοκληρωμένα</span><span class="badge badge-important">Ελλειπή</span><span class="badge badge-inverse">Χωρίς στοιχεία</span></td>
        </tr>
    </tfoot>
</table>


