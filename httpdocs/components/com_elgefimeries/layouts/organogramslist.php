<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
	require JPATH_COMPONENT_SITE . '/libraries/tbs/tbs_class.php';
?>

<table class="table table-bordered sum">
    <thead>
        <tr>
            <th><?php echo JText::_('COM_ELGEFIMERIES_HOSPITAL'); ?></th>
            <th><?php echo JText::_('COM_ELGEFIMERIES_FILE_UPLOAD'); ?></th>
            <th><?php echo JText::_('COM_ELGEFIMERIES_FILE'); ?></th>
        </tr>
    </thead>
    <?php
	
		$TBS = new clsTinyButStrong;
		$TBS->LoadTemplate(JPATH_COMPONENT_SITE .'/layouts/organogramslist_tmpl.html');
		$data = $this->state->get('data')->organograms;
		$TBS->MergeBlock('b', $data);
        $TBS->Show(TBS_OUTPUT);
	?>
    
</table>


