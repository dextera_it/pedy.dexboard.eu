<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
   
    JText::script('COM_ELGEFIMERIES_CLINICS_SELECT');
    JText::script('COM_ELGEFIMERIES_DOCTORS_SELECT');
    JText::script('COM_ELGEFIMERIES_HIERARCHY_SELECT');
    JText::script('COM_ELGEFIMERIES_SPECIALIZATION_SELECT');
?>
<script language="javascript" type="text/javascript" >
   
    var clinics = new Array();
    if (userGroups.indexOf(10) >-1 ) {
        clinics = [{cN:'Clinic 1',cI:'1'}];
    }
    else {
        clinics= [{cN:'Clinic 1',cI:'1'},{cN:'Clinic 2',cI:'2'},{cN:'Clinic 3', cI:'3'},{cN:'Clinic 4', cI:'4'},{cN:'Clinic 5', cI:'5'},{cN:'Clinic 6', cI:'6'}];
    }
</script>    

<div class="elg" >
    <div class="elg-left" id="elgMainArea">
        <div id="scheduler"></div>
    </div>
    <div class="elg-left" id="elgControls" >
        <p>
            <label for="dC"><?php echo JText::_('COM_ELGEFIMERIES_CLINICS') ?></label><input id="dC"  />
        </p>
        <p>
            <label for="dD"><?php echo JText::_('COM_ELGEFIMERIES_DOCTORS') ?></label><input id="dD" disabled="disabled" />
        </p>
        <p>
            <label for="dH"><?php echo JText::_('COM_ELGEFIMERIES_DOCTORS_HIERARCHY') ?></label><input id="dH" disabled="disabled" />
        </p>
        <p>
            <label for="dS"><?php echo JText::_('COM_ELGEFIMERIES_DOCTORS_SPECIALIZATION') ?></label><input id="dS" disabled="disabled" />
        </p>
    </div>
    
</div>
<div class="elg-clear-fix" >&nbsp;</div>

