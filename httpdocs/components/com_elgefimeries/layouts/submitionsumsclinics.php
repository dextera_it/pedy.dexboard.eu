<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
   
    $uploads = $this->data->uploads;
    $years = $this->data->years;
    $fileGroups = $this->data->fileGroups;
    if(isset($this->data->clinics)) $clinics = $this->data->clinics;
    $selectedYear = $this->state->get('year', date('Y'));
    $idHospital = $this->state->get('idHospital', 0);
    
?>
<div class="elg">

	<div class="clearfix well well-small">
		<div class="span2">
			<form action="<?php echo JRoute::_(JURI::base()  .$this->commonUrl . '&view=' . $this->state->get('view') . '&controller=submitionsums'); ?>" method="post">
                            <label for="year">Έτος</label>
                            <select class="span12" name="year" id="year">
                                <?php foreach($years as $year): ?>
                                <option value="<?php echo $year; ?>" <?php echo ($selectedYear == $year ? ' selected="selected" ' : "" ); ?> ><?php echo $year; ?></option>
                                <?php endforeach; unset($year); ?>
                            </select>
                            <input class="btn btn-primary" type="submit" value="Ανανέωση">
        		</form>
		</div>
		<div class="span10">
			<div class="alert alert-info">
                            <h1><?php echo $selectedYear; ?></h1>
				<p>Πληροφορίες</p>
			</div>
		</div>
	</div>

	<table class="table table-bordered sum">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>ΙΑΝ</th>
				<th>ΦΕΒ</th>
				<th>ΜΑΡ</th>
				<th>ΑΠΡ</th>
				<th>ΜΑΙ</th>
				<th>ΙΟΥΝ</th>
				<th>ΙΟΥΛ</th>
				<th>ΑΥΓ</th>
				<th>ΣΕΠ</th>
				<th>ΟΚΤ</th>
				<th>ΝΟΕ</th>
				<th>ΔΕΚ</th>
			</tr>
		</thead>
		<tbody>
                    <?php 
                        $currentYear = date('Y');
                        $currentMonth = date('m');
                        $units = array();
                        $values = array();
                        foreach($uploads as $upload) {
                            $upload->totalNedded = $fileGroups[$upload->idFileGroup]->cnt;
                            $upload->totalClinics = (isset($clinics) ? $clinics[$upload->id]->cnt: 1);
                            if(!isset($units[$upload->id]))
                                $units[$upload->id] = array('id' => $upload->id, 'unit'=>$upload->unit, 'Upload_year'=>$upload->Upload_year);
                            if(isset($values[$upload->id])) {
                                $upload->totalNedded = $fileGroups[$upload->idFileGroup]->cnt;
                                $values[$upload->id][$upload->Upload_month] = $upload;
                            }
                            else {
                                $values[$upload->id] = array();
                                $values[$upload->id][$upload->Upload_month] = $upload;
                            }
                            
                        }
                        unset($upload);
                        foreach($units as $unit ) : ?>
                            <tr>
                                <td> <?php echo $unit['unit']; ?></td>
                                <?php 
                                    for($m = 1; $m <= 12; $m ++): 
                                        if(isset($values[$unit['id']][$m])) :
                                            $write = $values[$unit['id']][$m]->totalUploads . "/" . ($values[$unit['id']][$m]->totalNedded * $values[$unit['id']][$m]->totalClinics );
                                            if($values[$unit['id']][$m]->totalUploads  < $values[$unit['id']][$m]->totalNedded) :
                                                $class="badge-important";
                                            elseif($values[$unit['id']][$m]->totalUploads  == $values[$unit['id']][$m]->totalNedded) :
                                                $class="badge-success";
                                            elseif($values[$unit['id']][$m]->totalUploads  == $values[$unit['id']][$m]->totalNedded) :
                                                $class="badge-warning";
                                            endif;
                                            $link = JRoute::_(Juri::base() . $this->commonUrl . "&view=submitiondetails&controller=submitionsums&model=submitiondetails&year=" . $selectedYear . "&idClinic=" . $unit['id'] . "&month=" .$m . "&idFileGroup=" . $values[$unit['id']][$m]->idFileGroup);
                                        elseif( $unit['Upload_year'] == $currentYear && $m > $currentMonth ) :
                                            $write = "";
                                            $class = "";
                                            $link = "";
                                        else :
                                            $write = "0/". $fileGroups[$this->state->get('idFileGroup',0)]->cnt;
                                            $class = "badge-inverse";
                                            $link = "";
                                        endif;                               
                                ?>
                                    
                                <td><a <?php echo ($link == "" ? "" : ' href="' . $link . '"'); ?> class="badge <?php echo $class ?>"><?php echo $write; ?></a></td>
                                <?php endfor; ?>
                            </tr>
                        <?php endforeach; unset($unit);?>
                    </tbody>
		<tfoot>
			<tr>
				<td colspan="13"><span class="badge badge-success">Ολοκληρωμένα</span><span class="badge badge-important">Ελλειπή</span><span class="badge badge-warning">Επαναΰποβολές</span><span class="badge badge-inverse">Χωρίς στοιχεία</span></td>
			</tr>
		</tfoot>
	</table>
</div>