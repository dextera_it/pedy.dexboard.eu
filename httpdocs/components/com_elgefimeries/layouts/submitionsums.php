<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
   
    $uploads = $this->data->uploads;
    $years = $this->data->years;
    $fileGroups = $this->data->fileGroups;
    if(isset($this->data->clinics)) $clinics = $this->data->clinics;
    $selectedYear = $this->state->get('year', date('Y'));
    $idHospital = $this->state->get('idHospital', 0);
    
?>
<div class="elg">

	<div class="clearfix well well-small">
		<div class="span2">
			<form action="<?php echo JRoute::_(JURI::base()  .$this->commonUrl . '&view=' . $this->state->get('view') . '&controller=submitionsums'); ?>" method="post">
                            <label for="year">Έτος</label>
                            <select class="span12" name="year" id="year">
                                <?php foreach($years as $year): ?>
                                <option value="<?php echo $year; ?>" <?php echo ($selectedYear == $year ? ' selected="selected" ' : "" ); ?> ><?php echo $year; ?></option>
                                <?php endforeach; unset($year); ?>
                            </select>
                            <input class="btn btn-primary" type="submit" value="Ανανέωση">
        		</form>
		</div>
		<div class="span10">
			<div class="alert alert-info">
                            <h1><?php echo $selectedYear; ?></h1>
				<p>Πληροφορίες</p>
			</div>
		</div>
	</div>
	
	<ul class="nav nav-tabs">
		<li class="active" ><a href="#tab-1" data-toggle="tab"><?php echo Jtext::_('COM_ELGEFIMERIES_DUTIES'); ?></a></li>
                <li><a href="#tab-2" data-toggle="tab"><?php echo JText::_('COM_ELGEFIMERIES_ORGANOGRAMS'); ?></a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="tab-1">
                    <?php require JPATH_COMPONENT_SITE . '/layouts/submitionsums_ajax.php'; ?>
                   
		</div>

		<div class="tab-pane" id="tab-2">
			
		</div>
	</div>
	
</div>

<script type="text/javascript" src="media/com_elgefimeries/js/filesdata.js" ></script>