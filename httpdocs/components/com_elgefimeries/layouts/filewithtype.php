<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
    JText::script('COM_ELG_SELECT');
    JText::script('COM_ELGEFIMERIES_FILE_NOT_ALLOWED');
    JText::script('COM_ELGEFIMERIES_RESUBMIT_TITLE');
    JText::script('COM_ELGEFIMERIES_RESUBMIT_TEXT');
    JText::script('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT');
    JText::script('COM_ELG_OK');
?>
 
<div class="elg add-file" >
    
        <h3><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_FILES_ESYNET') ?></h3>
        <form id="foptions">
        <div class="user-options" >
          
            <div class="form-group">
                <label for="fileTypes" ><?php echo Jtext::_('COM_ELGEFIMERIES_FILE_TYPE_TO_ATTACH'); ?></label>
                <select id="fileTypes" name="fileTypes"  class="form-control">

                </select>
            </div>
            <div class="form-group" >
                <label for="years" ><?php echo Jtext::_('COM_ELGEFIMERIES_YEAR'); ?></label>
                <select id="years" name="years" class="form-control" >
                    <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                    <?php for($i = 2010; $i<= 2015; $i++):  ?>
                    <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="months" ><?php echo Jtext::_('COM_ELGEFIMERIES_MONTHS'); ?></label>
                <select id="months" name="months"  class="form-control">
                       <option value="" ><?php echo JText::_('COM_ELG_SELECT'); ?></option>
                       <option value="1" ><?php echo JText::_('JANUARY'); ?></option>
                       <option value="2" ><?php echo JText::_('FEBRUARY'); ?></option>
                       <option value="3" ><?php echo JText::_('MARCH'); ?></option>
                       <option value="4" ><?php echo  JText::_('APRIL');?></option>
                       <option value="5" ><?php echo JText::_('MAY'); ?></option>
                       <option value="6" ><?php echo JText::_('JUNE'); ?></option>
                       <option value="7" ><?php echo JText::_('JULY'); ?></option>
                       <option value="8" ><?php echo JText::_('AUGUST'); ?></option>
                       <option value="9" ><?php echo JText::_('SEPTEMBER'); ?></option>
                       <option value="10" ><?php echo JText::_('OCTOBER'); ?></option>
                       <option value="11" ><?php echo JText::_('NOVEMBER'); ?></option>
                       <option value="12" ><?php echo JText::_('DECEMBER'); ?></option>
                </select>
            </div>
        </div>
        </form>
        <div class="clearfix">&nbsp;</div>
    <div id="uploadSection" style="display:none">
        <div class="fileUploadholder" >
             <div id="dropzone" class="fade well"><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_DROP_FILE_HERE') ?></div>
             <div class="button-holder">
                 <span class="btn btn-success fileinput-button">
                   <i class="glyphicon glyphicon-plus"></i>
                   <span><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_DUTY_FILE') ?></span>
                   <input id="fileupload" type="file" name="url[]" multiple>
                 </span>
              </div>   
             <br/>
             <div class="progress">
                 <div class="bar" ></div>
             </div>

             <div id="files" class="files"></div>
        </div>
        <div class="clearfix"></div>
       
              
</div>
<?php require_once JPATH_COMPONENT_SITE . '/layouts/partmessenger.php' ?>

   
        <?php include JPATH_COMPONENT_SITE . '/layouts/filelistwithtype.php'; ?>
   

<script type="text/javascript">
    var sc, sft, clinics, fileTypes, deleteFileType = 3;
    jQuery(document).ready(function(){
        initFUpload();
       
        
        fileTypes = <?php echo json_encode($this->data->fileTypes); ?>;
        var co = "", fto = "";
		var ftl = fileTypes.length;
        for(var i= 0; i < ftl; i++){
            fto += '<option value="' + fileTypes[i].EsynetFileTypeID + '" >' + fileTypes[i].EsynetFileTypeName + '</option>'; 
        }
        fto = '<option value="">' + Joomla.JText._('COM_ELG_SELECT') + '</option>' + fto; 
        jQuery("#fileTypes").html(fto);
        fillFilterFileTypes(fto, <?php echo $this->state->get('fltFileType', "''"); ?>);
        setFilterMonth(<?php echo $this->state->get('fltMonth', "''");?>);
        setFilterYear(<?php echo $this->state->get('fltYear', "''") ; ?>);
        setFilterUploadDate(<?php echo $this->state->get('fltUploadDate', "''") ; ?>)
        
        if(ftl > 0) sft = fileTypes[0].EsynetFileTypeID;
        jQuery("#fileTypes").change(function(){
            sft = this.value;
        });
        jQuery('#years').change(enableUpload);
        jQuery('#fileTypes').change(enableUpload);
        jQuery('#months').change(enableUpload);
        enableUpload();
        
        
       
    });
    
    function isSelectionComplete(){
        if(jQuery('#years').val() == '') return false;
        //if(jQuery('#clinics').val() == '') return false;
        if(jQuery('#fileTypes').val() == '') return false;
        if(jQuery('#months').val() == '') return false;
        return true;
        
    }
    
    function enableUpload() {
        if (!isSelectionComplete()) {
            jQuery('#uploadSection').slideUp('medium');
            return false;
        }
        
        jQuery('#uploadSection').slideDown('medium');
    }
    
	function initFUpload() {
        var bar = jQuery('.progress .bar');
        var fu = jQuery('#fileupload').fileupload({
            start : function(e){bar.css('background-color', 'green');},
		    acceptFileTypes:  /(xlsx|csv)$/i,
            url: "<?php echo $this->commonUrl. '&format=ajax&controller=filessaveesynet&model=multiplefilessave&' .  JSession::getFormToken() . '=1';?>",
            formData: function() {
                return jQuery('#foptions').serializeArray();
            },
            progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
                jQuery('.progress .bar').css(
                'width',
                 progress + '%'
                );},
            dropZone: jQuery('#dropzone') ,
            done: function(e,data){
				if(data.result == 'canupdate')
				{
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else if(data.result == 'imported') {
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else {
                                    var fltMonth = jQuery('#fltMonth').val();
                                    var fltFileType = jQuery('#fltFileType').val();
                                    var fltYear = jQuery('#fltYear').val();
                                    var fltUploadDate = jQuery('[name=fltUploadDate]').val();
                                    var limit = <?php echo $this->state->get('limit', 0); ?>;
                                    var limitstart = <?php echo $this->state->get('limitstart', 0); ?>;
                                    var qs = '?option=com_elgefimeries&Itemid=113&lang=el&view=filewithtype&controller=filewithtype' 
                                            + (fltMonth == '' ? '' : '&fltMonth=' + fltMonth)
                                            + (fltFileType == '' ? '' : '&fltFileType=' + fltFileType)
                                            + (fltYear == '' ? '' : '&fltYear=' + fltYear )
                                            + (fltUploadDate == '' ? '' : '&fltUploadDate=' + fltUploadDate )
                                            + (limit == 0 ? '' : '&limit=' + limit )
                                            + (limitstart == 0 ? '' : '&limitstart=' + limitstart )
                                    location.href = qs;
				}
			}
		}).bind('fileuploadprocessfail', function (e, data) {
			if(data.files[data.index].error == 'File type not allowed') {
				alert(Joomla.JText._('COM_ELGEFIMERIES_FILE_NOT_ALLOWED') );
			}
			else {
				alert(data.files[data.index].error);
			}
		});
		
		
		
    }
	
	
</script>

