<?php
/* ------------------------------------------------------------------------
  # com_elgefimeries e-logism
  # ------------------------------------------------------------------------
  # author    e-logism
  # copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
  # @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
  # Websites: http://www.e-logism.gr

  ----------------------------------* */
defined('_JEXEC') or die('Restricted access');
$filesDirectory = $this->filesDirectory;
$importedDirectory = $this->importedDirectory;
// $unimportedFiles = $this->unimportedFiles;   
?>      
<div class="filters">
    <fieldset style="border-style: solid; border-width:1px; border-color:#DDDDDD; border-radius: 4px; margin: 1ex 0; ">
        <legend style="width:12ex;font-weight:bold; border: none;font-size: 14px;margin:1ex"><?php echo JText::_('COM_ELG_SEARCH'); ?></legend>
       
        <div class="col">
            <label for="fltUploadDate" ><?php echo Jtext::_('COM_ELGEFIMERIES_FILE_UPLOAD'); ?></label>
            <div class="input-append date" id="fltUploadDate"  data-date-format="dd/mm/yyyy">
                <input size="16" type="text" name="fltUploadDate" value="<?php echo ($this->state->get('fltUploadDate') == '0000-00-00' || $this->state->get('fltUploadDate') == '0' ? '' : $this->state->get('fltUploadDate')); ?>"  >
                <span class="add-on"><i class="icon-calendar"></i></span>
            </div>
        </div>
         <div class="col">
            <label for="fltUploadDate" ><?php echo Jtext::_('COM_ELGEFIMERIES_VALID_DATE_FROM'); ?></label>
            <div class="input-append date" id="fltValidDate"  data-date-format="dd/mm/yyyy">
                <input size="16" type="text" name="fltValidDate" value="<?php echo ($this->state->get('fltValidDate') == '0000-00-00' || $this->state->get('fltValidDate') == '0' ? '' : $this->state->get('fltValidDate')); ?>"  >
                <span class="add-on"><i class="icon-calendar"></i></span>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col">
            
            <button class="btn btn-primary" style="margin-top:1ex"><?php echo Jtext::_('COM_ELG_SEARCH'); ?></button>
        </div>

    </fieldset>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var uD = jQuery('#fltUploadDate').datetimepicker(
        {
            todayHighlight: true,
            language: 'el',
            autoclose: true,
            minView: 2,
            startView: 2
        });
        
        var vD = jQuery('#fltValidDate').datetimepicker(
        {
            todayHighlight: true,
            language: 'el',
            autoclose: true,
            minView: 2,
            startView: 2
        });
    });
    
</script>
