<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
    JText::script('COM_ELG_SELECT');
    JText::script('COM_ELGEFIMERIES_FILE_NOT_ALLOWED');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_TITLE');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_TEXT');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT');
	JText::script('COM_ELG_OK');
?>
 <script type="text/javascript" >
	var clinics;
 </script>
<div class="elg add-file" >
    
        <h3><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_FILES') ?></h3>
        <form id="foptions">
		
        <?php
			if($this->isSum)
			{
				require JPATH_COMPONENT_SITE . '/layouts/addfileuploadnoclinicsfields.php';
			}
			else
			{
				require JPATH_COMPONENT_SITE . '/layouts/addfileuploadfields.php'; 
			}	
			?>
           
        </form>
        <div class="clearfix">&nbsp;</div>
    <div id="uploadSection" style="display:none">
        <div class="fileUploadholder" >
             <div id="dropzone" class="fade well"><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_DROP_FILE_HERE') ?></div>
             <div class="button-holder">
                 <span class="btn btn-success fileinput-button">
                   <i class="glyphicon glyphicon-plus"></i>
                   <span><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_DUTY_FILE') ?></span>
                   <input id="fileupload" type="file" name="url[]" multiple>
                 </span>
              </div>   
             <br/>
             <div class="progress">
                 <div class="bar" ></div>
             </div>

             <div id="files" class="files"></div>
        </div>
        <div class="clearfix"></div>
       
              
</div>
<div>
 <?php include JPATH_COMPONENT_SITE . '/layouts/filelist.php'; ?>
 <?php require_once JPATH_COMPONENT_SITE . '/layouts/partmessenger.php' ?>
 </div>
<script type="text/javascript">
    var sc, sft, fileTypes;
    jQuery(document).ready(function(){
        initFUpload();
        
        fileTypes = <?php echo json_encode($this->data->fileTypes); ?>;
        
        
        
        setFilterMonth(<?php echo $this->state->get('fltMonth', "''");?>);
        setFilterYear(<?php echo $this->state->get('fltYear', "''") ; ?>);
        setFilterUploadDate(<?php echo $this->state->get('fltUploadDate', "''") ; ?>)
        
            
        var fto = "", ftl = fileTypes.length;
        for(var i= ftl-1; i >=0; i--){
            fto += '<option value="' + fileTypes[i].EfimeriaFileTypeID + '" >' + fileTypes[i].EfimeriesFileTypeName + '</option>'; 
        }
        fto = '<option value="">' + Joomla.JText._('COM_ELG_SELECT') + '</option>' + fto; 
        jQuery("#fileTypes").html(fto);
        if(ftl > 0) sft = fileTypes[0].EfimeriaFileTypeID;
       
        jQuery("#fileTypes").change(function(){
            sft = this.value;
        });
        
        jQuery('#years').change(enableUpload);
       
        jQuery('#fileTypes').change(enableUpload);
        jQuery('#months').change(enableUpload);
        enableUpload();
    });
    
    function isSelectionComplete(){
        if(jQuery('#years').val() == '') return false;
        if(jQuery('#clinics').val() == '') return false;
        if(jQuery('#fileTypes').val() == '') return false;
        if(jQuery('#months').val() == '') return false;
        return true;
        
    }
    
    function enableUpload() {
        if(clinics.length <= 0){
            jQuery('#uploadSection').slideUp('medium');
            return false;
        }
        
        if (!isSelectionComplete()) {
            jQuery('#uploadSection').slideUp('medium');
            return false;
        }
        
        jQuery('#uploadSection').slideDown('meduim');
    }
    
	function initFUpload() {
        var bar = jQuery('.progress .bar');
        var fu = jQuery('#fileupload').fileupload({
            start : function(e){bar.css('background-color', 'green');},
		    acceptFileTypes:  /(xlsx|csv)$/i,
            url: "<?php echo $this->commonUrl. '&format=ajax&controller=multiplefilessave&model=multiplefilessave&' .  JSession::getFormToken() . '=1';?>",
            formData: function() {
                return jQuery('#foptions').serializeArray();
            },
            progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
                jQuery('.progress .bar').css(
                'width',
                 progress + '%'
                );},
            dropZone: jQuery('#dropzone') ,
            done: function(e,data){
				if(data.result == 'canupdate')
				{
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else if(data.result == 'imported') {
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else {
					location.reload(true);
				}
			}
		}).bind('fileuploadprocessfail', function (e, data) {
			if(data.files[data.index].error == 'File type not allowed') {
				alert(Joomla.JText._('COM_ELGEFIMERIES_FILE_NOT_ALLOWED') );
			}
			else {
				alert(data.files[data.index].error);
			}
		});
		
		
		
    }
	
	
</script>

