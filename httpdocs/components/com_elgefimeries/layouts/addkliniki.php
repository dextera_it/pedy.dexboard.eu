<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
  
?>
 
<div class="elg add-file" >
	<h3><?php echo JText::_('COM_ELGEFIMERIES_ADD_KLINIKI') ?></h3>
	<form id="foptions" method="post" action="?option=com_elgefimeries&controller=addklinikisave">
		<div class="user-options" >
			<div class="form-group" >
				<label for="clinics" ><?php echo Jtext::_('COM_ELGEFIMERIES_CLINIC'); ?></label>
				<input id="kliniki" name="kliniki" />
			</div>
			<div class="form-group" >
				<label for="tomeas" ><?php echo Jtext::_('COM_ELGEFIMERIES_TOMEAS'); ?></label>
				<?php echo $this->tomeas; ?>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
		<div class="form-group">
			<button class="btn btn-primary" ><?php echo JText::_('COM_ELG_SUBMIT') ?></button>
		</div>
	</form>
	
<div>