<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
    $filesDirectory =  $this->filesDirectory;
    $importedDirectory = $this->importedDirectory;
   // $unimportedFiles = $this->unimportedFiles;   
    ?>
	<div class="elg" >
    <hr />
    <h3><?php echo JText::_('COM_ELGEFIMERIES_UPLOADED_FILES'); ?></h3>
    <form name="adminForm" id="adminForm" method="post" action="<?php echo JRoute::_($this->commonUrl . '&view=' . $this->state->get('view') . '&model=submitiondetails&controller=submitionsums'); ?>">
     <input type="hidden" name="filter_order" value="<?php echo $this->state->get('filter_order'); ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->state->get('filter_order_Dir'); ?>" />
	<input type="hidden" name="idHospital" value="<?php echo $this->state->get('idHospital'); ?>" />
        <?php 
        require_once JPATH_SITE . '/components/com_elgefimeries/layouts/filelistfilters2.php';
        $importedData = $this->importedData;               
        if(count($importedData) > 0) :?>
		<div class="clearfix"></div>
        <table class="table uploadedfiles table-striped">
            <thead>
               
                <tr>
					<th> <?php echo JText::_('COM_ELGEFIMERIES_FILE'); ?> </th>
					  <th> <?php echo JText::_('COM_ELGEFIMERIES_FILE_UPLOAD'); ?> </th>
                    <th> <?php echo JText::_('COM_ELGEFIMERIES_CLINIC'); ?> </th>
                    <th> <?php echo JText::_('COM_ELGEFIMERIES_FILE_TYPE'); ?> </th>
                    <th> <?php echo JText::_('COM_ELGEFIMERIES_REL_YEAR'); ?> </th>
					<th> <?php echo JText::_('COM_ELGEFIMERIES_REL_MONTH'); ?> </th>
					<th> <?php echo JText::_('COM_ELGEFIMERIES_FILE_EDIT_DATE'); ?></th>
					<th> <?php echo JText::_('COM_ELG_ACTION'); ?></th>
                </tr>
            </thead>
            <tbody>
                    <?php foreach($importedData as $data):  ?>
                <tr>
                    <td><?php echo $data->UploadFileName;  ?></td>
                    <td><?php echo  JHTML::_('date', $data->UploadDate, JText::_('DATE_FORMAT_LC3'));?></td>
					<td><?php echo	$data->Kliniki; ?></td>
					<td><?php echo	$data->EfimeriesFileTypeName; ?></td>
					<td class="number" ><?php echo	$data->UploadYear; ?></td>
					<td  class="number" ><?php echo	$data->UploadMonth; ?></td>
					<td><?php echo	($data->UploadIntegrationDate == '' ? '' : JHTML::_('date', $data->UploadIntegrationDate, JText::_('DATE_FORMAT_LC3'))); ?></td>
					<td>	
						<?php if($data->UploadIntegrationPass == 0) : 
							$fileDir = $this->filesDirectory ;
							$canDelete = true;
							else:
							$fileDir = $this->importedDirectory ;
							$canDelete = false;
							endif;
						
						?>
						<a href="<?php echo $fileDir . "/" . $data->UploadFileName ?>" class="text-info" target="_blank" ><?php echo JText::_('COM_ELG_SHOW') ; ?></a>
						<?php if($canDelete) : ?>
							<br /><a href="#" onclick="toDeleteFile('<?php  echo $fileDir . "/" . $data->UploadFileName; ?>')" class="text-warning" ><?php echo JText::_('COM_ELG_DELETE') ; ?></a>
						<?php endif;?>
						</td>
                    <!-- td></td -->
                </tr>
                    <?php endforeach; unset($data); ?>
            </tbody>
        </table>
<?php 
echo ($this->state->get('pagination')->getListFooter());
endif; ?>
                </form>
        <form method="post" action="index.php?option=com_elgefimeries&controller=deletefile"  id="dffrm">
            <input type="hidden" value="" id="fltd" name="fltd" />
            <input type="hidden" id="deleteFileType" name="deleteFileType" value="0" />
        </form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo JText::_('COM_ELG_DELETE'); ?></h4>
      </div>
      <div class="modal-body text-left">
          <p><?php echo JText::_('COM_ELG_FILE_TO_DELETE') . ' "<em id="fileToDelete"></em>";'; ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" ><?php echo JText::_('JNO') ?></button>
        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal" onclick="deleteFile()" ><?php echo JText::_('JYES') ?></button>
       
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript" >
    var fileToDelete = '';
    function toDeleteFile(file) {
        if(file.trim(/ /) != '') {
            fileToDelete = file;
            var modal = jQuery('#myModal');
            modal.find('#fileToDelete').text( file.replace(/^.*[\\\/]/, ''));
            modal.modal();
        }
    }
    function deleteFile() {
        jQuery('#fltd').val(fileToDelete);
		jQuery('#deleteFileType').val(deleteFileType);
        jQuery('#dffrm').submit();
        
    }
	
	jQuery(document).ready(function(){
		setFilterMonth(<?php echo $this->state->get('fltMonth', "''");?>);
        setFilterYear(<?php echo $this->state->get('fltYear', "''") ; ?>);
        setFilterUploadDate(<?php echo $this->state->get('fltUploadDate', "''") ; ?>)
	});
	
</script>