<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
?>
<script language="javascript" type="text/javascript" >
   
    var clinics = new Array();
    if (userGroups.indexOf(10) >-1 ) {
        clinics = [{cN:'Clinic 1',cI:'1'}];
    }
    else {
        clinics= [{cN:'Clinic 1',cI:'1'},{cN:'Clinic 2',cI:'2'},{cN:'Clinic 3', cI:'3'},{cN:'Clinic 4', cI:'4'},{cN:'Clinic 5', cI:'5'},{cN:'Clinic 6', cI:'6'}];
    }
</script>  
<div class="elg" >
    
        <fieldset class="selection" >
            <legend><?php echo Jtext::_('COM_ELGEFIMERIES_DUTY_ADD') ?></legend>
            <div class="duty-options">
                <div>
                    <label for="dutyDate"><?php echo JText::_('COM_ELGEFIMERIES_DUTY_DATE'); ?></label><input id="dutyDate" />
                </div>
                <div>
                    <label for="dutyType"><?php echo JText::_('COM_ELGEFIMERIES_DUTY_TYPE'); ?></label><input id="dutyType" />
                </div>
                <div>
                    <label for="dutyPlace"><?php echo JText::_('COM_ELGEFIMERIES_DUTY_PLACE'); ?></label><input id="dutyPlace" />
                </div>
            </div>
        
        <div class="duty-doctors" >
            <h3><?php echo JText::_('COM_ELGEFIMERIES_AVAILABLE_MED_STUFF'); ?></h3>
            <div id="elgAvailableStuff"></div>
            <button class="btn-primary" ><?php echo JText::_('COM_ELGEFIMERIES_DUTΥ_ADD_TO') ?></button>
            
        </div>
        </fieldset>    
    
        <fieldset class="report" >
            <legend><?php echo JText::_('COM_ELGEFIMERIES_DUTY_SELECTION_REPORT') ?> </legend>
            <h3><?php echo JText::_('COM_ELGEFIMERIES_DOCTORS_YOU_SELECTED'); ?> </h3>
            <div id="elgSelectedStuff"></div>
            <button class="btn-primary" ><?php echo JText::_('COM_ELGEFIMERIES_DUTΥ_REMOVE_FROM') ?></button>
            <h3><?php echo JText::_('COM_ELGEFIMERIES_DOCTORS_FOR_CHECK') ?></h3>
            <div id="elgNotOkSelected"></div>
           
        </fieldset>
    
    
</div>
<div class="elg-clear-fix" >&nbsp;</div>

