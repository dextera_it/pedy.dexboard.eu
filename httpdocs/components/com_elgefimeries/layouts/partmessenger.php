<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
defined('_JEXEC') or die('Restricted access');
?>
<div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="msgModalLabel"></h4>
      </div>
      <div class="modal-body text-left">
          <p id="msgText"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal" id="btnMsgDefault"></button>
        <button type="button" class="btn" data-dismiss="modal" id="btnMsgSecondary" style="display:none"></button>       
      </div>
    </div>
  </div>
</div>