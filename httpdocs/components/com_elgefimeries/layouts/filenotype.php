<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
    JText::script('COM_ELGEFIMERIES_FILE_NOT_ALLOWED');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_TITLE');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_TEXT');
	JText::script('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT');
	JText::script('COM_ELG_OK');
?>
<script language="javascript" type="text/javascript" >
    
    jQuery(document).ready(function(){
        
        
    });

</script>  
<div class="elg add-file" >
    
        <h3><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_ORGANOGRAMS') ?></h3>
        <form id="foptions">
        <div class="user-options" >
            
           <div class="form-group" >
                <label for="clinics" ><?php echo Jtext::_('COM_ELGEFIMERIES_VALID_DATE_FROM'); ?></label>
                <div class="input-append date" id="uploadDateValidFrom"  data-date-format="dd/mm/yyyy">
						<input size="16" type="text" name="uploadDateValidFrom" value="" readonly="readonly" >
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
            </div>
           
           
        </div>
        </form>
        <div class="clearfix">&nbsp;</div>
    <div id="uploadSection" style="display:none">
        <div class="fileUploadholder" >
             <div id="dropzone" class="fade well"><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_DROP_FILE_HERE') ?></div>
             <div class="button-holder">
                 <span class="btn btn-success fileinput-button">
                   <i class="glyphicon glyphicon-plus"></i>
                   <span><?php echo JText::_('COM_ELGEFIMERIES_UPLOAD_ORGANOGRAM_FILE') ?></span>
                   <input id="fileupload" type="file" name="url[]" multiple>
                 </span>
              </div>   
             <br/>
             <div class="progress">
                 <div class="bar" ></div>
             </div>

             <div id="files" class="files"></div>
        </div>
    </div>
 <div class="clearfix"></div>
<?php include JPATH_COMPONENT_SITE . '/layouts/filelistorganogram.php'; ?>
<?php require_once JPATH_COMPONENT_SITE . '/layouts/partmessenger.php' ?>
<script type="text/javascript">
    var sc, sft, clinics;
    jQuery(document).ready(function(){
        initFUpload();
        
	
	
		var sD= jQuery('#uploadDateValidFrom').datetimepicker(
		{
		   todayHighlight: true,
		   language: 'el',
		   autoclose: true,
		   minView: 2,
		   startView: 2
		   
		   
		}).on('changeDate',function(){jQuery('#uploadSection').css('display','block')});
                
           
        setFilterUploadDate(<?php echo $this->state->get('fltUploadDate', "''") ; ?>)
        

    });
    
	function initFUpload() {
        var bar = jQuery('.progress .bar');
        jQuery('#fileupload').fileupload({
            start : function(e){bar.css('background-color', 'green');},
            url: "<?php echo $this->commonUrl. '&format=ajax&controller=multiplefilessaveorganogram&model=multiplefilessave&' .  JSession::getFormToken() . '=1';?>",
            formData: function() {
                return jQuery('#foptions').serializeArray();
            },
            progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
                jQuery('.progress .bar').css(
                'width',
                 progress + '%'
                );},
			acceptFileTypes:  /(xlsx|csv)$/i,
            dropZone: jQuery('#dropzone') ,
            done: function(e, data){
				if(data.result == 'canupdate')
				{
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else if(data.result == 'imported') {
					var modal = jQuery('#msgModal');
					modal.find('#msgModalLabel').text(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_TITLE'));
					modal.find('#msgText').html(Joomla.JText._('COM_ELGEFIMERIES_RESUBMIT_NOT_ALLOWED_TEXT').replace(/#/, '<br />'));
					var btn = modal.find('#btnMsgDefault');
					btn.text(Joomla.JText._('COM_ELG_OK'));
					if(!btn.hasClass('btn-primary')) btn.addClass('btn-primary');
					modal.modal();
				}
				else {
					location.reload(true);
				}
			}
        }).bind('fileuploadprocessfail', function (e, data) {
			if(data.files[data.index].error == 'File type not allowed') {
				alert(Joomla.JText._('COM_ELGEFIMERIES_FILE_NOT_ALLOWED') );
			}
			else {
				alert(data.files[data.index].error);
			}
		});;
    }
</script>

