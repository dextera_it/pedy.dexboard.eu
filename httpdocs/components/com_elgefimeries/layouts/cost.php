<?php
/*------------------------------------------------------------------------
# com_elgefimeries e-logism
# ------------------------------------------------------------------------
# author    e-logism
# copyright Copyright (C) 2013 e-logism.gr. All Rights Reserved.
# @license - GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Websites: http://www.e-logism.gr
 
----------------------------------**/
    defined('_JEXEC') or die('Restricted access');
    require JPATH_COMPONENT_SITE . '/libraries/tbs/tbs_class.php';
    JText::script('COM_ELGEFIMERIES_SUBMIT_COMPLETED');
    $TBS = new clsTinyButStrong;
    $TBS->LoadTemplate(JPATH_COMPONENT_SITE . '/layouts/cost_tmpl.html');
    $TBS->MergeBlock('b', $this->vathmoi);
    $TBS->MergeField('f1', $this->coef);
    $TBS->MergeField('f2', $this->active);
    $TBS->MergeField('f3', $this->ready);
    $TBS->MergeField('f4', $this->mixed);
   
    $TBS->Show(TBS_OUTPUT);
   
?>
