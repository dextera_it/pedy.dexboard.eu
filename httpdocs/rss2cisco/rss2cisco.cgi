#!/usr/bin/env perl

#############################################################################
# @(#) RSS2cisco, An RSS feed to Cisco IP Phone Script version 2.0
# Copyright 2004, Joshua Cantara <jcantara@grappone.com>
#
# then heavily hacked in 2005 by Dirk Jagdmann <doj@cubic.org> to support
# caching and better RSS parsing and new title overview.
#
# Then in 2008, Nic Tjirkalli <nictjir@gmail.com> hacked some more to
# make it work with SIP images on a range of CISCO IP Phones
# and translate German Characters
# Some basic info on the script is available at :-
# http://www.tjir.za.net/rss.html

#
# This program is licensed under the GPL: http://www.gnu.org/licenses/gpl.txt



# Needs a whole wack of perl modules to work - I needed :-
# If you need them, you can search/download them from http://search.cpan.org
#
# LWP::Simple
# HTML::Parser
# HTML::Tagset
# URI
# XML::RAI
# Date::Format
# XML::RSS::Parser
# XML::Elemental
# XML::NamespaceSupport
# Class::ErrorHandler
# Class::XPath


# Some global variable initialisation
# Do Not alter these Global definitions
my $DEBUG = "NO";
my $FixGerman = "NO";


#
# =============================================================
# Variables/items taht are user changeable or customisable
# =============================================================
#


# Add the URL of the RSS feed and a title to the array bellow

    #   'http://www.sportal.de/rss/sportal.rss', 'German Test',
my @feeds = (
             'http://newsrss.bbc.co.uk/rss/newsplayer_uk_edition/world/rss.xml', 'BBC News',
             'http://rss.cnn.com/rss/edition.rss', 'CNN Top Stories',
             'http://rss.cnn.com/rss/edition_business.rss', 'CNN World Business',
         'http://www.eskom.co.za/live/loadshedding.rss' , 'Eskom Loadshedding',
             'http://feeds.iol.co.za/rss/feed_news.rss', 'IOL ZA',
         'http://www.itweb.co.za/sections/newsfeed/download/Top.xml','IT Web ZA',
         'http://www.moneyweb.co.za/mw/rss/mw/en/rss_daily_news.xml','Moneyweb ZA',
             'http://www.news24.com/news24RSSFeed/0,,2,00.xml', 'News 24 Top Stories',
             'http://www.sky.com/skynews/rss/article/0,,30000-1-100,00.xml', 'SKY News',
             );

# Static sites that are not RSS feeds - just sites that have static content
my @sites = (
             );



# Change any of these menu strings etc as required
my $menutitle='News of the World';
my $menuprompt='Choose your propaganda';
my $defaultrefresh=600;

# If you want strange characters in German RSS feeds to be translated to
# hopefully correct German leave this uncomented. might break other
# character sets.
# my $FixGerman = "YES";


# Comment out if you do not want a menu option that displays the HTTP
# environment variables sent by the phone when making a request
# my $DEBUG="YES";


####################################################################
## There should be nothing to change after here
## Unless you wanna rewrite the internals
####################################################################

use strict;
use LWP::Simple qw($ua get);
use XML::RAI;
use CGI;
use URI::Escape;

my %property;

# construct url to script
$ENV{SERVER_NAME} =~ s!/+$!!;
$ENV{SCRIPT_NAME} =~ s!^/+!!;
my $pathto = "http://$ENV{SERVER_NAME}/$ENV{SCRIPT_NAME}";


# Get environment variables returned by phone web browser
getenv();    

# Get phone model
my $model = $property{'HTTP_X_CISCOIPPHONEMODELNAME'};


# Determine if phone supports XML 3.1 (only newer 7970s do this
# From what we can see, 7960s with SIP iamges do XML 3.0
# XML 3.0 dose not support SoftKeyItem directive

my $age = "NEW";

if ($model =~ /79[4,6]0/) {
  $age =  "OLD";
}

my $query = new CGI;
my $item=$query->param('item');
my $url=$query->param('opts');

if ($url && $item)
{
    printitem($url, $item, $query->param('maxitem'));
}
elsif ($url)
{
    if ($url eq "DEBUG") { printenv() ; }
    else { printtitles($url); }
}
else
{
    printmenu();
}

sub getenv {

my $var;
my $val;

foreach $var (sort(keys(%ENV))) {
    $val = $ENV{$var};
    $val =~ s|\n|\\n|g;
    $val =~ s|"|\\"|g;
    $property{$var} = $val;
}
}


sub printenv {

my $k;

    print "Content-Type: text/xml\n\n";

    print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";

    print "<CiscoIPPhoneText><Title>HTTP Header Environment</Title><Text>";

    foreach my $k (sort keys %property)
    {
          #print uri_escape($k).":".uri_escape($property{$k})."\n";
          my $value = $property{$k};
      $value =~ s/\<|\>/ /g;
          print "$k:$value \n";
    }
    print "</Text>";

if ($age eq "NEW") {

    my $pth = substr($pathto,0,255);
    print "<SoftKeyItem>";
    print " <Name>Back</Name>";
    print " <URL>$pth</URL>";
    print " <Position>3</Position>";
    print "</SoftKeyItem>";
}

    print "</CiscoIPPhoneText>";

}



sub printmenu {
    my $pth= "$pathto?opts=";

    print "Content-Type: text/xml\n\n";
    #foreach my $k (sort keys %ENV)
    #{
    #   print "$k: $ENV{$k}\n";
    #}

    print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    print "<CiscoIPPhoneMenu><Title>".substr($menutitle,0,31)."</Title><Prompt>".substr($menuprompt,0,31)."</Prompt>\n";

    for (my $i = 0; $i <= ($#feeds) && $i<=64*2;)
    {
        my $fe = $feeds[$i++];
        my $nm = $feeds[$i++];
    print "<MenuItem>";
    print "<Name>".substr(cmxml_escape($nm), 0, 63)."</Name>";
    print "<URL>".substr($pth.uri_escape($fe), 0, 255)."</URL>";
    print "</MenuItem>\n";
    }

    for (my $i = 0; $i <= ($#sites) && $i<=64*2;)
    {
        my $fe = $sites[$i++];
        my $nm = $sites[$i++];
        print "<MenuItem>";
        print "<Name>".substr(cmxml_escape($nm), 0, 63)."</Name>";
        print "<URL>".substr($fe, 0, 255)."</URL>";
        print "</MenuItem>\n";
    }


    # If DEBUG set to YES print an option to display HTTP Header 
    # Environment variables
    if ($DEBUG eq "YES") {
    print "<MenuItem>";
    print "<Name>HTTP Header Variables</Name>";
    print "<URL>$pth"."DEBUG</URL>";
    print "</MenuItem>\n";
    }

    print "</CiscoIPPhoneMenu>\n";
    exit 0;
}

sub printitem
{
    my ($url, $item, $maxitem) = @_;

    my $maxlength=4000;

    my $rai=XML::RAI->parse(getfeed($url));
    printmenu() unless $rai;

    my $title=''; $title=substr(cmxml_escape($rai->channel->title),0,31) if $rai->channel->title;

    my $it=${$rai->items}[$item-1]; # get item
    my $date=''; $date=substr(cmxml_escape($it->issued),0,31) if $it->issued;

    my $txt='';
    $txt .= cmxml_escape("$item: ".$it->title) if $it->title;
    $txt .= "\n--------------------------------\n";
    $txt .= cmxml_escape($it->content) if $it->content;
    my $txt1 = $it->content;
    $txt=substr($txt,0,3999);

    print "Content-Type: text/xml\n\n";
    print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    print "<CiscoIPPhoneText>";
    print "<Title>$title</Title>" if $title;
    print "<Prompt>$date</Prompt>" if $date;
    print "<Text>$txt</Text>";
    #print "<Text>$txt1</Text>";

if ($age eq "NEW") {
    if($item>1)
    {
    my $i=$item-1;
    my $pth = substr("$pathto?opts=".uri_escape($url)."\&amp;maxitem=$maxitem\&amp;item=$i",0,255);
    print "<SoftKeyItem>";
    print " <Name>&lt;&lt;</Name>";
    print " <URL>$pth</URL>";
    print " <Position>1</Position>";
    print "</SoftKeyItem>";
    }
    if($item<$maxitem)
    {
    my $i=$item+1;
    my $pth = substr("$pathto?opts=".uri_escape($url)."\&amp;maxitem=$maxitem\&amp;item=$i",0,255);
    print "<SoftKeyItem>";
    print " <Name>&gt;&gt;</Name>";
    print " <URL>$pth</URL>";
    print " <Position>2</Position>";
    print "</SoftKeyItem>";
    }

}

if ($age eq "NEW") {

    my $pth = substr("$pathto?opts=".uri_escape($url),0,255);
    print "<SoftKeyItem>";
    print " <Name>Back</Name>";
    print " <URL>$pth</URL>";
    print " <Position>3</Position>";
    print "</SoftKeyItem>";
}

    print "</CiscoIPPhoneText>\n";
}

sub printtitles {
    my ($url) = @_;


    my $rai=XML::RAI->parse(getfeed($url));
    printmenu() unless $rai;

    my $title=''; $title=substr(cmxml_escape($rai->channel->title),0,31) if $rai->channel->title;
    my $date=''; $date=substr(cmxml_escape($rai->channel->issued),0,31) if $rai->channel->issued;

    my $refresh=$defaultrefresh;
    if($rai->channel->src->query('ttl'))
    {
    $refresh=$rai->channel->src->query('ttl')*60;
    }

    print "Content-Type: text/xml\nRefresh: $refresh\n\n";
    print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    print "<CiscoIPPhoneMenu>";
    print "<Title>$title</Title>" if $title;
    print "<Prompt>$date</Prompt>" if $date;

    my $maxitem=0;
    foreach my $item (@{$rai->items}) { $maxitem++; } # todo: better array size detection

    my $pth = "$pathto?opts=".uri_escape($url)."\&amp;maxitem=$maxitem\&amp;item=";
    my $z=0;
    foreach my $item (@{$rai->items})
    {
    last if ++$z >99;
    print "<MenuItem>";
       # print "<Name>Item $z</Name>";
    print "<Name>".substr(cmxml_escape($item->title), 0, 63)."</Name>";
    print "<URL>".substr("$pth$z",0,255)."</URL>";
#   print "<URL>http://www.tjir.za.net</URL>";
    print "</MenuItem>\n";
    }

if ($age eq "NEW") {
    print "<SoftKeyItem>";
    print " <Name>Select</Name>";
    print " <URL>SoftKey:Select</URL>";
    print " <Position>1</Position>";
    print "</SoftKeyItem>";

    print "<SoftKeyItem>";
    print " <Name>Reload</Name>";
    print " <URL>".substr($pth,0,255)."</URL>";
    print " <Position>2</Position>";
    print "</SoftKeyItem>";

    print "<SoftKeyItem>";
    print " <Name>Back</Name>";
    print " <URL>".substr($pathto,0,255)."</URL>";
    print " <Position>3</Position>";
    print "</SoftKeyItem>";
}

    print "</CiscoIPPhoneMenu>\n";
}

sub getfeed
{
    my ($url) = @_;
    my $feed;

    # construct cache filename
    my $fn=$url;
    $fn =~ s/[^\w\d]/_/g;
    $fn = "/tmp/$fn";

    # check if cached rss is valid
    if(-f $fn)
    {
    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size, $atime,$mtime,$ctime,$blksize,$blocks) = stat($fn);
    if($mtime > time()-($defaultrefresh-30) && $size>100)
    {
        open(A, $fn);
        $feed.=$_ while <A>;
        close(A);
        return $feed;
    }
    }

    # get RSS data
    $ua->timeout(15);
    $feed = get($url);

    printmenu() if length($feed)==0;

    # save into cache
    open(A, ">$fn");
    print A $feed;
    close(A);

    return $feed;
}

sub cmxml_escape
{
    ($_) = @_;
    return unless $_;

    $_=HTMLentity2latin1($_);

    s/\s+/ /g;          # strip ws
    s/<.+?>//g;         # strip html tags

    # escape special chars
#    s/</\&lt;/g;
#   s/>/\&gt;/g;
#   s/ß/\&szlig;/g;
#   s/ü/\&uuml;/g;
#
    #
    # Translating German characters fudged by web browser to 
    # hopefully real German - list is far from complete
    #
    if ($FixGerman eq "YES") {
    s/Ã/ü/g;
    s/ü¼/ü/g;
    s/ü¶/ö/g;
    s/ü¤/ä/g;
    s/üŸ/ß/g;
    s/üœ/Ü/g;
    s/Â//g;
    s/ü„u/Ä/g;
    s/©/é/g;
    }

    if ($age eq "NEW") {
    # Older phone's SIP iamge web browser appear to have an issue
    # with &apos and possibly &quot so we wont change this
    s/'/\&apos;/g; # '
    s/"/\&quot;/g; # "
    }

    s/\&(?!lt|gt|apos|quot|amp)/\&amp;/g;

    return $_;
}


sub HTMLentity2latin1
{
    ($_) = @_;
    return unless $_;
    s/\&AElig;/AE/g;    s/\&#198;/AE/g;
    s/\&Aacute;/Á/g;    s/\&#193;/Á/g;
    s/\&Acirc;/Â/g; s/\&#194;/Â/g;
    s/\&Agrave;/À/g;    s/\&#192;/À/g;
    s/\&Alpha;/A/g; s/\&#913;/A/g;
    s/\&Aring;/Å/g; s/\&#197;/Å/g;
    s/\&Atilde;/Ã/g;    s/\&#195;/Ã/g;
    s/\&Auml;/Ä/g;  s/\&#196;/Ä/g;
    s/\&Beta;/B/g;  s/\&#914;/B/g;
    s/\&Ccedil;/Ç/g;    s/\&#199;/Ç/g;
    s/\&Chi;/X/g;   s/\&#935;/X/g;
    s/\&Dagger;/++/g;   s/\&#8225;/++/g;
    s/\&Delta;/D/g; s/\&#916;/D/g;
    s/\&ETH;/Ð/g;   s/\&#208;/Ð/g;
    s/\&Eacute;/É/g;    s/\&#201;/É/g;
    s/\&Ecirc;/Ê/g; s/\&#202;/Ê/g;
    s/\&Egrave;/È/g;    s/\&#200;/È/g;
    s/\&Epsilon;/E/g;   s/\&#917;/E/g;
    s/\&Eta;/H/g;   s/\&#919;/H/g;
    s/\&Euml;/Ë/g;  s/\&#203;/Ë/g;
    s/\&Gamma;/G/g; s/\&#915;/G/g;
    s/\&Iacute;/Í/g;    s/\&#205;/Í/g;
    s/\&Icirc;/Î/g; s/\&#206;/Î/g;
    s/\&Igrave;/Ì/g;    s/\&#204;/Ì/g;
    s/\&Iota;/I/g;  s/\&#921;/I/g;
    s/\&Iuml;/Ï/g;  s/\&#207;/Ï/g;
    s/\&Kappa;/K/g; s/\&#922;/K/g;
    s/\&Lambda;/L/g;    s/\&#923;/L/g;
    s/\&Mu;/M/g;    s/\&#924;/M/g;
    s/\&Ntilde;/Ñ/g;    s/\&#209;/Ñ/g;
    s/\&Nu;/N/g;    s/\&#925;/N/g;
    s/\&OElig;/½/g; s/\&#338;/½/g;
    s/\&Oacute;/Ó/g;    s/\&#211;/Ó/g;
    s/\&Ocirc;/Ô/g; s/\&#212;/Ô/g;
    s/\&Ograve;/Ò/g;    s/\&#210;/Ò/g;
    s/\&Omega;/O/g; s/\&#937;/O/g;
    s/\&Omicron;/o/g;   s/\&#927;/o/g;
    s/\&Oslash;/Ø/g;    s/\&#216;/Ø/g;
    s/\&Otilde;/Õ/g;    s/\&#213;/Õ/g;
    s/\&Ouml;/Ö/g;  s/\&#214;/Ö/g;
    s/\&Phi;/P/g;   s/\&#934;/P/g;
    s/\&Pi;/pi/g;   s/\&#928;/pi/g;
    s/\&Prime;/&quot;/g;s/\&#8243;/&quot;/g;
    s/\&Psi;/psi/g; s/\&#936;/psi/g;
    s/\&Rho;/P/g;   s/\&#929;/P/g;
    s/\&Scaron;/¦/g;    s/\&#352;/¦/g;
    s/\&Sigma;/E/g; s/\&#931;/E/g;
    s/\&THORN;/Þ/g; s/\&#222;/Þ/g;
    s/\&Tau;/T/g;   s/\&#932;/T/g;
    s/\&Theta;/(x)/g;   s/\&#920;/(x)/g;
    s/\&Uacute;/Ú/g;    s/\&#218;/Ú/g;
    s/\&Ucirc;/Û/g; s/\&#219;/Û/g;
    s/\&Ugrave;/Ù/g;    s/\&#217;/Ù/g;
    s/\&Upsilon;/Y/g;   s/\&#933;/Y/g;
    s/\&Uuml;/Ü/g;  s/\&#220;/Ü/g;
    s/\&Xi;/[x]/g;  s/\&#926;/[x]/g;
    s/\&Yacute;/Ý/g;    s/\&#221;/Ý/g;
    s/\&Yuml;/¾/g;  s/\&#376;/¾/g;
    s/\&Zeta;/Z/g;  s/\&#918;/Z/g;
    s/\&aacute;/á/g;    s/\&#225;/á/g;
    s/\&acirc;/â/g; s/\&#226;/â/g;
    s/\&acute;/'/g; s/\&#180;/'/g;
    s/\&aelig;/æ/g; s/\&#230;/æ/g;
    s/\&agrave;/à/g;    s/\&#224;/à/g;
    s/\&alefsym;/x/g;   s/\&#8501;/x/g;
    s/\&alpha;/a/g; s/\&#945;/a/g;
    s/\&and;/&amp;/g;   s/\&#8743;/&amp;/g;
    s/\&ang;/&lt;/g;    s/\&#8736;/&lt;/g;
    s/\&aring;/å/g; s/\&#229;/å/g;
    s/\&asymp;/~/g; s/\&#8776;/~/g;
    s/\&atilde;/ã/g;    s/\&#227;/ã/g;
    s/\&auml;/ä/g;  s/\&#228;/ä/g;
    s/\&bdquo;/&quot;/g;s/\&#8222;/&quot;/g;
    s/\&beta;/ß/g;  s/\&#946;/ß/g;
    s/\&brvbar;/|/g;    s/\&#166;/|/g;
    s/\&bull;/·/g;  s/\&#8226;/·/g;
    s/\&cap;/cap/g; s/\&#8745;/cap/g;
    s/\&ccedil;/ç/g;    s/\&#231;/ç/g;
    s/\&cedil;/,/g; s/\&#184;/,/g;
    s/\&cent;/¢/g;  s/\&#162;/¢/g;
    s/\&chi;/æ/g;   s/\&#967;/æ/g;
    s/\&circ;/^/g;  s/\&#710;/^/g;
    s/\&clubs;/*/g; s/\&#9827;/*/g;
    s/\&cong;/~=/g; s/\&#8773;/~=/g;
    s/\&copy;/(c)/g;    s/\&#169;/(c)/g;
    s/\&crarr;/&lt;-|/g;s/\&#8629;/&lt;-|/g;
    s/\&cup;/u/g;   s/\&#8746;/u/g;
    s/\&curren;/¤/g;    s/\&#164;/¤/g;
    s/\&dArr;/V/g;  s/\&#8659;/V/g;
    s/\&dagger;/+/g;    s/\&#8224;/+/g;
    s/\&darr;/v/g;  s/\&#8595;/v/g;
    s/\&deg;/°/g;   s/\&#176;/°/g;
    s/\&delta;/ð/g; s/\&#948;/ð/g;
    s/\&diams;/#/g; s/\&#9830;/#/g;
    s/\&divide;/÷/g;    s/\&#247;/÷/g;
    s/\&eacute;/é/g;    s/\&#233;/é/g;
    s/\&ecirc;/ê/g; s/\&#234;/ê/g;
    s/\&egrave;/è/g;    s/\&#232;/è/g;
    s/\&empty;/ø/g; s/\&#8709;/ø/g;
    s/\&emsp;/ /g;  s/\&#8195;/ /g;
    s/\&ensp;/ /g;  s/\&#8194;/ /g;
    s/\&epsilon;/e/g;   s/\&#949;/e/g;
    s/\&equiv;/=/g; s/\&#8801;/=/g;
    s/\&eta;/n/g;   s/\&#951;/n/g;
    s/\&eth;/ô/g;   s/\&#240;/ô/g;
    s/\&euml;/ë/g;  s/\&#235;/ë/g;
    s/\&euro;/¤/g;  s/\&#8364;/¤/g;
    s/\&exist;/E/g; s/\&#8707;/E/g;
    s/\&fnof;/f/g;  s/\&#402;/f/g;
    s/\&forall;/V/g;    s/\&#8704;/V/g;
    s/\&frac12;/1\/2/g; s/\&#189;/1\/2/g;
    s/\&frac14;/1\/4/g; s/\&#188;/1\/4/g;
    s/\&frac34;/3\/4/g; s/\&#190;/3\/4/g;
    s/\&frasl;/\//g;    s/\&#8260;/\//g;
    s/\&gamma;/y/g; s/\&#947;/y/g;
    s/\&ge;/&gt;=/g;    s/\&#8805;/&gt;=/g;
    s/\&hArr;/&lt;=&gt;/g;s/\&#8660;/&lt;=&gt;/g;
    s/\&harr;/&lt;-&gt;/g;s/\&#8596;/&lt;-&gt;/g;
    s/\&hearts;/V/g;    s/\&#9829;/V/g;
    s/\&hellip;/.../g;  s/\&#8230;/.../g;
    s/\&iacute;/í/g;    s/\&#237;/í/g;
    s/\&icirc;/î/g; s/\&#238;/î/g;
    s/\&iexcl;/¡/g; s/\&#161;/¡/g;
    s/\&igrave;/ì/g;    s/\&#236;/ì/g;
    s/\&image;/I/g; s/\&#8465;/I/g;
    s/\&infin;/oo/g;    s/\&#8734;/oo/g;
    s/\&int;/f/g;   s/\&#8747;/f/g;
    s/\&iota;/i/g;  s/\&#953;/i/g;
    s/\&iquest;/¿/g;    s/\&#191;/¿/g;
    s/\&isin;/E/g;  s/\&#8712;/E/g;
    s/\&iuml;/ï/g;  s/\&#239;/ï/g;
    s/\&kappa;/k/g; s/\&#954;/k/g;
    s/\&lArr;/&lt;=/g;  s/\&#8656;/&lt;=/g;
    s/\&lambda;/l/g;    s/\&#955;/l/g;
    s/\&lang;/&lt;/g;   s/\&#9001;/&lt;/g;
    s/\&laquo;/«/g; s/\&#171;/«/g;
    s/\&raquo;/»/g; s/\&#187;/»/g;
    s/\&larr;/&lt;-/g;  s/\&#8592;/&lt;-/g;
    s/\&lceil;/|-/g;    s/\&#8968;/|-/g;
    s/\&ldquo;/&quot;/g;s/\&#8220;/&quot;/g;
    s/\&le;/&lt;=/g;    s/\&#8804;/&lt;=/g;
    s/\&lfloor;/|_/g;   s/\&#8970;/|_/g;
    s/\&lowast;/*/g;    s/\&#8727;/*/g;
    s/\&loz;/&lt;&gt;/g;s/\&#9674;/&lt;&gt;/g;
    s/\&lrm;/&lt;--&gt;/g;s/\&#8206;/&lt;--&gt;/g;
    s/\&lsaquo;/&lt;/g; s/\&#8249;/&lt;/g;
    s/\&lsquo;/&apos;/g;s/\&#8216;/&apos;/g;
    s/\&macr;/'/g;  s/\&#175;/'/g;
    s/\&mdash;/-/g; s/\&#8212;/-/g;
    s/\&micro;/µ/g; s/\&#181;/µ/g;
    s/\&middot;/·/g;    s/\&#183;/·/g;
    s/\&minus;/-/g; s/\&#8722;/-/g;
    s/\&mu;/µ/g;    s/\&#956;/µ/g;
    s/\&nabla;/V/g; s/\&#8711;/V/g;
    s/\&nbsp;/ /g;  s/\&#160;/ /g;
    s/\&ndash;/-/g; s/\&#8211;/-/g;
    s/\&ne;/!=/g;   s/\&#8800;/!=/g;
    s/\&ni;/E/g;    s/\&#8715;/E/g;
    s/\&not;/¬/g;   s/\&#172;/¬/g;
    s/\&notin;/¬E/g;    s/\&#8713;/¬E/g;
    s/\&nsub;/¬&lt;/g;  s/\&#8836;/¬&lt;/g;
    s/\&ntilde;/ñ/g;    s/\&#241;/ñ/g;
    s/\&nu;/v/g;    s/\&#957;/v/g;
    s/\&oacute;/ó/g;    s/\&#243;/ó/g;
    s/\&ocirc;/ô/g; s/\&#244;/ô/g;
    s/\&oelig;/½/g; s/\&#339;/½/g;
    s/\&ograve;/ò/g;    s/\&#242;/ò/g;
    s/\&oline;/'/g; s/\&#8254;/'/g;
    s/\&omega;/w/g; s/\&#969;/w/g;
    s/\&omicron;/o/g;   s/\&#959;/o/g;
    s/\&oplus;/(+)/g;   s/\&#8853;/(+)/g;
    s/\&or;/v/g;    s/\&#8744;/v/g;
    s/\&ordf;/ª/g;  s/\&#170;/ª/g;
    s/\&ordm;/º/g;  s/\&#186;/º/g;
    s/\&oslash;/ø/g;    s/\&#248;/ø/g;
    s/\&otilde;/ô/g;    s/\&#245;/ô/g;
    s/\&otimes;/(x)/g;  s/\&#8855;/(x)/g;
    s/\&ouml;/ö/g;  s/\&#246;/ö/g;
    s/\&para;/¶/g;  s/\&#182;/¶/g;
    s/\&part;/a/g;  s/\&#8706;/a/g;
    s/\&permil;/0\/oo/g;s/\&#8240;/0\/oo/g;
    s/\&perp;/^/g;  s/\&#8869;/^/g;
    s/\&phi;/phi/g; s/\&#966;/phi/g;
    s/\&pi;/pi/g;   s/\&#960;/pi/g;
    s/\&piv;/piv/g; s/\&#982;/piv/g;
    s/\&plusmn;/±/g;    s/\&#177;/±/g;
    s/\&pound;/£/g; s/\&#163;/£/g;
    s/\&prime;/'/g; s/\&#8242;/'/g;
    s/\&prod;/|'|/g;    s/\&#8719;/|'|/g;
    s/\&prop;/~/g;  s/\&#8733;/~/g;
    s/\&psi;/V/g;   s/\&#968;/V/g;
    s/\&rArr;/=&gt;/g;  s/\&#8658;/=&gt;/g;
    s/\&radic;/sqrt/g;  s/\&#8730;/sqrt/g;
    s/\&rang;/&gt;/g;   s/\&#9002;/&gt;/g;
    s/\&rceil;/'|/g;    s/\&#8969;/'|/g;
    s/\&rdquo;/&quot;/g;s/\&#8221;/&quot;/g;
    s/\&real;/R/g;  s/\&#8476;/R/g;
    s/\&reg;/®/g;   s/\&#174;/®/g;
    s/\&rfloor;/_|/g;   s/\&#8971;/_|/g;
    s/\&rho;/p/g;   s/\&#961;/p/g;
    s/\&rlm;/&lt;-&gt;/g;s/\&#8207;/&lt;-&gt;/g;
    s/\&rsaquo;/&gt;/g; s/\&#8250;/&gt;/g;
    s/\&rsquo;/'/g; s/\&#8217;/'/g;
    s/\&sbquo;/,/g; s/\&#8218;/,/g;
    s/\&scaron;/¦/g;    s/\&#353;/¦/g;
    s/\&sdot;/·/g;  s/\&#8901;/·/g;
    s/\&sect;/§/g;  s/\&#167;/§/g;
    s/\&shy;/-/g;   s/\&#173;/-/g;
    s/\&sigma;/o/g; s/\&#963;/o/g;
    s/\&sigmaf;/l/g;    s/\&#962;/l/g;
    s/\&sim;/~/g;   s/\&#8764;/~/g;
    s/\&spades;/*/g;    s/\&#9824;/*/g;
    s/\&sub;/sub/g; s/\&#8834;/sub/g;
    s/\&sube;/sub=/g;   s/\&#8838;/sub=/g;
    s/\&sum;/E/g;   s/\&#8721;/E/g;
    s/\&sup1;/¹/g;  s/\&#185;/¹/g;
    s/\&sup2;/²/g;  s/\&#178;/²/g;
    s/\&sup3;/³/g;  s/\&#179;/³/g;
    s/\&sup;/sup/g; s/\&#8835;/sup/g;
    s/\&supe;/sup=/g;   s/\&#8839;/sup=/g;
    s/\&szlig;/ß/g; s/\&#223;/ß/g;
    s/\&tau;/t/g;   s/\&#964;/t/g;
    s/\&there4;/:=/g;   s/\&#8756;/:=/g;
    s/\&theta;/Ø/g; s/\&#952;/Ø/g;
    s/\&thetasym;/v/g;  s/\&#977;/v/g;
    s/\&thinsp;/ /g;    s/\&#8201;/ /g;
    s/\&thorn;/þ/g; s/\&#254;/þ/g;
    s/\&tilde;/~/g; s/\&#732;/~/g;
    s/\&times;/x/g; s/\&#215;/x/g;
    s/\&trade;/(TM)/g;  s/\&#8482;/(TM)/g;
    s/\&uArr;/^/g;  s/\&#8657;/^/g;
    s/\&uacute;/ú/g;    s/\&#250;/ú/g;
    s/\&uarr;/^/g;  s/\&#8593;/^/g;
    s/\&ucirc;/û/g; s/\&#251;/û/g;
    s/\&ugrave;/ù/g;    s/\&#249;/ù/g;
    s/\&uml;/&quot;/g;  s/\&#168;/&quot;/g;
    s/\&upsih;/Y/g; s/\&#978;/Y/g;
    s/\&upsilon;/y/g;   s/\&#965;/y/g;
    s/\&uuml;/ü/g;  s/\&#252;/ü/g;
    s/\&weierp;/P/g;    s/\&#8472;/P/g;
    s/\&xi;/x/g;    s/\&#958;/x/g;
    s/\&yacute;/ý/g;    s/\&#253;/ý/g;
    s/\&yen;/¥/g;   s/\&#165;/¥/g;
    s/\&yuml;/ÿ/g;  s/\&#255;/ÿ/g;
    s/\&zeta;/z/g;  s/\&#950;/z/g;
    s/\&zwj;//g;    s/\&#8205;//g;
    s/\&zwnj;//g;   s/\&#8204;//g;
    return $_;
}