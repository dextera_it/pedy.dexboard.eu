#!/usr/bin/perl
use strict;
#use warnings;
use CGI;
use LWP::Simple qw($ua get);
use XML::RSS;
use Cisco::IPPhone;
use vars qw($pathto @descriptions @feeds);
($#descriptions,$#feeds) = (-1,-1);
# RSS2cisco, An RSS feed to Cisco IP Phone Script version 2.0
# Copyright 2007, Joshua Cantara
# This program is licensed under the GPL: http://www.gnu.org/licenses/gpl.txt
# Newest version can always be found at: http://dontpokebadgers.com/rss2cisco/
# ************************************************************
# ATTENTION: EDIT THE FOLLOWING VARIABLES!!
# ************************************************************
# Change the following to the folder location of rss2cisco.pl on your server
# --------> DO NOT ADD A TRAILING SLASH. <--------
$pathto = 'http://192.168.10.172/data/pedy/rss2cisco';
# Add/Remove RSS feeds below. An example is provided.
# --------> REMOVE "http://" <--------
push(@descriptions,'Wetter');
push(@feeds,'wetter.com/wetter_rss/wetter.xml');
push(@descriptions,'BBC World News');
push(@feeds,'newsrss.bbc.co.uk/rss/newsonline_world_edition/front_page/rss.xml');
push(@descriptions,'Cisco Press Promos');
push(@feeds,'www.ciscopress.com/promotions/promotion_rss.asp');
push(@descriptions,'Cisco News');
push(@feeds,'www.channelinsider.com/rss-feeds-1.xml');
push(@descriptions,'Queenstown weather');
push(@feeds,'www.rssweather.com/wx/za//queenstown/rss.php');
# --------> There is a CISCO IMPOSED MAXIMUM of 64 feeds. <--------
# ************************************************************
# ATTENTION: DON'T CHANGE ANYTHING BELOW THIS LINE!
# ************************************************************
my $query = new CGI;  if ($query->param('rssurl') eq '') { &printmenu; }
elsif ($query->param('rssurl') ne '') { &printfeed; }
else { &error; }
exit;
################################################
# PRINT A MENU OF FEEDS
################################################
sub printmenu {
my $xmlmenu = new Cisco::IPPhone;
$xmlmenu->Menu( { Title => "Your RSS Feeds", Prompt => "Choose a Feed", Text => "" });
my $i = 0;
foreach my $item (@descriptions)
 {
 $item = encode_entities($item);
 my $url = "$pathto/rss2cisco.pl?rssurl=$i";
 $url = encode_entities($url);
 $xmlmenu->AddMenuItem({ Name => $item, URL => $url});
 $i++;
 }
print $xmlmenu->Content;
}
################################################
# PRINT A SINGLE RSS FEED
################################################
sub printfeed {
# Get and parse RSS feed
my $query = new CGI;
my $feednum = $query->param('rssurl');
my $rss = new XML::RSS();
my $rssfeed = 'http://' . $feeds[$feednum];
$ua->timeout(15);
my $raw = get($rssfeed);
$rss->parse($raw);
# Read RSS news items and convert
my $body = "";
foreach my $item (@{$rss->{'items'}})
 {
 my $itemtitle = encode_entities($item->{'title'});
 my $itemdescription = $item->{'description'};
 $itemdescription = encode_entities($itemdescription);
 $body .= $itemtitle . "\n------------------------------\n" . $itemdescription . "\n\n";
 }
if (length($body) > 3600)
 {
 $body = substr($body,0,3600);
 $body .= qq|\n------------------------------\n|;
 $body .= qq|Sorry, this feed has exceeded the maximum display size and has been truncated.|;
 }
# Prepare and return final Cisco XML document
my $xmloutput = new Cisco::IPPhone;
my $title = encode_entities($descriptions[$feednum]);
$xmloutput->Text({ Title => $title, Prompt => "Viewing Feed...", Text => $body });
print $xmloutput->Content;
}
###############################################################
# PRINT ERROR MESSAGE
###############################################################
sub error {
my $xmloutput = new Cisco::IPPhone;
$xmloutput->Text({ Title => "Error", Prompt => "Please Go Back", Text => "Sorry, an error has occured." });
print $xmloutput->Content;
}
###############################################################
# ENCODE/DECODE ENTITIES
###############################################################
sub encode_entities {
my $text = shift (@_);
$text =~ s/<(.|\n)+?>//g;
$text =~ s/’/'/g;
$text =~ s/&amp;/&amp;/g;
$text =~ s/<!--</g; $text =~ s/-->/&gt;/g;
return $text;
}