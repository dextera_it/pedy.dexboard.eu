-- MySQL dump 10.16  Distrib 10.1.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pedy
-- ------------------------------------------------------
-- Server version	10.1.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `make_healthunit_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root2`@`localhost` PROCEDURE `make_healthunit_history`()
BEGIN
	declare finished integer default 0;
    declare vPersonelId, vHealthUnitId integer;
    declare vamka varchar(50);
    declare vAutodate datetime; 
	DECLARE pcur CURSOR FOR SELECT PersonelId, amka, HealthUnitId, Autodate from Personel where StatusId = 1;
    declare continue handler for not found set finished = 1;
    
    open pcur;
    get_p: loop
		
        fetch pcur into vPersonelId, vamka, vHealthUnitId, vAutodate;
          if finished = 1 then
			leave get_p;
		end if;
			update PersonelAttendanceBook pa inner join Personel p on pa.PersonelId = p.PersonelId and p.amka = vamka and p.StatusId != 1  set pa.PersonelId = vPersonelId;
			insert into PersonelHealthUnitHistory
            (PersonelId, HealthUnitId, StartDate, EndDate, StatusId)
            select 
            vPersonelId, HealthUnitId, Autodate, null, StatusId
            from Personel where amka = vAmka and StatusId != 1;
			
            insert into PersonelHealthUnitHistory
            (PersonelId, HealthUnitId, StartDate, EndDate, StatusId)
            values
            (vPersonelId, vHealthUnitId, vAutodate, null, 1);
            
            delete from Personel where amka = vamka and Statusid != 1;
      
    end loop get_p;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `make_special_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root2`@`localhost` PROCEDURE `make_special_history`()
BEGIN
	declare finished integer default 0;
    declare vPersonelId, vPersonelSpecialityId integer;
    declare vamka varchar(50);
    declare vAutodate datetime; 
	DECLARE pcur CURSOR FOR SELECT PersonelId, amka, PersonelSpecialityId, Autodate from Personel where StatusId = 1;
    declare continue handler for not found set finished = 1;
    
    open pcur;
    get_p: loop
		
        fetch pcur into vPersonelId, vamka, vPersonelSpecialityId, vAutodate;
          if finished = 1 then
			leave get_p;
		end if;
			update PersonelAttendanceBook pa inner join Personel p on pa.PersonelId = p.PersonelId and p.amka = vamka and p.StatusId != 1  set pa.PersonelId = vPersonelId;
			insert into PersonelSpecialityHistory
            (PersonelId, SpecialityId, StartDate, EndDate, StatusId)
            select 
            vPersonelId, PersonelSpecialityId, Autodate, null, StatusId
            from Personel where amka = vAmka and StatusId != 1;
			
            insert into PersonelSpecialityHistory
            (PersonelId, SpecialityId, StartDate, EndDate, StatusId)
            values
            (vPersonelId, vPersonelSpecialityId, vAutodate, null, 1);
            
            delete from Personel where amka = vamka and Statusid != 1;
      
    end loop get_p;
END ;;
DELIMITER ;

